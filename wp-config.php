<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'speeveed_wp850' );

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', '');

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


define('FS_METHOD','direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ukt16r7fvgyuxme14gub6r7tsnllfl0laanbxouuu96xpb80wmjeoejwo6klnnz1' );
define( 'SECURE_AUTH_KEY',  'kmflqjcwjplyxz4fjtxuvrhybyl69vuazahxgjjplx3o5p5hngjzajsyre7gkwwk' );
define( 'LOGGED_IN_KEY',    'crfh6xjcodhjts0h1omby89yitkb305sswkhdgn8i15fspcbj6v2exul0gfxtoea' );
define( 'NONCE_KEY',        'umbauuuqcnwmrne1ecaf0fdvrgvhzd42bufa1eymom26d2u03yhbmucijgodjsdw' );
define( 'AUTH_SALT',        'ze3lglwk2yioxczire9ogmsxpm6hgu6nmqguzrws4lqoxrlfktndvnsmryejlch6' );
define( 'SECURE_AUTH_SALT', 'pfjzyus54sryabvusdupkfn5tpp1u7g0qacvn8angpfmjmo3xpjnkgxwbud0vxyr' );
define( 'LOGGED_IN_SALT',   'ok9n7jtxbsvxbtioxr5h2g2zbbur6krvjfmbvinwefv56itxkwsxl2kdkmmemp7w' );
define( 'NONCE_SALT',       'rdiltq5ipd9ulduncufxirphqyprmrbh7iarzklztje54cswe3an4cozwf9sk43j' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpoc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'WP_MEMORY_LIMIT', '512M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
