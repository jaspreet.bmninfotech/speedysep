<?php
/*
Plugin Name: Add Expires Headers Plugin
Plugin URI: http://www.addexpiresheaders.com/wp-plugin
Description: This plugin will add expires headers for various types of resources of website to have better performance optimization.
Author: Passionate Brains
Version: 1.2
Author URI: http://www.passionatebrains.com/
License: GPLv2 or later
*/

/*
 * The main class intilization and declaration 
 */
if(!class_exists('addExpiresHeaders')) 
{
	class addExpiresHeaders	
	{	
		/*
		 * Declaring key here as well as tabs which 
		 * is intiated when registering settings
		 */ 
		private $add_aeh_options_page_key = 'add_aeh_options_page';
		private $aeh_options_key = 'aeh_plugin_options';
		public $imageType = array('gif','jpeg','jpg','png','ico','tiff','webp');
		public $audioType = array('mp3','wav','ogg','gsm','dct','vox','raw');
	    public $videoType = array('mp4','mkv','avi','3gp','wmv','webm','flv');
		public $textType  = array('css');
		public $applicationType = array('javascript');
		function __construct() {
			$this->init_constants();
			$this->init_operations();
		}
		
		function init_constants() {
			global $wpdb;
			define('AEH_PATH', dirname(__FILE__));
			define('AEH_URL', plugins_url('',__FILE__));
		}
		
		function init_operations() {
			add_action('plugins_loaded', array( &$this, 'aeh_init_operations'));
		}
		
		function aeh_init_operations()
		{	
			add_action( 'admin_init', array( &$this, 'register_aeh_options_page' ) );
			add_action( 'admin_menu', array( &$this, 'add_aeh_in_menu' ) );
			add_action( 'init', array( &$this, 'init_gzip_compression' ) );
			add_action( 'plugin_loaded', array( &$this, 'load_settings' ) );
			add_filter( 'plugin_action_links_'.plugin_basename(__FILE__),  array( $this , 'plugin_action_links' ) );
		}
		/*
		* Init gzip compression base on setting of plugin
        */
		function init_gzip_compression()
		{
                    if(!is_admin()){//Front end init time tasks
                        $aeh_options = get_option('add_expires_headers_options');
                        if($aeh_options['enable_gzip']){//Gzip compression is enabled
                            ob_start('ob_gzhandler');
                        }
                    }
		}
		/*
		* Load settings and Expires Headers if already enabled 
        */
		static function load_settings()
		{ 
			$add_expires_headers_options = get_option('add_expires_headers_options');
			if ($add_expires_headers_options){  
                $instance = new addExpiresHeaders();			
			    $instance->write_to_htaccess();
			}	
		}	
		/*
		* remove settings and Expires Headers on deactivating plugin 
        */
		static function remove_settings()
		{ 
			$instance = new addExpiresHeaders();
			$instance->delete_from_htaccess();
			
		}	
		/*
		* Add more links besides deactivate/activate links on plugins page
		*/
		function plugin_action_links( $links ) {
		   $links[] = '<a href="'. esc_url( get_admin_url(null, 'options-general.php?page=aeh_plugin_options') ) .'">Settings</a>';
		   return $links;
		}
		
		/*
		 * Registers the display templates page via the Settings API
		 */
		function register_aeh_options_page() {
			register_setting( $this->add_aeh_options_page_key, $this->add_aeh_options_page_key );
		}
		
		/******************************************************************************
		 * Now we just need to define an admin page.
		 ******************************************************************************/
	
		/*
		 * Called during admin_menu, adds an options
		 * page under Settings 
		*/
		
		function add_aeh_in_menu(){	    
			add_options_page('AddExpiresHeaders', 'AddExpiresHeaders', 'manage_options', $this->aeh_options_key, array(&$this, 'aeh_option_page'));
		}
		
		/*
		 * Plugin Options page rendering goes here, checks
		 * for active tab and replaces key with the related
		 * settings key. Uses the plugin_options_tabs method
		 * to render the tabs.
		 */
		function aeh_option_page() {
			?>
			<div class="wrap">
				<?php 
				include_once('add-expires-headers-options.php');
				displayAddExpiresHeadersOptions();				
				?>
			</div>
			<?php
		}
		
		
		function write_to_htaccess()
	    {   
	        //clean up old rules first
	        if ($this->delete_from_htaccess() == -1) 
	        {
	            return -1; //unable to write to the file
	        }
	
	        $htaccess = ABSPATH . '.htaccess';
	        //get the subdirectory if it is installed in one
	        $siteurl = explode( '/', get_option( 'siteurl' ) );
			if (isset($siteurl[3])) 
	        {
	            $dir = '/' . $siteurl[3] . '/';
	        } 
	        else
	        {
	            $dir = '/';
			}        
	        
	        if (!$f = @fopen($htaccess, 'a+')) 
	        {
	            @chmod( $htaccess, 0644 );
	            if (!$f = @fopen( $htaccess, 'a+')) 
	            {
	                return -1;
	            }					
	        }
	        //backup_a_file($htaccess); //should we back up htaccess file??
	        @ini_set( 'auto_detect_line_endings', true );
	        $ht = explode( PHP_EOL, implode( '', file( $htaccess ) ) ); //parse each line of file into array
		
	        $rules = $this->getrules();
	        if ($rules == -1)
	        {
	            return -1;
	        }
	        
			$rulesarray = explode( PHP_EOL, $rules );
			$contents = array_merge( $rulesarray, $ht );
	        
	        if (!$f = @fopen($htaccess, 'w+')) 
	        {
	            return -1; //we can't write to the file
	        }
	        
	        $blank = false;
	        
	        //write each line to file
			foreach ( $contents as $insertline ) 
		    {
		            if ( trim( $insertline ) == '' ) 
		            {
		                if ( $blank == false ) 
		                {
		                    fwrite( $f, PHP_EOL . trim( $insertline ) );
		                }
		                $blank = true;	
		            }
		            else
		            {
		                $blank = false;
				fwrite( $f, PHP_EOL . trim( $insertline ) );
		            }
			}
	        @fclose( $f );
			return 1; //success
	    }
		function getrules()
	    {
	        @ini_set( 'auto_detect_line_endings', true );
			
	        //figure out what server they're using
	        if (strstr(strtolower(filter_var($_SERVER['SERVER_SOFTWARE'], FILTER_SANITIZE_STRING)), 'apache'))
	        {
	            $aiowps_server = 'apache';
	        } 
	        else if (strstr(strtolower(filter_var($_SERVER['SERVER_SOFTWARE'], FILTER_SANITIZE_STRING)), 'nginx'))
	        {
	            $aiowps_server = 'nginx';
	        } 
	        else if (strstr(strtolower(filter_var($_SERVER['SERVER_SOFTWARE'], FILTER_SANITIZE_STRING)), 'litespeed'))
	        {
	            $aiowps_server = 'litespeed';
	        }
	        else 
	        { //unsupported server
	            return -1;
	        }

	        $rules = '';
	        $add_expires_headers_options = get_option('add_expires_headers_options');
	        //$expire_time = $add_expires_headers_options['num_expiry_days'];
	        if($add_expires_headers_options['enable_aeh']=='1'){
	        	//write the rules
				if($add_expires_headers_options['disable_etags']=='1'){
					$rules .= '# Disable ETags'.PHP_EOL;
					$rules .= '<IfModule mod_headers.c>' . PHP_EOL;
					$rules .= 'Header unset ETag' . PHP_EOL;
					$rules .= '</IfModule>' . PHP_EOL;
					$rules .= 'FileETag None' . PHP_EOL;
					$rules .= '# Disable ETags'.PHP_EOL;
				}	
	        	$rules .= '<IfModule mod_expires.c>' . PHP_EOL;
	        	$rules .= 'ExpiresActive on' . PHP_EOL;
				//Working on Image Type File formats
				$imageTypes = $this->imageType;
				foreach ($imageTypes as $singleImageType){
					if (array_key_exists('aeh_'.$singleImageType, $add_expires_headers_options)){
						if($add_expires_headers_options['aeh_'.$singleImageType]=='1'){
						    $expiryDays = (isset($add_expires_headers_options['aeh_'.$singleImageType.'_days']))? $add_expires_headers_options['aeh_'.$singleImageType.'_days']:30;
							$rules .= 'ExpiresByType image/'.$singleImageType.' "access plus '.$expiryDays.' days"'.PHP_EOL;
						}
					}	
				}
				//Working on Audio Type File formats
                $audioTypes = $this->audioType;
				foreach ($audioTypes as $singleAudioType){
					if (array_key_exists('aeh_'.$singleAudioType, $add_expires_headers_options)){
						if($add_expires_headers_options['aeh_'.$singleAudioType]=='1'){
						    $expiryDays = (isset($add_expires_headers_options['aeh_'.$singleAudioType.'_days'])&& !empty($add_expires_headers_options['aeh_'.$singleAudioType.'_days']))? $add_expires_headers_options['aeh_'.$singleAudioType.'_days']:30;
							$rules .= 'ExpiresByType audio/'.$singleAudioType.' "access plus '.$expiryDays.' days"'.PHP_EOL;
						}
					}	
				}
                //Working on Video Type File formats
                $videoTypes = $this->videoType;
				foreach ($videoTypes as $singleVideoType){
					if (array_key_exists('aeh_'.$singleVideoType, $add_expires_headers_options)){
						if($add_expires_headers_options['aeh_'.$singleVideoType]=='1'){
						    $expiryDays = (isset($add_expires_headers_options['aeh_'.$singleVideoType.'_days'])&& !empty($add_expires_headers_options['aeh_'.$singleVideoType.'_days']))? $add_expires_headers_options['aeh_'.$singleVideoType.'_days']:30;
							$rules .= 'ExpiresByType video/'.$singleVideoType.' "access plus '.$expiryDays.' days"'.PHP_EOL;
						}
					}	
				}	
                //Working on Text Type File formats
                $textTypes = $this->textType;
				foreach ($textTypes as $singleTextType){
					if (array_key_exists('aeh_'.$singleTextType, $add_expires_headers_options)){
						if($add_expires_headers_options['aeh_'.$singleTextType]=='1'){
						    $expiryDays = (isset($add_expires_headers_options['aeh_'.$singleTextType.'_days'])&& !empty($add_expires_headers_options['aeh_'.$singleTextType.'_days']))? $add_expires_headers_options['aeh_'.$singleTextType.'_days']:30;
							$rules .= 'ExpiresByType text/'.$singleTextType.' "access plus '.$expiryDays.' days"'.PHP_EOL;
						}
					}	
				}
                
                //Working on Application Type File formats
                $applicationTypes = $this->applicationType;
				foreach ($applicationTypes as $singleApplicationType){
					if (array_key_exists('aeh_'.$singleApplicationType, $add_expires_headers_options)){
						if($add_expires_headers_options['aeh_'.$singleApplicationType]=='1'){
						    $expiryDays = (isset($add_expires_headers_options['aeh_'.$singleApplicationType.'_days'])&& !empty($add_expires_headers_options['aeh_'.$singleApplicationType.'_days']))? $add_expires_headers_options['aeh_'.$singleApplicationType.'_days']:30;
							$rules .= 'ExpiresByType application/'.$singleApplicationType.' "access plus '.$expiryDays.' days"'.PHP_EOL;
						}
					}	
				}
	        	$rules .= '</IfModule>' . PHP_EOL;
	        }
	        
	        //Add outer markers if we have rules
			if ($rules != '')
		        {
		            $rules = "# BEGIN Add Expires Headers Plugin" . PHP_EOL . $rules . "# END Add Expires Headers Plugin" . PHP_EOL;
			}
	        
	        return $rules;
	    }
		function delete_from_htaccess($section = 'Add Expires Headers Plugin')
	    {
	        $htaccess = ABSPATH . '.htaccess';
				
			@ini_set('auto_detect_line_endings', true);
			if (!file_exists($htaccess)) 
	        {
	            $ht = @fopen($htaccess, 'a+');
	            @fclose($ht);
			}
	        $ht_contents = explode(PHP_EOL, implode('', file($htaccess))); //parse each line of file into array
			if ($ht_contents) 
	        { //as long as there are lines in the file
	            $state = true;
	            if (!$f = @fopen($htaccess, 'w+')) 
	            {
	                @chmod( $htaccess, 0644 );
					if (!$f = @fopen( $htaccess, 'w+')) 
	                {
	                    return -1;
	                }
	            }
	            
	            foreach ( $ht_contents as $n => $markerline ) 
	            { //for each line in the file
	                if (strpos($markerline, '# BEGIN ' . $section) !== false) 
	                { //if we're at the beginning of the section
	                    $state = false;
	                }
	                if ($state == true) 
	                { //as long as we're not in the section keep writing
	                    fwrite($f, trim($markerline) . PHP_EOL);
					}
	                if (strpos($markerline, '# END ' . $section) !== false) 
	                { //see if we're at the end of the section
	                    $state = true;
	                }
	            }
	            @fclose($f);
	            return 1;
	        }
	        return 1;
	    }
		
	} //end class
}
$GLOBALS['aeh_plugin'] = new addexpiresheaders();
register_activation_hook( __FILE__, array( 'addExpiresHeaders', 'load_settings' ) );
register_deactivation_hook( __FILE__, array( 'addExpiresHeaders', 'remove_settings' ) );