<?php 
function displayAddExpiresHeadersOptions()
{   
    global $wpdb, $aeh_plugin;
	$errors = '';
	$enable_aeh = '';
	$enable_gzip = '';
	$disable_etags = '';
	$applicationTypes = $aeh_plugin->applicationType;
	$textTypes = $aeh_plugin->textType;
	$imageTypes = $aeh_plugin->imageType;
	$audioTypes = $aeh_plugin->audioType;
	$videoTypes = $aeh_plugin->videoType;
	$all_files_format = array_merge($imageTypes, $audioTypes, $videoTypes, $applicationTypes, $textTypes);
	foreach ( $all_files_format as $format){
		${'aeh_'.$format} = '';
	}
	if (isset($_POST['aeh_save_settings']))
	{   $number_of_formats = (int)count($all_files_format);
        $j = 0;
		foreach ( $all_files_format as $format){
			if (!empty($_POST['aeh_'.$format.'_days'])) {
				if (is_int((int)$_POST['aeh_'.$format.'_days'])&& ((int)$_POST['aeh_'.$format.'_days']>0)){
					${'aeh_'.$format.'_days'}= esc_sql((int)$_POST['aeh_'.$format.'_days']);
				} else {
					$errors .= 'Please enter an positive integer amount for the "Number of Days" in "'.$format.'" file format<br/>';
				}
			}
			if (isset($_POST['aeh_'.$format])){
				$j++ ;
			}
		}
		if($number_of_formats == $j ) {
				$errors .= 'You must choose at least one file type.<br/>';
				$empty_expires_headers_formats = 0;
		}
		if (strlen($errors)> 0){
			echo '<div id="message" class="error"><p>' . $errors . '</p></div>';
		}
		else{
			$options = array(
			    'enable_aeh' => isset($_POST["enable_aeh"])?1:0,
				'enable_gzip' => isset($_POST["enable_gzip"])?1:0,
				'disable_etags' => isset($_POST["disable_etags"])?1:0,
			);
			foreach ( $all_files_format as $format){
				$options['aeh_'.$format] = isset($_POST['aeh_'.$format])?1:0;
				if (is_int((int)$_POST['aeh_'.$format.'_days'])){
					$options['aeh_'.$format.'_days'] = isset($_POST['aeh_'.$format."_days"])?(int)$_POST['aeh_'.$format."_days"]:0;
				}
			}	
			update_option('add_expires_headers_options', $options); //store the results in WP options table
			echo '<div id="message" class="updated fade">';
			echo '<p>Settings Saved</p>';
			echo '</div>';
			//Now let's modify the .htaccess file
			$write_result = $aeh_plugin->write_to_htaccess();
			if ($write_result){
				echo '<div id="message" class="updated fade">';
				echo '<p>Your .htaccess file was successfully updated!</p>';
				echo '</div>';
			} else {
				echo '<div id="message" class="error"><p>Unable to update changes. Make sure your .htaccess file is editable!</p></div>';
			}
		}
	}
    $aeh_def_days  = 30;
	$add_expires_headers_options = get_option('add_expires_headers_options');
	if ($add_expires_headers_options)
	{   
        $enable_aeh = $add_expires_headers_options['enable_aeh'];
		$enable_gzip = $add_expires_headers_options['enable_gzip'];
		$disable_etags = $add_expires_headers_options['disable_etags'];
		foreach($all_files_format as $format){
			${'aeh_'.$format} = $add_expires_headers_options['aeh_'.$format];
			${'aeh_'.$format.'_days'} = $add_expires_headers_options['aeh_'.$format.'_days'];
		}                
	}
?>
<div id="poststuff"><div id="post-body">
	<div class="postbox welcome-panel">
		<h3 class="hndle" style="font-size:18px"><label for="title">Add Expires Headers</label></h3>
		<div class="inside">
			<div class="welcome-panel-column-container">
				<div class="welcome-panel-column">
					<h3>Get Started</h3>
					<a class="button button-primary button-hero" href="http://addexpiresheaders.com/wp-plugin">Documentation</a>
					<p class="">or, start upadting options.</p>
				</div>
				<div class="welcome-panel-column">
					<h3>Useful Links</h3>
					<ul>
						<li><a href="http://addexpiresheaders.com/wp-plugin">Plugin Requirements</a></li>
						<li><a href="http://www.addexpiresheaders.com/donate">Donate Us</a></li>
					</ul>
				</div>
				<div class="welcome-panel-column welcome-panel-last">
					<h3>More Actions</h3>
					<ul>
						<li><a href="http://www.passionatebrains.com/services">Paid Services</a></li>
						<li><a href="http://addexpiresheaders.com/wp-plugin">Pro Version</a></li>
					</ul>
				</div>
			</div>	
		</div>
	</div>
        
<form action="" method="POST">
            
<div class="postbox">
<h3 class="hndle"><label for="title">Add Expires Headers Main Options</label></h3>
<div class="inside">
	<table class="form-table">
	<tr valign="top">
		<th scope="row"><label for="Enable-aeh"> Enable Expires Headers : </label></th>
		<td>
		<input type="checkbox" name="enable_aeh" <?php if($enable_aeh) echo ' checked="checked"'; ?> />
		</td>
		<th scope="row"><label for="Enable-gzip"> Enable Gzip Compression : </label></th>
		<td>
		<input type="checkbox" name="enable_gzip" <?php if($enable_gzip) echo ' checked="checked"'; ?> />
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="Enable-aeh"> Disable ETags : </label></th>
		<td>
		<input type="checkbox" name="disable_etags" <?php if($disable_etags) echo ' checked="checked"'; ?> />
		</td>
	</tr>
	
</table>
</div></div>

<div class="postbox">
<h3 class="hndle"><label for="title">Media Settings</label></h3>
<div class="inside">
    <datalist id = "expiry-days">
		<option value = "1" >Single Day </option>
		<option value = "7">Week</option>
		<option value = "30">Month</option>
		<option value = "365">Year</option>
	</datalist>
	<table class="form-table">
	<tr>
	    <td style="width:30%">
		    <table>
			<tr valign="top">
				<th scope="row"><label for="FileTypes"> Image Types</label></th>
				<th scope="row"><label for="NumDays">Expiry Time</label></th>
			</tr>
			<?php
			$length = count($imageTypes);
			for($i=0;$i<$length;$i++){ 
			echo"<tr>
				<td>
					<div><input type='checkbox' name='aeh_".$imageTypes[$i]."'";?><?php if(${'aeh_'.$imageTypes[$i]}) echo 'checked="checked"'; ?><?php echo ">".strtoupper($imageTypes[$i])."</div>
					
				</td>
				<td>
				<input type='text' size='10' list='expiry-days' name='aeh_".$imageTypes[$i]."_days' value='";?><?php if(isset(${'aeh_'.$imageTypes[$i].'_days'})){
				echo ${'aeh_'.$imageTypes[$i].'_days'}; } else{ echo $aeh_def_days;}?><?php echo "'/><span>days</span> 
				</td>
			</tr>";
			}
			?>
			</table>
			
		</td>
		<td style="width:5%">
        <td style="width:30%">
		    <table>
			<tr valign="top">
				<th scope="row"><label for="FileTypes">Audio Types</label></th>
				<th scope="row"><label for="NumDays">Expiry Time</label></th>
			</tr>
			<?php
			$length = count($audioTypes);
			for($i=0;$i<$length;$i++){ 
			echo"<tr>
				<td>
					<div><input type='checkbox' name='aeh_".$audioTypes[$i]."'";?><?php if(${'aeh_'.$audioTypes[$i]}) echo 'checked="checked"'; ?><?php echo ">".strtoupper($audioTypes[$i])."</div>
					
				</td>
				<td>
				<input type='text' size='10' list='expiry-days' name='aeh_".$audioTypes[$i]."_days' value='";?><?php if(isset(${'aeh_'.$audioTypes[$i].'_days'})){
				echo ${'aeh_'.$audioTypes[$i].'_days'}; } else{ echo $aeh_def_days;}?><?php echo "'/><span>days</span> 
				</td>
			</tr>";
			}
			?>
			</table>	
		</td>
		<td style="width:5%">
        <td style="width:30%">
		    <table>
			<tr valign="top">
				<th scope="row"><label for="FileTypes">Video Types</label></th>
				<th scope="row"><label for="NumDays">Expiry Time</label></th>
			</tr>
			<?php
			$length = count($videoTypes);
			for($i=0;$i<$length;$i++){ 
			echo"<tr>
				<td>
					<div><input type='checkbox' name='aeh_".$videoTypes[$i]."'";?><?php if(${'aeh_'.$videoTypes[$i]}) echo 'checked="checked"'; ?><?php echo ">".strtoupper($videoTypes[$i])."</div>
					
				</td>
				<td>
				<input type='text' size='10' list='expiry-days' name='aeh_".$videoTypes[$i]."_days' value='";?><?php if(isset(${'aeh_'.$videoTypes[$i].'_days'})){
				echo ${'aeh_'.$videoTypes[$i].'_days'}; } else{ echo $aeh_def_days;}?><?php echo "'/><span>days</span> 
				</td>
			</tr>";
			}
			?>
			</table>
		</td>		
	</tr>
	</table>
</div></div>

<div class="postbox">
<h3 class="hndle"><label for="title">CSS, JS and Other Settings</label></h3>
<div class="inside">
	<table class="form-table">
	<tr>
	    <td style="width:40%">
		    <table>
			<tr valign="top">
				<th scope="row"><label for="FileTypes">Files</label></th>
				<th scope="row"><label for="NumDays">Expiry Time</label></th>
			</tr>
			<?php
			$length = count($textTypes);
			for($i=0;$i<$length;$i++){ 
			echo"<tr>
				<td>
					<div><input type='checkbox' name='aeh_".$textTypes[$i]."'";?><?php  if(${'aeh_'.$textTypes[$i]}) echo 'checked="checked"'; ?><?php echo ">".strtoupper($textTypes[$i])."</div>
					
				</td>
				<td>
				<input type='text' size='10' list='expiry-days' name='aeh_".$textTypes[$i]."_days' value='";?><?php if(isset(${'aeh_'.$textTypes[$i].'_days'})){
				echo ${'aeh_'.$textTypes[$i].'_days'}; } else{ echo $aeh_def_days;}?><?php echo "'/><span>days</span> 
				</td>
			</tr>";
			}
			?>
			</table>
		</td>
		<td style="width:20%">
        <td style="width:40%">
		    <table>
			<tr valign="top">
				<th scope="row"><label for="FileTypes">Files</label></th>
				<th scope="row"><label for="NumDays">Expiry Time</label></th>
			</tr>
			<?php
			$length = count($applicationTypes);
			for($i=0;$i<$length;$i++){ 
			echo"<tr>
				<td>
					<div><input type='checkbox' name='aeh_".$applicationTypes[$i]."'";?><?php  if(${'aeh_'.$applicationTypes[$i]}) echo 'checked="checked"'; ?><?php echo ">".strtoupper($applicationTypes[$i])."</div>
					
				</td>
				<td>
				<input type='text' size='10' list='expiry-days' name='aeh_".$applicationTypes[$i]."_days' value='";?><?php if(isset(${'aeh_'.$applicationTypes[$i].'_days'})){
				echo ${'aeh_'.$applicationTypes[$i].'_days'}; } else{ echo $aeh_def_days;}?><?php echo "'/><span>days</span> 
				</td>
			</tr>";
			}
			?>
			</table>	
		</td>
	</tr>
	</table>
</div></div>
    
<p><input type="submit" name="aeh_save_settings" value="Save" class="button-primary" /></p>
</form>      

<div style="background: #D7E7F5; border: 1px solid #1166BB; color: #333333; margin: 20px 0; padding: 10px;">
    Checkout <a href="http://www.passionatebrains.com" target="_blank">more about us</a>.
</div>
        
</div></div><!-- end of poststuff -->
<?php 
}
