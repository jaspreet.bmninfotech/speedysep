=== Add Expires Headers ===
Contributors: passionatebrains
Donate link: http://www.addexpiresheaders.com/donate
Tags: expires header, expires headers, far future expiration, cache, expiry header, expiry, wp-cache, minify, gzip, speed optimization, etags
Requires at least: 3.5
Tested up to: 5.3.2
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin will help to decrease page loading time and optimize your website by adding expires headers of various file types.

== Description ==
Plugin will improve your website loading speed by caching various types of static files in browser of User. It is light weight plugin but its impact on page loading speed in very crucial and easy noticeable.

= Advantages =
1) Reduce page loading time of website.

2) Improve user experience as page loads very quickly then before.

3) Decrease total data-size of page.

4) Have functionality to disable Etags which can have negative impacts on  performance.

5) Along with expires headers plugin provide gzip compression which will help to speed-up your website by compressing the page output.

6) Larger band of predefined file types are covered so it will increase bandwidth of files which can have expiry headers.

= Documentation =
For Plugin documentation, please refer our <a href="http://addexpiresheaders.com/wp-plugin" rel="follow">plugin website</a>.

= Requirements =
1) Make sure that the "mod_expires" module is enabled on your website hosting server.

2) It is necessary to have read/write permission of .htaccess file to plugin. If not then update file permissions accordingly.

== Installation ==
1) Deactivate and uninstall any other expires headers plugin you may be using. 

2) Login as an administrator to your WordPress Admin account. Using the “Add New” menu option under the “Plugins” section of the navigation, you can either search for: "add expires headers" or if you’ve downloaded the plugin already, click the “Upload” link, find the .zip file you download and then click “Install Now”. Or you can unzip and FTP upload the plugin to your plugins directory (wp-content/plugins/).

3) Activate the plugin through the "Plugins" menu in the WordPress administration panel.

== Usage ==

To use this plugin do the following:

1) Firstly activate Plugin.

2) Check "Enable Expires Headers" checkbox to enable plugin working.

3) Check Files type you want to have expires headers and also add respective expires days for type using input box.

4) Once you hit "save" button all options you selected in database of website and accordingly .htaccess file will updated and add expires headers for respective selected files.

== Frequently Asked Questions ==

= Does this plugin have custom expiry time for different resources? =
Yes

= Does this plugin help in gzip compression of output html? =
yes

= Can we add custom file types for adding expires headers? =
No , But Presently we added wide range of files type which generally needed. We are working on pro-version of plugin which cover this functionality. Please check <a href="http://addexpiresheaders.com/wp-plugin/" rel="follow">here</a> for more info.

== Changelog ==

= 1.0 =
Initial Version of Plugin

= 1.1 =
Added Activation and Deactivation hooks.
Added Settings link on plugins page.

= 1.2 =
Adding functionality to disable Etags.

== Screenshots ==
1. Plugin Settings

