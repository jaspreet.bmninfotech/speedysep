<div class="spacer20"></div>
<div class="container-fluid ">
	<div class="sales-reports expenses-reports">
	<h3><?php _e('Expense Report', 'speedy') ?></h3>
	<?php if(isset($_GET['view_expense']) and $_GET['view_expense']!=''){ 
	$expense_id=$_GET['view_expense'];
	$expense=get_post($expense_id); //print_r($expense);
	?>
	<table class="table">
		<thead>
			<tr>
				<th colspan='6'><?php  _e('Expense Details', 'speedy') ?></th>
			</tr>
		</thead>
		<tbody>
		<tr>
			<td><?php  _e('From Date', 'speedy') ?></td>
			<td><?php echo get_post_meta($expense_id, 'from_date', true) ?></td>
		</tr>
		<tr>
			<td><?php  _e('To Date', 'speedy') ?></td>
			<td><?php echo get_post_meta($expense_id, 'to_date', true) ?></td>
		</tr>
		<tr>
			<td><?php  _e('Paid By', 'speedy') ?></td>
			<td><?php echo get_post_meta($expense_id, 'paid_by', true) ?></td>
		</tr>
		<tr>
			<td><?php  _e('Description', 'speedy') ?></td>
			<td><?php echo $expense->post_content ?></td>
		</tr>
		<tr>
			<td><?php  _e('Items', 'speedy') ?></td>
			<td>
				<table class="inner-table">
				<tr>
					<td><?php  _e('Date', 'speedy') ?></td>
					<td><?php  _e('Item', 'speedy') ?></td>
					<td><?php  _e('Description', 'speedy') ?></td>
					<td><?php  _e('Cost', 'speedy') ?></td>
				</tr>
				<?php 
				$item_array=array();
				$each_item=get_post_meta($expense_id, 'items', true); 
				$item_count=count($each_item['date']);
				
				//echo "<pre>"; print_r($each_item); echo "</pre>";
				for($i=0; $i<$item_count; $i++){
					//print_r($item);
					?>
					<tr>
						<td><?php echo $each_item['date'][$i] ?></td>
						<td><?php echo $each_item['item'][$i] ?></td>
						<td><?php echo $each_item['desc'][$i] ?></td>
						<td><?php echo $each_item['cost'][$i] ?></td>
					</tr>
					<?php
				}
				?>
				</table>
			</td>
		</tr>
		<tr>
			<td><?php  _e('Total Cost', 'speedy') ?></td>
			<td><?php echo get_post_meta($expense_id, 'total', true) ?></td>
		</tr>
		
		</tbody>
	</table>
	<?php }else{ ?>
	<form method="get">
		<div class="row search_row">
			<div class="col-md-4">
				<input type="hidden" name="page" value="speedy-expenses-reports">
				<input type="date" class="form-control" name="from_date" value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; } ?>" placeholder="From Date">
			</div>
			<div class="col-md-4">
				<input type="date" class="form-control" name="to_date" value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; } ?>"  placeholder="To Date">
			</div>
			<div class="col-md-4">
				<input type="submit" class="btn black_button sale_search" value="search" name="search">
				<button class="btn black_button sale_search" name="download_excel_expenses"><?php _e('Download Excel', 'speedy') ?></button>
			</div>
		</div>
		
	</form>
	<table class="table">
		<thead>
			<tr>
				<th colspan='6'><?php  _e('List of Open Expenses', 'speedy') ?></th>
			</tr>
		</thead>
		<tbody>
		<tr>
			<td><?php  _e('S.No', 'speedy') ?></td>
			<td><?php  _e('Date', 'speedy') ?></td>
			<td><?php  _e('Description', 'speedy') ?></td>
			<td><?php  _e('Total Cost', 'speedy') ?></td>
		</tr>
		<?php			
			$count=1;
			
			global $wpdb;
			$another_where='';
			if(isset($_GET['from_date']) and $_GET['from_date']!='' and isset($_GET['to_date']) and $_GET['to_date']!=''){
				$another_where=" AND (post_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')";
			}
			
			
			$expenses = $wpdb->get_results( 
												"
												SELECT ID, post_content, post_date 
												FROM $wpdb->posts
												WHERE  post_type = 'speedy-expenses'  $another_where ORDER BY ID DESC
												"
											);

			foreach ( $expenses as $expense ){ ?>
				<tr>
					<td><a href="<?php echo admin_url().'/?page=speedy-expenses-reports&view_expense='.$expense->ID ?>"><?php echo $count ?></a></td>
					<td><a href="<?php echo admin_url().'/?page=speedy-expenses-reports&view_expense='.$expense->ID ?>"><?php echo$expense->post_date ?></a></td>
					<td><a href="<?php echo admin_url().'/?page=speedy-expenses-reports&view_expense='.$expense->ID ?>"><?php echo $expense->post_content ?></a></td>
					<td><a href="<?php echo admin_url().'/?page=speedy-expenses-reports&view_expense='.$expense->ID ?>"><?php echo SITE_CURRENCY_SYMBOL.get_post_meta($expense->ID, 'total', true) ?></a></td>
				</tr>
			<?php $count++;  ?>
			<?php } 	?>
			
		</tbody>
	</table>
	<?php } ?>
	</div>
</div>
