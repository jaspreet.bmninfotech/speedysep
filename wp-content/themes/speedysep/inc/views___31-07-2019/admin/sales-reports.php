<div class="spacer20"></div>
<div class="container-fluid ">
	<div class="sales-reports">
	<h3><?php _e('Sales Report', 'speedy') ?></h3>
	<form method="get">
		<div class="row">
			<div class="col-md-5">
				<input type="hidden" name="page" value="sales-reports">
				<input type="date" class="form-control" name="from_date" value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; } ?>" placeholder="From Date">
			</div>
			<div class="col-md-5">
				<input type="date" class="form-control" name="to_date" value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; } ?>"  placeholder="To Date">
			</div>
			<div class="col-md-2">
				<button class="btn black_button sale_search" name="download_excel"><?php _e('Download Excel', 'speedy') ?></button>
			</div>
		</div>
		<div class="row sale_row_2">
			<div class="col-md-5">
				<select name="type" class="form-control">
					<option value=""><?php _e('Type', 'speedy') ?></option>
					<option value="vectorizing" <?php if(isset($_GET['type']) and $_GET['type']=='vectorizing'){ echo "selected"; } ?>><?php _e('Vectorizing', 'speedy') ?></option>
					<option value="seperation" <?php if(isset($_GET['type']) and $_GET['type']=='seperation'){ echo "selected"; } ?>><?php _e('Separation', 'speedy') ?></option>
				</select>
			</div>
			<div class="col-md-5">
				<input type="text" class="form-control" name="customer_name" value="<?php if(isset($_GET['customer_name'])){ echo $_GET['customer_name']; } ?>" placeholder="Customer Name">
			</div>
			<div class="col-md-2">
				<input type="submit" class="btn black_button sale_search" value="search" name="search">
			</div>
		</div>
	</form>
	<table class="table">
		<thead>
			<tr>
				<th><?php  _e('S. NO', 'speedy') ?></th>
				<th><?php  _e('Customer Name', 'speedy') ?></th>
				<th><?php  _e('Type', 'speedy') ?></th>
				<th><?php  _e('Orders', 'speedy') ?></th>
				<th><?php  _e('Total Spent', 'speedy') ?></th>
				<th><?php  _e('Avg. Order Price', 'speedy') ?></th>
			</tr>
		</thead>
		<tbody>
		<?php			
			$count=1;
			$args = array(
				'role' => 'subscriber', 
				'meta_query' => array(
					array(
						'key' => 'first_name',
						'value' => $_GET['customer_name'],
						'compare' => 'LIKE'
					)
				)
			);
			$users= get_users($args);
			//$users = get_users('orderby=nicename&role=subscriber');
			foreach ($users as $user) {
			//get this user orders data
			$total_orders=0;
			$total_spend=0;
			$avg_price=0;
			global $wpdb;
			$another_where='';
			if(isset($_GET['from_date']) and $_GET['from_date']!='' and isset($_GET['to_date']) and $_GET['to_date']!=''){
				$another_where=" AND (post_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')";
			}
			
			
			$user_orders = $wpdb->get_results( 
												"
												SELECT ID, post_title 
												FROM $wpdb->posts
												WHERE  post_type = 'service-orders' AND post_author = ".$user->ID." $another_where
												"
											);

			foreach ( $user_orders as $user_order ){
				if(isset($_GET['type']) and $_GET['type']!=''){
					if(get_post_meta($user_order->ID, 'order_type', true)== $_GET['type']){
						$total_orders+=1;
						$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
					}
				}else{
				$total_orders+=1;
				$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
				}
			}
			
			if($total_orders==0){				
				$avg_price=0;
			}else{
				$avg_price=round($total_spend/$total_orders);
			}
			if(isset($_GET['type']) and $_GET['type']!=''){
				if($_GET['type']=='seperation'){
					$type='separation';
				}else{
				$type=$_GET['type'];
				}
			}else{
				$type='Both';
			}
			?>
			<tr>
				<td><?php echo $count ?></td>
				<td><?php echo get_user_meta($user->ID, 'first_name', true) ?></td>
				<td><?php echo $type ?></td>
				<td><?php echo $total_orders ?></td>
				<td><?php echo SITE_CURRENCY_SYMBOL.$total_spend ?></td>
				<td><?php echo SITE_CURRENCY_SYMBOL.$avg_price ?></td>
			</tr>
			<?php $count++; } ?>
		</tbody>
	</table>
	</div>
</div>
