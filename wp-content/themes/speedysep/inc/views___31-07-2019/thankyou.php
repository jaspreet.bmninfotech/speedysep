<div class="spacer35"></div>
<div class="thankyou-page text-center">
	<h1><?php echo get_user_meta(get_current_user_id(), 'first_name', true); ?><br>Thank you</h1>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/thankyou-img.png" alt="thankyou" />
	<div class="additional-text">
		<p>We got your Order! Wel’ll let you know when it is ready.<br>
		for any other questions feel free to contact us.<br>
		Thank you for your business!</p>
	</div>
</div>