<?php
/**
 * Twenty Sixteen functions and definitions
 *
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own twentysixteen_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function twentysixteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
		 * If you're building a theme based on Twenty Sixteen, use a find and replace
		 * to change 'twentysixteen' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'twentysixteen' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Twenty Sixteen 1.2
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'twentysixteen' ),
				'social'  => __( 'Social Links Menu', 'twentysixteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom color scheme.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Dark Gray', 'twentysixteen' ),
					'slug'  => 'dark-gray',
					'color' => '#1a1a1a',
				),
				array(
					'name'  => __( 'Medium Gray', 'twentysixteen' ),
					'slug'  => 'medium-gray',
					'color' => '#686868',
				),
				array(
					'name'  => __( 'Light Gray', 'twentysixteen' ),
					'slug'  => 'light-gray',
					'color' => '#e5e5e5',
				),
				array(
					'name'  => __( 'White', 'twentysixteen' ),
					'slug'  => 'white',
					'color' => '#fff',
				),
				array(
					'name'  => __( 'Blue Gray', 'twentysixteen' ),
					'slug'  => 'blue-gray',
					'color' => '#4d545c',
				),
				array(
					'name'  => __( 'Bright Blue', 'twentysixteen' ),
					'slug'  => 'bright-blue',
					'color' => '#007acc',
				),
				array(
					'name'  => __( 'Light Blue', 'twentysixteen' ),
					'slug'  => 'light-blue',
					'color' => '#9adffd',
				),
				array(
					'name'  => __( 'Dark Brown', 'twentysixteen' ),
					'slug'  => 'dark-brown',
					'color' => '#402b30',
				),
				array(
					'name'  => __( 'Medium Brown', 'twentysixteen' ),
					'slug'  => 'medium-brown',
					'color' => '#774e24',
				),
				array(
					'name'  => __( 'Dark Red', 'twentysixteen' ),
					'slug'  => 'dark-red',
					'color' => '#640c1f',
				),
				array(
					'name'  => __( 'Bright Red', 'twentysixteen' ),
					'slug'  => 'bright-red',
					'color' => '#ff675f',
				),
				array(
					'name'  => __( 'Yellow', 'twentysixteen' ),
					'slug'  => 'yellow',
					'color' => '#ffef8e',
				),
			)
		);

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Sixteen 1.6
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentysixteen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentysixteen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentysixteen_resource_hints', 10, 2 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'twentysixteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
	/**
	 * Register Google fonts for Twenty Sixteen.
	 *
	 * Create your own twentysixteen_fonts_url() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function twentysixteen_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family' => urlencode( implode( '|', $fonts ) ),
					'subset' => urlencode( $subsets ),
				),
				'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Theme block stylesheet.
	wp_enqueue_style( 'twentysixteen-block-style', get_template_directory_uri() . '/css/blocks.css', array( 'twentysixteen-style' ), '20181230' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20181230', true );

	wp_localize_script(
		'twentysixteen-script',
		'screenReaderText',
		array(
			'expand'   => __( 'expand child menu', 'twentysixteen' ),
			'collapse' => __( 'collapse child menu', 'twentysixteen' ),
		)
	);
	//include bootstrap
	wp_enqueue_style( 'speedysep-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Enqueue styles for the block-based editor.
 *
 * @since Twenty Sixteen 1.6
 */
function twentysixteen_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'twentysixteen-block-editor-style', get_template_directory_uri() . '/css/editor-blocks.css', array(), '20181230' );
	// Add custom fonts.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );
}
add_action( 'enqueue_block_editor_assets', 'twentysixteen_block_editor_styles' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

function speedysep_footer_menu() {
  register_nav_menus(
    array(
      'speedysep-footer-menu' => __( 'Speedy Sep Footer Menu' )
    )
  );
}
add_action( 'init', 'speedysep_footer_menu' );

function speedysep_user_menu() {
  register_nav_menus(
    array(
      'speedysep-user-menu' => __( 'Speedy Sep User Menu' )
    )
  );
}
add_action( 'init', 'speedysep_user_menu' );

/**
 * Proper way to enqueue scripts and styles.
 */
function speedysep_scripts() {
    wp_enqueue_style( 'bootstrap-style-cdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap-style-theme-cdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' );
    wp_enqueue_style( 'fontawsome-style', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css' );
    wp_enqueue_script( 'bootstrap-script', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'speedysep-script', get_stylesheet_directory_uri().'/js/script.js', array(), '1.0.0', true );
	// in JavaScript, object properties are accessed as ajax_object_ojsm.ajax_url
	wp_localize_script( 'speedysep-script', 'ajax_object_speedy',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'speedysep_scripts' );

add_shortcode('speedysep_registration', 'speedysep_registration_callback');
function speedysep_registration_callback(){
	ob_start();
	include('inc/views/registration.php');
	return ob_get_clean();
}

add_shortcode('speedysep_login', 'speedysep_login_callback');
function speedysep_login_callback(){
	ob_start();
	include('inc/views/login.php');
	return ob_get_clean();
}

//thankyou shortcode
add_shortcode('speedysep_thankyou', 'speedysep_thankyou_callback');
function speedysep_thankyou_callback(){
	ob_start();
	include('inc/views/thankyou.php');
	return ob_get_clean();
}

//register user
add_action( 'wp_ajax_register_user', 'register_user_callback' );
add_action( 'wp_ajax_nopriv_register_user', 'register_user_callback' );
function register_user_callback() { //print_r($_POST); 
	//process form
	
	$user_name=sanitize_text_field($_POST['email']);
	$random_password=sanitize_text_field($_POST['password']);
	$user_email=sanitize_text_field($_POST['email']);
	if($user_email!=''){
	if (email_exists($user_email) == false ) {
		//$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_id = wp_create_user( $user_name, $random_password, $user_email );
		update_user_meta($user_id, 'first_name', sanitize_text_field($_POST['full_name']));
		wp_new_user_notification($user_id, $deprecated = null, $notify = 'both');
		echo "yes";
	} else {
		echo "no";
	}
	}else{
		echo "empty";
	}
	die(0);
}

//Login user
add_action( 'wp_ajax_login_user', 'login_user_callback' );
add_action( 'wp_ajax_nopriv_login_user', 'login_user_callback' );
function login_user_callback() { //print_r($_POST); 
	//process form
	$username=sanitize_text_field($_POST['email']);
	$password=sanitize_text_field($_POST['password']);
	
	$creds = array(
        'user_login'    => sanitize_text_field($_POST['email']),
        'user_password' => sanitize_text_field($_POST['password']),
        'remember'      => true
    );
 
    $user = wp_signon( $creds, false );
 
    if ( is_wp_error( $user ) ) {
        $linda_error_message=$user->get_error_message();
		die( $linda_error_message);
    }else{
		echo "loggedin";
	}
	
	die(0);
}

//remove adminbar for non admin
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

/******************************************************USER DASHBOARD AREA  START***************************************/

add_shortcode('place_an_order', 'place_an_order_callback');
function place_an_order_callback(){
	ob_start();
	include('inc/views/place_an_order.php');
	return ob_get_clean();
}


add_shortcode('speedy_user_checkout', 'speedy_user_checkout_callback');
function speedy_user_checkout_callback(){
	ob_start();
	include('inc/views/checkout.php');
	return ob_get_clean();
}

/*
* Creating a function to create our CPT
*/
 
function speedysep_custom_post_type() {
	
	//register order
	  $order_labels = array(
        'name'                => _x( 'Orders', 'Post Type General Name', 'ojsm' ),
        'singular_name'       => _x( 'Order', 'Post Type Singular Name', 'ojsm' ),
        'menu_name'           => __( 'Orders', 'ojsm' ),
        'parent_item_colon'   => __( 'Parent Order', 'ojsm' ),
        'all_items'           => __( 'Orders', 'ojsm' ),
        'view_item'           => __( 'View Order', 'ojsm' ),
        'add_new_item'        => __( 'Add New Order', 'ojsm' ),
        'add_new'             => __( 'Add New', 'ojsm' ),
        'edit_item'           => __( 'Edit Order', 'ojsm' ),
        'update_item'         => __( 'Update Order', 'ojsm' ),
        'search_items'        => __( 'Search Order', 'ojsm' ),
        'not_found'           => __( 'Not Found', 'ojsm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'ojsm' ),
    );
	
	$args_order = array(
        'label'               => __( 'Orders', 'ojsm' ),
        'description'         => __( 'Orders of online job and servies.', 'ojsm' ),
        'labels'              => $order_labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
		'show_in_menu'  =>	true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 9,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type' => 'post',
        'capabilities' => array(
        'create_posts' => false,
        ),
        'map_meta_cap' => true,
    );
     
    register_post_type( 'service-orders', $args_order );
	
 
} 
add_action( 'init', 'speedysep_custom_post_type', 0 );


add_action( 'admin_post_nopriv_create_order', 'create_order_callback');
add_action( 'admin_post_create_order', 'create_order_callback');

//add_action( 'wp_ajax_create_order', 'create_order_callback' );
//add_action( 'wp_ajax_nopriv_create_order', 'create_order_callback' );
function create_order_callback() { //print_r($_POST); print_r($_FILES);  exit('asdf');
	//process form
	$req_post = array(
		  'post_title'    => sanitize_text_field($_POST['modifications']),
		  'post_content'  => sanitize_text_field($_POST['notes']),
		  'post_status'   => 'private',
		  'post_type'	  => 'service-orders',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		$_SESSION['order_id']=$post_id;
		update_post_meta($post_id, 'no_of_colors_wanted', sanitize_text_field($_POST['color_no']));
		if($_POST['dimension_any']!=''){
		update_post_meta($post_id, 'dimensions_of_artwork', sanitize_text_field($_POST['dimension_any']));
		}else{
		update_post_meta($post_id, 'dimensions_of_artwork', sanitize_text_field($_POST['dimension_any']));	
		}
		update_post_meta($post_id, 'modifications_details', sanitize_text_field($_POST['modifications']));
		update_post_meta($post_id, 'order_type', sanitize_text_field($_POST['order_type']));
		//upload files
		if ( $_FILES ) { 
		$files = $_FILES["project_files"];  
		foreach ($files['name'] as $key => $value) {            
				if ($files['name'][$key]) { 
					$file = array( 
						'name' => $files['name'][$key],
						'type' => $files['type'][$key], 
						'tmp_name' => $files['tmp_name'][$key], 
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					); 
					$_FILES = array ("project_files" => $file); 
					foreach ($_FILES as $file => $array) {              
						$newupload = my_handle_attachment($file,$post_id); 
					}
				} 
			} 
		}
		
		wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$post_id.'&step=3');
		
	die(0);
}



add_action( 'admin_post_nopriv_create_order_pricing', 'create_order_pricing_callback');
add_action( 'admin_post_create_order_pricing', 'create_order_pricing_callback');

function create_order_pricing_callback() { //print_r($_POST); echo $_SESSION['order_id']; exit('asdf');
	//process form
	$order_id=$_SESSION['order_id'];
	update_post_meta($order_id, 'package', sanitize_text_field($_POST['package']));
	update_post_meta($order_id, 'extra_fast', sanitize_text_field($_POST['extra_fast']));
	update_post_meta($order_id, 'additional_formats', sanitize_text_field($_POST['additional_formats']));
	update_post_meta($order_id, 'concept_recreation', sanitize_text_field($_POST['concept_recreation']));
	//order total amount
	$page_id=sanitize_text_field($_POST['page_id']);
	$package_price=get_post_meta($page_id, sanitize_text_field($_POST['package']).'_plan_price', true);
	if(sanitize_text_field($_POST['extra_fast'])){
		$package_price+=get_post_meta($page_id, 'extra_fast_delivery_price', true);
	}
	if(sanitize_text_field($_POST['additional_formats'])){
		$package_price+=get_post_meta($page_id, 'additional_formats_price', true);
	}
	if(sanitize_text_field($_POST['concept_recreation'])){
		$package_price+=get_post_meta($page_id, 'concept_recreation_price', true);
	}
	
	update_post_meta($order_id, 'order_total', $package_price);
	
		
	wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=4');
		
	die(0);
}

add_action( 'admin_post_nopriv_payment_form', 'payment_form_callback');
add_action( 'admin_post_payment_form', 'payment_form_callback');

function payment_form_callback() { //print_r($_POST); exit('asdf');
	//process form
	
	$user_id=get_current_user_id();
	update_user_meta($user_id, 'business_name', sanitize_text_field($_POST['business_name']));
	update_user_meta($user_id, 'billing_email', sanitize_text_field($_POST['billing_email']));
	update_user_meta($user_id, 'phone_no', sanitize_text_field($_POST['phone_no']));
	update_user_meta($user_id, 'billing_address', sanitize_text_field($_POST['billing_address']));
	update_user_meta($user_id, 'city', sanitize_text_field($_POST['city']));
	update_user_meta($user_id, 'state', sanitize_text_field($_POST['state']));
	update_user_meta($user_id, 'billing_zipcode', sanitize_text_field($_POST['billing_zipcode']));
	update_user_meta($user_id, 'resale_tax_id', sanitize_text_field($_POST['resale_tax_id']));
	
	if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)==''){
		$customer_id=stripe_create_customer();
		update_user_meta(get_current_user_id(), 'customer_stripe_id', $customer_id);
	}else{
		//save a new card
		save_new_card_stripe();
	}
	
	wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=4');	
	die(0);
}

add_action( 'admin_menu', 'speedysep_user_admin_dashboard_menu');
function speedysep_user_admin_dashboard_menu() {
		add_menu_page('Settings', 'Settings', 'manage_options', 'speedysep-settings', 'speedysep_main_menu_callback', "");
}

//menu callback function
function speedysep_main_menu_callback(){
	//include settings view
	include_once(get_stylesheet_directory().'/inc/stripe/views/settings.php');
}

//save stripe settings
add_action( 'admin_post_nopriv_update_stripe_settings', 'save_update_stripe_settings' );
add_action( 'admin_post_update_stripe_settings', 'save_update_stripe_settings' );

function save_update_stripe_settings() { 
	//Secure XSS Attackes
	if (! isset( $_POST['update_stripe_settings_nonce'] ) || ! wp_verify_nonce( $_POST['update_stripe_settings_nonce'], 'update_stripe_settings' )){
		print 'Sorry, you cannot do it';
		exit;
	} else {
	   //save settings
	   update_option('stripe_settings', $_POST['settings']);
		wp_redirect(admin_url().'/admin.php?page='.$_POST['page_url']);
	}
	
}

function stripe_create_customer() {
		$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
		try {
		  // Use Stripe's library to make requests...
		  $token=$_POST['stripeToken'];
		   $email=$_POST['billing_email'];
		  $customer = \Stripe\Customer::create(array(
			  'email' => $email,
			  'source'  => $token
		  ));
		  
		  
		  return $customer->id;
		  
		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo '<h1>Too many requests made to the API too quickly!</h1>';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo '<h1>Authentication Failed, please try again!</h1>';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo '<h1>Server not Responding, please try again later!</h1>';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo "<h1>".$e->getMessage()."</h1>";
		}
		
	  
}


function stripe_charge_customer($amount) {
		$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
		try {
		  
		  $charge = \Stripe\Charge::create(array(
			  'customer' => $customer_id,
			  'amount'   => 100*$amount,
			  'currency' => 'usd',
		  ));
		  return $charge;
		}catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo '<h1>Too many requests made to the API too quickly!</h1>';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo '<h1>Authentication Failed, please try again!</h1>';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo '<h1>Server not Responding, please try again later!</h1>';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo "<h1>".$e->getMessage()."</h1>";
		}
}

function retrive_customer($customer_stripe_id){
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		  
		 $customer_details= \Stripe\Customer::retrieve($customer_stripe_id);
		return $customer_details;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}
//echo "<pre>"; $customer=retrive_customer('cus_EvFG8rAlaQlybQ'); echo $customer->sources; echo "</pre>"; exit;

function save_new_card_stripe(){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	$token=$_POST['stripeToken'];
	try {
		$card = \Stripe\Customer::createSource(
		  $customer_id,
		  [
			'source' => $token,
		  ]
		);
		return $customer_details;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//delete card
function delete_customer_card_stripe($card_id){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$card = \Stripe\Customer::deleteSource(
				  $customer_id,
				  $card_id
				);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//retrive a card
function retrive_customer_card_stripe($card_id){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$customer = \Stripe\Customer::retrieve($customer_id);
		$card = $customer->sources->retrieve($card_id);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//change default
function change_customer_default_card_stripe($card_id){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$card = \Stripe\Customer::update(
		  $customer_id,
		  [
			'default_source' => $card_id,
		  ]
		);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//place order
add_action( 'admin_post_nopriv_place_order', 'place_order_callback' );
add_action( 'admin_post_place_order', 'place_order_callback' );

function place_order_callback() { 
	//Secure XSS Attackes
	if (! isset( $_POST['place_order_nonce'] ) || ! wp_verify_nonce( $_POST['place_order_nonce'], 'place_order' )){
		print 'Sorry, you cannot do it';
		exit;
	} else {
	   $order_id=$_SESSION['order_id']; //echo $order_id;
	   $amount=get_post_meta($order_id, 'order_total', true);
		$response=stripe_charge_customer($amount);
	   if($response->id){
		update_post_meta($order_id, 'order_status', "process");
		update_post_meta($order_id, 'payment', "paid");
		update_post_meta($order_id, 'tx_id', $response->id);
		$now = current_time( 'mysql' ); 
		update_post_meta($order_id, 'start_date', $now);
		$delivery_time=intval(sanitize_text_field($_POST['delivery_time']));
		$delivery_date = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); 
		update_post_meta($order_id, 'delivery_date', $delivery_date);
		update_post_meta($order_id, 'total_hours', $delivery_time);
								
		 wp_redirect(site_url().'/thankyou');	
	   }else{
		    $order_id=$_SESSION['order_id'];
		  wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=5&payment=failed');	
	   }
	}
	
}
/******************************************************USER DASHBOARD AREA ends***************************************/

// define constants
define('SITE_CURRENCY', 'usd');
define('SITE_CURRENCY_SYMBOL', '$');

function my_handle_attachment($file_handler,$post_id,$set_thu=false) {
  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );
  if ( is_numeric( $attach_id ) ) {
	  if(get_post_meta( $post_id, 'project_files', true)==''){
		  $project_files=array($attach_id);
		update_post_meta( $post_id, 'project_files', $project_files );
	  }else{
		  $project_files=get_post_meta( $post_id, 'project_files', true);
		   $project_files[]=$attach_id;
		 update_post_meta( $post_id, 'project_files',  $project_files); 
	  }
  }
  return $attach_id;
}