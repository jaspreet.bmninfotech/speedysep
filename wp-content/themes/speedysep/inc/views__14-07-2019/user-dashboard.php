<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="gray-bg place-an-order">
			<h2><?php _e('Place Your Order Here', 'speedy') ?></h2>
			<a href="?type=vectorizing"><?php _e('Vectorizing', 'speedy') ?></a>
			<a href="?type=seperation"><?php _e('Separation', 'speedy') ?></a>
		</div>
	</div>
</div>