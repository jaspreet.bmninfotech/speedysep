<div class="spacer20"></div>
<div class="container-fluid ">
	<div class="customer-details">
		<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Name', 'speedy')  ?></div>
				<div class="col-sm-4"><input type="text" name="name"></div>
				<div class="col-sm-5"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Email', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="email" name="email"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Phone', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="phone"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Company', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="company_name"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Address', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="address"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('State', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="state"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Zip Code', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="zip"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"></div>
				<input type="hidden" name="action" value="add_customer">
				<div class="col-sm-5"><button type="submit" name="submit" class="black_button" >Add Customer</button></div>
				<div class="col-sm-4"></div>
			</div>
		</form>
	</div>
</div>