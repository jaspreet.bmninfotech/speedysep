<style type="text/css">
	#wpfooter{
		position: relative;
	}
	.StripeElement {
  box-sizing: border-box;
  height: 34px;
  padding: 8px 12px;
  /*border: 1px solid transparent;*/
  border-radius: 4px;
  background-color: white;
  box-shadow: 0 1px 3px 0 #E6EBF1;
  /*-webkit-transition: box-shadow 150ms ease;*/
  /*transition: box-shadow 150ms ease;*/
  border: 1px solid #7e8993;
  font-size: 14px;
}
.StripeElement:focus,.StripeElement:active {
	box-shadow: 0 0 0 1px #007cba;
    outline: 2px solid transparent;
}
input.InputElement.is-empty.Input.Input--empty {
    font-size: 14px;
    box-shadow: 0 0 0 1px #007cba;
    outline: 2px solid transparent;
}
.ElementsApp input{
  font-size: 14px !important;
  height: 1.0em;
}
.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #CFD7DF;
}
.StripeElement--invalid {
  border-color: #FA755A;
}
.StripeElement--webkit-autofill {
  background-color: #FEFDE5 !important;
}
</style>

<div class="spacer20"></div>

<div class="container-fluid ">
	<?php //if view customer 
		if(isset($_GET['customer']) and $_GET['customer']!=''){ 
		//get user data
		$user=get_userdata($_GET['customer']);
		//print_r($user);
		?>
		<div class="customer-details">
			<?php if(isset($_GET['view']) and $_GET['view']=='edit'){ ?> 
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('Name', 'speedy')  ?></div>
					<div class="col-sm-4"><input type="text" name="name" value="<?php echo get_user_meta($user->ID, 'first_name', true) ?>">
					</div>
					<div class="col-sm-5"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('Email', 'speedy')  ?></div>
					<div class="col-sm-5"><input type="email" name="email" value="<?php echo $user->user_email ?>" ></div>
					<div class="col-sm-4"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('Phone', 'speedy')  ?></div>
					<div class="col-sm-5"><input type="text" name="phone" value="<?php echo get_user_meta($user->ID, 'phone_no', true) ?>" ></div>
					<div class="col-sm-4"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('Address', 'speedy')  ?></div>
					<div class="col-sm-5"><input type="text" name="address" value="<?php echo get_user_meta($user->ID, 'address', true) ?>" ></div>
					<div class="col-sm-4"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('State', 'speedy')  ?></div>
					<div class="col-sm-5"><input type="text" name="state" value="<?php echo get_user_meta($user->ID, 'state', true) ?>" ></div>
					<div class="col-sm-4"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"><?php  _e('Zip Code', 'speedy')  ?></div>
					<div class="col-sm-5"><input type="text" name="zip" value="<?php echo get_user_meta($user->ID, 'billing_zipcode', true) ?>"></div>
					<div class="col-sm-4"></div>
				</div>
				<div class="row">
					<div class="col-sm-3 text-center"></div>
					<input type="hidden" name="user_id" value="<?php echo $user->ID ?>">
					<input type="hidden" name="action" value="update_user_info">
					<div class="col-sm-5"><button type="submit" name="submit" class="black_button" >Update Profile</button></div>
					<div class="col-sm-4"></div>
				</div>
			</form>
		<?php }elseif(isset($_GET['addcard']) and $_GET['addcard']=='payment'){ ?>
					<div class="spacer35"></div>
						<div class="payment-method">
							<div class="text-center">
								<span class="black_button"><?php _e('Payment Method', 'speedy') ?></span>
							</div>
							<div class="spacer10"></div>
							<div class="row">
								<div class="col-sm-10 col-sm-offset-1 card-auth-form">

									<?php 
										//delete a card
										if(isset($_GET['delete_card']) and $_GET['card']!=''){
											$result = delete_user_card($_GET['card'],$user->ID);
											if($result->deleted==true){
												echo "<div class='alert alert-success'>"; _e('Card deleted successfully', 'speedy'); echo "</div>";
											}else{
												echo "<div class='alert alert-danger'>"; _e('Card not deleted, please try again', 'speedy'); echo "</div>";
											}
										} ?>

										<div class="text-center">
    									<h2><?php _e('Credit Card Payment Authorization Form', 'speedy') ?></h2>
										</div>
										<div class="step4 gray-bg">
    									<h2><?php _e('General Information', 'speedy') ?></h2>
  										<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" id="payment-form" class="payment-Form">
								        <div class="row">
								          
								          <div class="form-group col-sm-6">
							              <label for="business_name">
					                  <?php _e('Business Name', 'speedy') ?><span class="required">*</span></label>
							              <input type="text" class="form-control" name="business_name" id="business_name" value="<?php echo get_user_meta($user->ID, 'business_name', true) ?>" placeholder="<?php _e('Business Name', 'speedy') ?>" required>
								          </div>

								        </div>

        								<div class="spacer20"></div>
        								<h2><?php _e('Billing Information', 'speedy') ?></h2>
							        	<div class="row">
							            <div class="form-group col-sm-6">
							              <label for="billing_address">
							                <?php _e('Billing Address', 'speedy') ?><span class="required">*</span></label>
							              <input type="text" class="form-control" name="billing_address" id="billing_address" value="<?php echo get_user_meta($user->ID, 'billing_address', true) ?>" placeholder="<?php _e('Billing Address', 'speedy') ?>" required>
							            </div>

							            <div class="form-group col-sm-6">
						                <label for="city">
					                    <?php _e('City', 'speedy') ?><span class="required">*</span>
					                  </label>

						                <input type="text" class="form-control" name="city" id="city" placeholder="<?php _e('City', 'speedy') ?>" value="<?php echo get_user_meta($user->ID, 'city', true) ?>" required>
							            </div>
							        	</div>

							        	<div class="row">
							            <div class="form-group col-sm-6">
							              <label for="state">
							              <?php _e('State', 'speedy') ?><span class="required">*</span></label>
							              <input type="text" class="form-control" name="state" id="state" placeholder="<?php _e('State', 'speedy') ?>" value="<?php echo get_user_meta($user->ID, 'state', true) ?>" required>
							            </div>

							            <div class="form-group col-sm-6">
							              <label for="zipcode">
							              <?php _e('Billing Zipcode', 'speedy') ?><span class="required">*</span></label>
							              <input type="text" class="form-control" name="billing_zipcode" id="zipcode" value="<?php echo get_user_meta($user->ID, 'billing_zipcode', true) ?>" placeholder="<?php _e('Billing Zipcode', 'speedy') ?>" required>
							            </div>
							        	</div>

        								<div class="spacer20"></div>
        								<h2><?php _e('Credit Card Details', 'speedy') ?></h2>
        								<div class="spacer10"></div>
        								<div class="row visa_mastercard_div">

							            <div class="form-group col-sm-6">
						                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
							            </div>
        								</div>

    										<div class="row">

							            <div class="form-group col-sm-6">
						               

						                <div class="form-row">

				                    	<label for="card-element">
				                        <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
					                    </label>
					                    <div id="card-element">
					                        <!-- A Stripe Element will be inserted here. -->
					                    </div>

						                    <!-- Used to display form errors. -->
					                    <div id="card-errors" role="alert" class="card-errors"></div>
						                </div>
							            </div>
							            <script src="https://js.stripe.com/v3/"></script>
							            <div class="form-group col-sm-6">
						                <label for="card_name">
				                    <?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
						                <input type="text" id="card_name" name="card_name" class="form-control field" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required />
							            </div>
        								</div>

								        <div class="spacer20"></div>

								        <input type="hidden" name="action" value="admin_add_card_form">
								        <input type="hidden" name="userid" value="<?php echo $user->ID ; ?>">

								        <button type="submit" class="btn user-submit-btn" name="auth_card">
								            <?php _e('Submit', 'speedy') ?>
								        </button>
								    	</form>
								    	
										</div>
										<!-- list show card -->

										<div class="user_cards">
											<div class="cards-inner">
												<div class="row">
													<?php  
													$userInfo = get_user_meta($user->ID);
													if(array_key_exists('customer_stripe_id', $userInfo) && $userInfo['customer_stripe_id'] != ''){

														$customer=retrive_customer(get_user_meta($user->ID, 'customer_stripe_id', true)); 
													}else{
														$customer = '';
													}

													//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
													if(isset($customer) && $customer->default_source != ''){
													$customer_default_card=retrive_customer_card_stripe($customer->default_source,get_user_meta($user->ID, 'customer_stripe_id', true));
													?>
						
													<div class="col-md-7">
							
														<?php foreach($customer->sources->data as $customer_card){ ?>
														<div class="row">
															<div class="col-sm-9">
																<div class="each-card-inner">
																
																	<p><?php _e('Card number', 'speedy') ?><br>
																		<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
																	<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
																		<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
																	<div class="card-holder-name">
																		<p><?php _e('Cardholder`s name', 'speedy') ?><br>
																		<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
																	</div>
																</div>
															</div>
															<div class="col-sm-3 text-left">
																<a href="?page=speedy-customer-list&customer=<?php echo $user->ID ?>&addcard=payment&delete_card=1&card=<?php echo  $customer_card['id'] ?>" onclick="return confirm('Are you sure to remove **** **** **** <?php echo $customer_card['last4'] ?>? This card will not be available for payments. You will be able to add this card or another card again.  ')" title="<?php _e('Delete Card', 'speedy') ?>" class="card-delete">Remove</a>
																
															</div>
														</div>
														<?php } ?>
													</div>
													<?php }else{ ?> 
													<p><?php _e('No Payment method available.', 'speedy') ?> </p>
													<?php }  ?>
												</div>
											</div>
					
										</div>
										<!-- endlist show card -->

								</div> <!--  !-->
								<div class="deliver-order-prog"></div>
							</div>
						</div>
					</div>

			<?php }else{ ?>

				<?php 
						if(isset($_SESSION["error"])){
                echo '<div class="row marg_left_blogs"><div class="col-md-10"><div class="alert alert-danger" style="text-align:center;"><button type="button" class="close close-btn" data-dismiss="alert">×</button><strong>'. $_SESSION["error"] .'</strong></div></div></div>';
                unset($_SESSION["error"]);
            }
            if(isset($_SESSION["success"])){
                echo '<div class="row marg_left_blogs"><div class="col-md-10"><div class="alert alert-success" style="text-align:center;"><button type="button" class="close close-btn" data-dismiss="alert">×</button><strong>'. $_SESSION["success"] .'</strong></div></div></div>';
                unset($_SESSION["success"]);
            }

				  ?>
				 
				<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 text-right">
	 			  
						<a href="?page=speedy-customer-list&customer=<?php echo $user->ID ?>&addcard=payment"><button class="black_button ">Add Card</button></a>
					
					</div>
				</div>
					<div class="deliver-order-prog"></div>
			</div>
				<div class="spacer30"></div><br>
				<div class="col-md-6">
					<div class="row">
					<?php 
					if(get_user_meta($user->ID, 'profile_pic', true)!=''){
						$image=get_user_meta($user->ID, 'profile_pic', true); ?>
						<div class="col-sm-3 text-right"><img src="<?php echo $image ?>" style="max-height: 55px;" /></div>
						
					<?php }else{ ?>
						<div class="col-sm-3 text-center"><i class="fa fa-user"></i></div>
					<?php } ?>
					<div class="col-sm-4"><input type="text" value="<?php echo get_user_meta($user->ID, 'first_name', true) ?>" class="customer-name" disabled></div>
					<div class="col-sm-5"> <a href="?page=speedy-customer-list&customer=<?php echo $user->ID ?>&view=edit"><button class="black_button profile_edit_btn">Edit</button></a></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Email', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo $user->user_email ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Phone', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo get_user_meta($user->ID, 'phone_no', true) ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Address', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo get_user_meta($user->ID, 'address', true) ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('State', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo get_user_meta($user->ID, 'state', true) ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Zip Code', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo get_user_meta($user->ID, 'billing_zipcode', true) ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
			
					<?php 
					$total_orders=0;
					$total_spend=0;
					$vect_orders=0;
					$sep_orders=0;
					global $wpdb;
					$user_orders = $wpdb->get_results( 
														"
														SELECT ID 
														FROM $wpdb->posts
														WHERE  post_type = 'service-orders' AND post_author = ".$user->ID."
														"
													);

					foreach ( $user_orders as $user_order ){ 
						$total_orders+=1;
						$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
						if(get_post_meta($user_order->ID, 'order_type', true)=='vectorizing'){
							$vect_orders+=1;
						}elseif(get_post_meta($user_order->ID, 'order_type', true)=='seperation'){
							$sep_orders+=1;
						}
					}
					if(get_user_meta($user->ID, 'customer_stripe_id', true)!=''){
					$customer=retrive_customer(get_user_meta($user->ID, 'customer_stripe_id', true));  
					//$customer_default_card=array();
					//echo $customer->default_source;
					//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
					if($customer->default_source){

						$customer_default_card=retrive_customer_card_stripe($customer->default_source, get_user_meta($user->ID, 'customer_stripe_id', true));
						//print_r($customer_default_card);
					}
					?>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Credit Card No.', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="**** **** **** <?php echo $customer_default_card->last4 ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Expiry Date', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo $customer_default_card->exp_month ?>/<?php echo substr($customer_default_card->exp_year, -2); ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<?php } ?>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('No. of Orders placed', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo $total_orders ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Vectorizing Ordres', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo $vect_orders ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Separation Orders', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo $sep_orders ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
					<div class="row">
						<div class="col-sm-3 text-center"><?php  _e('Total Amount Paid', 'speedy')  ?></div>
						<div class="col-sm-5"><input type="text" class="form-control" value="<?php echo  SITE_CURRENCY_SYMBOL.$total_spend ?>" disabled></div>
						<div class="col-sm-4"></div>
					</div>
			</div>
			<div class="col-md-6">
				<?php $user_id = $user->ID; ?>
				<div class="row">

				<!-- Refund code   -->
					<div class="col-md-12 search_row">
						<div class="content-box">
							<div class="box-title">
								<h4 class="text-center"><strong>Refund Customer</strong></h4>
							</div>
							<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>">
								<div class="form-group row">
							    <label for="refundAmount" class="col-sm-2 col-form-label">Amount</label>
							    <div class="col-sm-10">
							      <input type="text" class="form-control" name="amountRefund" id="refundAmount" placeholder="Amount">
							      <input type="hidden" name="userid" value="<?php echo 
							      $user_id; ?>">
							    </div>
								</div>
								<div class="form-group row">
								  <div class="col-sm-12 text-center">
								  	<input type="hidden" name="action" value="admin_refund" class="adminRefund">
								  	<button type="submit" name="refund_submit" class="black_button btn-padd-black refundbtn">Refund</button>
								  </div>
								</div>
							</form>
						</div>
						<div class="deliver-order-prog"></div>
					</div>
					<!-- end refund -->
					<!-- custom form -->
					<?php         
  				$selected = showselectedPlanByUser($user->ID);
      		
					?>
					<?php if(isset($selected) && $selected->name == 'CUSTOM') { ?>
					<div class="col-md-12 search_row">
						<form action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" method="post" enctype="multipart/form-data" id="" class="editMembership">
							<div class="content-box">
							<div class="member-errors text-center" style="color: red;"></div>
								<div class="box-title">
									<h4 class="text-center"><strong>Edit Customer Membership</strong></h4>
								</div>
								<div class="form-group row">
								    <label for="basicOrder" class="col-sm-4 col-form-label">Basic Order</label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control" id="basicOrder" placeholder="Basic Order" required value="<?php echo $selected->basic_order ; ?>">
								    </div>
								</div>
						
								<div class="form-group row">
							    <label for="standardOrder" class="col-sm-4 col-form-label">
								    Standard Order</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control" id="standardOrder" placeholder="Standard Order" required value="">
							    </div>
								</div>
								<div class="form-group row">
							    <label for="premiumOrder" class="col-sm-4 col-form-label">
								    Premium Orders</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control" id="premiumOrder" placeholder="Premium Order" required value="">
							    </div>
								</div>
								<div class="form-group row">
							    <label for="editmembershipMockups" class="col-sm-4 col-form-label">Mockups</label>
							    <div class="col-sm-8">
							      <input type="number" class="form-control" id="editmembershipMockups" name="editmemMockups" placeholder="Mockups" required value=""> 
							    </div>
								</div>
								<div class="form-group row">
							    <label for="editvideoMockups" class="col-sm-4 col-form-label">Video Mockups</label>
							    <div class="col-sm-8">
							      <input type="number" class="form-control" id="editvideoMockups" name="editmemvideoMockups" placeholder="Video Mockups" value="">
							    </div>
								</div>
								<div class="form-group row">
							    <label for="editmonthlyfee" class="col-sm-4 col-form-label">Monthly Fee</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control" id="editmonthlyfee" name="monthlyFee" placeholder="Monthly Fee" required value="">
							    </div>
								</div>
								<div class="form-group row">
									<input type="hidden" name="action1" id="adminUpdateMem" value="custom_mem_update">
									<input type="hidden" name="action" id="cancelMem" value="cancel-membership">
							    <div class="col-sm-4 ">
							    	<!-- <button type="button" name="submit1" class="black_button btn-padd-black">Edit Membership</button> -->
							    </div>
							    <?php if(isset($selected) &&$selected->status == 'Active') { ?>
							    	  <div class="col-sm-3"><button type="button" name="submit4" class="black_button btn-padd-black">Update</button></div>
							    <?php }else{ ?>
							    <div class="col-sm-3"><button type="button" name="submit2" class="black_button btn-padd-black btn-mem-update" onclick="updateMembership(this,<?php echo $user_id ; ?>)">Submit</button></div>
							  <?php } ?>
							    <div class="col-sm-5">
							    	 <?php if(isset($selected) && $selected->status == 'Active') { ?>
							    	<button type="button" name="submit3" class="black_button btn-lg-width cancel-mem-btn" onclick="cancelMembership(this,<?php echo $user_id ; ?>)">Cancel Membership</button>
							    <?php } ?>
							    </div>       
								</div>
								
							</div>
						</form>
					</div>
				<?php } ?>
					<div class="deliver-order-prog"></div>
				</div>
				
				<div class="modal fade custompaymentmodal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:45px;">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header text-center">
				        <h5 class="modal-title" id="exampleModalLabel">Payment Method</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div class="container-fluid">
				      		<span class="headinfo"><h4 style="display: inline;"><?php _e('Please enter credit card details below ', 'speedy') ?></h4></span><span class="red" ><h4 style="color: red; display:inline;"> Check Your Information</h4></span>
				      		<div class="spacer30"></div>
							      <?php

							        if($customer){

							       		if($customer->default_source){
												$customer_default_card = retrive_customer_card_stripe($customer->default_source, get_user_meta($user_id, 'customer_stripe_id', true));
										?>
									<div class="default-card-form cardinfo">
										<div class="errormsg"></div>
				      			<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" enctype="multipart/form-data" id="
												custom-payment-form-default" class="paymentcustomFormDefault">
										<div class="row">
						
												<?php foreach($customer->sources->data as $customer_card){ ?>
													<?php 
													if($customer_card->id == $customer->default_source)
													{
														$carddet = true;
														$existdiv = 'selecteddefault';
													}else{
														$carddet= false;
														$existdiv = '';
													}
													
												?>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12 checkCardDiv">
														<?php if($carddet== false){  
															$hide = 'show-div';
															$show = 'hide-div';
															
														 }else{ 

															$hide = 'hide-div';
															$show = 'show-div';
														 } 
														 $v=(string)$customer_card['id'];
														 ?>
														<div class="form-check  <?php echo $hide;?>">
														  <label class="form-check-label" >
														  <input class="form-check-input"  type="radio" name="defaultcard"   onchange="setDefaultCard(this,'<?php echo $customer_card['id'] ;?>',<?php echo $user->ID; ?>)">
														    Set as Default Card
														  </label>
														</div>
														
														<h5 class="text-center <?php echo $show;?> <?php echo $existdiv; ?>"><b><?php if($carddet== true) { echo 'Default Card' ; }    ?></b></h5>
													
														<div class="each-card-inner">
															<p><?php _e('Card number', 'speedy') ?><br>
															<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
															<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
															<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
															<div class="card-holder-name">
																<p><?php _e('Cardholder`s name', 'speedy') ?><br>
																<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
															</div>
														</div>
														
													</div>
							
												</div>
											</div>
												<?php } ?>
											<div class="deliver-order-prog"></div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
												<input type="hidden" name="action" value="custom_mem_update">
												<input type="hidden" name="basicOrder1" value="" class="basicOrder1">
												<input type="hidden" name="standardOrder1" value="" class="standardOrder1">
												<input type="hidden" name="premiumOrder1" value="" class="premiumOrder1">
												<input type="hidden" name="editmemMock1" value="" class="editMemMock1">
												<input type="hidden" name="editvideoMock1" value="" class="editvideoMock1">
												<input type="hidden" name="editmonfee1" value="" class="editmonfee1">
												<input type="hidden" name="userid" value="<?php echo 
							      					$user->ID; ?>">

                      	<button type="submit" class="btn btn-success-big " name="default_card"><?php _e('Submit', 'speedy') ?></button>
											 	<button type="button" class="black_button right" id="showcard1">Add Card</button>
											</div>
										</div>
									</form>
										<div class="spacer20"></div>
						
										<hr>
									</div>
										<?php } } ?>
										<?php if($customer && $customer->default_source){
											$nocus = 'cus';
										}else{
											$nocus = 'nocus';
										} ?>
										<div class="addCardDiv <?php echo $nocus ?>" style="display: none;" id="addCardDiv">
											<h4 class="text-center">Add Card Details</h4>
											<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" enctype="multipart/form-data" id="
												custom-payment-form" class="paymentcustomForm">
												<div class="form-group row">
											    <label for="monthamount" class="col-sm-4 col-form-label">Amount/permonth</label>
											    <div class="col-sm-8">
											      <input type="text" readonly name="monthamount" class="form-control" id="monthamount" value="">
											    </div>
												</div>
												<div class="form-group row">
											    <label for="cardName" class="col-sm-4 col-form-label"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
											    <div class="col-sm-8">
											      <input type="text" name="cardname"  class="form-control" id="cardName" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required>
											      <input type="hidden" name="action" value="custom_mem_update">
											      <input type="hidden" name="basicOrder2" value="" class="basicOrder2">
													<input type="hidden" name="standardOrder2" value="" class="standardOrder2">
													<input type="hidden" name="premiumOrder2" value="" class="premiumOrder2">
													<input type="hidden" name="editmemMock2" value="" class="editMemMock2">
													<input type="hidden" name="editvideoMock2" value="" class="editvideoMock2">
													<input type="hidden" name="editmonfee2" value="" class="editmonfee2">
													<input type="hidden" name="userid" value="<?php echo 
							      					$user->ID; ?>">
											    </div>
												</div>
										 		<div class="form-group row">
										 		
					                <label for="cardelement" class="col-sm-4 col-form-label">
					                <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
					                </label>
					                <div class="col-sm-8">
					                  <div id="cardelement" >
					                  </div>
					                    <!-- A Stripe Element will be inserted here. -->
					                  <!-- Used to display form errors. -->
					                  	<div id="cardErrors" role="alert" class="cardErrors"></div>
					                	
					                </div>         
					             	</div>
	                   		<script src="https://js.stripe.com/v3/"></script>
					                
	          					
		                		<div class="row">
			                    <div class="col-md-12 text-center">
			                    	<input type="hidden" name="paywithcard">
		                      	<button type="submit" class="btn btn-success-big " name="paywithcard"><?php _e('Submit', 'speedy') ?></button>
			                     		<button type="button" class="black_button right" id="showcardForm">Add Card</button>
			                    </div>
				                </div>
		            			</form>
				      			</div>
				      	</div>
				    	</div>
				  	</div>
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<?php         
  				$selected = showselectedPlanByUser($user->ID);
  				$month= [];
  				$month['order'] = 0;
  				$month['mockups'] = 0;
      			if(isset($selected))
      			{
      				if($selected->frequency == 'Y')
						    {
						        if(strtolower($selected->name) == 'bronze')
						        {
						            $month['order'] = 2;
						            $month['mockups'] = 2;
						        }
						        if(strtolower($selected->name) == 'silver')
						        {
						            $month['order'] = 5;
						            $month['mockups'] = 6;
						        }if(strtolower($selected->name) == 'gold')
						        {
						            $month['order'] = 8;
						            $month['mockups'] = 14;
						        }
						        
						    }
						    if($selected->frequency == 'M')
						    {
						        if(strtolower($selected->name) == 'bronze')
						        {
						            $month['order'] = 2;
						            $month['mockups'] = 2;
						        }
						        if(strtolower($selected->name) == 'silver')
						        {
						            $month['order'] = 5;
						            $month['mockups'] = 6;
						        }if(strtolower($selected->name) == 'gold')
						        {
						            $month['order'] = 8;
						            $month['mockups'] = 14;
						        }
						        
						    }
      			}
					?>
				<?php  if($selected != null && $selected->name != 'CUSTOM' && $selected->status=='Active') { ?>
					<div class="col-md-12 search_row">
						<div class="content-box-black">
							<!-- <div class="box-title"> -->
								<!-- <h4 class="text-center"><strong>Refund Customer</strong></h4> -->
							<!-- </div> -->
							<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" class="updateMembershipForm"> 
								<p class="errupdateform" style="color: red"></p>
								
								<div class="form-group row">
								    <label for="orderpermonth" class="col-sm-5 col-form-label">Order per Month</label>
								    <div class="col-sm-7">
								      <input type="text" class="form-control" name="orderpermonth" id="orderpermonth" placeholder="Order" value="<?php echo $month['order'] ;?>" readonly>
								      
								    </div>
								</div>
								<div class="form-group row">
							    <label for="mockpermonth" class="col-sm-5 col-form-label">Mockups per Month</label>
							    <div class="col-sm-7">
							      <input type="text" class="form-control" name="mockpermonth" id="mockpermonth" placeholder="Mockups" value="<?php echo $month['mockups'] ?>" readonly>
							   
							    </div>
								</div>
								
								<div class="form-group row">
							    <label for="hasMembership" class="col-sm-5 col-form-label">Membership</label>
							    <div class="col-sm-7">
							    	<select name="membership" class="membershipDropdown" onchange="getmocks(this.value,<?php echo $user->ID ; ?>)">
							    		<option value="">-Select Membership-</option>
							    		<?php 
							    			global $wpdb;
												$memberships = $wpdb->get_results("SELECT * FROM $wpdb->memberships");
							    		?>
							    		<?php if(!empty($memberships)){
							    			foreach($memberships as $key=>$value){ ?>
							    			<option  <?php if(isset($value) && $value->mem_ship_id == $selected->mem_ship_id ) {?> selected="selected"<?php } ?> value="<?php echo $value->mem_ship_id ?>" ><?php echo $value->name ;?>  &nbsp;&nbsp;&nbsp;--<?php echo $value->frequency ;?></option>
							    		<?php } } ?>
							    		

							    	</select>
							     	
							      <input type="hidden" name="userid" value="<?php  echo 
							      $user->ID; ?>">
							    </div>
								</div>

								<div class="changecustom" style="display: none;">
									<div class="form-group row">
								    <label for="updatebasicOrder" class="col-sm-5 col-form-label">Basic Order<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="text" class="form-control" name= "updatebasicOrder" id="updatebasicOrder" placeholder="Basic Order" required value="">

								    </div>
									</div>
						
									<div class="form-group row">
								    <label for="updatestandardOrder" class="col-sm-5 col-form-label">
									    Standard Order<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="text" class="form-control" name="updatestandardOrder" id="updatestandardOrder" placeholder="Standard Order" required value="">
								    </div>
									</div>
									<div class="form-group row">
								    <label for="updatepremiumOrder" class="col-sm-5 col-form-label">
									    Premium Orders<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="text" class="form-control" name="updatepremiumOrder" id="updatepremiumOrder" placeholder="Premium Order" required value="">
								    </div>
									</div>
									<div class="form-group row">
								    <label for="updatememberMockups" class="col-sm-5 col-form-label">Mockups<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="number" class="form-control"  id="updatememberMockups" name="updatememMockups" placeholder="Mockups" required value=""> 
								    </div>
									</div>
									<div class="form-group row">
								    <label for="updatevideoMockups" class="col-sm-5 col-form-label">Video Mockups<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="number" class="form-control" name="updatevideoMockups" id="updatevideoMockups" placeholder="Video Mockups" value="">
								    </div>
									</div>
									<div class="form-group row">
								    <label for="updatemonthlyfee" class="col-sm-5 col-form-label">Monthly Fee<span class="required">*</span></label>
								    <div class="col-sm-7">
								      <input type="text" class="form-control" id="updatemonthlyfee" name="updatemonthlyFee" placeholder="Monthly Fee" required value="">
								    </div>
									</div>
								</div>

								<div class="getoldplan" style="display: none"><?php echo $selected->mem_ship_id ;?></div>
								<div class="form-group row">
								  <div class="col-sm-12 text-center">
								  	<input type="hidden" name="action" value="update_admin_membership" class="hiddenUpdateMembership">
								  	<button type="button" name="update" class="white_button btn-padd-black updateAdminMembership">Update</button>
								  </div>
								</div>
								<div class="form-group row">
									<div class="col-sm-6 oldplan"></div>
									<div class="col-sm-6 newplan"></div><br>
									<div class="text-center total_moc_order"></div>
								</div>
							</form>
						</div>
						<div class="deliver-order-prog"></div>
					</div>
				<?php } ?>
			</div>
					<div class="modal fade updatepaymentmodal" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true" style="top:45px;">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header text-center">
				        <h5 class="modal-title" id="updateModalLabel">Payment Method</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div class="container-fluid">
				      		<span class="headinfo"><h4 style="display: inline;"><?php _e('Please enter credit card details below ', 'speedy') ?></h4></span><span class="red" ><h4 style="color: red; display:inline;"> Check Your Information</h4></span>
				      		<div class="spacer30"></div>
							      <?php

							        if($customer){

							       		if($customer->default_source){
												$customer_default_card = retrive_customer_card_stripe($customer->default_source, get_user_meta($user_id, 'customer_stripe_id', true));
										?>
									<div class="update-default-card-form cardinfo">
										<div class="errormsg"></div>
				      				<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" enctype="multipart/form-data" id="
												update_payment_form_default" class="update_payment_form_default">
										<div class="row">
						
												<?php foreach($customer->sources->data as $customer_card){ ?>
													<?php 
													if($customer_card->id == $customer->default_source)
													{
														$carddet = true;
														$existdiv = 'selecteddefault';
													}else{
														$carddet= false;
														$existdiv = '';
													}
													
												?>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12 checkCardDiv">
														<?php if($carddet== false){  
															$hide = 'show-div';
															$show = 'hide-div';
															
														 }else{ 

															$hide = 'hide-div';
															$show = 'show-div';
														 } 
														 $v=(string)$customer_card['id'];
														 ?>
														<div class="form-check  <?php echo $hide;?>">
														  <label class="form-check-label" >
														  <input class="form-check-input"  type="radio" name="defaultcard"   onchange="setDefaultCard(this,'<?php echo $customer_card['id'] ;?>',<?php echo $user->ID; ?>)">
														    Set as Default Card
														  </label>
														</div>
														
														<h5 class="text-center <?php echo $show;?> <?php echo $existdiv; ?>"><b><?php if($carddet== true) { echo 'Default Card' ; }    ?></b></h5>
													
														<div class="each-card-inner">
															<p><?php _e('Card number', 'speedy') ?><br>
															<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
															<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
															<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
															<div class="card-holder-name">
																<p><?php _e('Cardholder`s name', 'speedy') ?><br>
																<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
															</div>
														</div>
														
													</div>
							
												</div>
											</div>
												<?php } ?>
											<div class="deliver-order-prog"></div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
												<input type="hidden" name="action" value="update_admin_membership">
											
												<input type="hidden" name="userid" value="<?php echo 
							      					$user->ID; ?>">
				      					<input type="hidden" name="getmockupsmonthly" value="" class="getmockupsmonthly">
					      				<input type="hidden" name="getordermonthly" value="" class="getordermonthly">
					      				<input type="hidden" name="getmembership" value="" class="getmembership">
					      				<input type="hidden" name="getupdatebasicorder" value="" class="getupdatebasicorder">
					      				<input type="hidden" name="getupdatestandardorder" value="" class="getupdatestandardorder">
					      				<input type="hidden" name="getupdatepremiumorder" value="" class="getupdatepremiumorder">
					      				<input type="hidden" name="getupdatevideomock" value="" class="getupdatevideomock">
					      				<input type="hidden" name="getupdatememmock" value="" class="getupdatememmock">
					      				<input type="hidden" name="monthamount" class="savemonthamount" value="">
					      				<input type="hidden" name="update_default_card">
                      	<button type="submit" class="btn btn-success-big " name="update_default_card"><?php _e('Submit', 'speedy') ?></button>
											 	<button type="button" class="black_button right" id="updateshowcard1">Add Card</button>
											</div>
										</div>
									</form>
										<div class="spacer20"></div>
						
										<hr>
									</div>
										<?php } } ?>
										<?php if($customer && $customer->default_source){
											$nocus = 'cus';
										}else{
											$nocus = 'nocus';
										} ?>
										<div class="updateaddCardDiv <?php echo $nocus ?>" style="display: none;" id="updateaddCardDiv">
											<h4 class="text-center">Add Card Details</h4>
											<form method="post" action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" enctype="multipart/form-data" id="
												update_payment_form" class="updatepaymentForm">
												<div class="form-group row">
											    <label for="monthamount" class="col-sm-4 col-form-label">Amount/permonth</label>
											    <div class="col-sm-8">
											      <input type="text" readonly name="monthamount" class="form-control" id="updatemonthamount" value="">
											    </div>
												</div>
												<div class="form-group row">
											    <label for="updatecardName" class="col-sm-4 col-form-label"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
											    <div class="col-sm-8">
											      <input type="text" name="cardname"  class="form-control" id="updatecardName" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required>
											      <input type="hidden" name="action" value="update_admin_membership">
											      
														<input type="hidden" name="userid" value="<?php echo 
							      					$user->ID; ?>">
							      				<input type="hidden" name="getmockupsmonthly" value="" class="getmockupsmonthly">
							      				<input type="hidden" name="getordermonthly" value="" class="getordermonthly">
							      				<input type="hidden" name="getmembership" value="" class="getmembership">
							      				<input type="hidden" name="getupdatebasicorder" value="" class="getupdatebasicorder">
							      				<input type="hidden" name="getupdatestandardorder" value="" class="getupdatestandardorder">
							      				<input type="hidden" name="getupdatepremiumorder" value="" class="getupdatepremiumorder">
							      				<input type="hidden" name="getupdatevideomock" value="" class="getupdatevideomock">
							      				<input type="hidden" name="getupdatememmock" value="" class="getupdatememmock">
											    </div>
												</div>
										 		<div class="form-group row">
					                <label for="updatecardElement" class="col-sm-4 col-form-label">
					                <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
					                </label>
					                <div class="col-sm-8">
					                  <div id="updatecardElement" >
					                    <!-- A Stripe Element will be inserted here. -->
					                  <!-- Used to display form errors. -->
					                	</div>
					                  	<div id="updatecardErrors" role="alert" class="updatecardErrors"></div>
					                </div>         
					             	</div>
					                
	                   		<script src="https://js.stripe.com/v3/"></script>
	          					
		                		<div class="row">
			                    <div class="col-md-12 text-center">
			                    	<input type="hidden" name="paywithcard">
		                      	<button type="submit" class="btn btn-success-big " name="paywithcard"><?php _e('Submit', 'speedy') ?></button>
			                     		<button type="button" class="black_button right" id="updateshowCardForm">Add Card</button>
			                    </div>
				                </div>
		            			</form>
				      			</div>
				      	</div>
				    	</div>
				  	</div>
					</div>
				</div>

			
			<?php } ?>
		</div>
		<?php }else{ ?>
		<?php if(isset($_GET['users']) and $_GET['users']=='deleted'){ ?>
		<div class="delete_user_message updated notice" id="delete_user_message"><p><?php  echo "Seleted User(s) deleted successfully";  ?></p></div>
		<?php } ?>
		
		<div class="delete_user_message" id="delete_user_message"></div>
	<form id="deleteCustomers">
	<div class="text-right">
	<a href="<?php echo admin_url() ?>/admin.php?page=speedy-customer-list&download_users_expenses_list=1" class="black_button add_customer_btn"><?php  _e('Download', 'speedy') ?></a>
	<input type="submit" name="submit" value="Delete" id="deleteUsers" class="black_button add_customer_btn">
	<a href="<?php echo admin_url() ?>/admin.php?page=speedy-add-customer" class="black_button add_customer_btn"><?php  _e('Add', 'speedy') ?></a>
	</div>
	<div class="sales-reports">
	<h3><?php _e('Customer List', 'speedy') ?></h3>

	<table class="table">
		<thead>
			<tr>
				<th><?php  _e('Name', 'speedy') ?></th>
				<th><?php  _e('Company', 'speedy') ?></th>
				<th><?php  _e('Location', 'speedy') ?></th>
				<th><?php  _e('Email', 'speedy') ?></th>
				<th><?php  _e('Phone No.', 'speedy') ?></th>
			</tr>
		</thead>
		<tbody>
		<?php			
			$count=1;
			$args = array(
				'role' => 'subscriber', 
			);
			$users= get_users($args);
			//$users = get_users('orderby=nicename&role=subscriber');
			foreach ($users as $user) {
			
			?>
			<tr>
				<td><input type="checkbox" name="users" value="<?php echo $user->ID ?>"> <?php echo $count.".<a href='?page=speedy-customer-list&customer=".$user->ID."'>" .get_user_meta($user->ID, 'first_name', true)."</a>" ?></td>
				<td><?php echo get_user_meta($user->ID, 'company_name', true) ?></td>
				<td><?php echo get_user_meta($user->ID, 'address', true) ?></td>
				<td><?php echo $user->user_email ?></td>
				<td><?php echo get_user_meta($user->ID, 'phone_no', true) ?></td>
			</tr>
			<?php $count++; } ?>
		</tbody>
	</table>
	</div>
	</form>
	
		<?php } ?>
</div>

<script type="text/javascript">
	var ajax_url='<?php echo admin_url('admin-membership.php') ?>';
	jQuery('.member-errors').html('');
	function cancelMembership(e,userid)
	{
		 var data = {
		    'action': jQuery('#cancelMem').val(), 
		   	'userid': userid
		};
		jQuery.post(ajax_url, data, function(response) {
      console.log(response);
     	 jQuery('.cancel-mem-btn').removeAttr("disabled");
	      if(response == 'cancelled'){
	        // jQuery('.registration-message').text('Logging in...');
	      
	        window.location = "<?php echo admin_url() ?>admin.php?page=speedy-customer-list&customer="+userid;
	              
	      }else {
	        jQuery('.member-errors').html(response);
	      }
  		});
		
	}
	function updateMembership(e,userid)
	{
		var isempty = false;
		var basicOrder 	= jQuery('#basicOrder').val();
	   	var standardOrder 	= jQuery('#standardOrder').val();
	   	var premiumOrder 	= jQuery('#premiumOrder').val();
	   	var mockups 		= jQuery('#editmembershipMockups').val();
	   	var videoMockups 	= jQuery('#editvideoMockups').val();
	   	var monthlyFee 	= jQuery('#editmonthlyfee').val();
	   	if(basicOrder == '' || standardOrder == '' || premiumOrder == '' || mockups == '' || videoMockups == '' || monthlyFee == ''){
	   		isempty = true;
	   	}
	   	jQuery('#monthamount').val('');
	   	if(isempty == false)
	   	{
	   		jQuery('#exampleModal').modal('show');
	   		jQuery('#monthamount').val(monthlyFee);
	   		jQuery('.basicOrder1').val(basicOrder);	
	   		jQuery('.standardOrder1').val(standardOrder);
	   		jQuery('.premiumOrder1').val(premiumOrder);
	   		jQuery('.editMemMock1').val(mockups);
	   		jQuery('.editvideoMock1').val(videoMockups);
	   		jQuery('.editmonfee1').val(monthlyFee);	
	   		jQuery('.basicOrder2').val(basicOrder);	
	   		jQuery('.standardOrder2').val(standardOrder);
	   		jQuery('.premiumOrder2').val(premiumOrder);
	   		jQuery('.editMemMock2').val(mockups);
	   		jQuery('.editvideoMock2').val(videoMockups);
	   		jQuery('.editmonfee2').val(monthlyFee);

	   	}

		//  var data = {
		//     'action': jQuery('#adminUpdateMem').val(), 
		//    	'userid': userid,
		//    	'basicOrder' : jQuery('#basicOrder').val(),
		//    	'standardOrder' : jQuery('#standardOrder').val(),
		//    	'premiumOrder' : jQuery('#premiumOrder').val(),
		//    	'mockups' : jQuery('#editmembershipMockups').val(),
		//    	'videoMockups' : jQuery('#editvideoMockups').val(),
		//    	'monthlyFee' : jQuery('#editmonthlyfee').val()

		// };
		// jQuery.post(ajax_url, data, function(response) {
  //     console.log(response);
  //    	 jQuery('.btn-mem-update').removeAttr("disabled");
	 //      if(response == 'updated'){
	 //        // jQuery('.registration-message').text('Logging in...');
	      
	 //        window.location = "<?php // echo admin_url() ?>admin.php?page=speedy-customer-list&customer="+userid;
	              
	 //      }else {
	 //        jQuery('.member-errors').html(response);
	 //      }
  // 		});
		
	}
	
</script>



<script type="text/javascript">
	jQuery(document).ready(function(){
		if(jQuery('#addCardDiv').hasClass('nocus')){
			jQuery('.addCardDiv').css('display','block');
		}
		jQuery('#showcardForm').click(function(){
			if(jQuery('.default-card-form').css('display') == 'none') {
				jQuery('#showcard1').text('Add Card');
				jQuery('.default-card-form').css('display','block');
				jQuery('addCardDiv').css('display','none');
			}else{
				jQuery('#showcardForm').text('Add Card');
				jQuery('.default-card-form').css('display','none');
			}

		});
		jQuery('#showcard1').click(function(){
			if(jQuery('.addCardDiv').css('display') == 'none') {
				jQuery('#showcardForm').text('Close Form');
				jQuery('.addCardDiv').css('display','block');
				jQuery('.default-card-form').css('display','none');
			}else{
				jQuery('#showcardForm').text('Add Card');
				jQuery('.addCardDiv').css('display','none');
				jQuery('.default-card-form').css('display','block');
			}

		});


		if(jQuery('#updateaddCardDiv').hasClass('nocus')){
			jQuery('.updateaddCardDiv').css('display','block');
		}
		jQuery('#updateshowCardForm').click(function(){
			if(jQuery('.update-default-card-form').css('display') == 'none') {
				jQuery('#showcard1').text('Add Card');
				jQuery('.update-default-card-form').css('display','block');
				jQuery('.updateaddCardDiv').css('display','none');
			}else{
				jQuery('#updateshowCardForm').text('Add Card');
				jQuery('.update-default-card-form').css('display','none');
			}

		});
		jQuery('#updateshowcard1').click(function(){
			if(jQuery('.updateaddCardDiv').css('display') == 'none') {
				jQuery('#updateshowCardForm').text('Close Form');
				jQuery('.updateaddCardDiv').css('display','block');
				jQuery('.update-default-card-form').css('display','none');
			}else{
				jQuery('#updateshowCardForm').text('Add Card');
				jQuery('.updateaddCardDiv').css('display','none');
				jQuery('.update-default-card-form').css('display','block');
			}

		});
		
	});
</script>

<script type="text/javascript">
	
	// jQuery(document).ready(function() {

      function setDefaultCard(ele,cardid,userid){
      	// var selectdiv = jQuery(ele);
      	// console.log(selectdiv);
        // ele.preventDefault();
        jQuery('.errormsg').html('');
        jQuery(ele).parents('.checkCardDiv').find('h5').html('');
        jQuery.ajax({
            type: "POST",
            url: "admin-ajax.php",
            dataType: "json",
            data: {card:cardid, userid:userid, action:'change_user_default_card'},
            success : function(data){
            	console.log(data.code);
                if (data.code == 200){
 
                  	
                  	if(jQuery(ele).parent().parent('.form-check').hasClass('show-div'))
                  	{
                  		jQuery('.selecteddefault').prev('.form-check').removeClass('hide-div').addClass('show-div');
                  		jQuery('.checkCardDiv').find('.selecteddefault').removeClass('.selecteddefault').removeClass('show-div').addClass('hide-div');

                  		jQuery(ele).parent().parent('.form-check').removeClass('show-div');
                  		jQuery(ele).parent().parent('.form-check').addClass('hide-div');
                  		jQuery(ele).parents('.checkCardDiv').find('h5').removeClass('hide-div').addClass('show-div').addClass('selecteddefault').html('<b>Default Card</b>');

                  	}
                } else {
                   jQuery('.errormsg').html(data.msg);
                }
            }
        });


      }
      function getmocks(val,userid)
      {
      	
      	jQuery('.changecustom').css('display','none');
      	jQuery.ajax({
            type: "POST",
            url: "admin-ajax.php",
            dataType: "json",
            data: {mem_id:val, userid:userid, action:'change_plan_dropdown'},
            success : function(res){
            	console.log(res.code);
                if (res.code == 200){
 									var mem = res.data;
        					jQuery('#mockpermonth').val(mem.newmockup);
        					jQuery('#orderpermonth').val(mem.neworder);
        					jQuery('.oldplan').html('<p>Existing Plan :- '+mem.oldplan+'</p>'
        						+'<span >mockups :<span class="sumoldmock">'+mem.oldmockups+'</span></span><br><span >orders :<span class="sumoldorder">'+mem.oldorder+'</span></span><br>'
        					
        						);
        					jQuery('.newplan').html('<p> New Plan :- '+mem.newplan+'</p>'
        						+'<span class="sumnewmock">mockups :'+mem.newmockup+'</span><br><span class="sumneworder">orders :'+mem.neworder+'</span></div><br>'
        					
        						);
        					jQuery('.total_moc_order').html('<span class="gettotalorder"><b>Total Orders:'+mem.totalorder +' </b></span><span class="gettotalmockup"> <b>Total Mockups: '+ mem.totalmockups +'</b></span>');
        					jQuery('#updateaddCardDiv').find('#updatemonthamount').val(mem.newamount);
        					jQuery('.update-default-card-form').find('.savemonthamount').val(mem.newamount);
        					jQuery('.updateAdminMembership').prop('disabled', false);
        					if(mem.newplan == 'CUSTOM')
			      			{
			      				jQuery('.changecustom').css('display','block');
			      				jQuery('#updateaddCardDiv').find('#updatemonthamount').val('');
			      				jQuery('.update-default-card-form').find('.savemonthamount').val('');
			      				
			      				// jQuery('.updateAdminMembership').prop('disabled', true);
			      			}
                } else {
                   
                }
            }
        });
      }
      jQuery('#updatememberMockups').keyup(function() {
      	var mock = jQuery('#updatememberMockups').val();
      	var videoMockups = jQuery('#updatevideoMockups').val();
      	if(videoMockups == '')
      	{
      		videoMockups = 0;
      	}if(mock == '')
      	{
      		mock = 0;
      	}
      	var total = parseInt(mock) + parseInt(videoMockups);
      	jQuery('#mockpermonth').val(total);
      	jQuery('.newplan').find('.sumnewmock').html('mockups :'+total);
      	var oldmock = jQuery('.oldplan').find('.sumoldmock').text();
      	var showtotal = total + parseInt(oldmock);
      	jQuery('.total_moc_order').find('.gettotalmockup').html(' <b>Total Mockups: '+ showtotal +'</b>');
      });
      jQuery('#updatevideoMockups').keyup(function() {
      	var mock = jQuery('#updatememberMockups').val();
      	var videoMockups = jQuery('#updatevideoMockups').val();
      	if(mock == '')
      	{
      		mock = 0;
      	}
      	if(videoMockups == '')
      	{
      		videoMockups = 0;
      	}
      	var total = parseInt(mock) + parseInt(videoMockups);
      	jQuery('#mockpermonth').val(total);
      	jQuery('.newplan').find('.sumnewmock').html('mockups :'+total);
      	var oldmock = jQuery('.oldplan').find('.sumoldmock').text();
      	var showtotal = total + parseInt(oldmock);
      	jQuery('.total_moc_order').find('.gettotalmockup').html(' <b>Total Mockups: '+ showtotal +'</b>');
      });

      jQuery('#updatebasicOrder').keyup(function() {
      	var order = jQuery('#updatebasicOrder').val();
      	var standardOrder = jQuery('#updatestandardOrder').val();
      	var premiumOrder = jQuery('#updatepremiumOrder').val();
      	if(order == '')
      	{
      		order = 0;
      	}
      	if(standardOrder == '')
      	{
      		standardOrder = 0;
      	}
      	if(premiumOrder == '')
      	{
      		premiumOrder = 0;
      	}
      		var total = parseInt(order) + parseInt(standardOrder)+ parseInt(premiumOrder);
      		jQuery('#orderpermonth').val(total);
      		jQuery('.sumneworder').val('orders :'+total);
      		var oldorder = jQuery('.oldplan').find('.sumoldorder').text();
      	var showtotal = total + parseInt(oldorder);
      	
      		jQuery('.total_moc_order').find('.gettotalorder').html('<b>Total Orders:'+showtotal +' </b>');

      });
      jQuery('#updatestandardOrder').keyup(function() {
      	var order = jQuery('#updatebasicOrder').val();
      	var standardOrder = jQuery('#updatestandardOrder').val();
      	var premiumOrder = jQuery('#updatepremiumOrder').val();
      	if(order == '')
      	{
      		order = 0;
      	}
      	if(standardOrder == '')
      	{
      		standardOrder = 0;
      	}
      	if(premiumOrder == '')
      	{
      		premiumOrder = 0;
      	}
      		var total = parseInt(order) + parseInt(standardOrder)+ parseInt(premiumOrder);
      		jQuery('#orderpermonth').val(total);
      		jQuery('.sumneworder').val('orders :'+total);
      		var oldorder = jQuery('.oldplan').find('.sumoldorder').text();
      	var showtotal = total + parseInt(oldorder);
      	
      		jQuery('.total_moc_order').find('.gettotalorder').html('<b>Total Orders:'+showtotal +' </b>');
      });
      jQuery('#updatepremiumOrder').keyup(function() {
      	var order = jQuery('#updatebasicOrder').val();
      	var standardOrder = jQuery('#updatestandardOrder').val();
      	var premiumOrder = jQuery('#updatepremiumOrder').val();
      	if(order == '')
      	{
      		order = 0;
      	}
      	if(standardOrder == '')
      	{
      		standardOrder = 0;
      	}
      	if(premiumOrder == '')
      	{
      		premiumOrder = 0;
      	}
      		var total = parseInt(order) + parseInt(standardOrder)+ parseInt(premiumOrder);
      		jQuery('#orderpermonth').val(total);
      		jQuery('.sumneworder').val('orders :'+total);
      		var oldorder = jQuery('.oldplan').find('.sumoldorder').text();
      	var showtotal = total + parseInt(oldorder);
      	
      		jQuery('.total_moc_order').find('.gettotalorder').html('<b>Total Orders:'+showtotal +' </b>');
      });

      jQuery('.updateAdminMembership').click(function(e){
      	e.preventDefault();
      		jQuery('.getordermonthly').val('');
      		jQuery('.getmockupsmonthly').val('');
      		jQuery('.errupdateform').html('');
      		var oldplan = 	jQuery('.getoldplan').text();
      		var newplan =  	jQuery('.membershipDropdown').val();
      		var mockup 	= 	jQuery('#mockpermonth').val();
      		var order 	= 	jQuery('#orderpermonth').val();

      		if(oldplan == newplan)
      		{
      			jQuery('.errupdateform').html('Please change plan first');
      		}else if(newplan == 8 || newplan == 9){
      			var basicorder = jQuery('#updatebasicOrder').val();
      			var standardorder = jQuery('#updatestandardOrder').val();
      			var premiumorder = jQuery('#updatepremiumOrder').val();
      			var updatememberMockups = jQuery('#updatememberMockups').val();
      			var updatevideoMockups = jQuery('#updatevideoMockups').val();

      			var updatemonthlyfee = jQuery('#updatemonthlyfee').val();

      			if(basicorder == '' && standardorder == '' && premiumorder == '' && updatememberMockups =="" && updatevideoMockups == '')
      			{
      					jQuery('.errupdateform').html('please fill all required field');
      					jQuery('#updateModal').modal('hide');
      			}else{
      				jQuery('#updateModal').modal('show');
      			}
      			jQuery('#updateaddCardDiv').find('#updatemonthamount').val(updatemonthlyfee);
      			jQuery('.update-default-card-form').find('.savemonthamount').val(updatemonthlyfee);
      			jQuery('.getordermonthly').val(order);
      			jQuery('.getmockupsmonthly').val(mockup);
      			jQuery('.getmembership').val(newplan);
      			jQuery('.getupdatebasicorder').val(basicorder);
      			jQuery('.getupdatestandardorder').val(standardorder);
      			jQuery('.getupdatepremiumorder').val(premiumorder);
      			jQuery('.getupdatevideomock').val(updatevideoMockups);
      			jQuery('.getupdatememmock').val(updatememberMockups);
      			
      		}else{
      			jQuery('#updateModal').modal('show');
      			jQuery('.getordermonthly').val(order);
      			jQuery('.getmockupsmonthly').val(mockup);
      			jQuery('.getmembership').val(newplan);
					}
      		
      });
  // });
</script>
<?php if(!isset($_GET['addcard']) && !($_GET['addcard']=='payment')){ ?>

<script>



  // Create a Stripe client.
<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripe = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '14px',
    
    '::placeholder': {
      color: '#aab7c4',
      fontSize: '14px',
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#cardelement');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('cardErrors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementsByClassName('paymentcustomForm');
console.log(form);

form[0].addEventListener('submit', function(event) {
  event.preventDefault();

   var card_name = document.getElementById('cardName').value;

   jQuery(".InputElement").css('font-size',
   '14px');
  stripe.createToken(card, {name: card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('cardErrors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
       var card_number = result.token.card.last4;
        var hiddenInput2 = document.createElement('input');
        hiddenInput2.setAttribute('type', 'hidden');
        hiddenInput2.setAttribute('name', 'lastFour');
        hiddenInput2.setAttribute('value', card_number);
        form[0].appendChild(hiddenInput2);
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementsByClassName('paymentcustomForm');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form[0].appendChild(hiddenInput);
    
  // Submit the form
  form[0].submit();
}


</script>
<?php } ?>
<script>

  // Create a Stripe client.
<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripess = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elementsss = stripess.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '14px',
    
    '::placeholder': {
      color: '#aab7c4',
      fontSize: '14px',
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var newcard = elementsss.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
newcard.mount('#updatecardElement');

// Handle real-time validation errors from the card Element.
newcard.addEventListener('change', function(event) {
  var displayError = document.getElementsByClassName('updatecardErrors');
  
  if (event.error) {
    displayError[0].textContent = event.error.message;
  } else {
    displayError[0].textContent = '';
  }
});

// Handle form submission.
var form = document.getElementsByClassName('updatepaymentForm');
console.log(form[0]);

form[0].addEventListener('submit', function(event) {
  event.preventDefault();

   var new_card_name = document.getElementById('updatecardName').value;

   jQuery(".InputElement").css('font-size',
   '14px');
  stripess.createToken(newcard, {name: new_card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementsByClassName('updatecardErrors');
      errorElement[0].textContent = result.error.message;
    } else {
      // Send the token to your server.
       var get_card_number = result.token.card.last4;
        var hiddenInput2 = document.createElement('input');
        hiddenInput2.setAttribute('type', 'hidden');
        hiddenInput2.setAttribute('name', 'lastFour');
        hiddenInput2.setAttribute('value', get_card_number);
        form[0].appendChild(hiddenInput2);
      stripeTokenHandler3(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler3(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementsByClassName('updatepaymentForm');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form[0].appendChild(hiddenInput);
    
  // Submit the form
  form[0].submit();
}

</script>

<?php if(isset($_GET['addcard']) and $_GET['addcard']=='payment'){ ?>
<script>
	// Create a Stripe client.
<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripeset = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elementst = stripeset.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '14px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var paycard = elementst.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
paycard.mount('#card-element');

// Handle real-time validation errors from the card Element.
paycard.addEventListener('change', function(event) {
  var displayError = document.getElementsByClassName('card-errors');
  if (event.error) {
    displayError[0].textContent = event.error.message;
  } else {
    displayError[0].textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
console.log(form);
form.addEventListener('submit', function(event) {
  event.preventDefault();
	 var cardName = document.getElementById('card_name').value;
  stripeset.createToken(paycard, {name: cardName}).then(function(result1) {
    if (result1.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementsByClassName('card-errors');
      errorElement[0].textContent = result1.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler12(result1.token);
    }
  });
});


// Submit the form with the token ID.
function stripeTokenHandler12(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput4 = document.createElement('input');
  hiddenInput4.setAttribute('type', 'hidden');
  hiddenInput4.setAttribute('name', 'stripeToken');
  hiddenInput4.setAttribute('value', token.id);
  form.appendChild(hiddenInput4);

  // Submit the form
  form.submit();
}

</script>
<?php } ?>