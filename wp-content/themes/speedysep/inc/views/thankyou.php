<div class="spacer35"></div>
<div class="thankyou-page text-center">
	<h1><?php echo get_user_meta(get_current_user_id(), 'first_name', true); ?><br>Thank you</h1>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/thankyou-img.png" alt="thankyou" />
	<div class="additional-text">
		<p>We got your Order! Wel’ll let you know when it is ready.<br>
		for any other questions feel free to contact us.<br>
		Thank you for your business!</p>
	</div>
</div>
<?php $order_id=$_GET['order'] ?>
<script>
dataLayer.push({
'event':'autoEvent',
'eventCategory':'Ecommerce',
'eventAction':'Purchase',
'eventValue':<?php echo get_post_meta($order_id, 'order_total', true); ?>,                   // Total transaction value (incl. tax and shipping)  
 'ecommerce': {
   'purchase': {
     'actionField': {
       'id': '<?php echo $order_id ?>',                               // real order id  - is required.
       'affiliation': 'Internet Shop',        
       'revenue': <?php echo get_post_meta($order_id, 'order_total', true); ?>                         // Total transaction value  - real data is required.
     },
     'products': [{                                       // List of productFieldObjects.
       'name': 'Vectorizing <?php echo get_post_meta($order_id, "package", true);  ?>',            // Name - is required.
       'price': <?php echo get_post_meta($order_id, 'order_total', true); ?>,                               //product price - is required
       'quantity': 1,                                      //quantity- is required.
       'category': '<?php if(get_post_meta($order_id, "order_type", true)=='seperation'){ echo "separation"; }else{ echo get_post_meta($order_id, "order_type", true); } ?> '                      //product category - is required.
      },
	{                              
       'name': 'Extra Fast Delivery 1-24 Hours',            // Name - is required.
       'price': <?php if(get_post_meta($order_id, 'extrafast', true)=="yes"){ echo "10.00"; }else{ echo "0"; } ?>,                                  //product price - is required
       'quantity': <?php if(get_post_meta($order_id, 'extrafast', true)=="yes"){ echo "1"; }else{ echo "0"; } ?>,                                         //quantity- is required.
       'category': 'Additional Service'           //product category - is required.
      }
]
   }
 }
});

jQuery(document).ready(function() {

    setTimeout( function(){ 
    window.location='<?php echo site_url() ?>/user-dashboard/?order_id=<?php echo $order_id ?>';
  }  , 3000 );

});
</script>
