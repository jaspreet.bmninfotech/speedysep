<div class="spacer35"></div>
<?php if((! isset($_GET['type']) ||  $_GET['type']=='') and  $_GET['order']==''){ ?>
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="gray-bg place-an-order">
			<h2><?php _e('Place Your Order Here', 'speedy') ?></h2>
			<a href="<?php echo site_url() ?>/place-an-order/?type=vectorizing"><?php _e('Vectorizing', 'speedy') ?></a>
			<a href="<?php echo site_url() ?>/place-an-order/?type=seperation"><?php _e('Separation', 'speedy') ?></a>
		</div>
	</div>
</div>
<?php } ?>

<!-- if $_GET['vectorizing'] or seperation or set -->
<?php if(isset($_GET['type']) and  $_GET['type']!='' and ($_GET['type']=='vectorizing' || $_GET['type']=='seperation') and !isset($_GET['order'])){ ?>
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="place-an-order-form">
			<span class="type_button"><?php if($_GET['type']=='seperation'){ echo "Separation"; } else{ echo $_GET['type']; } ?></span>
			<div class="spacer35"></div>
			<div class="spacer10"></div>
			<div class="prog-main">
			<h2 style="position: absolute; width: 96%; top: 75px; color:#00000087; text-transform:uppercase;">Loading</h2>
			<div class="progress deliver-order-prog">
				<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
				  0%
				</div>
			</div>
			</div>
			<div class="text-left">
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderForm">
			
			  <div class="form-group">
				<input type="file" name="project_files[]" multiple="multiple" id="file" class="inputfile" data-multiple-caption="{count} files selected"  accept="image/*" required>
				<label for="file"><i class="fas fa-cloud-upload-alt"></i><span>Choose files&hellip;</span></label>
				<div id="file-error" style="color:red;"></div>
				<input type="url" class="form-control"  name="files_link" id="files_link" placeholder="<?php _e('Enter a URL Link(Dropbox, WeTransfer, GoogleDrive etc)', 'speedy') ?>"  required>
			  </div>
			  
			   <?php if($_GET['type']=='vectorizing'){ ?> 
						<div class="row">
							<div class="col-md-9">
								<div class="form-group packages radio">
									<label style="display:block"><?php _e('Art Work Complexity Level:', 'speedy') ?></label>
									<label class="packages-label package_basic radio-label active" for="packageBasic"><input type="radio" id="packageBasic" name="packages" value="basic" checked> BASIC</label> 
									<label class="packages-label package_standard radio-label" for="packageStandard"><input type="radio" id="packageStandard" name="packages" value="standard">STANDARD</label> 
									<label class="packages-label package_premium radio-label" for="packagePremium"><input type="radio" id="packagePremium" name="packages" value="premium">PREMIUM</label> 
									
								</div>
							</div>
							<div class="col-md-3 text-right">
								<div class="form-group weekend-delivery radio">
									<button type="button" class="btn btn-success btn-lg example-btn" data-toggle="modal" data-target="#packagesModal">Examples</button>
								</div>
							</div>
						</div>
			   <?php }else{  ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group color_type radio">
									<label style="display:block"><?php _e('Type of Separation:', 'speedy') ?></label>
									<label class="color_type-label radio-label active" for="spotColor"><input type="radio" id="spotColor" name="sep_type" value="spot color" checked> SPOT COLOR </label> 
									<label class="color_type-label radio-label" for="cmyk"><input type="radio" id="cmyk" name="sep_type" value="cmyk">CMYK <small>(4 Color Process)</small></label> 
									<label class="color_type-label radio-label" for="process_color"><input type="radio" id="process_color" name="sep_type" value="process color">PROCESS COLOR</label>
									<label class="color_type-label radio-label" for="custom_color"><input type="radio" id="custom_color" name="sep_type" value="custom color">CUSTOM COLOR</label> 
									
								</div>
							</div>
							
						</div>
			   <?php } ?>
			  
			  <div class="form-group">
				<label for="color_no" class="slider-label"><?php _e('Number of Colors Desired:', 'speedy') ?></label>
			  1 <input id="color_no" name="color_no" type="text" data-slider-min="1" data-slider-max="16" data-slider-step="1" data-slider-value="1" class="form-control"/> 16 
				<span id="ex6CurrentSliderValLabel"><span id="ex6SliderVal" class="sliderCurrentValue">1</span></span>
			 </div>
			  
			  <div class="form-group dimension-type">
				<label for="dimension" style="display:block"><?php _e('Print Dimensions:', 'speedy') ?></label>
				
				<label for="dimension_width"><input type="number" id="dimension_width" name="dimension_width" class="dimension-input" required> Inches wide</label>  
				<label for="dimension_height"><input type="number" class="dimension-input"  name="dimension_height" id="dimension_height" required> Inches height</label>  
			  </div>
			  <?php if($_GET['type']=='seperation'){ ?>   
			<div class="form-group output-temp">				 
				<label for="outputTemp"><input type="checkbox" class="output-temp-input"  name="output_temp" id="outputTemp"> Upload my output template</label>  
				
				<label for="outputTempFile" style="display:block"></label>  
				<input type="file" class="output-temp-file"   name="project_files_temp[]" id="outputTempFile" required>
				<label for="outputTempFile" class="outputTempFileDiv"><span class="file_text">No File Selected</span> <span class="browse">Browse File</span></label>
			</div>
			
			<div class="form-group speedy-standard-temp">	
				<div class="row">
					<div class="col-md-12">			
					<label for="speedyOutputTemp"><input type="checkbox" class="speedy-standard-temp-input"  name="speedy_output_temp" id="speedyOutputTemp" required> SpeedySep Standard Output Template <button type="button" class="btn standardTempBtn" data-toggle="modal" data-target="#standardTempModal">Click here to view</button></label>  
					</div>
				</div>
			</div>
			  <?php } ?>
			 
			<div class="form-group extra-format">
				<label style="display:block"><?php _e('Choose Extra Format:', 'speedy') ?></label>
				
				<label class="any-label illustrator-label active" for="illustrator"><input type="checkbox" id="illustrator" name="extra-format[]" value="illustrator" checked> Illustrator</label> 
				<label class="any-label png-label active" for="png"><input type="checkbox" id="png" name="extra-format[]" value="png" checked> PNG</label> 
				<label class="any-label jpg-label active" for="jpg"><input type="checkbox" id="jpg" name="extra-format[]" value="jpg" checked> JPG</label> 
				<label class="any-label" for="psd"><input type="checkbox" id="psd" name="extra-format[]" value="psd"> PSD</label> 
				<label class="any-label" for="eps"><input type="checkbox" id="eps" name="extra-format[]" value="eps"> EPS</label> 
				<label class="any-label" for="pdf"><input type="checkbox" id="pdf" name="extra-format[]" value="pdf"> PDF</label> 
				<label class="any-label" for="dxf"><input type="checkbox" id="dxf" name="extra-format[]" value="dxf"> DXF</label> 
				<label class="any-label" for="svg"><input type="checkbox" id="svg" name="extra-format[]" value="svg"> SVG</label> 
				<label class="any-label" for="dwg"><input type="checkbox" id="dwg" name="extra-format[]" value="dwg"> DWG</label> 
				<p class="small">*green higlighted files are always Free</p>
			</div>
			
			<div class="row">
				<div class="col-md-4">
					<div class="form-group halftones">
						<label style="display:block"><?php _e('Halftones:', 'speedy') ?></label>
						
						<label class="halftones-label" for="halftonesYes"><input type="radio" id="halftonesYes" name="halftones" value="yes"> YES</label> 
						<label class="halftones-label active" for="halftonesNo"><input type="radio" id="halftonesNo" name="halftones" value="no" checked> NO</label> 
						
						<p class="small">*5$ Extra</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group extrafast radio">
						<label style="display:block"><?php _e('Extra Fast Delivery:', 'speedy') ?></label>
						
						<label class="extrafast-label radio-label" for="extrafastYes"><input type="radio" id="extrafastYes" name="extrafast"  value="yes"> YES</label> 
						<label class="extrafast-label radio-label active" for="extrafastNo"><input type="radio" id="extrafastNo" name="extrafast" value="no" checked> NO</label> 
						
						<p class="small">*10$ Extra</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group underbase radio">
						<label style="display:block"><?php _e('Underbase:', 'speedy') ?></label>
						
						<label class="underbase-label radio-label" for="underbaseYes"><input type="radio" id="underbaseYes" name="underbase"  value="yes"> YES</label> 
						<label class="underbase-label radio-label active" for="underbaseNo"><input type="radio" id="underbaseNo" name="underbase" value="no" checked> NO</label> 
						
						<p class="small"></p>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4">
					<div class="form-group conceptual-recreation radio">
						<label style="display:block"><?php _e('Conceptual Recreation:', 'speedy') ?></label>
						
						<label class="conceptual-recreation-label conceptual-label radio-label" for="conceptualYes"><input type="radio" id="conceptualYes" name="conceptual" value="yes"> YES</label> 
						<label class="conceptual-recreation-label conceptual-label-no radio-label active" for="conceptualNo"><input type="radio" id="conceptualNo" name="conceptual" value="no" checked> NO</label> 
						
						<p class="small">*10$ Extra</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group weekend-delivery radio">
						<label style="display:block"><?php _e('Weekend Delivery:', 'speedy') ?></label>
						
						<label class="weekend-label radio-label" for="weekendYes"><input type="radio" id="weekendYes" name="weekend" value="yes"> YES</label> 
						<label class="weekend-label radio-label active" for="weekendNo"><input type="radio" id="weekendNo" name="weekend" value="no" checked> NO</label> 
						
						<p class="small">*10$ Extra</p>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="modifications" class="slider-label"><?php _e('Modifications:', 'speedy') ?></label>
			  0 <input id="modifications"  name="modifications" type="text" data-slider-min="0" data-slider-max="16" data-slider-step="1" data-slider-value="0" class="form-control"/> 16 
				<span id="modificationsSliderValLabel"><span id="modificationsSliderVal" class="sliderCurrentValue">0</span></span>
			</div>
			
			<div class="form-group">
				<label for="mockups" class="slider-label"><?php _e('Mockups:', 'speedy') ?></label>
			  0 <input id="mockups" name="mockups" type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="0" class="form-control"/> 20 
				<span id="mockupsSliderValLabel"><span id="mockupsSliderVal" class="sliderCurrentValue">0</span></span>
			</div>
			
			  
			  
			   <div class="form-group">
				<label for="notes"><?php _e('Notes', 'speedy') ?></label>
				
				<textarea  class="form-control"  name="notes" id="notes" placeholder="<?php _e('Write us a note here regarding the image/design', 'speedy') ?>" rows="3" ></textarea>
			  </div>
			  
			  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="create_order">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
			  <button type="submit" class="btn type_button" name="create_order">Next</button>
			  <div class="formErrorMessaages" id="formErrorMessaages" style="color:red;"></div>
			</form>
			</div>

		</div>
	</div>
</div>

<!-- Modal -->
<div id="packagesModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/mix-example-popup.jpg">
      </div>
    </div>

  </div>
</div>

<!-- standardTempModal -->
<div id="standardTempModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/output-template.jpg">
      </div>
    </div>

  </div>
</div>


<style>
.extra-format label.any-label {
    display: inline-block;
    width: 74px;
    /* height: 27px; */
    text-align: center;
    border: 1px solid #ddd;
    line-height: 50px;
    cursor: pointer;
    line-height: 2;
}
.extra-format input[type=checkbox] {
    display: none;
}

.extra-format .any-label{
	background:#a9acb1;
	color:#fff;
}

.extra-format .any-label.active {
    background:#00a652;
}

label.halftones-label{
    display: inline-block;
    width: 80px;
    text-align: center;
    border: 1px solid #ddd;
    line-height: 50px;
    cursor: pointer;
    line-height: 2;
	padding:5px;
	color:#ffffff;
	background:#a9acb1;
}

.halftones-label.active {
    background:#000000;
	color:#ffffff;
}

.halftones input[type=radio]{
		display:none;
}


label.radio-label{
    display: inline-block;
    width: 80px;
    text-align: center;
    border: 1px solid #ddd;
    line-height: 50px;
    cursor: pointer;
    line-height: 2;
	padding:5px;
	color:#ffffff;
	background:#a9acb1;
	padding-left:0px !important;
}

.radio label{
	padding-left:0px;
}

.radio-label.active {
    background:#000000;
	color:#ffffff;
}

label.packages-label{
	 width: 90px;
}

.color_type label.color_type-label{
	width: auto;
    padding: 4px 30px;
    padding-left: 30px !important;
}

.radio input[type=radio]{
		display:none;
}

#packagesModal button.close, #standardTempModal button.close {
    opacity: 1;
    position: absolute;
    right: -5px;
    top: -5px;
    z-index: 6;
    color: #fff;
    background: #000 !important;
    width: 25px;
    height: 25px;
    border-radius: 100px;
    /* font-size: 24px; */
}
#packagesModal .modal-header, #standardTempModal .modal-header{
     padding: 0px !important;
    border-bottom: 0px; 
}

.example-btn{
	margin-top: 18px;
    font-size: 18px;
    padding: 7px 13px;
}

.standardTempBtn, .standardTempBtn:focus, .standardTempBtn:hover{
	    background: #000;
    border-radius: 0;
	color:#fff;
}

.outputTempFileDiv{
	    width: 100%;
    border: 1px solid #cccccc;
}

.outputTempFileDiv span{
	padding: 8px !important;
    padding-top: 10px;
    /* margin-top: 32px; */
    display: inline-block;
}
.outputTempFileDiv .browse{
	    padding: 8px;
		background: #000;
		float: right;
		color: #fff;
}

.output-temp-file{
	display:none !important;
}

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/bootstrap-slider.js" integrity="sha256-WzyQA5KtHvGY2wDq2tVFehwI8SnSjpoUahzv9QEPDCU=" crossorigin="anonymous"></script>
<script>
jQuery("#color_no").slider();
jQuery("#color_no").on("slide", function(slideEvt) {
	jQuery("#ex6SliderVal").text(slideEvt.value);
});

jQuery("#modifications").slider();
jQuery("#modifications").on("slide", function(slideEvt) {
	jQuery("#modificationsSliderVal").text(slideEvt.value);
});

jQuery("#mockups").slider();
jQuery("#mockups").on("slide", function(slideEvt) {
	jQuery("#mockupsSliderVal").text(slideEvt.value);
});
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/css/bootstrap-slider.css" integrity="sha256-jdZgwzG/YTTVCVs+8oG9il7UD/T5P1oaQ2S83BF1D8s=" crossorigin="anonymous" />
<?php }elseif(isset($_GET['order']) and  $_GET['order']!='' and $_GET['step']==3){ ?> 
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="place-an-order-form vect-pricing step3 <?php echo $_GET['type'] ?>">
			<span class="type_button"><?php if($_GET['type']=='seperation'){ echo "Separation"; } else{ echo $_GET['type']; } ?></span>
			<div class="spacer20"></div>
			<div class = "table-responsive">
				<table>
				<?php if($_GET['type']!='seperation'){ 
					$page_id=294;
				}else{ $page_id=414; } ?>
				
				<?php 
				$order_id=$_GET['order'];
				$halftones=get_post_meta($order_id, 'halftones', true);
				$extrafast=get_post_meta($order_id, 'extrafast', true);
				$conceptual=get_post_meta($order_id, 'conceptual', true);
				$weekend=get_post_meta($order_id, 'weekend', true);
				$extra_format=get_post_meta($order_id, 'extra-format', true);
				//print_r($extra_format);
				
				$package='basic';
				$package_price=9;
				
				if(is_array($extra_format)){
				$result = remove_element($extra_format,'illustrator');
				$result = remove_element($result,'png');
				$result = remove_element($result,'jpg');
				}
				$extra_format='no';
				if(!empty($result)){
					$extra_format='yes';
					$package='standard';
					$package_price=19;
				}
				//echo $extra_format;
				$modifications_details=get_post_meta($order_id, 'modifications_details', true);
				$mockups=get_post_meta($order_id, 'mockups', true);
				
				$order_total=0;
				if($halftones=='yes'){
					$order_total+=5;
				}
				if($extrafast=='yes'){
					$order_total+=10;
				}
				if($conceptual=='yes' and $extra_format=='yes'){
					$package='premium'; 
					$package_price=29;
				}elseif($conceptual=='yes'){
					$order_total+=10;
				}
				$order_total+=$package_price;
				if($weekend=='yes'){
					$order_total+=10;
				}
				if($extra_format=='yes' and $package=='basic'){
					$order_total+=5;
				}
				if($modifications_details>0){
					$modification_cost=$modifications_details*2.5;
					$order_total+=$modification_cost;
				}
				if($mockups>0){
					$mockups_cost=$mockups*1;
					$order_total+=$mockups_cost;
				}
				
				
				?>
				
					<thead>
						<th></th>
						<th class="buy-now buy-now-first buy-now-top <?php echo $_GET['type']; if($package=='basic'){ echo " active-package active-package-top"; } ?>" data-id="basic"><?php echo get_field('basic_plan_text', $page_id)?></th>
						<th class="buy-now buy-now-second buy-now-top <?php echo $_GET['type']; if($package=='standard'){ echo " active-package active-package-top"; } ?>" data-id="standard"><?php echo get_field('standard_plan_text', $page_id)?></th>
						<th class="buy-now buy-now-third buy-now-top <?php echo $_GET['type']; if($package=='premium'){ echo " active-package active-package-top"; } ?>" data-id="premium"><?php echo get_field('premium_plan_text', $page_id)?></th>
					</thead>
					<tbody>
						<tr class="even">
							<td><?php echo get_field('choose_plan_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="basic">
								<span class="plan-price"><?php echo SITE_CURRENCY_SYMBOL."".get_field('basic_plan_price', $page_id)?></span>
								<!--<a href="#" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="basic"><?php echo get_field('buy_now_text', $page_id)?></a> -->
							</td>
							<td class="buy-now buy-now-second  <?php if($package=='standard'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="standard">
								<span class="plan-price"><?php echo  SITE_CURRENCY_SYMBOL."".get_field('standard_plan_price', $page_id)?></span>
								<!--<a href="#" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="standard"><?php echo get_field('buy_now_text', $page_id)?></a> -->
							</td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium">
								<span class="plan-price"><?php echo  SITE_CURRENCY_SYMBOL."".get_field('premium_plan_price', $page_id)?></span>
								<!--<a href="" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="premium"><?php echo get_field('buy_now_text', $page_id)?></a> -->
							</td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('illustrator_ai_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second  <?php if($package=='standard'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="even">
							<td><?php echo get_field('transparent_png_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="odd">
							<td><?php echo get_field('eps_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even">
							<td><?php echo get_field('other_formats_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="odd">
							<td><?php echo get_field('concept_recreation_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even">
							<td><?php echo get_field('modifications_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="basic"><?php echo get_field('basic_modifications', $page_id)?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="standard"><?php echo get_field('standard_modifications', $page_id)?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="premium"><?php echo get_field('premium_modifications', $page_id)?></td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('mockup_text', $page_id)?></td>
							<td class="buy-now buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="basic"><?php if(get_field('basic_mockup', $page_id)!=''){   echo get_field('basic_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?>  <?php echo $_GET['type'] ?>" data-id="standard"><?php if(get_field('standard_mockup', $page_id)!=''){   echo get_field('standard_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium"><?php if(get_field('premium_mockup', $page_id)!=''){   echo get_field('premium_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<?php if($_GET['type']!='seperation'){ ?>
						<tr class="even details">
							<td><?php echo get_field('details_text', $page_id)?></td>
							<td class="buy-now  buy-now-first <?php if($package=='basic'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="basic">
								<?php echo get_field('basic_details', $page_id)?>
							</td>
							<td class="buy-now buy-now-second <?php if($package=='standard'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="standard">
								<?php echo get_field('standard_details', $page_id)?>
							</td>
							<td class="buy-now buy-now-third <?php if($package=='premium'){ echo "active-package"; } ?> <?php echo $_GET['type'] ?>" data-id="premium">
								<?php echo get_field('premium_details', $page_id)?>
							</td>
						</tr>
						<?php } ?>
						<tr class="odd">
							<td><?php echo get_field('delivery_text', $page_id)?></td>
							<td class="buy-now buy-now-first buy-now-bottom <?php if($package=='basic'){ echo "active-package active-package-bottom "; } ?>  <?php echo $_GET['type'] ?>" data-id="basic"><?php echo get_field('basic_delivery_time', $page_id)?></td>
							<td class="buy-now buy-now-second buy-now-bottom <?php if($package=='standard'){ echo "active-package active-package-bottom "; } ?>  <?php echo $_GET['type'] ?>" data-id="standard"><?php echo get_field('standard_delivery_time', $page_id)?></td>
							<td class="buy-now buy-now-third buy-now-bottom <?php if($package=='premium'){ echo "active-package active-package-bottom "; } ?>  <?php echo $_GET['type'] ?>" data-id="premium"><?php echo get_field('premium_delivery_time', $page_id)?></td>
						</tr>
						
					</tbody>
				</table>
				</div>
				<div class="spacer10"></div>
				<div class = "table-responsive">
				
				<table class="pricing-sub-table">
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('extra_fast_delivery_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="extraFast" name="extraFast" <?php if($extrafast=='yes'){ echo "checked"; } ?> value="yes"> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('extra_fast_delivery_price', $page_id)?></td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php _e('Weekend Delivery(Saturday or Sunday)', 'speedy') ?></td>
						<td class="right"><input type="checkbox" id="weekendDelivery" name="weekendDelivery" <?php if($weekend=='yes'){ echo "checked"; } ?>> <?php echo SITE_CURRENCY_SYMBOL ?>10</td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('additional_formats_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="additionalFormats" name="additionalFormats" <?php if($extra_format=='yes'){ echo "checked"; } ?>> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('additional_formats_price', $page_id)?></td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('concept_recreation_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="conceptRecreation" name="conceptRecreation" <?php if($conceptual=='yes'){ echo "checked"; } ?>> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('concept_recreation_price', $page_id)?></td>
					</tr>
					
					<tr class="odd">
						<td colspan="3" class="left"><?php _e('Halftones', 'speedy') ?></td>
						<td class="right"><input type="checkbox" id="halftones" name="halftones" <?php if($halftones=='yes'){ echo "checked"; } ?>> <?php echo SITE_CURRENCY_SYMBOL ?>5</td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php _e('Modifications ($2.5 per modification)', 'speedy') ?></td>
						<td class="right"><input type="number" id="modifications" name="modifications" placeholder="Enter a No." value="<?php echo $modifications_details ?>"></td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php _e('Mockups ($1 per mockup)', 'speedy') ?></td>
						<td class="right"><input type="number" id="mockup" name="mockup" placeholder="Enter a No."  value="<?php echo $mockups ?>"></td>
					</tr>
				</table>
				<br>
				<table class="pricing-sub-table">
					<tr class="odd">
						<td colspan="3" class="left"><?php _e('Total', 'speedy') ?></td>
						<td class="right orderTotal">$<span id="orderTotal"><?php echo $order_total; ?></span></td>
					</tr>
				</table>
				
				</div>
				<div class="spacer10"></div>
				<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderFormStep3 text-right">
				  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
				  <?php	global $post;
						$post_slug=$post->post_name; ?>
						
					<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
					<input type="hidden" name="action" value="create_order_pricing">
					<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
					<input type="hidden" name="package" id="package" value="<?php echo $package ?>">
					<input type="hidden" name="extra_fast" id="extra_fast" value="<?php echo $extrafast ?>">
					<input type="hidden" name="page_id" id="page_id" value="<?php echo $page_id; ?>">
					<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order']; ?>">
					<input type="hidden" name="additional_formats" id="additional_formats" value="<?php echo $extra_format ?>">
					<input type="hidden" name="concept_recreation" id="concept_recreation" value="<?php echo $conceptual ?>">
					<input type="hidden" name="half_tones" id="half_tones" value="<?php echo $halftones ?>">
					<input type="hidden" name="weekend_delivery" id="weekend_delivery" value="<?php echo $weekend ?>">
					<input type="hidden" id="mockup1" name="mockup"  value="<?php echo $mockups ?>">
					<input type="hidden" id="modifications1" name="modifications" value="<?php echo $modifications_details ?>">
					<input type="hidden" name="total_amount" id="total_amount" value="<?php echo $order_total; ?>">
				  <button type="submit" class="btn type_button" name="create_order" id="setPricingPlan">NEXT</button>
				</form>
			
		</div>
	</div>
</div>
<?php }elseif($_GET['step']==4){ ?>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 card-auth-form">
		<?php 
		//delete a card
		if(isset($_GET['delete_card']) and $_GET['card']!=''){
			$result=delete_customer_card_stripe($_GET['card']);
			if($result->deleted==true){
				echo "<div class='alert alert-success'>"; _e('Card deleted successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not deleted, please try again', 'speedy'); echo "</div>";
			}
		} 
		//change default card
		if(isset($_GET['change_default']) and $_GET['card']!=''){
			$result=change_customer_default_card_stripe($_GET['card']);
			if($result){
				echo "<div class='alert alert-success'>"; _e('Card changed successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not changed, please try again', 'speedy'); echo "</div>";
			}
		} 
		
		if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)!=''){
		//check if user has atleast one default card
		$customer=retrive_customer(get_user_meta(get_current_user_id(), 'customer_stripe_id', true));
		}else{
			$customer['default_source']='';
		}
		//echo "<pre>"; print_r($customer); echo "</pre>";	
		?>
		<?php if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)=='' || isset($_GET['add_card']) || $customer->default_source==''){ ?>
			<div class="text-center">
			<h2><?php _e('Credit Card Payment Authorization Form', 'speedy') ?></h2>
			</div>
			<div class="user_cards">
				<h2 style="margin-bottom: 7px;"><i class="fas fa-calendar-alt"></i> <?php _e('SUMMARY', 'speedy') ?></h2>
			</div>
			<div class="step4 gray-bg">
				<!--<h2><?php _e('General Informations', 'speedy') ?></h2>-->
				<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" id="payment-form" class="paymentAuthForm">
					 <?php $user_id=get_current_user_id() ?>
					 <!--
				 <div class="row">
				 
					  <div class="form-group col-sm-6">
						<label for="business_name"><?php _e('Business Name', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="business_name" id="business_name" value="<?php echo get_user_meta($user_id, 'business_name', true) ?>" placeholder="<?php _e('Business Name', 'speedy') ?>"  required>
					  </div>
					  
					  <div class="form-group col-sm-6">
					</div>
				  </div>
				  
				  <div class="spacer20"></div>
				<h2><?php _e('Billing Information', 'speedy') ?></h2>
				<div class="row">
					  <div class="form-group col-sm-6">
						<label for="billing_address"><?php _e('Billing Address', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_address" id="billing_address"  value="<?php echo get_user_meta($user_id, 'billing_address', true) ?>" placeholder="<?php _e('Billing Address', 'speedy') ?>"  required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="city"><?php _e('City', 'speedy') ?><span class="required">*</span></label>
						
						<input type="text" class="form-control"  name="city" id="city" placeholder="<?php _e('City', 'speedy') ?>" value="<?php echo get_user_meta($user_id, 'city', true) ?>" required>
					  </div>
				  </div>
				  
				  <div class="row">
					  <div class="form-group col-sm-6">
						<label for="state"><?php _e('State', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="state" id="state" placeholder="<?php _e('State', 'speedy') ?>"  value="<?php echo get_user_meta($user_id, 'state', true) ?>" required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="zipcode"><?php _e('Billing Zipcode', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_zipcode" id="zipcode"  value="<?php echo get_user_meta($user_id, 'billing_zipcode', true) ?>"  placeholder="<?php _e('Billing Zipcode', 'speedy') ?>"  required>
					  </div>
				  </div -->
				  
				  <div class="spacer20"></div>
				<h2><?php _e('Credit Card Details', 'speedy') ?></h2>
				 <div class="spacer10"></div>
				<div class="row visa_mastercard_div">
				<!--
					  <div class="form-group col-sm-6">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/visa_martercard.png">
					  </div>
					  -->
					  <div class="form-group col-sm-6">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
					  </div>
				  </div>
				  
				<div class="row">
				<!--
					  <div class="form-group col-sm-6">
						<label for="card_type"><?php _e('Credit Card Type', 'speedy') ?><span class="required">*</span></label>
						<div>
						<input type="radio"  name="card_type" id="card_type_visa"  value="visa" required> <?php _e('Visa', 'speedy') ?>
						<input type="radio"  name="card_type" id="card_type_master"  value="master" required> <?php _e('Master Card', 'speedy') ?>
						</div>
					  </div>
					  -->
					  
					  <div class="form-group col-sm-6">
						<script src="https://js.stripe.com/v3/"></script>

						  <div class="form-row">
							<label for="card-element">
							  <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
							</label>
							<div id="card-element">
							  <!-- A Stripe Element will be inserted here. -->
							</div>

							<!-- Used to display form errors. -->
							<div id="card-errors" role="alert"></div>
						  </div>
					  </div>
					  <div class="form-group col-sm-6">
						<label for="card_name"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
						<input type="text" id="card_name" name="card_name" class="form-control field" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required />
					  </div>
				</div>
				<!--
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_1">
						<input type="checkbox" id="accept_terms_1" name="accept_terms_1" required />
						<p class="term_1">
						I hereby authorize        <?php echo bloginfo('name') ?>              - his offers, agents, managers affiliates, employees, contractors, and
						representatives to charge my credit card for the stated U.S Dollar Amount effective this date. Client
						agrees that        <?php echo bloginfo('name') ?>            will keep this credit card on file for the year of 2019 for all future purchases,
						Client agrees that this engagement is for all digital design purposes and that SpeedySep INC has the
						right to refund or deny any payment at the companies solde discretion. Client irrevocably waives
						any right to dispute this/these credit card charge(s).
						</p>
						</label>
					  </div>
				</div>
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_2">
						<input type="checkbox" id="accept_terms_2" name="accept_terms_2" required />
						<p class="term_1">
						I accept the <a href="<?php echo site_url().'/terms-and-conditions'; ?>" target="_blank">Terms and Conditions</a>
						</p>
						</label>
					  </div>
				</div>-->
				<div class="spacer20"></div>
				<div class="card-footer text-left" style="background: #f5f5f5; padding: 15px;  margin-top: 15px;  border: 2px solid #000; border-radius: 10px;">
					
						
						<input type="text" name="coupon" id="coupon" placeholder="Coupon Code" style="padding: 5px;">
					  <button class="btn payFormCouponBtn" name="submit_coupon" style="background: black; color: white;">Apply</button><span class="coupon-prog" style="display:none"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/loader.gif" style="width: 15px; margin-left: 10px;"></span>

					<div class="order-total-text order-discount-text">Total Discount:<?php echo SITE_CURRENCY_SYMBOL ?><span id="discountAmountText">0</span></div>
				</div>
			  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="payment_form">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
				<input type="hidden" name="order_id" value="<?php echo $_GET['order'] ?>">
			  <!--<button type="submit" class="btn user-submit-btn" name="auth_card"><?php _e('SUBMIT', 'speedy') ?></button>-->
			  			<div class="user_cards">
							
							<div class="card-footer text-left">
								<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=3" style="background: #000; color: #fff; border: 0; padding: 14px 30px; border-radius: 8px; margin-top: -3px;"><?php _e('Back', 'speedy') ?></a>
								<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&add_card=1" style="background: #000; color: #fff; border: 0; padding: 14px 30px; border-radius: 8px; margin-top: -3px;"><?php _e('Change Card', 'speedy') ?></a>
								  <?php	global $post;
										$post_slug=$post->post_name; 
										$now = current_time( 'mysql' ); 
												if(get_post_meta($_GET['order'], 'extrafast', true)=='yes'){
													$delivery_time=16;
												}else{
												$delivery_time=intval(get_field($package.'_delivery_time', $page_id));
												}
										?>
										
									<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
									
									<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
									<input type="hidden" name="delivery_time" value="<?php echo $delivery_time ?>">
									<input type="hidden" name="order_id" value="<?php echo $_GET['order'] ?>">
									<input type="hidden" name="discount" id="discountValue">
									<input type="hidden" name="coupon_code" id="couponCode">
									<input type="hidden" name="extraFast" id="extraFast" value="<?php echo get_post_meta($_GET['order'], 'extrafast', true) ?>">
									<input type="hidden" name="orderAmount" id="order_amount" value="<?php echo get_post_meta($_GET['order'], 'order_total', true); ?>">
									
									 

								  <button type="submit" class="btn" name="place_order">Place Order</button>
									
								
								<div class="order-total-text">Total:<?php echo SITE_CURRENCY_SYMBOL ?><span id="orderAmountText"><?php echo get_post_meta($_GET['order'], 'order_total', true); ?></span></div>
							</div>
						</div>
			</form>
			
			<!-- Modal -->
			<div id="closingPopup" class="modal fade" role="dialog">
			  <div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close closepopup" data-dismiss="modal">&times;</button>
				  </div>
				  <div class="modal-body">
					<h2>Your first order is <span>free!</span></h2>
					<p>use code freespeedy, click apply.</p>
					<p>And your credit card will not get charged</p>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default closepopup" data-dismiss="modal">GET MY FREE ORDER</button>
				  </div>
				</div>

			  </div>
			</div>
			<style>
			#closingPopup{
				background: #00000059;
			}
			#closingPopup .modal-dialog{
				background:#f1f2f2;
			}
			#closingPopup .modal-dialog{
				margin-top:75px;
				min-width:700px;
			}
			.modal-header .close {
				margin-top: 11px;
				margin-right: 15px;
			}
			.modal-content p { 
				line-height: 25px;
				color:#000;
				text-align:center;
				font-size: 24px;
			}
			#closingPopup .btn.closepopup{
				background: #10c462;
				color: #fff;
				border: 0;
				font-size: 35px;
				padding: 5px 35px;
			}
			#closingPopup h2{
				text-align:center;
				font-size: 27px !important;
				text-transform: uppercase;
			}
			#closingPopup h2 span{
				display: block;
				text-align: center;
				font-size: 94px;
				text-transform: uppercase;
				line-height: 126px;
			}
			#closingPopup .modal-header {
				padding: 30px;
				border-bottom: 0px solid #e5e5e5;
			}
			#closingPopup .modal-header .close {
				margin-top: 9px;
				margin-right: 18px;
				font-size: 41px;
				font-weight: 100;
			}
			#closingPopup .modal-footer {
				text-align: center;
				border-top: 0px;
				padding-bottom: 80px;
			}
			@media only screen and (min-width: 768px)
				.page-template-user-dashboard #closingPopup.modal.fade .modal-dialog {
					min-width: 700px !important;
				}
			}
			</style>
			<script>
			var mouseX = 0;
    		var mouseY = 0;
    		var popupCounter = 0;

    		document.addEventListener("mousemove", function(e) {
    			mouseX = e.clientX;
    			mouseY = e.clientY;
    			
    		});


    		jQuery(document).mouseleave(function () {
    			if (mouseY < 100) {
    				if (popupCounter < 1) {
						jQuery('#closingPopup').show();
    					jQuery('#closingPopup').addClass('in');
    				}
    				popupCounter ++;
    			}
    		});
			
			jQuery('.closepopup').on('click', function(){
				jQuery('#closingPopup').hide();
    			jQuery('#closingPopup').removeClass('in');
			});

    	</script>
			</div>
		<?php }else{ ?> 
			<div class="user_cards">
				<h2><i class="fas fa-calendar-alt"></i> <?php _e('SUMMARY', 'speedy') ?></h2>
				<div class="cards-inner">
					<div class="row">
						<?php  $customer=retrive_customer(get_user_meta(get_current_user_id(), 'customer_stripe_id', true));  
						//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
						if($customer->default_source){
						$customer_default_card=retrive_customer_card_stripe($customer->default_source);
						//print_r($customer_default_card->id);
						?>
						<div class="col-md-7">
							<div class="default-cards">
								<h4><?php _e('You will be charged:', 'speedy') ?></h4>
								<p><?php _e('Card number', 'speedy') ?><br>
									<strong>**** **** **** <?php echo $customer_default_card->last4 ?></strong></p>
								<p><?php _e('Cardholder`s name', 'speedy') ?><br>
									<strong><?php echo $customer_default_card->name ?></strong></p>
							</div>
						</div>
						<div class="col-md-5">
							
							<?php foreach($customer->sources->data as $customer_card){ ?>
							<div class="each-card-inner">
								<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&delete_card=1&card=<?php echo  $customer_card['id'] ?>" onclick="return confirm('Are you sure to delete this card?')" title="<?php _e('Delete Card', 'speedy') ?>" class="card-delete"><i class="fa fa-remove"></i></a>
							
								<p><?php _e('Card number', 'speedy') ?><br>
									<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
								<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
									<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
								<div class="card-holder-name">
									<p><?php _e('Cardholder`s name', 'speedy') ?><br>
									<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
									<?php if($customer_default_card->id != $customer_card['id']){ ?>
									<p class="charge-card"><a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&change_default=1&card=<?php echo  $customer_card['id'] ?>"><?php _e('charge', 'speedy') ?></a></p>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php }else{ ?>
						<p><?php _e('No Payment method available.', 'speedy') ?> </p>
						<?php } ?>
					</div>
				</div>
				
				<div class="card-footer text-left">
					<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="couponForm">
					   <?php wp_nonce_field( 'get_coupon', 'get_coupon_nonce' ); ?>
						<input type="hidden" name="action" value="get_coupon">
						
						<input type="text" name="coupon" id="coupon" placeholder="Coupon Code">
					  <button type="submit" class="btn" name="submit_coupon">Apply</button><span class="coupon-prog" style="display:none"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/loader.gif" style="width: 15px; margin-left: 10px;"></span>
					</form>
					<div class="order-total-text order-discount-text">Total Discount:<?php echo SITE_CURRENCY_SYMBOL ?><span id="discountAmountText">0</span></div>
				</div>
				
				<div class="card-footer text-left">
					<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=3"><button><?php _e('Back', 'speedy') ?></button></a>
					<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&add_card=1"><button><?php _e('Change Card', 'speedy') ?></button></a>
					<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderFormCheckout">
					   <?php wp_nonce_field( 'place_order', 'place_order_nonce' ); ?>
					  <?php	global $post;
							$post_slug=$post->post_name; 
							$now = current_time( 'mysql' ); 
									if(get_post_meta($_GET['order'], 'extrafast', true)=='yes'){
										$delivery_time=16;
									}else{
									$delivery_time=intval(get_field($package.'_delivery_time', $page_id));
									}
							?>
							
						<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
						<input type="hidden" name="action" value="place_order">
						<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
						<input type="hidden" name="delivery_time" value="<?php echo $delivery_time ?>">
						<input type="hidden" name="order_id" value="<?php echo $_GET['order'] ?>">
						<input type="hidden" name="discount" id="discountValue">
						<input type="hidden" name="coupon_code" id="couponCode">
						<input type="hidden" name="extraFast" id="extraFast" value="<?php echo get_post_meta($_GET['order'], 'extrafast', true) ?>">
						<input type="hidden" name="orderAmount" id="order_amount" value="<?php echo get_post_meta($_GET['order'], 'order_total', true); ?>">
					  <button type="submit" class="btn" name="place_order">Place Order</button>
					</form>
					<div class="order-total-text">Total:<?php echo SITE_CURRENCY_SYMBOL ?><span id="orderAmountText"><?php echo get_post_meta($_GET['order'], 'order_total', true); ?></span></div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
<style>
/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script>
	// Create a Stripe client.
	<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripe = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
	 var card_name = document.getElementById('card_name').value;
  stripe.createToken(card, {name: card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
<?php } ?>

<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{ 
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{ 
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );

'use strict';

;( function( $, window, document, undefined )
{
	$( '.output-temp-file' ).each( function()
	{ 
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{ console.log($label);
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span.file_text' ).html(fileName);
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
</script>