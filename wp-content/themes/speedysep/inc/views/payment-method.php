<div class="spacer35"></div>
<div class="payment-method">
	<div class="text-center">
		<span class="black_button"><?php _e('Payment Method', 'speedy') ?></span>
	</div>
	<div class="spacer10"></div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 card-auth-form">
		<?php 
		//delete a card
		if(isset($_GET['delete_card']) and $_GET['card']!=''){
			$result=delete_customer_card_stripe($_GET['card']);
			if($result->deleted==true){
				echo "<div class='alert alert-success'>"; _e('Card deleted successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not deleted, please try again', 'speedy'); echo "</div>";
			}
		} 
		//change default card
		if(isset($_GET['change_default']) and $_GET['card']!=''){
			$result=change_customer_default_card_stripe($_GET['card']);
			if($result){
				echo "<div class='alert alert-success'>"; _e('Card changed successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not changed, please try again', 'speedy'); echo "</div>";
			}
		} 
		
		?>
		<?php if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)=='' || isset($_GET['add_card'])){ ?>
			<?php if(! isset($_GET['add_card'])){ ?>
			<div class="payment-method-inner text-center">
				<a href="?add_card=1">
				<i class="far fa-credit-card"></i>
				<h4><?php _e('Add a payment method', 'speedy') ?></h4>
				</a>
			</div>
			<?php }else{ ?>
			<div class="text-center">
			<h2><?php _e('Credit Card Payment Authorization Form', 'speedy') ?></h2>
			</div>
			<div class="step4 gray-bg">
				<h2><?php _e('General Information', 'speedy') ?></h2>
				<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" id="payment-form" class="paymentAuthForm">
				  <div class="row">
				  <?php $user_id=get_current_user_id() ?>
					  <div class="form-group col-sm-6">
						<label for="business_name"><?php _e('Business Name', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="business_name" id="business_name" value="<?php echo get_user_meta($user_id, 'business_name', true) ?>" placeholder="<?php _e('Business Name', 'speedy') ?>"  required>
					  </div>
					  
				  </div>
				  
				  <div class="spacer20"></div>
				<h2><?php _e('Billing Information', 'speedy') ?></h2>
				<div class="row">
					  <div class="form-group col-sm-6">
						<label for="billing_address"><?php _e('Billing Address', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_address" id="billing_address"  value="<?php echo get_user_meta($user_id, 'billing_address', true) ?>" placeholder="<?php _e('Billing Address', 'speedy') ?>"  required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="city"><?php _e('City', 'speedy') ?><span class="required">*</span></label>
						
						<input type="text" class="form-control"  name="city" id="city" placeholder="<?php _e('City', 'speedy') ?>" value="<?php echo get_user_meta($user_id, 'city', true) ?>" required>
					  </div>
				  </div>
				  
				  <div class="row">
					  <div class="form-group col-sm-6">
						<label for="state"><?php _e('State', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="state" id="state" placeholder="<?php _e('State', 'speedy') ?>"  value="<?php echo get_user_meta($user_id, 'state', true) ?>" required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="zipcode"><?php _e('Billing Zipcode', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_zipcode" id="zipcode"  value="<?php echo get_user_meta($user_id, 'billing_zipcode', true) ?>"  placeholder="<?php _e('Billing Zipcode', 'speedy') ?>"  required>
					  </div>
				  </div>
				  
				  <div class="spacer20"></div>
				<h2><?php _e('Credit Card Details', 'speedy') ?></h2>
				 <div class="spacer10"></div>
				<div class="row visa_mastercard_div">
					 
					  <div class="form-group col-sm-6">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
					  </div>
				  </div>
				  
				<div class="row">
					  
					  <div class="form-group col-sm-6">
						<script src="https://js.stripe.com/v3/"></script>

						  <div class="form-row">
							<label for="card-element">
							  <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
							</label>
							<div id="card-element">
							  <!-- A Stripe Element will be inserted here. -->
							</div>

							<!-- Used to display form errors. -->
							<div id="card-errors" role="alert"></div>
						  </div>
					  </div>
					  <div class="form-group col-sm-6">
						<label for="card_name"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
						<input type="text" id="card_name" name="card_name" class="form-control field" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required />
					  </div>
				</div>
				
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_1">
						<input type="checkbox" id="accept_terms_1" name="accept_terms_1" required />
						<p class="term_1">
						I hereby authorize        <?php echo bloginfo('name') ?>              - his offers, agents, managers affiliates, employees, contractors, and
						representatives to charge my credit card for the stated U.S Dollar Amount effective this date. Client
						agrees that        <?php echo bloginfo('name') ?>            will keep this credit card on file for the year of 2019 for all future purchases,
						Client agrees that this engagement is for all digital design purposes and that SpeedySep INC has the
						right to refund or deny any payment at the companies solde discretion. Client irrevocably waives
						any right to dispute this/these credit card charge(s).
						</p>
						</label>
					  </div>
				</div>
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_2">
						<input type="checkbox" id="accept_terms_2" name="accept_terms_2" required />
						<p class="term_1">
						I accept the <a href="<?php echo site_url().'/terms-and-conditions'; ?>" target="_blank">Terms and Conditions</a>
						</p>
						</label>
					  </div>
				</div>
				<div class="spacer20"></div>
			  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="payment_form">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
			  <button type="submit" class="btn user-submit-btn" name="auth_card"><?php _e('Submit', 'speedy') ?></button>
			</form>
			</div>
			<?php } ?>
		<?php }else{ ?> 
			<div class="user_cards">
				<div class="cards-inner">
					<div class="row">
						<?php  $customer=retrive_customer(get_user_meta(get_current_user_id(), 'customer_stripe_id', true));  
						//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
						if($customer->default_source){
						$customer_default_card=retrive_customer_card_stripe($customer->default_source);
						?>
						
						<div class="col-md-7">
							
							<?php foreach($customer->sources->data as $customer_card){ ?>
							<div class="row">
							<div class="col-sm-9">
							<div class="each-card-inner">
								
								<p><?php _e('Card number', 'speedy') ?><br>
									<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
								<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
									<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
								<div class="card-holder-name">
									<p><?php _e('Cardholder`s name', 'speedy') ?><br>
									<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
								</div>
							</div>
							</div>
							<div class="col-sm-3 text-left">
								<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&delete_card=1&card=<?php echo  $customer_card['id'] ?>" onclick="return confirm('Are you sure to remove **** **** **** <?php echo $customer_card['last4'] ?>? This card will not be available for payments. You will be able to add this card or another card again.  ')" title="<?php _e('Delete Card', 'speedy') ?>" class="card-delete">Remove</a>
							
							</div>
							</div>
							<?php } ?>
						</div>
						<?php }else{ ?> 
						<p><?php _e('No Payment method available.', 'speedy') ?> </p>
						<?php }  ?>
					</div>
				</div>
				
				<div class="card-footer">
					<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&add_card=1"><button><?php _e('Add new card', 'speedy') ?></button></a>
					
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
	
	
</div>
<style>
/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script>
	// Create a Stripe client.
<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripe = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
	 var card_name = document.getElementById('card_name').value;
  stripe.createToken(card, {name: card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>