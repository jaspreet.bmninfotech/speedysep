<div class="spacer35"></div>
<div class="open-orders">

<?php
$user_id=get_current_user_id();
if(isset($_GET['order_id'])){ 
	$order=get_post($_GET['order_id']);
?>
	<h3><?php _e('Open Orders', 'speedy') ?></h3>
	<?php if($order->post_author==$user_id){ ?>
			<div class="order-delivered text-center">
				<div class="order-no">
				<span>
				<?php _e('Order', 'speedy'); ?>#<?php echo $_GET['order_id'] ?>
				</span>
				</div>
				<div class="spacer10"></div>
				<div class="order-delivered-text">
					<h3><?php _e('Share your experience', 'Speedy') ?></h3>
					<p>(this is to help us to improve our services)</p>
					<div class="spacer10"></div>
					<div class="row">
						<div class="col-md-4 col-md-offset-2">
							<h3>Response</h3>
						</div>
						<div class="col-md-4">
							<div class='rating-stars text-center'>
								<ul id='stars-response' class="stars">
								  <li class='star' title='Poor' data-value='1'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Fair' data-value='2'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Good' data-value='3'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Excellent' data-value='4'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='WOW!!!' data-value='5'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								</ul>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-md-offset-2">
							<h3>Service as Expected</h3>
						</div>
						<div class="col-md-4">
							<div class='rating-stars text-center'>
								<ul id='stars_serv_as_expected' class="stars">
								  <li class='star' title='Poor' data-value='1'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Fair' data-value='2'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Good' data-value='3'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Excellent' data-value='4'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='WOW!!!' data-value='5'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								</ul>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-md-offset-2">
							<h3><?php _e('Would you use our service again', 'speedy') ?></h3>
						</div>
						<div class="col-md-4">
							<div class='rating-stars text-center'>
								<ul id='use_our_service_again' class="stars">
								  <li class='star' title='Poor' data-value='1'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Fair' data-value='2'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Good' data-value='3'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='Excellent' data-value='4'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								  <li class='star' title='WOW!!!' data-value='5'>
									<i class='fa fa-star fa-fw'></i>
								  </li>
								</ul>
							</div>
							
						</div>
					</div>
					<div class="spacer10"></div>
					<div class="row">
						<div class="col-md-12">
							<h3><?php _e('Overall Experience', 'speedy') ?></h3>
						</div>
						<div class="col-md-6 col-md-offset-3">
							<div class="conv-message-box">
								<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="rating-from">
									<textarea name="requestMessage" id="requestMessage" placeholder="How was your overall experience with us?" required></textarea>
									<div class="buttons">
										
										<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
										<input type="hidden" name="user_name" id="user_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
										<button type="submit" name="send_message" id="send_message_button"><i class="fas fa-paper-plane"></i></button>
									</div>
									<input type="hidden" name="response" id="response">
									<input type="hidden" name="ServiceAsExpected" id="ServiceAsExpected">
									<input type="hidden" name="userOurServiceAgian" id="userOurServiceAgian">
								</form>
							</div>
							<div class="text-center" id="messaageResponse"></div>
							
						</div>
						<div class="col-md-3">
							<div class="completed-deliverd-buttons order-complete-page">
								<a href="<?php echo site_url() ?>">
								<button class="black_button"><?php _e('Skip this step', 'speedy') ?></button> 
								</a> 
								<a href="<?php echo site_url() ?>">
								<button class="black_button"><?php _e('Place another order', 'speedy') ?></button>
								</a>
							</div>
						</div>
					</div>

						
				</div>
		<?php  ?>
	<?php }else{ ?> 
		<p><?php _e('No record found', 'speedy') ?></p>
	<?php }?>
<?php  
} ?>
</div>
<style>
* {
  -webkit-box-sizing:border-box;
  -moz-box-sizing:border-box;
  box-sizing:border-box;
}

*:before, *:after {
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}

.clearfix {
  clear:both;
}

.text-center {text-align:center;}

a {
  color: tomato;
  text-decoration: none;
}

a:hover {
  color: #2196f3;
}

pre {
display: block;
padding: 9.5px;
margin: 0 0 10px;
font-size: 13px;
line-height: 1.42857143;
color: #333;
word-break: break-all;
word-wrap: break-word;
background-color: #F5F5F5;
border: 1px solid #CCC;
border-radius: 4px;
}

.header {
  padding:20px 0;
  position:relative;
  margin-bottom:10px;
  
}

.header:after {
  content:"";
  display:block;
  height:1px;
  background:#eee;
  position:absolute; 
  left:30%; right:30%;
}

.header h2 {
  font-size:3em;
  font-weight:300;
  margin-bottom:0.2em;
}

.header p {
  font-size:14px;
}



#a-footer {
  margin: 20px 0;
}

.new-react-version {
  padding: 20px 20px;
  border: 1px solid #eee;
  border-radius: 20px;
  box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);
  
  text-align: center;
  font-size: 14px;
  line-height: 1.7;
}

.new-react-version .react-svg-logo {
  text-align: center;
  max-width: 60px;
  margin: 20px auto;
  margin-top: 0;
}





.success-box {
  margin:50px 0;
  padding:10px 10px;
  border:1px solid #eee;
  background:#f9f9f9;
}

.success-box img {
  margin-right:10px;
  display:inline-block;
  vertical-align:top;
}

.success-box > div {
  vertical-align:top;
  display:inline-block;
  color:#888;
}



/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2em; /* Change the size of the stars */
  color:#737067; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#ffd53a;
}

</style>
<script>
jQuery(document).ready(function($){

  /* 1. Visualizing things on Hover - See next part for action on click */
  $('.stars li').on('mouseover', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
	// Now highlight all the stars that's not after the current hovered star
	$(this).parent().children('li.star').each(function(e){
	  if (e < onStar) {
		$(this).addClass('hover');
	  }
	  else {
		$(this).removeClass('hover');
	  }
	});
	
  }).on('mouseout', function(){
	$(this).parent().children('li.star').each(function(e){
	  $(this).removeClass('hover');
	});
  });
  
  
  
  
  /* 2. Action to perform on click */
  $('#stars-response li').on('click', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	var stars = $(this).parent().children('li.star');
	
	for (i = 0; i < stars.length; i++) {
	  $(stars[i]).removeClass('selected');
	}
	
	for (i = 0; i < onStar; i++) {
	  $(stars[i]).addClass('selected');
	}
	
	// JUST RESPONSE (Not needed)
	var ratingValue = parseInt($('#stars-response li.selected').last().data('value'), 10);
	
	$('#response').val(ratingValue);
	
  });
  
  /* 2. Action to perform on click for service as expected */
  $('#stars_serv_as_expected li').on('click', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	var stars = $(this).parent().children('li.star');
	
	for (i = 0; i < stars.length; i++) {
	  $(stars[i]).removeClass('selected');
	}
	
	for (i = 0; i < onStar; i++) {
	  $(stars[i]).addClass('selected');
	}
	
	// JUST RESPONSE (Not needed)
	var ratingValue = parseInt($('#stars_serv_as_expected li.selected').last().data('value'), 10);
	
	$('#ServiceAsExpected').val(ratingValue);
	
  });
  
  /* 2. Action to perform on click */
  $('#stars-response li').on('click', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	var stars = $(this).parent().children('li.star');
	
	for (i = 0; i < stars.length; i++) {
	  $(stars[i]).removeClass('selected');
	}
	
	for (i = 0; i < onStar; i++) {
	  $(stars[i]).addClass('selected');
	}
	
	// JUST RESPONSE (Not needed)
	var ratingValue = parseInt($('#stars-response li.selected').last().data('value'), 10);
	
	$('#response').val(ratingValue);
	
  });
  
  /* 2. Action to perform on click for use our service again? */
  $('#use_our_service_again li').on('click', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	var stars = $(this).parent().children('li.star');
	
	for (i = 0; i < stars.length; i++) {
	  $(stars[i]).removeClass('selected');
	}
	
	for (i = 0; i < onStar; i++) {
	  $(stars[i]).addClass('selected');
	}
	
	// JUST RESPONSE (Not needed)
	var ratingValue = parseInt($('#use_our_service_again li.selected').last().data('value'), 10);
	
	$('#userOurServiceAgian').val(ratingValue);
	
  });
  
  
});

</script>
							