<?php
/**
 * Template Name: User Dashboard
 *
 */
 
$user_id=get_current_user_id();
if($user_id<=0){
  wp_redirect(site_url());
}

?>

<link href="<?php echo get_stylesheet_directory_uri().'/css/user_dashboard.css' ?>" rel="stylesheet" id="user-dashboard-css">
<link href="<?php echo get_stylesheet_directory_uri().'/css/user_dashboard1.css' ?>" rel="stylesheet" id="user-dashboard1-css">


<style type="text/css">

  .footerarea{display:none;}

  .content-bottom-widgets, .copyrightarea, .sidebar.widget-area{display: none;}

  .StripeElement {
  box-sizing: border-box;
  height: 40px;
  padding: 10px 12px;
  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;
  box-shadow: 0 1px 3px 0 #E6EBF1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}
.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #CFD7DF;
}
.StripeElement--invalid {
  border-color: #FA755A;
}
.StripeElement--webkit-autofill {
  background-color: #FEFDE5 !important;
}
/*
.payment_popupbox .modal-content {
  position: relative;
  background-color: #FFFFFF;
  border: 1px solid #999999;
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 0;
  outline: none;
  -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
  background-clip: padding-box;
}
.payment_popupbox .modal-header {
  border-bottom: 1px solid #56BC67;
  background: #56BC67;
  min-height: 16.4286px;
  padding: 10px 15px;
.payment_popupbox .close {
  float: right;
  font-size: 21px;
  font-weight: 700;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  opacity: .2;
  filter: alpha(opacity=20);
}
.payment_popupbox .modal-body {
  position: relative;
  padding: 20px;
}

.payment_popupbox .modal-footer {
  padding: 19px 20px 20px;
  margin-top: 0;
  text-align: right;
  border-top: 1px solid #E5E5E5;
}
.payment_popupbox {
    padding: 0px 0px 0px 0px;
    background: #FFFFFF;
    overflow: hidden;
    margin: 50px auto 0 auto;
    color: #999999;
}
.outer_payment_popupbox{
  margin :0 24%;
}
.payment_popupbox .modal-title{
  margin: 10px 0;
  color : #FFFFFF;
}
.outer_payment_popupbox  .mfp-close {
    color: #FFFFFF;
    font-size: 40px;
    top: 61px;
    right: 12px;
}
.info_chk{padding:20px 15px 30px; text-align: left; }
.info_chk h2 {
    margin: 0;
    font-size: 18px !important;
    margin-bottom: 13px;
}
.btn-center{
  text-align: center;
}
.btn-green,.btn-green:active,.btn-green:focus,.btn-green:hover{
  background: #56BC67;
  color: #fff;
  padding: 10px 25px;
}
*/  
.success_payment_popup{
    margin: 0 30% 0 30% !important;
}
.padd-r-20{
  padding-right: 20px;
}
.mock_div,.order_div{
  margin-bottom: 10px;
}
.mock_div label{
  width: 41%;
}
.mock_div input{
  border-radius: 50px;
}
.order_div input{
  border-radius: 50px;
}
.order_div label{
  width: 41%;
}

.head_ordermock {    margin-bottom: 28px; }

.comup label {font-size: 24px;font-family: 'Arial Rounded MT BOld';font-weight:300;}
.dorers label {font-size: 24px;font-family: 'Arial Rounded MT BOld';font-weight:300;}

div#success_status_popup {
    margin: 0px 30% !important;
}

#success_status_popup button {
    color: #000000 !important;
        top: 0px;
}
.backg_white .modal-header {background: green !important;
    color: #fff !important;  }
.backg_white {
    background: #ffffff !important;
    color: #000000;
    padding: 0px 0px 50px;
    margin-top: 0px;
}

</style>
<link href="<?php echo get_stylesheet_directory_uri().'/css/user_dashboard.css' ?>" rel="stylesheet" id="user-dashboard-css">
<?php
$user_id=get_current_user_id();
 $show_status = showselectedPlan();

 ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    
    <div id="throbber" style="display:none; min-height:120px;"></div>
    <div id="noty-holder"></div>
    <div id="">
      <!-- Navigation -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php //twentysixteen_the_custom_logo(); ?>
          <img width="240" height="38" src="<?php echo site_url(); ?>/wp-content/uploads/2019/04/cropped-Speedy-Seps-f.png" class="custom-logo" alt="SpeedySep">
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav"> 
          <li class="">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Notifications"><i class="fas fa-bell"></i><span class="notif-count" style="display:none"></span></a>
            <ul class="dropdown-menu notification-dropdown">
              <h3><i class="fas fa-bell"></i> Notifications</h3>
            
            </ul>
          </li>         
          <li class="">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user-tie"></i></a>
            <ul class="dropdown-menu user-dropdown">
              <li><a href="<?php echo site_url(); ?>/profile"><i class="fa fa-fw fa-user"></i> Edit Profile</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        <!-- Top Menu for mobile only -->
        <ul class="nav navbar-right top-nav mobile-top"> 
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Notifications"><i class="fas fa-bell"></i></a>
            <ul class="dropdown-menu notification-dropdown">
              <h3><i class="fas fa-bell"></i> Notifications</h3>
            </ul>
          </li>         
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user-tie"></i></a>
            <ul class="dropdown-menu user-dropdown">
              <li><a href="#"><i class="fa fa-fw fa-user"></i> Edit Profile</a></li>
              <li><a href="#"><i class="fa fa-fw fa-cog"></i> Change Password</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav side-nav">
        <?php
          $show_status = showselectedPlan();

          $menu_name = 'speedysep-user-menu';
          $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
          $menu_items = wp_get_nav_menu_items(8);
          foreach ( (array) $menu_items as $key => $menu_item ) { 
            $class='';
            $current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
            foreach($menu_item->classes as $item_class){
              $class.=$item_class." ";
            }
            $title = $menu_item->title;
            if($show_status->status == 'Active'){
              $url = $menu_item->url;
            }else{
              $url = home_url().'/membership';
            }
            if($menu_item->menu_item_parent>0){

              $menu_list .= '<li  class="menu-sub-item ' . $current . '"><a href="'.$url.'" >'.$title.'</a></li>';

            }else{
            $menu_list .= '<li class="' . $current . '"><a href="'.$url.'"><i class="'.$class.'"></i>&nbsp;  '.$title.'</a></li>';
            }
          }
        echo $menu_list;
        ?>
        </ul>
        </div>
        <!-- /.navbar-collapse -->
      </nav>

 
<?php
     $selected = showselectedPlan();




    if($selected->status == 'Active'){ 
    
      ?>
      
      <div class="spacer35"></div>
      
      <?php
       if(isset($_SESSION["error"])){
                echo '<div class="row marg_left_blogs"><div class="col-md-10"><div class="alert alert-danger" style="text-align:center;"><button type="button" class="close close-btn" data-dismiss="alert">×</button><strong>'. $_SESSION["error"] .'</strong></div></div></div>';
                unset($_SESSION["error"]);
            } 

      ?>  

        <div class="spacer35"></div>        
        <div class="row marg_left_blogs" id="main" style="display: block;">
              <div class="col-xs-11 col-sm-11 col-md-11" id="content" style ="background: whitesmoke;padding-bottom: 15px;">
                <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12" style="text-align: center;margin-bottom: 20px;"><h2>MY MEMBERSHIP</h2></div>
                 <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
                  <h3 class="">Plans & Billing</h3>
                </div>
                <div class="col-xs-6 col-lg-4 col-sm-4 col-md-4 annal_bill" id="content_fm">
                  <div class="marg_zero"> Next Bill Date</div>
                  <?php if(isset($selected->end_date) && $selected->end_date != null){
                    $date = date('m-d-Y',strtotime($selected->end_date));
                  }else{
                    $date = 'N/A';
                  } ?>
                  <strong class="marg_zero"><?php echo $date ; ?></strong>
                </div>
                
                
                
                
                
                <div class="col-xs-6 col-lg-4 col-sm-4 col-md-4 annal_bill plan_mar_btm" id="content_fm">
                  <div>Next Bill Charges</div>
                  <?php
                  $per= '--';
                    if(isset($selected->frequency) && $selected->frequency == 'M')
                    {
                      $per= '$'.$selected->amount.'/month';
                    }
                    if(isset($selected->frequency) && $selected->frequency == 'Y')
                    {
                      $per= '$'.($selected->amount*12).'/year';
                    }
                   ?>
                  <strong class="marg_zero"> <?php echo  $per ; ?></strong>
                </div>

                <div class="col-xs-6 col-lg-4 col-sm-4 col-md-4 annal_bill plan_mar_btm" id="content_fm">
                  <div class="">Credit Card</div>
                   <?php if($selected->last_four != null)
                  {
                    $last_four = 'xxxx xxxx xxxx '.$selected->last_four;
                  }else{
                    $last_four = 'xxxx xxxx xxxx xxxx';
                  }
                  ?>
                  <strong class="marg_zero"><?php echo $last_four; ?></strong>

                </div>

              </div>
            </div>
        
   <?php }
?>
            

            <div class="spacer35"></div>
          <div class="row marg_left_blogs" id="main" >
            <div class="col-sm-11 col-md-11 planbox" id="content">
          
              <div class="row choose_order">
            
                <div class="col-xs-12 col-lg-6 col-sm-12 col-md-12 annal_bill plan_mar_btm" id="content_fm">
                   
                  <h2>www.speedysep.com</h2>
                  
                  
                   <?php if($selected->name == 'CUSTOM' || $selected == ' '){

                    $sho = 'none';

                  }else{ $sho = 'block'; } ?>

                  <p id="switch_btn_annal" style="display:<?php echo $sho; ?>">
                  
                  <span class="save_txt">Save <span class="per_15">15%</span> with annual billing</span>
                    
                    <?php 

                     if(isset($selected)){

                            $mem_id = 0;

                            }else{

                              $mem_id = " ";

                            }


                    ?>
                  <button class="btn btn-dark pln_chng" id="pln_chng" onclick="planChange('<?php echo $selected->type; ?>', '<?php echo $selected->amount; ?>', '<?php echo $mem_id; ?>')">SWITCH TO ANNUAL BILLNG</button>
                  </p>
                  <h6>PLAN RATE</h6>

                  

                  <div class="dropdown44" id="drop1" style="display:block;">
                    

                    <?php
                    



                    if(isset($selected) && $selected != null){
                         echo  '<a href="#" class="js-link" id="def_lod"><span class="badge badge-plan mln badge-plan-sm mrm business background_'.strtolower($selected->name).'">'. $selected->name .'</span>'. $selected->type .'<i class="fa fa-chevron-down"></i></a>';
                        
                        
                    }else{
                        
                      echo '<a href="#" class="js-link" id="def_lod"><span class="badge badge-success mln badge-plan-sm mrm business"> Free </span>0 mockups | pay per order<i class="fa fa-chevron-down"></i></a>'; 
                    }
                    
                    ?>
                     
                    
                     
                     
                    
                     
                      <ul class="js-dropdown-list" style="display:none;">
                        <?php

                          
                          $plan_monthly = showMembershipDataMonthly();

                           if(isset($selected)){

                            $mem_id = 0;

                            }else{

                              $mem_id = " ";

                            }

                          foreach ($plan_monthly as $key => $value) {
                            
                            $d = 1;
                            
                            
                                                        

                          echo '<li id="'.$value->mem_ship_id.'_name" onclick="changePlan(event, '.$value->mem_ship_id.', '.$value->amount.','.$d.','.$mem_id.')" class="list_month" style="display: block;"><span class="badge badge-plan mln badge-plan-sm mrm business background_'.strtolower($value->name).'">'.$value->name.'</span>'.$value->type.'</li>';
                          }

                        ?>
                     
                     </ul>
                  </div>

                  <div class="dropdown44" id="drop2" style="display:none;">


                      <a href="#" class="js-link" id="default_plan"><span class="badge badge-plan mln badge-plan-sm mrm business"><?php echo $selected->name; ?></span><?php echo $selected->type; ?><i class="fa fa-chevron-down"></i></a>
                     
                     <ul class="js-dropdown-list">
                        <?php
                        $plan_yearly = showMembershipDataYearly();

                         if(isset($selected)){

                            $mem_id = 0;

                            }else{

                              $mem_id = " ";

                            }

                        foreach ($plan_yearly as $key => $value) {

                            $amnt = $value->amount * 12;
                            
                            $d = 2;
                          
                          echo '<li id="'.$value->mem_ship_id.'_name" onclick="changePlan(event, '.$value->mem_ship_id.', '.$amnt.','.$d.','.$mem_id.')" class="list_year" style="display: block;"><span class="badge badge-plan mln badge-plan-sm mrm business background_'.strtolower($value->name).' ">'.$value->name.'</span>'.$value->type.'</li>';
                        }

                        ?>
                     
                     </ul>
                  </div>
                  
                  <div class="spacer35"></div>
                </div>


                        

              

                  <div id="custom_plan_box" class="custom_plan_box"  style="display: <?php if($selected->name == 'CUSTOM'){echo 'block';}else{echo 'none';} ?> ">
                    
                         <div class="col-xs-12 col-lg-6 col-sm-12 col-md-6" id="content_uptool">
                            <div class="head_ordermock">
                              <p>Let us know how many need and we will contact you with a custom offer within 24 hours</p>

                            </div>
                            <div class="customrightbody form-inline">
                              <div class="mock_div ">
                              <span class="padd-r-20 comup"><label>Mockups/month</label></span>
                              <span><input type="text" class="form-control mock" name="mock"> </span>
                              </div> 
                              <div class="order_div dorers">
                              <span class="padd-r-20"><label>Orders/month</label></span>
                              <span><input type="text" class="form-control order" name="order"> </span>
                              </div>
                            </div>
                         </div>
                  </div>





              <div id="dont_custom_plan_box1" class="dont_custom_plan_box1" style=" display:<?php if($selected->name != 'CUSTOM'){echo 'block';}else{echo 'none';} ?>">

                  <div class="col-xs-12 col-lg-4 col-sm-8 col-md-8" id="content_uptool">

                       <?php if(!isset($selected)){ 
                      
                       ?>
                    
                    <div class="uptool" id="<?php echo $selected->mem_ship_id; ?>" >
                        <ul id="be-select" tabindex="0">
                          <li>0 Mockups/month</li>
                          <li>pay per order</li>
                        </ul>
                      </div>
                    
                    <?php
                        
                    }else{ ?>
                    
                    
                    
                    <div class="uptool deft mrg-b-20" id="<?php echo $selected->mem_ship_id; ?>_deft" style="display:block;"> 
                    
                                <ul id="be-select" tabindex="0">
                                 
                                 <?php
                                 
                                    $plan_ser = unserialize($selected->plan_data);
                                    
                                 if($plan_ser != null){
                                     
                                    foreach($plan_ser as $k=>$v)
                                    {
                                      echo '<li>'.$v.'</li>';
                                    }
                                 }
                                 
                                 ?>
                    
                                        </ul> </div>
                   <?php  }  ?>


                    <?php
                    foreach ($plan_yearly as $key => $value) {
                     ?>
                            


                    <div class="uptool mrg-b-20" id="<?php echo $value->mem_ship_id; ?>" style="display:none">
                      
                        <ul id="be-select" tabindex="0">
                            
                            <?php
                                 
                                    $plan_ser = unserialize($value->plan_data);
                                 
                                 if($plan_ser != null){
                                     
                                    foreach($plan_ser as $k=>$v)
                                    {
                                      echo '<li>'.$v.'</li>';
                                    }
                                 } 
                            ?>
                        </ul> 

                      </div>
                  <?php  }


                    foreach ($plan_monthly as $key => $value) { ?>
                            
                    <div class="uptool mrg-b-20" id="<?php echo $value->mem_ship_id; ?>" style="display:none">
                      
                        <ul id="be-select" tabindex="0">
                          
                           <?php
                                 
                                    $plan_ser = unserialize($value->plan_data);
                                 
                                 if($plan_ser != null){
                                     
                                    foreach($plan_ser as $k=>$v)
                                    {
                                      echo '<li>'.$v.'</li>';
                                    }
                                 } 
                            ?>

                        </ul> 
                      </div>
                    

                   <?php  }

                    ?>
                    
                    
                  </div>
                  <div class="col-xs-12 col-lg-2 col-sm-4 col-md-4 content_price" id="content_price">

                    <p>COST / <span id="dut">

                    <?php if($selected->frequency == 'M'){
                    echo  'MONTH';
                    }else{
                    echo  'YEAR';
                    }

                    ?></span></p>

                    <h3><span class="prc" id="prc"><?php if($selected->name == 'Free'){echo '0';}else{echo $selected->amount;} ?></span></h3>

                  </div>
              </div>

            </div>

                <div class="spacer35"></div>

               <div id="updt_box_custm" class=""  style="display: <?php if($selected->name == 'CUSTOM'){echo 'block';}else{echo 'none';} ?> ">
               
                    <div class="row  choose_order1" id="choose_order1" style="display: <?php if($selected->name == 'CUSTOM'){echo 'block';}else {echo 'none';}?>;">
                      <div class="col-xs-6 col-lg-4 col-sm-6 col-md-4 pln_sts" >
                        <h3>Custom Package</h3>
                      </div>
                      
                      <div class="col-xs-6 col-lg-4 col-sm-6 col-md-4 upd_plns" style="padding-top: 18px;">
                         <span><a href="#customPopup" class="payORupdate" onclick="" id="payORupdate">SUBMIT</a></span>
                      </div>
                      <div class="col-xs-6 col-lg-4 col-sm-6 col-md-4 btn_cncl">
                        <button class="btn" onclick="cancel()" id="cncl">Cancel</button>

                      </div>
                    </div>
              </div>

                <?php if ($selected == ' '){

                      $payment_btn = 'Pay Now';

                }elseif($selected->status == 'Pending'){

                      $payment_btn = 'Pay Now';
                }elseif($selected->status == 'Active'){

                      $payment_btn = 'Update';
                }

                if($selected->status == 'Active'){

                      $cncl_btn = 'Cancel';
                }else{$cncl_btn = ''; }

                ?>

             <div id="updt_box_pln" class="" style=" display:<?php if(!isset($selected) || $selected->name == 'CUSTOM'){echo 'none';}else{echo 'block';} ?>">

                    <div class="row  choose_order1" id="choose_order2" style="">
                      <div class="col-xs-6 col-lg-3 col-sm-6 col-md-3 pln_sts">
                        <h3 id="howmanyplan"><?php  echo $selected->name; ?></h3>
                      </div>
                      <div class="col-xs-6 col-lg-4 col-sm-6 col-md-4 upd_txt">
                        <p id="monthlytotal"><span id="uptd"></span> <span id="durationwise"><?php if($selected->amount == null){echo '';}else{if($selected->frequency == 'M'){echo 'Month';}else{echo 'Year';} } ?></span> Total Cost</p>
                        <div class="prct" id="prct"> <?php if($selected->amount == null){echo '$ 0';}else{echo '$ '.$selected->amount;} ?></div>
                      </div>
                      <div class="col-xs-6 col-lg-3 col-sm-6 col-md-3 upd_plns">
                         <span><a href="#payORupdate_popup" class="payORupdate" onclick="" id="first_pay">Pay Now</a></span>
                      </div>
                      <div class="col-xs-6 col-lg-2 col-sm-6 col-md-2 btn_cncl">
                        <button class="btn" onclick="cancel()" id="cnclafter"></button>

                      </div>
                    </div>
            </div>
              
              
            
            
            </div>
          </div> 
           <div class="spacer35"></div>

           <!--<a href="#success_status_popup" id="check" class="check">Submit</a>-->

          <!-- <div class="row">-->
          <!--  <div class="col-md-11">-->
          <!--    <div style="float: right;">-->
          <!--      <button type="button" class="btn btn-black" onclick="showPayment()">Payment</button>-->
          <!--    </div>-->
          <!--  </div>-->
          <!--</div>-->


          <span>
            <!--<input type="hidden" name="mock" value="5" class="mock">-->
            <!--<input type="hidden" name="order" value="5" class="order">-->
             <!--<a class="payCustom_popup" href="#customPopup"  onclick="" id="payCustom">Submit</a>-->
           <!--  <button class="payCustom_popup" data-ajax-popup="#payORupdate_popup" onclick="" id="payCustom">Submit</button> -->
           </span>
<style type="text/css">
  
  a.payCustom_popup {
    background: #ededed;
    padding: 4px 10px;
    border: 1px solid #999;
    color: #333;
}
</style>
           

            <div class="row marg_left_blogs pay_card" id="pay_card" style="display: none;">
              <div class="col-sm-11 col-md-11">
                <div class="spacer10"></div>
                <div class="text-center">
                  <h2>Credit Card Payment Form</h2>
                </div>
                <div class="step4 gray-bg" style="background-color: #f5f5f5; ">
                  <span><h2 style="display: inline;"><?php _e('Please enter credit card details below ', 'speedy') ?></h2></span><span class="red" ><h2 style="color: red; display:inline;"> Check Your Information</h2></span>
                  <div class="spacer10"></div>
                  <div class="row visa_mastercard_div">
                    <div class="form-group col-sm-6">
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            


          </div>      <!-- /.container-fluid -->
        </div>    <!-- /#page-wrapper -->
    </div><!-- /#wrapper -->

 <a href="#success_status_popup" class="succ-con"></a>
  <div style="display:none">
  <div id="success_status_popup1" class="popupouter success_payment_popup backg_white">
    <div class="popupbox backg_white">
      <div class="contactform">

         <div class="modal-header">
              <h4 class="modal-title">CONFIRMATION</h4>
            </div>
                <h1>We have received your request!</h1>

                <p>Thank You - we will contact you in 24 hours.</p>
      </div>
    </div>
  </div>
</div>
  
    <div style="display:none">
  <div id="payORupdate_popup" class="popupouter outer_payment_popupbox">
    <div class="popupbox payment_popupbox">
        <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
              <h4 class="modal-title">Set Payment Detail</h4>
            </div>
            <div class="info_chk " >
                  <span><h2 style="display: inline;"><?php _e('Please enter credit card details below ', 'speedy') ?></h2></span><span class="red" ><h2 style="color: red; display:inline;"> Check Your Information</h2></span>
                  <div class="spacer10"></div>
                  <div class="row visa_mastercard_div">
                    <div class="form-group col-sm-6">
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
                    </div>
                  </div>
            <form action="<?php echo esc_url( admin_url('admin-membership.php') ); ?>" method="post" enctype="multipart/form-data" id="payment-form" class="paymentAuthForm">
                  <div class="row">
                    <?php $user_id=get_current_user_id() ?>
                    <div class="form-group col-sm-6">
                      <label for="amount"><?php _e('Amount/permonth', 'speedy') ?><span class="required">*</span></label>
                        <?php if($selected->amount)
                     {
                      $am= $selected->amount;
                     }else{
                      $am= "";
                     } ?>
                      <input type="text" class="form-control custmr_nam_fld" readonly="readonly"   name="amount" id="amount" value="<?php echo $am ; ?>" placeholder="<?php _e('Amount', 'speedy') ?>"  required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <script src="https://js.stripe.com/v3/"></script>
                        <div class="form-row">
                        <label for="card-element">
                          <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
                        </label>
                        <div id="card-element" class="crd_name_font">
                          <!-- A Stripe Element will be inserted here. -->
                        </div>
                        <!-- Used to display form errors. -->
                        <div id="card-errors" role="alert"></div>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="card_name"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
                      <input type="text" id="card_name" name="card_name" class="form-control field custmr_nam_fld" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required />
                    </div>
                  </div>
                  <input type="hidden" name="action" value="payment_form">
                  <div class="row">
                    <div class="col-md-12 btn-center">
                      <button type="submit" class="btn btn-green " name="auth_card"><?php _e('Submit', 'speedy') ?></button>
                    </div>
                  </div>
                  </form>
          </div>
      </div>
    </div>
  </div>
</div>      

<div style="display:none">
  <div id="customPopup" class="customPopup  popupouter outer_payment_popupbox">
    <div class="popupbox payment_popupbox">
        <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
              <h4 class="modal-title">Update Account Information </h4>
            </div>
            <div class="info_chk">
              <div class="spacer10"></div>
                    <?php $user_id=get_current_user_id() ; ?>
                    <?php $userInfo = get_userdata($user_id); ?>
                    <?php  $name = get_user_meta($user_id, 'first_name', true);
                      if($name == null)
                      {
                        $name = '';
                      }
                     ?>
                    <?php 
                     $phone = get_user_meta($user_id, 'phone_no', true) ;
                     if($phone == null)
                     {
                      $phone = '';
                     }

                    ?>
                  
            <form action="<?php echo esc_url( admin_url('admin-ajax.php') ); ?>" method="post" enctype="multipart/form-data" id="update-user" class="updateUser">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="f_name"><?php _e('first Name', 'speedy') ?><span class="required">*</span></label>
                      <input type="text" id="first_name" name="first_name" class="form-control field" placeholder="<?php _e('First Name', 'speedy') ?>" required />
                      
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="l_name"><?php _e('Last Name', 'speedy') ?><span class="required">*</span></label>
                      <input type="text" id="last_name" name="last_name" class="form-control field" placeholder="<?php _e('Last Name', 'speedy') ?>" required />
                      
                    </div>
                  </div>
                  <div class="row">
                   
                    <div class="form-group col-sm-6">
                      <label for="email"><?php _e('Email', 'speedy') ?><span class="required">*</span></label>
                      <input type="text" class="form-control" readonly="readonly"   name="email" id="user_email" value="<?php echo $userInfo->user_email ; ?>" placeholder="<?php _e('Email', 'speedy') ?>"  required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="phone"><?php _e('Phone', 'speedy') ?><span class="required">*</span></label>
                      <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $phone ; ?>" placeholder="<?php _e('Phone', 'speedy') ?>"  required>
                    </div>
                  </div>
                  <input type="hidden" name="action" value="user_update">
                  <input type="hidden" name="add_mock"  class="add_mock">
                  <input type="hidden" name="add_order" class="add_order">
                  <div class="row">
                    <div class="col-md-12 btn-center">
                      <button type="submit" class="btn btn-green user-btn-update" name="user-btn-update"><?php _e('Submit', 'speedy') ?></button>
                    </div>
                  </div>
                  </form>
          </div>
      </div>
    </div>
  </div>
</div>

<script>




function planChange(type, amount, mem_id) {
  var j = document.getElementById("pln_chng");


  // document.getElementById("def_lod").innerHTML = type;
  // document.getElementById("prc").innerHTML = amount;
  // document.getElementById("prct").innerHTML = amount;

    
   if(j.innerHTML === "SWITCH TO ANNUAL BILLNG"){
   
      j.innerHTML = "SWITCH TO MONTHLY BILLNG";
           
          document.getElementById("drop1").style.display = "none";
          
          var element = document.getElementById("pln_chng");
            
            element.classList.add("marforleft");
            
          jQuery('.save_txt').addClass('hidden');
          jQuery('.js-dropdown-list').toggle();
          
               document.getElementById("drop2").style.display = "block";
                jQuery("#drop2").find('.js-dropdown-list').find('li:first-child').click().hide();

                document.getElementById("switch_btn_annal").style.textAlign = "Center";
                

                if(mem_id == null){

                  document.getElementById("first_pay").innerHTML = "Pay Now";
                  


                }
       
   }else{
   
      j.innerHTML = "SWITCH TO ANNUAL BILLNG";
           document.getElementById("drop1").style.display = "block";
            
            jQuery('.save_txt').removeClass('hidden');
            
           var element = document.getElementById("pln_chng");
                element.classList.remove("marforleft");
               document.getElementById("drop2").style.display = "none";

                jQuery('.js-dropdown-list').toggle();
               
                jQuery("#drop1").find('.js-dropdown-list').find('li:first-child').click().hide();



   }
}




</script>

<script type="text/javascript">
  jQuery('.payORupdate').magnificPopup({
    type: 'inline',
  });


  jQuery('.check').magnificPopup({
    type: 'inline',
  });


</script>


<script>
function changePlan(evt, plan, amount, dur, mem_id) {

  if(plan == 1){
      var name = 'BRONZE';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";

       document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 2){
      var name = 'SILVER';
document.getElementById("dont_custom_plan_box1").style.display = "block";
document.getElementById("custom_plan_box").style.display = "none";
document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 3){
      var name = 'GOLD';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";
      document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 4){
      var name = 'BRONZE';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";
      document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 5){
      var name = 'SILVER';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";
      document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 6){
      var name = 'GOLD';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";
      document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
  }else if(plan == 7){
      var name = 'FREE';
      document.getElementById("dont_custom_plan_box1").style.display = "block";
      document.getElementById("custom_plan_box").style.display = "none";
      document.getElementById("updt_box_custm").style.display = "none";
      document.getElementById("updt_box_pln").style.display = "block";
      
  }else if(plan == 8){
      var name = 'CUSTOM';
      document.getElementById("dont_custom_plan_box1").style.display = "none";
      document.getElementById("custom_plan_box").style.display = "block";
      
      document.getElementById("updt_box_custm").style.display = "block";
      document.getElementById("updt_box_pln").style.display = "none";

  }else if(plan == 9){
      var name = 'CUSTOM';
      document.getElementById("dont_custom_plan_box1").style.display = "none";
      document.getElementById("custom_plan_box").style.display = "block";

      document.getElementById("updt_box_custm").style.display = "block";
      document.getElementById("updt_box_pln").style.display = "none";
  }
  
var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("uptool");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("uptool");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(plan).style.display = "block";
  
  
  evt.currentTarget.className += " active";

  document.getElementById("amount").value = amount;
  


  if(plan != 9 && plan != 8){

    document.getElementById("switch_btn_annal").style.display = "block";  
  }else{document.getElementById("switch_btn_annal").style.display = "none";  }
  



  if(dur == 1){

    var d = 'MONTH';
  }else {

    var d = 'YEAR';

  }
  
  
//   document.getElementById("durationwise").innerHTML = d;
   
  document.getElementById("prc").innerHTML = '$' + amount;
  document.getElementById("prct").innerHTML = '$' + amount;
  document.getElementById("dut").innerHTML = d;
  document.getElementById("howmanyplan").innerHTML = name;
  document.getElementById("choose_order1").style.display = "block";
  document.getElementById("choose_order2").style.display = "block";
  
  
  if(mem_id == 0){


    if(plan == 7){}


      document.getElementById("first_pay").innerHTML = "Update Plan";
      document.getElementById("howmanyplan").innerHTML = "1 Plan Update";
      document.getElementById("monthlytotal").innerHTML = d + " TOTAL PLAN";
      document.getElementById("cnclafter").innerHTML = "Cancel";
      document.getElementById("first_pay").className = 'payORupdate';
      document.getElementById("first_pay").href = '#payORupdate_popup'; 
        
      

      if(plan == 7){
      // document.getElementById("updt_box_pln").style.display = "none";
      document.getElementById("first_pay").innerHTML = "Update Plan"; 
      document.getElementById("first_pay").className = 'payORupdate_updt';
      document.getElementById("first_pay").href = '';  
      }

  }else if(mem_id == null){

    if(plan == 7){
      document.getElementById("updt_box_pln").style.display = "none";
    }




      document.getElementById("first_pay").innerHTML = "Pay Now";
      document.getElementById("howmanyplan").innerHTML = name;
      document.getElementById("monthlytotal").innerHTML = d + " TOTAL PLAN";
      document.getElementById("cncl").innerHTML = "Cancel";

      var saveFifteen = document.getElementById("pln_chng").innerHTML;
      if(saveFifteen == 'SWITCH TO MONTHLY BILLNG'){
        // document.getElementById("first_pay").innerHTML = "Pay Now";



      }
     
  }
  
  
  // var typeofsave = document.getElementById("first_pay").innerHTML;
  
  
  // if(typeofsave == 'Update Plan'){
      
      
  //   //   document.getElementById("uptd").innerHTML = "Updated";
      
      
  // }
  
  //document.getElementsByClassName("js-dropdown-list").style.display = "block";

  // document.getElementsByClassName("prc").style.background = "#56bc67";
//   var payorupd = document.getElementById("payORupdate");
//   if(plan == 7){
//     payorupd.innerHTML = "Pay";
//     payorupd.setAttribute("data-btnType","pay");
//     payorupd.setAttribute("onclick","show_card('pay')");
//   }else {
//     payorupd.innerHTML = "Update Plan";
//     payorupd.setAttribute("data-btnType","update_plan");
//     payorupd.setAttribute("onclick","show_card('update_plan')");
//   }
}

function cancel(){
  document.getElementById("choose_order1").style.display = "none";
  document.getElementById("choose_order2").style.display = "none";
  // document.getElementById("pay_card").style.display = "none";

}

function show_card(){
//   document.getElementById("pay_card").style.display = "block";
}
function showPayment()
{
  jQuery('#pay_card').toggle();
}

</script>


<script type="text/javascript">
  jQuery('.payORupdate').magnificPopup({
    type: 'inline',
  });

  jQuery('.payCustom_popup').magnificPopup();

  jQuery('.payORupdate').click(function(){
    jQuery('.user-btn-update').removeAttr("disabled");
    var mock = jQuery('.mock').val();
    var order = jQuery('.order').val();
    jQuery('.add_mock').val(mock);
    jQuery('.add_order').val(order);


  });
  
</script>

<script>
  var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function(){
  //login form submit
jQuery('#update-user').on('submit', function(e) {
  e.preventDefault();
  //fire ajax call 
  var data = {
    'action': 'user_update', 
    'email': jQuery('#user_email').val(),
    'first_name':jQuery('#first_name').val(),
    'last_name':jQuery('#last_name').val(),
    'phone' : jQuery('#phone').val(),
    'mock'  : jQuery('.add_mock').val(),
    'order' : jQuery('.add_order').val()
  };
  // jQuery('.registration-message').text('Logging in...');
   jQuery('.user-btn-update').prop('disabled', true);

  jQuery.post(ajax_url, data, function(response) {
      console.log(response);
      jQuery('.user-btn-update').removeAttr("disabled");
      if(response == 'updateuser'){
        // jQuery('.registration-message').text('Logging in...');
        jQuery('#full_name').val('');   
        jQuery('#phone').val('');
        jQuery('#last_name').val('');
          
        window.location = "<?php echo site_url() ?>/membership?confirm=custom";
              
      }else {
        // jQuery('.registration-message').html('<p class="error">Invalid Email/password.</p>');
      }
  });
});
});
jQuery( window ).load(function() {
  var request = '<?php echo($_REQUEST['confirm']);?>';
  if(request == 'custom')
  {
        jQuery.magnificPopup.open({
            items : {
            src: "#success_status_popup1"
            },
            type: 'inline',
        });
      
  }
});
</script>








<script type="text/javascript">

jQuery(function() {
 var list = jQuery('.js-dropdown-list');
 var link = jQuery('.js-link');
 link.click(function(e) {
 e.preventDefault();
 
 list.slideToggle(200);
 });
 list.find('li').click(function() {
 var text = jQuery(this).html();
 var icon = '<i class="fa fa-chevron-down"></i>';
 link.html(text+icon);
 list.slideToggle(200);
 // if (text === '* Reset') {
 // link.html('Select Plan');
 // }
 });
});
</script>


<script>
jQuery(function($){
    $('[data-toggle="tooltip"]').tooltip();
    $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function() {                        
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
    });
})    
    
</script>

<script>
  // Create a Stripe client.
<?php $stripe_settings = get_option('stripe_settings'); ?>
var stripe = Stripe('<?php echo $stripe_settings['stripe_settings']['public_key'] ?>');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '14px',
    
    '::placeholder': {
      color: '#aab7c4',
      fontSize: '14px',
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
   var card_name = document.getElementById('card_name').value;
   
   jQuery(".InputElement").css('font-size',
   '14px');
  stripe.createToken(card, {name: card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.

       var card_number = result.token.card.last4;
        var hiddenInput2 = document.createElement('input');
        hiddenInput2.setAttribute('type', 'hidden');
        hiddenInput2.setAttribute('name', 'lastFour');
        hiddenInput2.setAttribute('value', card_number);
        form.appendChild(hiddenInput2);
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);
    
  // Submit the form
  form.submit();
}


</script>

<style type="text/css">
  .ElementsApp input{
    font-size: 14px !important;
    height: 1.0em;
  }
  .ElementsApp, .ElementsApp .InputElement{
    font-size: 14px !important;
  }
</style>