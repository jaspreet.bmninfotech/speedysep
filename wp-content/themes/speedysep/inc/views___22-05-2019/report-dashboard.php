<div class="spacer20"></div>
<div class="container-fluid ">
	<div class="reports-dashboard">
	<?php			
			
			$total_sales=0;
			$upcomming_earnings=0;
			$open_order_vect=0;
			$open_order_vect_total=0;
			$delivered_orders=0;
			$open_order_sep=0;
			$open_order_sep_total=0;
			$total_expenses=0;
			$completed_orders=0;
			$total_profit_by_percent_in_month=0;
			$open_orders=0;
			$revised_orders=0;
			global $wpdb;
			
			$orders_data = $wpdb->get_results( 
												"
												SELECT a.ID, b.* 
												FROM $wpdb->posts a JOIN $wpdb->postmeta b ON a.ID=b.post_id
												WHERE  a.post_type = 'service-orders' AND b.meta_key='order_status' and (b.meta_value='process' || b.meta_value='delivered' || b.meta_value='revised') 
												"
											);

			foreach ( $orders_data as $order_data ){
					$upcomming_earnings+=get_post_meta($order_data->ID, 'order_total', true);
					if(get_post_meta($order_data->ID, 'order_type', true)== 'vectorizing'){
						$open_order_vect+=1;
						$open_order_vect_total+=get_post_meta($order_data->ID, 'order_total', true);
					}elseif(get_post_meta($order_data->ID, 'order_type', true)== 'seperation'){
						$open_order_sep+=1;
						$open_order_sep_total+=get_post_meta($order_data->ID, 'order_total', true);
					}
					if(get_post_meta($order_data->ID, 'order_status', true)=='delivered'){
						$delivered_orders+=1;
					}else{
						$open_orders+=1;
					}
				
			}
			
			?>
	<div class="upcomming-earnings">
	<div class="row">
		<div class="col-xs-6">
			<h4>Upcoming Earnings</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><?php echo SITE_CURRENCY_SYMBOL.$upcomming_earnings ?></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="progress">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
			  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo ($delivered_orders/$open_orders)*100 ?>%">
			  </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<h4>Open Orders Vectorizing</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><span class="amount"><?php echo SITE_CURRENCY_SYMBOL.$open_order_vect_total ?></span> <span class="vect-order-count"><?php echo $open_order_vect ?></span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<h4>Open Orders Separations</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><span class="amount"><?php echo SITE_CURRENCY_SYMBOL.$open_order_sep_total ?></span> <span class="sep-order-count"><?php echo $open_order_sep ?></span></h4>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-right search-form">
		<form method="get">
			<input type="hidden" name="page" value="reports-dashboard">
			<select name="type" onchange="this.form.submit()">
					<option value=""><?php _e('Both', 'speedy') ?></option>
					<option value="vectorizing" <?php if(isset($_GET['type']) and $_GET['type']=='vectorizing'){ echo "selected"; } ?>><?php _e('Vectorizing', 'speedy') ?></option>
					<option value="seperation" <?php if(isset($_GET['type']) and $_GET['type']=='seperation'){ echo "selected"; } ?>><?php _e('Separation', 'speedy') ?></option>
			</select>
			
			<select name="year" onchange="this.form.submit()">
					<option value=""><?php _e('Year', 'speedy') ?></option>
					<?php for($y=2018; $y<=date('Y'); $y++){ ?>
					<option value="<?php echo $y ?>" <?php if(isset($_GET['year']) and $_GET['year']==$y){ echo "selected"; } ?>><?php echo $y ?></option>
					<?php } ?>
			</select>
			<select id='gMonth' name="month" onchange="this.form.submit()">
				<option value=''>Month</option>
				<option value='1' <?php if(isset($_GET['month']) and $_GET['month']=='1'){ echo "selected"; } ?>>January</option>
				<option value='2' <?php if(isset($_GET['month']) and $_GET['month']=='2'){ echo "selected"; } ?>>February</option>
				<option value='3' <?php if(isset($_GET['month']) and $_GET['month']=='3'){ echo "selected"; } ?>>March</option>
				<option value='4' <?php if(isset($_GET['month']) and $_GET['month']=='4'){ echo "selected"; } ?>>April</option>
				<option value='5' <?php if(isset($_GET['month']) and $_GET['month']=='5'){ echo "selected"; } ?>>May</option>
				<option value='6' <?php if(isset($_GET['month']) and $_GET['month']=='6'){ echo "selected"; } ?>>June</option>
				<option value='7' <?php if(isset($_GET['month']) and $_GET['month']=='7'){ echo "selected"; } ?>>July</option>
				<option value='8' <?php if(isset($_GET['month']) and $_GET['month']=='8'){ echo "selected"; } ?>>August</option>
				<option value='9' <?php if(isset($_GET['month']) and $_GET['month']=='9'){ echo "selected"; } ?>>September</option>
				<option value='10' <?php if(isset($_GET['month']) and $_GET['month']=='10'){ echo "selected"; } ?>>October</option>
				<option value='11' <?php if(isset($_GET['month']) and $_GET['month']=='11'){ echo "selected"; } ?>>November</option>
				<option value='12' <?php if(isset($_GET['month']) and $_GET['month']=='12'){ echo "selected"; } ?>>December</option>
			</select> 
		</form>
	</div>
</div>

<?php 
$total_sales=0;
$total_expenses=0;
$completed_orders=0;
$total_orders=0;
$total_sale_month=0;
global $wpdb;

$another_where="";
if(isset($_GET['month']) and $_GET['month']!=''){
	$month=$_GET['month'];
	$another_where.=" AND Month(post_date) = $month ";
}
if(isset($_GET['year']) and $_GET['year']!=''){
	$year=$_GET['year'];
	$another_where.=" AND YEAR(post_date) = $year";
}


$orders_data = $wpdb->get_results( 
									"
									SELECT ID, post_date
									FROM $wpdb->posts
									WHERE  post_type = 'service-orders' $another_where
									"
								);

foreach ( $orders_data as $order_data ){
		if(isset($_GET['type']) and $_GET['type']=='vectorizing'){
			if(get_post_meta($order_data->ID, 'order_type', true)=='vectorizing'){
				$total_sales+=get_post_meta($order_data->ID, 'order_total', true);
				$total_orders+=1;
				if(get_post_meta($order_data->ID, 'order_status', true)== 'completed'){
					$completed_orders+=1;
				}
			}
		}elseif(isset($_GET['type']) and $_GET['type']=='seperation'){
			if(get_post_meta($order_data->ID, 'order_type', true)=='seperation'){
				$total_sales+=get_post_meta($order_data->ID, 'order_total', true);
				$total_orders+=1;
				if(get_post_meta($order_data->ID, 'order_status', true)== 'completed'){
					$completed_orders+=1;
				}
			}
		}else{
			$total_sales+=get_post_meta($order_data->ID, 'order_total', true);
			$total_orders+=1;
			if(get_post_meta($order_data->ID, 'order_status', true)== 'completed'){
				$completed_orders+=1;
			}
		}
	//get month sale
	if(isset($_GET['month']) and $_GET['month']!=''){
		$month=$_GET['month'];
	}else{
		$month=date('m');
	}
	
	if(date("m",strtotime($order_data->post_date))==$month){
		$total_sale_month+=get_post_meta($order_data->ID, 'order_total', true);
	}
	
}
?>

<div class="upcomming-earnings">
	<div class="row">
		<div class="col-xs-6">
			<h4>Total Sales</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><?php echo SITE_CURRENCY_SYMBOL.$total_sales ?></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="progress">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
			  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo ($completed_orders/$total_orders)*100 ?>%">
			  </div>
			</div>
		</div>
	</div>
	
<?php 
//expenses date
$total_expenses=0;
$month_expenses=0;
$expenses_data = $wpdb->get_results( 
									"
									SELECT ID, post_date
									FROM $wpdb->posts
									WHERE  post_type = 'speedy-expenses' $another_where
									"
								);
//print_r($expenses_data);
foreach ( $expenses_data as $expense_data ){
		$total_expenses+=get_post_meta($expense_data->ID, 'total', true);
		//get month sale
		if(isset($_GET['month']) and $_GET['month']!=''){
			$month=$_GET['month'];
		}else{
			$month=date('m');
		}
		
		if(date("m",strtotime($expense_data->post_date))==$month){
			$month_expenses+=get_post_meta($expense_data->ID, 'total', true);
		}	
}
?>
	
	<div class="row">
		<div class="col-xs-6">
			<h4>Total Expenses</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><?php echo SITE_CURRENCY_SYMBOL.$total_expenses ?></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="progress">
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40"
			  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo ($total_expenses/$total_sales)*100 ?>%; background:#ed1c24;">
			  </div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-6">
			<h4>Total Profit by % in <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><?php echo round((($total_sale_month-$month_expenses)/$total_sale_month)*100) ?>%</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="progress">
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40"
			  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo round((($total_sale_month-$month_expenses)/$total_sale_month)*100) ?>%; background:#f46488">
			  </div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-6">
			<h4>Completed Orders</h4>
		</div>
		<div class="col-xs-6">
			<h4 class="text-right"><span class="vect-order-count"><?php echo $completed_orders ?></span></h4>
		</div>
	</div>
	
</div>

	</div>
</div>
