<div class="registration-page">
	<div class="text-center form-heading"> 
		<h2>CREATE NEW ACCOUNT</h2>
	</div>
	<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
		<div class="input-group form-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
			<input type="text" class="form-control" id="full_name" name="full_name" aria-describedby="emailHelp" placeholder="Full Name" required>
		</div>
		<div class="input-group form-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
			<input type="email" class="form-control" id="email" name="eamil" aria-describedby="emailHelp" placeholder="Email" required>
		</div>
		<div class="input-group form-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fas fa-lock"></i></span>
			<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
		</div>
		
		<div class="checkbox-main">
			<div class="round round-first">
				<input type="checkbox" id="checkbox" />
				<label for="checkbox"></label> <span class="round-checkbox-label">Remember Me</span>
			</div>
			 
			 <div class="round">
				<input type="checkbox" id="checkbox2" />
				<label for="checkbox2"></label> <span class="round-checkbox-label">Subscribe to Newsletter</span>
			 </div>
		 </div>
	  
		<div class="submit-div text-center">
			<button type="submit" id="submit" class="btn btn-primary">REGISTER</button>
		</div>
	</form>
	<div class="registration-message"><p><?php _e('Already have an account? ', 'speedy') ?><a href="#" title="<?php _e('Login', 'speedy') ?>"  id="speedyLogin" data-toggle="modal" data-target="#loginModal"><?php _e('Login', 'speedy') ?></a></p></div>
</div>

<?php //echo do_shortcode('[apsl-login-lite]') ?>