<div class="spacer35"></div>
<div class="edit-profile">
<?php $user_id=get_current_user_id();
	  $user_info = get_userdata($user_id); ?>
	  <?php  if(isset($_GET['message']) and isset($_GET['pass'])){ ?>
	   <div class="alert alert-success"><?php  _e('Profile update successfully', 'speedy') ?></div>
	  <?php }else{ ?>
	  <?php if(isset($_GET['message'])){ ?>
	  <div class="alert alert-success"><?php  _e('Profile update successfully. Password not changed', 'speedy') ?></div>
	  <?php } ?>
	  <?php } ?>
	  <?php //echo do_shortcode('[avatar_upload]'); ?>
	<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" id="profileForm" class="edit_profile">
		<div class="row">
			<div class="col-md-2"><label for="full_name"><?php  _e('Full Name', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="text" class="form-control" name="full_name" id="full_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><label for="user_email"><?php  _e('Email', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="email" class="form-control" name="user_email" id="user_email" value="<?php echo $user_info->user_email ?>">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><label for="company_name"><?php  _e('Company Name', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo get_user_meta($user_id, 'company_name', true) ?>">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><label for="street_address"><?php  _e('Street Addresss', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="text" class="form-control" name="street_address" id="street_address" value="<?php echo get_user_meta($user_id, 'address', true) ?>">
			</div>
		</div>
		<div class="spacer10"></div>
		<h4><?php  _e('Change Password', 'speedy') ?></h4>
		<div class="row">
			<div class="col-md-2"><label for="current_pass"><?php  _e('Current Password', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="password" class="form-control" name="current_pass" id="current_pass">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><label for="new_pass"><?php  _e('New Password', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="password" class="form-control" name="new_pass" id="new_pass">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><label for="confirm_pass"><?php  _e('Confirm Password', 'speedy') ?></label></div>
			<div class="col-md-10">
				<input type="password" class="form-control" name="confirm_pass" id="confirm_pass">
				<span>8 characters or longer. Combine upper and lowercase letters and numbers.</span>
			</div>
		</div>
		
		<div class="row" style="margin-bottom:0px;">
			<div class="col-md-12 text-right">
				 <?php wp_nonce_field( 'update_user_profile', 'update_user_profile_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="update_user_profile">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
				<input type="submit" class="black_button"  name="update_profile" id="update_profile" onClick="validatePassword();" value="Save Changes">
			</div>
		</div>
	</form>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script>
function validatePassword() {
	var validator = jQuery("#profileForm").validate({
		rules: {
			new_pass : {
					minlength : 8
				},
			confirm_pass: {
				equalTo: "#new_pass"
			},
			user_email: "required"
		},
		messages: {
			//new_pass: " Enter Password",
			confirm_pass: " Confirm Password Should be Same as Password"
		}
	});
	if (validator.form()) {
		console.log('Sucess');
	}
}

</script>
<style>
.error{
	    color: red;
    font-weight: 400;
}
</style>