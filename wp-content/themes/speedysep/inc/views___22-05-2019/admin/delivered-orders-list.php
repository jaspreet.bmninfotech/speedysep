<div class="spacer30"></div>
<div class="container-fluid">
	<div class="row">
		<div class="spacer10"></div>
		<div class="col-md-6">
			<div class="open-order-heading"><?php _e('Delivered Orders', 'speedy') ?></div>
			<div class="row">
				<div class="col-md-6">
					<h4 class="vector-orders"><?php _e('Vector Orders', 'speedy') ?></h4>
				</div>
				<div class="col-md-6">
					<h4 class="seperation-orders"><?php _e('Separation Orders', 'speedy') ?></h4>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
				<!--open order vectorizing -->
				<?php
				// The Query
				$args = array(
					'post_type' => 'service-orders',
					'posts_per_page'=>-1,
					"post_status"=>'any',
					'meta_key'   => 'delivery_date',
					'orderby'    => 'meta_value',
					'order'      => 'ASC',
					'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_status',
									'value'    => 'delivered',
									'compare' => '=',
								),
								
								array(
									'key'     => 'order_type',
									'value'   => 'vectorizing',
									'compare' => '=',
								),
																
							),
					
					);
				$vectorizing_orders = new WP_Query( $args );

				if ( $vectorizing_orders->have_posts() ) {
				
					// The Loop
					while ( $vectorizing_orders->have_posts() ) {
						$vectorizing_orders->the_post(); ?>
					<a href="<?php echo admin_url() ?>?page=single-order&order_id=<?php echo get_the_ID() ?>">
					<div class="each-order">
						<div class="each-order-header">
							<?php _e('Order', 'speedy') ?>#<?php echo get_the_ID() ?>
						</div>
						<div class="each-order-body">
							<div class="row">
								<div class="col-xs-8">
									<?php $author_id = get_post_field( 'post_author', $post_id ); ?>
									<p><?php echo get_user_meta($author_id, 'first_name', true); ?><br>
										<?php echo get_post_meta(get_the_ID(), 'modifications_details', true) ?></p>
										<p><?php _e('Delivery Date', 'speedy') ?><br>
											<?php echo date('d M', strtotime(get_post_meta(get_the_ID(), 'delivery_date', true)));  ?></p>
								</div>
								<div class="col-xs-4">
									<?php $proj_files=get_post_meta(get_the_ID(), 'project_files', true);
									$image_available=false;
									if(!empty($proj_files)){
									foreach($proj_files as $proj_file){ ?>
									<?php 
									$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
									if ( $image_attributes ) : ?>
										<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
									<?php  $image_available=true; break; ?>
									<?php endif; ?>
									<?php } 
									} //end if
									?>
									<?php if($image_available==false){ ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					</a>
					<?php } //end while
						} ?>				
				</div>
				<div class="col-md-6">
				<!--open order seperations-->
				<?php
				// The Query
				$args = array(
					'post_type' => 'service-orders',
					'posts_per_page'=>-1,
					"post_status"=>'any',
					'meta_key'   => 'delivery_date',
					'orderby'    => 'meta_value',
					'order'      => 'ASC',
					'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_status',
									'value'    => 'delivered',
									'compare' => '=',
								),
								
								array(
									'key'     => 'order_type',
									'value'   => 'seperation',
									'compare' => '=',
								),
																
							),
					
					);
				$vectorizing_orders = new WP_Query( $args );

				if ( $vectorizing_orders->have_posts() ) {
				
					// The Loop
					while ( $vectorizing_orders->have_posts() ) {
						$vectorizing_orders->the_post(); ?>
					<a href="<?php echo admin_url() ?>?page=single-order&order_id=<?php echo get_the_ID() ?>">
					<div class="each-order">
						<div class="each-order-header">
							<?php _e('Order', 'speedy') ?>#<?php echo get_the_ID() ?>
						</div>
						<div class="each-order-body">
							<div class="row">
								<div class="col-xs-8">
									<?php $author_id = get_post_field( 'post_author', $post_id ); ?>
									<p><?php echo get_user_meta($author_id, 'first_name', true); ?><br>
										<?php echo get_post_meta(get_the_ID(), 'modifications_details', true) ?></p>
										<p><?php _e('Delivery Date', 'speedy') ?><br>
											<?php echo date('d M', strtotime(get_post_meta(get_the_ID(), 'delivery_date', true)));  ?></p>
								</div>
								<div class="col-xs-4">
									<?php $proj_files=get_post_meta(get_the_ID(), 'project_files', true);
									$image_available=false;
									if(!empty($proj_files)){
									foreach($proj_files as $proj_file){ ?>
									<?php 
									$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
									if ( $image_attributes ) : ?>
										<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
									<?php  $image_available=true; break; ?>
									<?php endif; ?>
									<?php } 
									} //end if
									?>
									<?php if($image_available==false){ ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					</a>
					<?php } //end while
						} ?>
					
				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="delivered-order-search text-center">
			<?php
			if(isset($_POST['search'])){
				wp_redirect(site_url()."/wp-admin/admin.php?page=speedy-delivered-orders&type=".$_POST['type']."&s=".$_POST['s']);
			}
			global $wpdb;
			if(isset($_GET['type']) and $_GET['type']=='order_no'){
				$sear_result = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE  ID =".$_GET['s']);
			}elseif(isset($_GET['type']) and $_GET['type']=='customer_name'){
				$sear_result = $wpdb->get_results("SELECT a.*, b.* FROM $wpdb->posts a JOIN  $wpdb->postmeta b ON a.ID=b.post_id WHERE  b.meta_key='customer_name' AND b.meta_value LIKE '%".$_GET['s']."%' ");
			}elseif(isset($_GET['type']) and $_GET['type']=='date'){
				$sear_result = $wpdb->get_results("SELECT a.*, b.* FROM $wpdb->posts a JOIN  $wpdb->postmeta b ON a.ID=b.post_id WHERE  b.meta_key='delivery_date' AND b.meta_value LIKE '%".$_GET['s']."%' ");
			}

			?>
			<form action="" method="post">
				<input type="text" name="s" value="<?php if(isset($_GET['s'])){ echo $_GET['s']; } ?>">
				<select name="type">
					<option value="customer_name" <?php if(isset($_GET['type']) and $_GET['type']=='customer_name'){ echo "selected"; } ?>><?php _e('Customer Name', 'speedy') ?></option>
					<option value="order_no" <?php if(isset($_GET['type']) and $_GET['type']=='order_no'){ echo "selected"; } ?>><?php _e('Order No', 'speedy') ?></option>
					<option value="date" <?php if(isset($_GET['type']) and $_GET['type']=='date'){ echo "selected"; } ?>><?php _e('Date', 'speedy') ?></option>
				</select>
				<input type="submit" name="search" value="Search">
			</form>
			</div>
			
			<div class="row delivered-order-search-main">
				
					<!--delivered order vectorizing -->
				<?php

				if (!empty($sear_result)) {
				
					// The Loop
					foreach ( $sear_result as $sear_res ){ ?>
					<?php if(get_post_meta($sear_res->ID, 'order_status', true)=='delivered'){ ?>
					<div class="col-md-6">
						<a href="<?php echo admin_url() ?>?page=single-order&order_id=<?php echo $sear_res->ID ?>">
						<div class="each-order">
							<div class="each-order-header">
								<?php _e('Order', 'speedy') ?>#<?php echo $sear_res->ID ?>
							</div>
							<div class="each-order-body">
								<div class="row">
									<div class="col-xs-8">
										<?php $author_id = get_post_field( 'post_author', $sear_res->ID); ?>
										<p><?php echo get_user_meta($author_id, 'first_name', true); ?><br>
											<?php echo get_post_meta($sear_res->ID, 'modifications_details', true) ?></p>
											<p><?php _e('Delivery Date', 'speedy') ?><br>
												<?php echo date('d M', strtotime(get_post_meta($sear_res->ID, 'delivery_date', true)));  ?></p>
									</div>
									<div class="col-xs-4">
										<?php $proj_files=get_post_meta($sear_res->ID, 'project_files', true);
										$image_available=false;
										if(!empty($proj_files)){
										foreach($proj_files as $proj_file){ ?>
										<?php 
										$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
										if ( $image_attributes ) : ?>
											<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
										<?php  $image_available=true; break; ?>
										<?php endif; ?>
										<?php } 
										} //end if
										?>
										<?php if($image_available==false){ ?>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						</a>			
					</div>
					<?php } //end if. ?>
					<?php } //end while
						} ?>	
				<div class="col-md-6">
				<!--delivered order seperations-->
				<?php
				// The Query
				$args = array(
					'post_type' => 'service-orders',
					'posts_per_page'=>-1,
					"post_status"=>'any',
					'meta_key'   => 'delivered_date',
					'orderby'    => 'meta_value',
					'order'      => 'DESC',
					'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_status',
									'value'    => 'revised',
									'compare' => '=',
								),
								
								array(
									'key'     => 'order_type',
									'value'   => 'seperation',
									'compare' => '=',
								),
																
							),
					
					);
				$vectorizing_orders = new WP_Query( $args );

				if ( $vectorizing_orders->have_posts() ) {
				
					// The Loop
					while ( $vectorizing_orders->have_posts() ) {
						$vectorizing_orders->the_post(); ?>
					<a href="<?php echo admin_url() ?>?page=single-order&order_id=<?php echo get_the_ID() ?>">
					<div class="each-order">
						<div class="each-order-header">
							<?php _e('Order', 'speedy') ?>#<?php echo get_the_ID() ?>
						</div>
						<div class="each-order-body">
							<div class="row">
								<div class="col-xs-8">
									<?php $author_id = get_post_field( 'post_author', $post_id ); ?>
									<p><?php echo get_user_meta($author_id, 'first_name', true); ?><br>
										<?php echo get_post_meta(get_the_ID(), 'modifications_details', true) ?></p>
										<p><?php _e('Delivery Date', 'speedy') ?><br>
											<?php echo date('d M', strtotime(get_post_meta(get_the_ID(), 'delivery_date', true)));  ?></p>
								</div>
								<div class="col-xs-4">
									<?php $proj_files=get_post_meta(get_the_ID(), 'project_files', true);
									$image_available=false;
									if(!empty($proj_files)){
									foreach($proj_files as $proj_file){ ?>
									<?php 
									$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
									if ( $image_attributes ) : ?>
										<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
									<?php  $image_available=true; break; ?>
									<?php endif; ?>
									<?php } 
									} //end if
									?>
									<?php if($image_available==false){ ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					</a>
					<?php } //end while
						} ?>
				</div>
			</div>
		</div>
	</div>
</div>