<div class="spacer20"></div>
<div class="container-fluid open-orders admin-open-orders">
<?php //include('inc/header.php'); ?>
<?php
if(isset($_GET['order_id'])){
		update_post_meta($_GET['order_id'], 'order_admin_mess_status', 'read');
		update_post_meta($_GET['order_id'], 'order_revision_mess', 'read');
		update_post_meta($_GET['order_id'], 'order_completed_mess', 'read');
		update_post_meta($_GET['order_id'], 'order_placed_mess', 'read');
	$order=get_post($_GET['order_id']);
	$user_id=get_current_user_id();
?>
	<?php 
	$order_status_text="Open";
	if(get_post_meta($_GET['order_id'], 'order_status', true)=='delivered'){ 
	$order_status_text="Delivered";
	}elseif(get_post_meta($_GET['order_id'], 'order_status', true)=='completed'){
	$order_status_text="Completed";	
	}elseif(get_post_meta($_GET['order_id'], 'order_status', true)=='revised'){
		$order_status_text="Revision";	
	}?> 
	<h3 class="open-order-heading"><span class="order-heading"><?php echo $order_status_text." "; _e('Orders', 'speedy') ?></span> <?php _e('Order', 'speedy') ?>#<?php echo $order->ID ?></h3>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th class="order-column"><?php _e('Order', 'speedy') ?>#<?php echo $order->ID ?></th>
					<th><?php _e('Order Type', 'speedy') ?></th>
					<th><?php _e('Order Date', 'speedy') ?></th>
					<th><?php _e('Delivery Date', 'speedy') ?></th>
					<th><?php _e('Order Status', 'speedy') ?></th>
					<th><?php _e('Chat', 'speedy') ?></th>
					<th class="last_column"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><?php echo get_post_meta($order->ID, 'order_type', true) ?></td>
					<td><?php echo  date("d M Y", strtotime(get_post_meta($order->ID, 'start_date', true))); ?></td>
					<td><?php echo date("d M Y", strtotime(get_post_meta($order->ID, 'delivery_date', true)));  ?></td>
					<td><?php echo get_post_meta($order->ID, 'order_status', true) ?></td>
					<td><?php  if ( gearside_is_user_online($order->post_author) ){  echo "online"; }else{  echo "offline"; } ?></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
		<div class="row">
			<div class="col-sm-2 detail-order">
				<?php $proj_files=get_post_meta($_GET['order_id'], 'project_files', true);
				$image_available=false;
				if(!empty($proj_files)){
				foreach($proj_files as $proj_file){ ?>
				<?php 
				$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
				if ( $image_attributes ) : ?>
					<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
				<?php  $image_available=true; break; ?>
				<?php endif; ?>
				<?php } 
				} //end if
				?>
				<?php if($image_available==false){ ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
				<?php } ?>
			</div>
			<div class="col-sm-10 order-details">
				<div> <span><?php _e('Colors', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'no_of_colors_wanted', true) ?></span></div>
				<div> <span><?php _e('Dimension', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'dimensions_of_artwork', true) ?></span></div>
				<div> <span><?php _e('Modifications', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'modifications_details', true) ?></span></div>
				
				<div class="notes"> 
					<h5><?php _e('Notes', 'speedy') ?></h5>
					<p><?php echo get_post_meta($order->ID, 'notes', true) ?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-2">
				<div class="order-files">
					<h3><?php _e('Order Files', 'speedy') ?>:</h3>
					<div class="message-attachment">
					<?php  
					$order_files=get_post_meta($order->ID, 'project_files', true); 
					if(!empty($order_files)){
					foreach($order_files as $order_file){ 
							$image_available=false;
							$image_attributes = wp_get_attachment_image_src( $order_file, 'thumbnail' );
						if ( $image_attributes ) : ?>
							<img src="<?php echo $image_attributes[0]; ?>" />
						<?php  $image_available=true; ?>
						<?php endif; ?>
						
							<?php if($image_available==false){ ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
							<?php } ?>
							<span><a href="<?php echo wp_get_attachment_url($order_file); ?> " target="_blank"><i class="fa fa-download"></i></a></span>
							
					<?php }	} else{ _e('No file attached', 'speedy'); }?>
					</div>
				</div>
				<div class="conversation">
					<h3><?php _e('Conversation', 'speedy') ?>:</h3>
					<?php
					
					// The Query
					$args = array(
						'post_type' => 'customers-messages',
						'posts_per_page'=>-1,
						"post_status"=>'any',
						'orderby' => 'ID',
						'order'   => 'ASC',
						'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_id',
									'value'    => $_GET['order_id'],
									'compare' => '=',
								),
								array(
								'relation' => 'OR',
								array(
									'key'     => 'sender',
									'value'   => $user_id,
									'compare' => '=',
								),
								array(
									'key'     => 'reciver',
									'value'   => $user_id,
									'compare' => '=',
								),
								),
								
								
							),
						);
					$messages_query = new WP_Query( $args );
					
					if ( $messages_query->have_posts() ) {
					?>
					<?php
						
						// The Loop 
						while ( $messages_query->have_posts() ) {
							$messages_query->the_post(); ?>
						<div class="conversation_message">
						 
						<?php if(get_post_meta(get_the_ID(), 'message_type', true)=='revision'){ ?>
							<div class="text-center">
							<span class="black_button"><?php _e('Revision Request', 'speedy') ?></span>
							</div>
							<div class="text-center revision">
							<b><?php echo strip_tags(get_the_content()) ?></b>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<?php $proj_files=get_post_meta(get_the_ID(), 'message_attach', true);
							$image_available=false;
							$extension = end(explode(".", $proj_files));
							$allowed_files=array('png', 'jpg', 'gif', 'jpeg', 'svg');
							if(in_array($extension, $allowed_files)){?>
								<img src="<?php echo $proj_files; ?>" />
							<?php  $image_available=true; ?>
							<?php  
							} //end if
							?>
							<?php if($image_available==false){ ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
							<?php } ?>
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
							</div>
							<?php } ?>
						<?php }else{ ?> 
							<?php if(get_the_author_ID()!=$user_id){ 
							echo get_user_meta(get_the_author_ID(), "first_name", true);
							}else{
								echo "Designer";
							} ?>:<?php echo strip_tags(get_the_content()) ?>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							
							
							<?php $proj_files=get_post_meta(get_the_ID(), 'message_attach', true);
							$image_available=false;
							$extension = end(explode(".", $proj_files));
							$allowed_files=array('png', 'jpg', 'gif', 'jpeg', 'svg');
							if(in_array($extension, $allowed_files)){?>
								<img src="<?php echo $proj_files; ?>" />
							<?php  $image_available=true; ?>
							<?php  
							} //end if
							?>
							<?php if($image_available==false){ ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
							<?php } ?>
							
							
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
						<?php } ?>
						<?php }?>
						
						
						</div>
							
					<?php }
					} 	?>
							
				</div>
				<div class="conv-message-box">
				<form action="" method="post" enctype="multipart/form-data" class="send-message-form">
					<textarea name="requestMessage" id="requestMessage" required></textarea>
					<img src="<?php echo get_stylesheet_directory_uri().'/images/loader.gif' ?>" id="loader" style="display:none; width:15px; width: 15px; position: absolute; right: -5px; z-index: 555;" />
					<div class="buttons">
					
						<input type="file" name="attachment" id="attachment"  class="inputfile" data-multiple-caption="{count} files selected">
						<label id="file-text" style="padding: 0; display: inline; border: 0;"><span></span></label>
						<label id="#attach" for="attachment"><i class="fas fa-paperclip"></i>
						</label>
						<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
						<input type="hidden" name="buyer_id" id="buyer_id" value="<?php echo $order->post_author; ?>">
						<input type="hidden" name="user_name" id="user_name" value="<?php _e('Designer', 'speedy') ?>">
						<button type="submit" name="send_message" id="send_message_button"><i class="fas fa-paper-plane"></i></button>
					</div>
				</form>
				</div>
				<?php if(get_post_meta($_GET['order_id'], 'order_status', true)=='delivered'){ ?> 
				<div class="spacer10"></div>
				<div class="order-delivered text-center">
					<div class="order-delivered-text">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-complete.png">
						<h3><?php _e('YOUR ORDER IS READY!!!', 'Speedy') ?></h3>
						<p>Please download and review all files.<br>
							If everything looks right, please mark<br>
							this order as completed.<br>
							"Order will be marked as completed if there<br>
							was no revision to it in 48 after after the delivery"</p>
							<div class="spacer10"></div>
							<a href="<?php echo wp_get_attachment_url(get_post_meta($_GET['order_id'], 'delivered_files', true)) ?>"  download>
								<button class="black_button"><?php _e('DOWNLOAD FILES', 'speedy') ?></button>
							</a>
							
					</div>
				</div>
				<div class="deliver-order-div"> 
					<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="deliver-orders">
						<div class="buttons">
							<input type="file" name="attachment_deliver[]" id="attachment_deliver" multiple=multiple />
							</label>
							<input type="hidden" name="action" value="deliver_order">
							<input type="hidden" name="deliver_order_id" id="deliver_order_id" value="<?php echo $_GET['order_id'] ?>">
							<button type="submit" name="deliver_button" id="deliver_button"><?php _e('DELIVER AGAIN', 'speedy') ?></button>
						</div>
					</form>
				</div>
				<?php }elseif(get_post_meta($_GET['order_id'], 'order_status', true)=='completed'){?> 
					
				<div class="spacer10"></div>
				<div class="order-delivered text-center">
					<div class="order-delivered-text">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-complete.png">
						<h3><?php _e('CONGRATULATIONS THIS ORDER HAS', 'Speedy') ?><br><?php _e('BEEN MARKED AS COMPLETED BY ', 'Speedy'); echo strtoupper(get_user_meta($order->post_author, 'first_name', true)); ?></h3>
						
					</div>
				</div>
					
				<?php }else{ ?>
				<div class="deliver-order-div"> 
					<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="deliver-orders">
						<div class="buttons">
							<input type="file" name="attachment_deliver[]" id="attachment_deliver" multiple=multiple />
							</label>
							<input type="hidden" name="action" value="deliver_order">
							<input type="hidden" name="deliver_order_id" id="deliver_order_id" value="<?php echo $_GET['order_id'] ?>">
							<button type="submit" name="deliver_button" id="deliver_button"><?php _e('DELIVER ORDER', 'speedy') ?></button>
						</div>
					</form>
				</div>
				<?php } ?>
			</div>
		</div>
	
<?php  } ?>
</div>

<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
</script>