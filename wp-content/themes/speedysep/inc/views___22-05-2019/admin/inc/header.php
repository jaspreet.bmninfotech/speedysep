<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!--admin css-->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/speedy_admin.css' ?>">
<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<?php twentysixteen_the_custom_logo(); ?>
		</div>
		<!-- Top Menu Items -->
		<!-- Top Menu Items -->
		<ul class="nav navbar-right top-nav"> 
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Notifications"><i class="fas fa-bell"></i><span class="notif-count" style="display:none"></span></a>
				<ul class="dropdown-menu notification-dropdown">
					<h3><i class="fas fa-bell"></i> Notifications</h3>
				
				</ul>
			</li>					
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user-tie"></i></a>
				<ul class="dropdown-menu user-dropdown">
					<li><a href="<?php echo site_url(); ?>/wp-admin/profile.php"><i class="fa fa-fw fa-user"></i> Edit Profile</a></li>
					<li class="divider"></li>
					<li><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
				</ul>
			</li>
		</ul>
		
	</nav>
	
