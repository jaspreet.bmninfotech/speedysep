<div class="spacer20"></div>
<div class="container-fluid ">
	<?php //if view customer 
		if(isset($_GET['customer']) and $_GET['customer']!=''){ 
		//get user data
		$user=get_userdata($_GET['customer']);
		//print_r($user);
		?>
		<div class="customer-details">
			<?php if(isset($_GET['view']) and $_GET['view']=='edit'){ ?> 
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Name', 'speedy')  ?></div>
				<div class="col-sm-4"><input type="text" name="name" value="<?php echo get_user_meta($user->ID, 'first_name', true) ?>"></div>
				<div class="col-sm-5"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Email', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="email" name="email" value="<?php echo $user->user_email ?>" ></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Phone', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="phone" value="<?php echo get_user_meta($user->ID, 'phone_no', true) ?>" ></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Address', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="address" value="<?php echo get_user_meta($user->ID, 'address', true) ?>" ></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('State', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="state" value="<?php echo get_user_meta($user->ID, 'state', true) ?>" ></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Zip Code', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" name="zip" value="<?php echo get_user_meta($user->ID, 'billing_zipcode', true) ?>"></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"></div>
				<input type="hidden" name="user_id" value="<?php echo $user->ID ?>">
				<input type="hidden" name="action" value="update_user_info">
				<div class="col-sm-5"><button type="submit" name="submit" class="black_button" >Update Profile</button></div>
				<div class="col-sm-4"></div>
			</div>
			</form>
			<?php }else{ ?>
			<div class="row">
				<div class="col-sm-3 text-right"><i class="fa fa-user"></i></div>
				<div class="col-sm-4"><input type="text" value="<?php echo get_user_meta($user->ID, 'first_name', true) ?>" class="customer-name" disabled></div>
				<div class="col-sm-5"> <a href="?page=speedy-customer-list&customer=<?php echo $user->ID ?>&view=edit"><button class="black_button profile_edit_btn">Edit</button></a></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Email', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo $user->user_email ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Phone', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo get_user_meta($user->ID, 'phone_no', true) ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Address', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo get_user_meta($user->ID, 'address', true) ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('State', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo get_user_meta($user->ID, 'state', true) ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Zip Code', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo get_user_meta($user->ID, 'billing_zipcode', true) ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			
			<?php 
			$total_orders=0;
			$total_spend=0;
			$vect_orders=0;
			$sep_orders=0;
			global $wpdb;
			$user_orders = $wpdb->get_results( 
												"
												SELECT ID 
												FROM $wpdb->posts
												WHERE  post_type = 'service-orders' AND post_author = ".$user->ID."
												"
											);

			foreach ( $user_orders as $user_order ){ 
				$total_orders+=1;
				$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
				if(get_post_meta($user_order->ID, 'order_type', true)=='vectorizing'){
					$vect_orders+=1;
				}elseif(get_post_meta($user_order->ID, 'order_type', true)=='seperation'){
					$sep_orders+=1;
				}
			}
			$customer=retrive_customer(get_user_meta($user->ID, 'customer_stripe_id', true));  
			//$customer_default_card=array();
			//echo $customer->default_source;
			//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
			if($customer->default_source){
				$customer_default_card=retrive_customer_card_stripe($customer->default_source, get_user_meta($user->ID, 'customer_stripe_id', true));
				//print_r($customer_default_card);
			}
			?>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Credit Card No.', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="**** **** **** <?php echo $customer_default_card->last4 ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Expiry Date', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo $customer_default_card->exp_month ?>/<?php echo substr($customer_default_card->exp_year, -2); ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('No. of Orders placed', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo $total_orders ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Vectorizing Ordres', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo $vect_orders ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div><div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Separation Orders', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo $sep_orders ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div><div class="row">
				<div class="col-sm-3 text-right"><?php  _e('Total Amount Paid', 'speedy')  ?></div>
				<div class="col-sm-5"><input type="text" value="<?php echo  SITE_CURRENCY_SYMBOL.$total_spend ?>" disabled></div>
				<div class="col-sm-4"></div>
			</div>
			<?php } ?>
		</div>
		<?php }else{ ?>
	<div class="sales-reports">
	<h3><?php _e('Customer List', 'speedy') ?></h3>

	<table class="table">
		<thead>
			<tr>
				<th><?php  _e('Name', 'speedy') ?></th>
				<th><?php  _e('Company', 'speedy') ?></th>
				<th><?php  _e('Location', 'speedy') ?></th>
				<th><?php  _e('Email', 'speedy') ?></th>
				<th><?php  _e('Phone No.', 'speedy') ?></th>
			</tr>
		</thead>
		<tbody>
		<?php			
			$count=1;
			$args = array(
				'role' => 'subscriber', 
			);
			$users= get_users($args);
			//$users = get_users('orderby=nicename&role=subscriber');
			foreach ($users as $user) {
			
			?>
			<tr>
				<td><?php echo $count.".<a href='?page=speedy-customer-list&customer=".$user->ID."'>" .get_user_meta($user->ID, 'first_name', true)."</a>" ?></td>
				<td><?php echo get_user_meta($user->ID, 'company_name', true) ?></td>
				<td><?php echo get_user_meta($user->ID, 'address', true) ?></td>
				<td><?php echo $user->user_email ?></td>
				<td><?php echo get_user_meta($user->ID, 'phone_no', true) ?></td>
			</tr>
			<?php $count++; } ?>
		</tbody>
	</table>
	</div>
		<?php } ?>
</div>
