<div class="registration-page login-page">
	<div class="text-center form-heading"> 
		<h2><?php _e('LOGIN', 'speedy') ?></h2>
	</div>
	<form action="<?php //echo admin_url('admin-ajax.php') ?>" method="post" class="login-from">
		<div class="input-group form-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
			<input type="email" class="form-control" id="login_email" name="email" aria-describedby="emailHelp" placeholder="Email" required>
		</div>
		<div class="input-group form-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fas fa-lock"></i></span>
			<input type="password" class="form-control" id="login_password" name="password" placeholder="Password" required>
		</div>
		
		<div class="checkbox-main">
			<div class="round round-first">
				<input type="checkbox" id="checkbox" />
				<label for="checkbox"></label> <span class="round-checkbox-label"><?php _e('Remember Me', 'speedy') ?></span>
			</div>
		</div>
	  
		<div class="submit-div text-center">
			<button type="submit" id="submit" name="login" class="btn btn-primary"><?php _e('LOGIN', 'speedy') ?></button>
		</div>
	</form>
	<div class="registration-message"><p><?php _e('Don`t have an account? ', 'speedy') ?><a href="#" title="<?php _e('Signup', 'speedy') ?>" id="speedySignup" data-toggle="modal" data-target="#registrationModal"><?php _e('Signup', 'speedy') ?></a></p></div>
</div>