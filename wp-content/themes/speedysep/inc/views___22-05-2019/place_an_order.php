<div class="spacer35"></div>
<?php if((! isset($_GET['type']) ||  $_GET['type']=='') and  $_GET['order']==''){ ?>
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="gray-bg place-an-order">
			<h2><?php _e('Place Your Order Here', 'speedy') ?></h2>
			<a href="?type=vectorizing"><?php _e('Vectorizing', 'speedy') ?></a>
			<a href="?type=seperation"><?php _e('Separation', 'speedy') ?></a>
		</div>
	</div>
</div>
<?php } ?>

<!-- if $_GET['vectorizing'] or seperation or set -->
<?php if(isset($_GET['type']) and  $_GET['type']!='' and !isset($_GET['order'])){ ?>
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="place-an-order-form">
			<span class="type_button"><?php echo $_GET['type'] ?></span>
			<div class="spacer35"></div>
			<div class="text-left">
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderForm">
			  
			  <div class="form-group">
				<input type="file" name="project_files[]" multiple="multiple" id="file" class="inputfile" data-multiple-caption="{count} files selected">
				<label for="file"><i class="fas fa-cloud-upload-alt"></i><span>Choose files&hellip;</span></label>
				</div>
			  
			  <div class="form-group">
				<label for="color_no"><?php _e('How many colors you want in the art work?', 'speedy') ?></label>
				<input type="text" class="form-control"  name="color_no" id="color_no" placeholder="<?php _e('Same color or you have any specific colors write us here.', 'speedy') ?>"  required>
			  </div>
			  
			  <div class="form-group dimension-type">
				<label for="dimension" style="display:block"><?php _e('What is the dimension of the artwork?', 'speedy') ?></label>
				
				<label class="any-label" for="any"><input type="checkbox" id="any" name="dimension_any" value="any"> Any</label> or 
				<input type="text" class="dimension-input"  name="dimension" id="dimension">
			  </div>
			  
			  <div class="form-group">
				<label for="modifications"><?php _e('What are the modifications you want in the artwork?', 'speedy') ?></label>
				
				<input type="text" class="form-control"  name="modifications" id="modifications" placeholder="<?php _e('Add modifications details here', 'speedy') ?>" required>
			  </div>
			  
			   <div class="form-group">
				<label for="notes"><?php _e('Notes', 'speedy') ?></label>
				
				<textarea  class="form-control"  name="notes" id="notes" placeholder="<?php _e('Write us a note here regarding the image/design', 'speedy') ?>" rows="3" ></textarea>
			  </div>
			  
			  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="create_order">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
			  <button type="submit" class="btn type_button" name="create_order">Next</button>
			</form>
			</div>

		</div>
	</div>
</div>
<?php }elseif(isset($_GET['order']) and  $_GET['order']!='' and $_GET['step']==3){ ?> 
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="place-an-order-form vect-pricing step3 <?php echo $_GET['type'] ?>">
			<span class="type_button"><?php echo $_GET['type'] ?></span>
			<div class="spacer20"></div>
			<div class = "table-responsive">
				<table>
				<?php if($_GET['type']=='seperation'){ 
					$page_id=294;
				}else{ $page_id=414; } ?>
					<thead>
						<th></th>
						<th><?php echo get_field('basic_plan_text', $page_id)?></th>
						<th><?php echo get_field('standard_plan_text', $page_id)?></th>
						<th><?php echo get_field('premium_plan_text', $page_id)?></th>
					</thead>
					<tbody>
						<tr class="even">
							<td><?php echo get_field('choose_plan_text', $page_id)?></td>
							<td>
								<span class="plan-price"><?php echo SITE_CURRENCY_SYMBOL."".get_field('basic_plan_price', $page_id)?></span>
								<a href="#" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="basic"><?php echo get_field('buy_now_text', $page_id)?></a>
							</td>
							<td>
								<span class="plan-price"><?php echo  SITE_CURRENCY_SYMBOL."".get_field('standard_plan_price', $page_id)?></span>
								<a href="#" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="standard"><?php echo get_field('buy_now_text', $page_id)?></a>
							</td>
							<td>
								<span class="plan-price"><?php echo  SITE_CURRENCY_SYMBOL."".get_field('premium_plan_price', $page_id)?></span>
								<a href="" title="<?php echo get_field('buy_now_text', $page_id)?>" class="buy-now <?php echo $_GET['type'] ?>" id="premium"><?php echo get_field('buy_now_text', $page_id)?></a>
							</td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('illustrator_ai_text', $page_id)?></td>
							<td><?php if(get_field('basic_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="even">
							<td><?php echo get_field('transparent_png_text', $page_id)?></td>
							<td><?php if(get_field('basic_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="odd">
							<td><?php echo get_field('eps_text', $page_id)?></td>
							<td><?php if(get_field('basic_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even">
							<td><?php echo get_field('other_formats_text', $page_id)?></td>
							<td><?php if(get_field('basic_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="odd">
							<td><?php echo get_field('concept_recreation_text', $page_id)?></td>
							<td><?php if(get_field('basic_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even">
							<td><?php echo get_field('modifications_text', $page_id)?></td>
							<td><?php echo get_field('basic_modifications', $page_id)?></td>
							<td><?php echo get_field('standard_modifications', $page_id)?></td>
							<td><?php echo get_field('premium_modifications', $page_id)?></td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('mockup_text', $page_id)?></td>
							<td><?php if(get_field('basic_mockup', $page_id)!=''){   echo get_field('basic_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('standard_mockup', $page_id)!=''){   echo get_field('standard_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							<td><?php if(get_field('premium_mockup', $page_id)!=''){   echo get_field('premium_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even details">
							<td><?php echo get_field('details_text', $page_id)?></td>
							<td>
								<?php echo get_field('basic_details', $page_id)?>
							</td>
							<td>
								<?php echo get_field('standard_details', $page_id)?>
							</td>
							<td>
								<?php echo get_field('premium_details', $page_id)?>
							</td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('delivery_text', $page_id)?></td>
							<td><?php echo get_field('basic_delivery_time', $page_id)?></td>
							<td><?php echo get_field('standard_delivery_time', $page_id)?></td>
							<td><?php echo get_field('premium_delivery_time', $page_id)?></td>
						</tr>
						
					</tbody>
				</table>
				</div>
				<div class="spacer10"></div>
				<div class = "table-responsive">
				<table class="pricing-sub-table">
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('extra_fast_delivery_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="extraFast" name="extraFast"> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('extra_fast_delivery_price', $page_id)?></td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('additional_formats_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="additionalFormats" name="additionalFormats"> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('additional_formats_price', $page_id)?></td>
					</tr>
					<tr class="odd">
						<td colspan="3" class="left"><?php echo get_field('concept_recreation_text', $page_id)?></td>
						<td class="right"><input type="checkbox" id="conceptRecreation" name="conceptRecreation"> <?php echo SITE_CURRENCY_SYMBOL ?><?php echo get_field('concept_recreation_price', $page_id)?></td>
					</tr>
				</table>
				
				</div>
				<div class="spacer10"></div>
				<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderForm">
				  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
				  <?php	global $post;
						$post_slug=$post->post_name; ?>
						
					<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
					<input type="hidden" name="action" value="create_order_pricing">
					<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
					<input type="hidden" name="package" id="package">
					<input type="hidden" name="extra_fast" id="extra_fast">
					<input type="hidden" name="page_id" id="page_id" value="<?php echo $page_id; ?>">
					<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order']; ?>">
					<input type="hidden" name="additional_formats" id="additional_formats">
					<input type="hidden" name="concept_recreation" id="concept_recreation">
					<input type="hidden" name="total_amount" id="total_amount">
				  <button type="submit" class="btn type_button" name="create_order" id="setPricingPlan" disabled>Next</button>
				</form>
			
		</div>
	</div>
</div>
<?php }elseif($_GET['step']==4){ ?>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 card-auth-form">
		<?php 
		//delete a card
		if(isset($_GET['delete_card']) and $_GET['card']!=''){
			$result=delete_customer_card_stripe($_GET['card']);
			if($result->deleted==true){
				echo "<div class='alert alert-success'>"; _e('Card deleted successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not deleted, please try again', 'speedy'); echo "</div>";
			}
		} 
		//change default card
		if(isset($_GET['change_default']) and $_GET['card']!=''){
			$result=change_customer_default_card_stripe($_GET['card']);
			if($result){
				echo "<div class='alert alert-success'>"; _e('Card changed successfully', 'speedy'); echo "</div>";
			}else{
				echo "<div class='alert alert-danger'>"; _e('Card not changed, please try again', 'speedy'); echo "</div>";
			}
		} 
		
		//check if user has atleast one default card
		$customer=retrive_customer(get_user_meta(get_current_user_id(), 'customer_stripe_id', true));
		//echo "<pre>"; print_r($customer); echo "</pre>";	
		?>
		<?php if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)=='' || isset($_GET['add_card']) || $customer->default_source==''){ ?>
			<div class="text-center">
			<h2><?php _e('Credit Card Payment Authorization Form', 'speedy') ?></h2>
			</div>
			<div class="step4 gray-bg">
				<h2><?php _e('General Information', 'speedy') ?></h2>
				<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" id="payment-form" class="paymentAuthForm">
				  <div class="row">
				  <?php $user_id=get_current_user_id() ?>
					  <div class="form-group col-sm-6">
						<label for="business_name"><?php _e('Business Name', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="business_name" id="business_name" value="<?php echo get_user_meta($user_id, 'business_name', true) ?>" placeholder="<?php _e('Business Name', 'speedy') ?>"  required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="billing_email"><?php _e('Email', 'speedy') ?><span class="required">*</span></label>
						
						<input type="email" class="form-control"  name="billing_email" value="<?php echo get_user_meta($user_id, 'billing_email', true) ?>" id="billing_email" placeholder="<?php _e('Email', 'speedy') ?>" required>
					  </div>
				  </div>
				  
				  <div class="row">
					  <div class="form-group col-sm-6">
						<label for="phone_no"><?php _e('Phone Number', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="phone_no" id="phone_no" placeholder="<?php _e('Phone Number', 'speedy') ?>"   value="<?php echo get_user_meta($user_id, 'phone_no', true) ?>" required>
					  </div>
				  </div>
				  <div class="spacer20"></div>
				<h2><?php _e('Billing Information', 'speedy') ?></h2>
				<div class="row">
					  <div class="form-group col-sm-6">
						<label for="billing_address"><?php _e('Billing Address', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_address" id="billing_address"  value="<?php echo get_user_meta($user_id, 'billing_address', true) ?>" placeholder="<?php _e('Billing Address', 'speedy') ?>"  required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="city"><?php _e('City', 'speedy') ?><span class="required">*</span></label>
						
						<input type="text" class="form-control"  name="city" id="city" placeholder="<?php _e('City', 'speedy') ?>" value="<?php echo get_user_meta($user_id, 'city', true) ?>" required>
					  </div>
				  </div>
				  
				  <div class="row">
					  <div class="form-group col-sm-6">
						<label for="state"><?php _e('State', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="state" id="state" placeholder="<?php _e('State', 'speedy') ?>"  value="<?php echo get_user_meta($user_id, 'state', true) ?>" required>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="zipcode"><?php _e('Billing Zipcode', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="billing_zipcode" id="zipcode"  value="<?php echo get_user_meta($user_id, 'billing_zipcode', true) ?>"  placeholder="<?php _e('Billing Zipcode', 'speedy') ?>"  required>
					  </div>
				  </div>
				  
				  <div class="spacer20"></div>
				<h2><?php _e('Credit Card Details', 'speedy') ?></h2>
				 <div class="spacer10"></div>
				<div class="row visa_mastercard_div">
					  <div class="form-group col-sm-6">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/visa_martercard.png">
					  </div>
					  
					  <div class="form-group col-sm-6">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/secure-checkout.png">
					  </div>
				  </div>
				  
				<div class="row">
					  <div class="form-group col-sm-6">
						<label for="card_type"><?php _e('Credit Card Type', 'speedy') ?><span class="required">*</span></label>
						<div>
						<input type="radio"  name="card_type" id="card_type_visa"  value="visa" required> <?php _e('Visa', 'speedy') ?>
						<input type="radio"  name="card_type" id="card_type_master"  value="master" required> <?php _e('Master Card', 'speedy') ?>
						</div>
					  </div>
					  
					  <div class="form-group col-sm-6">
						<script src="https://js.stripe.com/v3/"></script>

						  <div class="form-row">
							<label for="card-element">
							  <?php _e('Credit Card Number', 'speedy') ?><span class="required">*</span>
							</label>
							<div id="card-element">
							  <!-- A Stripe Element will be inserted here. -->
							</div>

							<!-- Used to display form errors. -->
							<div id="card-errors" role="alert"></div>
						  </div>
					  </div>
				</div>
				
				<div class="row">
					  <div class="form-group col-sm-6">
						<label for="card_name"><?php _e('Customer Name On Card', 'speedy') ?><span class="required">*</span></label>
						<input type="text" id="card_name" name="card_name" class="form-control field" placeholder="<?php _e('Customer Name On Card', 'speedy') ?>" required />
					  </div>
					  
					  <div class="form-group col-sm-6">
						<label for="resale_tax_id"><?php _e('Resale TAX ID', 'speedy') ?><span class="required">*</span></label>
						<input type="text" class="form-control"  name="resale_tax_id" id="resale_tax_id" placeholder="<?php _e('Resale TAX ID', 'speedy') ?>"  required>
					  </div>
				</div>
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_1">
						<input type="checkbox" id="accept_terms_1" name="accept_terms_1" required />
						<p class="term_1">
						I hereby authorize        <?php echo bloginfo('name') ?>              - his offers, agents, managers affiliates, employees, contractors, and
						representatives to charge my credit card for the stated U.S Dollar Amount effective this date. Client
						agrees that        <?php echo bloginfo('name') ?>            will keep this credit card on file for the year of 2019 for all future purchases,
						Client agrees that this engagement is for smoking accessories or other souvenirs and that LLC has the
						right to refund or deny any payment at the companies solde discretion. Client irrevocably waives
						any right to dispute this/these credit card charge(s).
						</p>
						</label>
					  </div>
				</div>
				<div class="spacer20"></div>
				<div class="row">
					  <div class="form-group col-sm-12">
						<label for="accept_terms_2">
						<input type="checkbox" id="accept_terms_2" name="accept_terms_2" required />
						<p class="term_1">
						I hereby authorize  <?php echo bloginfo('name') ?> to add %3 to each credit card transaction
						</p>
						</label>
					  </div>
				</div>
				<div class="spacer20"></div>
			  <?php wp_nonce_field( 'create_order', 'create_order_nonce' ); ?>
			  <?php	global $post;
					$post_slug=$post->post_name; ?>
					
				<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
				<input type="hidden" name="action" value="payment_form">
				<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
				<input type="hidden" name="order_id" value="<?php echo $_GET['order'] ?>">
			  <button type="submit" class="btn user-submit-btn" name="auth_card"><?php _e('Submit', 'speedy') ?></button>
			</form>
			</div>
		<?php }else{ ?> 
			<div class="user_cards">
				<h2><i class="fas fa-calendar-alt"></i> <?php _e('Review your new automatic payment settings', 'speedy') ?></h2>
				<div class="cards-inner">
					<div class="row">
						<?php  $customer=retrive_customer(get_user_meta(get_current_user_id(), 'customer_stripe_id', true));  
						//echo  "<pre>".retrive_customer_card_stripe($customer->default_source)."</pre>"; 
						if($customer->default_source){
						$customer_default_card=retrive_customer_card_stripe($customer->default_source);
						//print_r($customer_default_card->id);
						?>
						<div class="col-md-7">
							<div class="default-cards">
								<h4><?php _e('Your bill will be charged to this credit card.', 'speedy') ?></h4>
								<p><?php _e('Card number', 'speedy') ?><br>
									<strong>**** **** **** <?php echo $customer_default_card->last4 ?></strong></p>
								<p><?php _e('Cardholder`s name', 'speedy') ?><br>
									<strong><?php echo $customer_default_card->name ?></strong></p>
							</div>
						</div>
						<div class="col-md-5">
							
							<?php foreach($customer->sources->data as $customer_card){ ?>
							<div class="each-card-inner">
								<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&delete_card=1&card=<?php echo  $customer_card['id'] ?>" onclick="return confirm('Are you sure to delete this card?')" title="<?php _e('Delete Card', 'speedy') ?>" class="card-delete"><i class="fa fa-remove"></i></a>
							
								<p><?php _e('Card number', 'speedy') ?><br>
									<strong>**** **** **** <?php echo $customer_card['last4'] ?></strong></p>
								<p class="card-expiry"><?php _e('Expires', 'speedy') ?><br>
									<strong><?php echo $customer_card['exp_month'] ?>/<?php echo substr($customer_card['exp_year'], -2); ?></strong></p>
								<div class="card-holder-name">
									<p><?php _e('Cardholder`s name', 'speedy') ?><br>
									<strong><?php if($customer_card['name']!=''){ echo $customer_card['name']; }else{ echo "No name"; } ?></strong></p>
									<?php if($customer_default_card->id != $customer_card['id']){ ?>
									<p class="charge-card"><a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&change_default=1&card=<?php echo  $customer_card['id'] ?>"><?php _e('charge', 'speedy') ?></a></p>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php }else{ ?>
						<p><?php _e('No Payment method available.', 'speedy') ?> </p>
						<?php } ?>
					</div>
				</div>
				
				<div class="card-footer">
					<a href="?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=4&add_card=1"><button><?php _e('Add new card', 'speedy') ?></button></a>
					<a href="<?php echo site_url() ?>/checkout/?type=<?php echo $_GET['type'] ?>&order=<?php echo $_GET['order'] ?>&step=5"><button><?php _e('Checkout', 'speedy') ?></button></a>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
<style>
/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script>
	// Create a Stripe client.
var stripe = Stripe('pk_test_FPct9RebVfV32N2Ojpmh0Z1D');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
	 var card_name = document.getElementById('card_name').value;
  stripe.createToken(card, {name: card_name}).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
<?php } ?>

<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
</script>