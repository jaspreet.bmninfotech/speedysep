<div class="spacer35"></div>
<div class="open-orders">
	
	

<?php
$user_id=get_current_user_id();
if(!isset($_GET['order_id'])){

// The Query
$args = array(
	'post_type' => 'service-orders',
	'posts_per_page'=>-1,
	"post_status"=>'any',
	"author"=>$user_id,
	'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'order_status',
								'value'    => 'delivered',
								'compare' => '=',
							)					
							
						),
	);
$buyers_requests_query = new WP_Query( $args );

if ( $buyers_requests_query->have_posts() ) {
?>

<h3><?php _e('Delivered Orders', 'speedy') ?></h3>
<?php
	$count=1;
	// The Loop
	while ( $buyers_requests_query->have_posts() ) {
		$buyers_requests_query->the_post(); ?>
		<?php if($count==1 || $count-1==6){ ?> 
		<div class="row">
		<?php }?>
			<div class="col-sm-2">
				<a href="?order_id=<?php echo get_the_ID() ?>">
				<div class="order-inner">
					<p><?php _e('Order', 'speedy') ?> #<?php echo get_the_ID() ?></p>
					<?php $proj_files=get_post_meta(get_the_ID(), 'project_files', true);
						$image_available=false;
						if(!empty($proj_files)){
						foreach($proj_files as $proj_file){ ?>
						<?php 
						$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
						if ( $image_attributes ) : ?>
							<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
						<?php  $image_available=true; break; ?>
						<?php endif; ?>
						<?php } 
						} //end if
						?>
						<?php if($image_available==false){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
						<?php } ?>
				</div>
				</a>
			</div>
		<?php if($count==6){ ?> 
		</div>
		<?php }?>
	<?php 
	$count++;
	}

?>
<?php 
	wp_reset_postdata();
}else{
	 _e('NO delivered Order available.', 'speedy'); 
}
}elseif(isset($_GET['order_id'])){ 
	$order=get_post($_GET['order_id']);
	update_post_meta($_GET['order_id'], 'order_delivered_mess', 'read');
?>
	<h3><?php _e('Delivered Orders', 'speedy') ?></h3>
	<?php if($order->post_author==$user_id){
		if(get_post_meta($_GET['order_id'], 'order_status', true)=='delivered' || get_post_meta($_GET['order_id'], 'order_status', true)=='completed'){ ?>
			<div class="order-delivered text-center">
				<div class="order-no">
				<span>
				<?php _e('Order', 'speedy'); ?>#<?php echo $_GET['order_id'] ?>
				</span>
				</div>
				<div class="spacer35"></div>
				<div class="spacer10"></div>
				<div class="order-delivered-text">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-complete.png">
					<h3><?php _e('YOUR ORDER IS READY!!!', 'Speedy') ?></h3>
					<p>Please download and review all files.<br>
						If everything looks right, please mark<br>
						this order as completed.<br>
						"Order will be marked as completed if there<br>
						was no revision to it in 48 after after the delivery"</p>
						<div class="spacer10"></div>
						<a href="<?php echo wp_get_attachment_url(get_post_meta($_GET['order_id'], 'delivered_files', true)) ?>"  download>
							<button class="black_button"><?php _e('DOWNLOAD FILES', 'speedy') ?></button>
						</a>
						<div class="spacer20"></div>
						<?php if(get_post_meta($_GET['order_id'], 'order_status', true)!='completed'){ ?>
						<div class="completed-deliverd-buttons">
							<a href="<?php echo site_url() ?>/request-a-revision/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Request a Revision', 'speedy') ?></button> 
							</a>  <?php _e('OR', 'speedy') ?>
							<a href="<?php echo site_url() ?>/completed/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Mark as Completed', 'speedy') ?></button>
							</a>
						</div>
						<?php } ?>
				</div>
		<?php }else{
		?>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th class="order-column"><?php _e('Order', 'speedy') ?>#<?php echo $order->ID ?></th>
					<th><?php _e('Order Type', 'speedy') ?></th>
					<th><?php _e('Order Date', 'speedy') ?></th>
					<th><?php _e('Delivery Date', 'speedy') ?></th>
					<th><?php _e('Order Status', 'speedy') ?></th>
					<th><?php _e('Chat', 'speedy') ?></th>
					<th class="last_column"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><?php echo get_post_meta($order->ID, 'order_type', true) ?></td>
					<td><?php echo  date("d M Y", strtotime(get_post_meta($order->ID, 'start_date', true))); ?></td>
					<td><?php echo date("d M Y", strtotime(get_post_meta($order->ID, 'delivery_date', true)));  ?></td>
					<td><?php echo get_post_meta($order->ID, 'order_status', true) ?></td>
					<td><?php echo "online/offline"; ?></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
		<div class="row">
			<div class="col-sm-2 detail-order">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
			</div>
			<div class="col-sm-10 order-details">
				<div> <span><?php _e('Colors', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'no_of_colors_wanted', true) ?></span></div>
				<div> <span><?php _e('Dimension', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'dimensions_of_artwork', true) ?></span></div>
				<div> <span><?php _e('Modifications', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'modifications_details', true) ?></span></div>
				
				<div class="notes"> 
					<h5><?php _e('Notes', 'speedy') ?></h5>
					<p><?php echo get_post_meta($order->ID, 'notes', true) ?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-2">
				<div class="conversation">
					<h3><?php _e('Conversation', 'speedy') ?>:</h3>
					<?php
					
					// The Query
					$args = array(
						'post_type' => 'customers-messages',
						'posts_per_page'=>-1,
						"post_status"=>'any',
						'orderby' => 'ID',
						'order'   => 'ASC',
						'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_id',
									'value'    => $_GET['order_id'],
									'compare' => '=',
								),
								array(
								'relation' => 'OR',
								array(
									'key'     => 'sender',
									'value'   => $user_id,
									'compare' => '=',
								),
								array(
									'key'     => 'reciver',
									'value'   => $user_id,
									'compare' => '=',
								),
								),
								
								
							),
						);
					$messages_query = new WP_Query( $args );
					
					if ( $messages_query->have_posts() ) {
					?>
					<?php
						
						// The Loop 
						while ( $messages_query->have_posts() ) {
							$messages_query->the_post(); ?>
						<div class="conversation_message">
						 
						<?php if(get_post_meta(get_the_ID(), 'message_type', true)=='revision'){ ?>
							<div class="text-center">
							<span class="black_button"><?php _e('Revision Request', 'speedy') ?></span>
							</div>
							<div class="text-center revision">
							<b><?php echo strip_tags(get_the_content()) ?></b>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png"> 
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
							</div>
							<?php } ?>
						<?php }else{ ?> 
							<?php if(get_the_author_ID()==$user_id){ 
							echo get_user_meta($user_id, "first_name", true);
							}else{
								echo "Designer";
							} ?>:<?php echo strip_tags(get_the_content()) ?>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png"> 
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
						<?php } ?>
						<?php }?>
						
						
						</div>
							
					<?php }
					} 	?>
							
				</div>
				<div class="conv-message-box">
				<form action="" method="post" enctype="multipart/form-data" class="send-message-form">
					<textarea name="requestMessage" id="requestMessage" required></textarea>
					<div class="buttons">
						<label id="#attach"><i class="fas fa-paperclip"></i>
						<input type="file" name="attachment" id="attachment">
						</label>
						<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
						<input type="hidden" name="user_name" id="user_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
						<button type="submit" name="send_message" id="send_message_button"><i class="fas fa-paper-plane"></i></button>
					</div>
				</form>
				</div>
			</div>
		</div>
		<?php } //order not delivered ?>
	<?php }else{ ?> 
		<p><?php _e('No record found', 'speedy') ?></p>
	<?php }?>
<?php  } ?>
</div>