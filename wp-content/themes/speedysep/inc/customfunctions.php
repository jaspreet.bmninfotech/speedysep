<?php
	
/**
 * 
 */
class CustomFunctions
{
	public $wpdb;
	function __construct()
	{
		global $wpdb;
    	$this->wpdb = $wpdb;
		add_action( 'wp_ajax_change_plan_dropdown', array( $this, 'change_plan_dropdown' ) ); 
		add_action( 'wp_ajax_change_plan_to_free', array( $this, 'change_plan_to_free' ) ); 
		add_action( 'wp_ajax_nopriv_change_plan_to_free', array( $this, 'change_plan_to_free' ) ); 
		add_action( 'admin_membership_update_admin_membership', array( $this, 'update_admin_membership' ) ); 
		add_action( 'admin_post_nopriv_update_admin_membership', array( $this, 'update_admin_membership' ));


	}

	function showselectedPlanUser($userid){

	$customer = $this->wpdb->get_row("SELECT p.status,p.last_four,p.start_date,p.end_date, u.mem_ship_id, u.name, u.amount,  u.frequency, u.type
	FROM wpoc_user_memberships p
	INNER JOIN wpoc_memberships u ON p.type = u.mem_ship_id WHERE p.user_id = '".$userid."' Order By p.mem_id DESC LIMIT 1" );

	return $customer;	
	}

	//register user
	// add_action( 'wp_ajax_change_plan_dropdown', 'change_plan_dropdown_callback' );
	// add_action( 'wp_ajax_nopriv_change_plan_dropdown', 'change_plan_dropdown_callback' );
	function change_plan_dropdown(){
		if(isset($_POST['userid']) && isset($_POST['mem_id']))
		{
			$userid = $_POST['userid'];
			$mem_id = $_POST['mem_id'];
			$oldmem = showselectedPlanByUser($userid);
			$userInfo = get_user_meta($userid);
			
			if(array_key_exists('orders', $userInfo))
			{
				$orders= get_user_meta($userid, 'orders', true);
			}else{
				$orders= 0;
			}
			if(array_key_exists('mockups', $userInfo) )
			{
				$mockups= get_user_meta($userid, 'mockups', true);
			}else{
				$mockups= 0;
			}
			
			$membership = $this->wpdb->get_row("SELECT * FROM wpoc_memberships 
									WHERE  mem_ship_id = ".$mem_id."  LIMIT 1");
			$changes = [];
			if($membership != null)
			{
				$changes['oldplan'] = $oldmem->name;
				$changes['newplan'] = $membership->name;
	
				$changes['oldorder'] = $orders;
				$changes['oldmockups'] = $mockups;
				$changes['neworder'] = $membership->mem_order;
				$changes['newmockup'] = $membership->mockups;
				$changes['totalorder'] = $changes['oldorder'] + $changes['neworder'] ;
				$changes['totalmockups'] = $changes['oldmockups'] + $changes['newmockup'];
				$changes['newamount'] = $membership->amount;
				$changes['newfrequency'] = $membership->frequency;

				echo json_encode(['code'=>200, 'data'=>$changes]);
				exit();

			}else{
				echo json_encode(['code'=>400, 'data'=>[]]);
				exit();
			}
		}
	}

	public function change_plan_to_free()
	{
		if(isset($_POST['userid']) && isset($_POST['getmembership']))
		{
			$userid = $_POST['userid'];
			$user  = $this->wpdb->get_row("SELECT * FROM wpoc_users 
									WHERE  ID = ".$userid."  LIMIT 1"
								);
				
			if($user != null)
			{
				$membership  = $_POST['getmembership'];
				$oldplan = getUsermembershipDetail($userid);
				$newplan = $this->wpdb->get_row("SELECT * FROM wpoc_memberships 
										WHERE  mem_ship_id = ".$membership."  LIMIT 1");

				$customer_id = $this->getUserStripeCustomerId($userid);
				$customer=retrive_customer(get_user_meta($userid, 'customer_stripe_id', true));

				$subscription = \Stripe\Subscription::retrieve($oldplan->stripe_subscription_id);
					if($subscription->cancel_at_period_end != 'true')
					{
						try{
								$updatesubcription = \Stripe\Subscription::update(
								  $oldplan->stripe_subscription_id,
								  [
								    'cancel_at_period_end' => true,
								  ]
								);
								$cancelwhere = [ 'mem_id'=>$oldplan->mem_id]; 
								$canceldata = ['is_updated'=>2,'updated'  => date("Y-m-d H:i:s")];
								$this->wpdb->update('wpoc_user_memberships',$canceldata,$cancelwhere);
								$newdata = ['status'=>'Pending','amount'=>$newplan->amount,'duration'=>$newplan->frequency,'type'=>$newplan->mem_ship_id,'user_id'=>$userid,'basic_order'=>0,'premium_order'=>0,'standard_order'=>0,'video_mockups'=>0,'is_updated'=>0,'created'  => date("Y-m-d H:i:s"),'to_be_started'=>$oldplan->end_date];
								$this->wpdb->insert('wpoc_user_memberships',$newdata);
				        $statusMsg = "Subcription Cancel Request Send Successfully , Subcription Will cancel at the end of current period !"; 
				        echo json_encode(['code'=>200, 'data'=>$statusMsg]);
								exit();
						 			

						}catch(Exception $e) { 
	            $statusMsg = $e->getMessage();         
	  					echo json_encode(['code'=>400, 'data'=>$statusMsg]);
							exit();
				    } 
				  }else{
	        	$statusMsg = "Already Send Request For Cancel Subcription!";
	      	 	echo json_encode(['code'=>400, 'data'=>$statusMsg]);
						exit();
		  		}
		  }else{
		  	$statusMsg = "Invalid user";
      	 	echo json_encode(['code'=>400, 'data'=>$statusMsg]);
					exit();
		  }
		}
		$statusMsg = "Invalid parameter supplied"; 
    echo json_encode(['code'=>400, 'data'=>$statusMsg]);
		exit();
	}
	public function update_admin_membership()
	{

		//monthamount,cardname,userid,paywithcard,lastFour,stripeToken

		$mockups	= isset($_POST['getmockupsmonthly']) ? $_POST['getmockupsmonthly'] : 0;
		$orders		=  isset($_POST['getordermonthly']) ? $_POST['getordermonthly'] : 0;
		$membership  = $_POST['getmembership'];
		
		$lastfour = $_POST['lastFour'];
		if(isset($_POST['getmockupsmonthly']) && isset($_POST['getordermonthly']) && isset($_POST['getmembership']))
		{

			$userid = $_POST['userid'];
			$oldplan = getUsermembershipDetail($userid);
			if($oldplan == null)
			{
				$freeplan = $this->wpdb->get_row("SELECT * FROM wpoc_memberships 
									WHERE  name = 'FREE'  LIMIT 1");
				$oldplan = getpendingmembership($userid);
				if($oldplan == null)
				{
				$savedata=  ['status'=>'Pending','amount'=>$freeplan->amount,'duration'=>$freeplan->frequency,'type'=>$freeplan->mem_ship_id,'user_id'=>$userid,'basic_order'=>0,'premium_order'=>0,'standard_order'=>0,'video_mockups'=>0,'is_updated'=>0,'created'  => date("Y-m-d H:i:s")];
			
				$this->wpdb->insert('wpoc_user_memberships',$savedata);
				$oldplan = getpendingmembership($userid);
				}
			}
		
			$newplan = $this->wpdb->get_row("SELECT * FROM wpoc_memberships 
									WHERE  mem_ship_id = ".$membership."  LIMIT 1");
			
			if(strtolower($newplan->name) == 'custom')
			{
				$basicOrder 	= sanitize_text_field($_POST['getupdatebasicorder']);
				$standardOrder 	= sanitize_text_field($_POST['getupdatestandardorder']);
				$premiumOrder 	= sanitize_text_field($_POST['getupdatepremiumorder']);
				$updatemockups 	= $_POST['getupdatememmock'];
				$videoMockups 	= $_POST['getupdatevideomock'];
				// $amount 			= sanitize_text_field($_POST['updatemonthlyFee']);
			}else{
				$basicOrder 	= 0;
				$standardOrder 	= 0;
				$premiumOrder 	= 0;
				$updatemockups 	= 0;
				$videoMockups 	= 0;
			
			}

			$user  = $this->wpdb->get_row("SELECT * FROM wpoc_users 
									WHERE  ID = ".$userid."  LIMIT 1"
								);
				
			if($user != null)
			{
				if(isset($_POST['paywithcard']) || array_key_exists('paywithcard', $_POST))
				{
					$token= $_POST['stripeToken'];
					$amount= $_POST['monthamount'];
					$customer_id = $this->getUserStripeCustomerId($userid);
					$customer=retrive_customer(get_user_meta($userid, 'customer_stripe_id', true));

					if($customer->default_source == null){
						$card = $this->savenewcardCustomerbyId($customer_id,$token);
					}else{
						$card = $customer->default_source;
					}
					
				}
				if(isset($_POST['update_default_card']) || array_key_exists('update_default_card', $_POST))
				{
					$amount= $_POST['monthamount'];
					$customer_id = $this->getUserStripeCustomerId($userid);
					$customer=retrive_customer(get_user_meta($userid, 'customer_stripe_id', true));

					if($customer->default_source != null){
						$card = $customer->default_source;
					}else{
						$card = null;
					}
					
				}
					
			
				if($card)
				{
					$checkUpgradeDowngrade =  $this->checkUpgradeDowngrade($oldplan,$newplan);

					if($checkUpgradeDowngrade == 'upgrade')
					{

						$date1=date_create($oldplan->start_date);
						$currentdate= date('Y-m-d'); 
						$date2=date_create($currentdate);
						$diff=date_diff($date1,$date2);
						$days =  $diff->format("%a");
						if(strtolower($newplan->name) == 'custom' && strtolower($oldplan->name) !='custom')
						{
							if($newplan->frequency == 'Y')
							{
								$interval = 'year';
								$amount = $amount*12;
							}else{
								$interval = 'month';
							}
							$mockups = $updatemockups;

								$plans = 	\Stripe\Plan::all(['limit' => 1]);
								foreach ($plans->data as $key => $value) {
									$product_id = $value->product;
								}
								$productname = \Stripe\Product::retrieve($product_id);

								try { 
						        $plan = \Stripe\Plan::create(array( 
						            "product" => $product_id,
						            "nickname" => $userid.'_custom', 
						            "amount" => $amount*100, 
						            "currency" => 'usd', 
						            "interval" => $interval, 
						            "interval_count" => 1 
						        )); 
						      
					    	}catch(Exception $e) { 
					    	
					        $api_error = $e->getMessage(); 
					    	}

					    	if(empty($api_error) && $plan){ 
					        // Creates a new subscription 
					        try { 

					            $updatesubcription = \Stripe\Subscription::create(array( 
					                "customer" => $customer_id, 
					                'cancel_at_period_end' => false,
				                	"items" => array( 
					                    array( 
					                        "plan" => $plan->id, 
					                    ), 
				                	), 
				                	'prorate' => true
					            )); 
					        }catch(Exception $e) { 
					            $api_error = $e->getMessage(); 
					        } 
					    	}
					    	
						}


						if(strtolower($newplan->name) == 'custom' && strtolower($oldplan->name) =='custom')
						{
							$plan_id = $oldplan->stripe_plan_id;
							if($newplan->frequency == 'Y')
							{
								$interval = 'year';
								$amount = $amount*12;
							}else{
								$interval = 'month';
							}
							$mockups = $updatemockups;
							if($plan_id != null)
							{
								$plan = \Stripe\Plan::retrieve($plan_id);
								if($plan != null)
								{
									$getcustomsubscription = \Stripe\Subscription::retrieve($oldplan->stripe_subscription_id);

									try{
										$updatesubcription = \Stripe\Subscription::update($oldplan->stripe_subscription_id, [
								  		'cancel_at_period_end' => false,
								  		'items' => [
										    [
										      'id' => $getcustomsubscription->items->data[0]->id,
										      'plan' => $plan->id,
										    ],
								  		],
								  		'prorate' => true,
										]);
									}catch(Exception $e) { 
					            $api_error = $e->getMessage(); 
					    		} 
				    		}
							}else{

								$plans = 	\Stripe\Plan::all(['limit' => 1]);
								foreach ($plans->data as $key => $value) {
									$product_id = $value->product;
								}
								$productname = \Stripe\Product::retrieve($product_id);

								try { 
						        $plan = \Stripe\Plan::create(array( 
						            "product" => $product_id,
						            "nickname" => $userid.'_custom', 
						            "amount" => $amount*100, 
						            "currency" => 'usd', 
						            "interval" => $interval, 
						            "interval_count" => 1 
						        )); 
						      
					    	}catch(Exception $e) { 
					    	
					        $api_error = $e->getMessage(); 
					    	}

					    	if(empty($api_error) && $plan){ 
					        // Creates a new subscription 
					        try { 

					            $updatesubcription = \Stripe\Subscription::create(array( 
					                "customer" => $customer_id, 
					                'cancel_at_period_end' => false,
				                	"items" => array( 
					                    array( 
					                        "plan" => $plan->id, 
					                    ), 
				                		), 
				                	'prorate' => true,
					            )); 
					        }catch(Exception $e) { 
					            $api_error = $e->getMessage(); 
					        } 
					    	}
							}
							
						}

						if(strtolower($newplan->name) != 'custom' && strtolower($newplan->name) != 'free')
						{

							$plan = \Stripe\Plan::retrieve($newplan->mem_ship_id);
							if($plan != null)
							{
								$subscription = \Stripe\Subscription::retrieve($oldplan->stripe_subscription_id);

								try{
									$updatesubcription = \Stripe\Subscription::update($oldplan->stripe_subscription_id, [
									  'cancel_at_period_end' => false,
									  'items' => [
										    [
										      'id' => $subscription->items->data[0]->id,
										      'plan' => $plan->id,
										    ],
									  	],
									  	'prorate' => true,
									]);
								}catch(Exception $e) { 
							            $api_error = $e->getMessage(); 
				        } 
			        }
		        }
				        if(empty($api_error) && $updatesubcription){ 
					            		// Retrieve subscription data 
	            		$subsData = $updatesubcription->jsonSerialize();

		            	if($subsData['status'] == 'active'){ 
						                	// Subscription info 
		                $subscrID = $subsData['id']; 
		                $custID = $subsData['customer']; 
		                $planID = $subsData['plan']['id']; 
		                $planAmount = ($subsData['plan']['amount']/100); 
		                $planCurrency = $subsData['plan']['currency']; 
		                $planinterval = $subsData['plan']['interval']; 
		                $planIntervalCount = $subsData['plan']['interval_count']; 
		                $created = date("Y-m-d H:i:s", $subsData['created']); 
		                $current_period_start = date("Y-m-d", $subsData['current_period_start']); 
		                $current_period_end = date("Y-m-d", $subsData['current_period_end']); 
		                $status = $subsData['status'];
		                $statusMsg = 'Your Subscription Payment has been Successful!'; 
							                // $where = [ 'mem_id'=>$getMembership->mem_id]; 
										$updatestatus = ['status' => 'Expired','is_updated'=>1];
										$where = ['mem_id'=>$oldplan->mem_id];
										$this->wpdb->update('wpoc_user_memberships',$updatestatus,$where); 
										$data = ['status'=>'Active','start_date'=>$current_period_start,'end_date'=>$current_period_end,'stripe_subscription_id'=>$subscrID,'plan_amount_currency'=>$planCurrency,'amount'=>$planAmount,'last_four'=>$lastfour,'stripe_plan_id'=>$planID,'duration'=>$newplan->frequency,'type'=>$newplan->mem_ship_id,'user_id'=>$userid,'basic_order'=>$basicOrder,'premium_order'=>$premiumOrder,'standard_order'=>$standardOrder,'video_mockups'=>$videoMockups,'is_updated'=>0,'created'  => date("Y-m-d H:i:s")];
										$this->wpdb->insert('wpoc_user_memberships',$data);
										// $wpdb->update($wpdb->user_memberships,$data,$where);
										$updatemocksOrder = $this->updatemockOrder($userid,$mockups,$orders);
											
											
										session_start();
								        $statusMsg = "Subcription Created Successful"; 
								        $_SESSION["mtabsuccess"]  = $statusMsg;
						        if($days <= 7)
										{
											$oldplanamount 		= $oldplan->membershipamount;
											$refundtocustomer 	= $this->refundamountToCustomer($oldplan);
										}
										wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
				        		exit();   
									}else{ 
							            	
			            	session_start();
			            	
                		$statusMsg = "Subscription activation failed!";
                	 	$_SESSION["membershiptaberror"]  = $statusMsg;
                	 	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
	        					exit();
						                	 
		            	}
	            	}else{
					        		
			        		session_start();
			        		$statusMsg = "Subscription creation failed! ".$api_error; 
			        		$_SESSION["membershiptaberror"]  = $statusMsg;
			        		 wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
			        				exit();
					        		
		        		}


					}elseif($checkUpgradeDowngrade == 'downgrade')
					{

							if(strtolower($newplan->name) != 'custom' && strtolower($newplan->name) != 'free')
							{
								$plan = \Stripe\Plan::retrieve($newplan->mem_ship_id);
								if($plan != null)
								{
									$subscription = \Stripe\Subscription::retrieve($oldplan->stripe_subscription_id);

								try{
									$updatesubcription = \Stripe\Subscription::update($oldplan->stripe_subscription_id, [
									  'cancel_at_period_end' => true,
									  'items' => [
										    [
										      'id' => $subscription->items->data[0]->id,
										      'plan' => $plan->id,
										    ],
									  	],
									  	'prorate' => false,
									]);
								}catch(Exception $e) { 
						            $api_error = $e->getMessage(); 
						        } 
						    }
								
							}elseif(strtolower($newplan->name) == 'custom' && strtolower($oldplan->name) =='custom')
							{
								$plan_id = $oldplan->stripe_plan_id;
								if($newplan->frequency == 'Y')
								{
									$interval = 'year';
									$amount = $amount*12;
								}else{
									$interval = 'month';
								}
								$mockups = $updatemockups;
								if($plan_id != null)
								{
									$plan = \Stripe\Plan::retrieve($plan_id);
									if($plan != null)
									{
										$subscription = \Stripe\Subscription::retrieve($oldplan->stripe_subscription_id);

										try{
											$updatesubcription = \Stripe\Subscription::update($oldplan->stripe_subscription_id, [
											  'cancel_at_period_end' => true,
											  'items' => [
												    [
												      'id' => $subscription->items->data[0]->id,
												      'plan' => $plan->id,
												    ],
											  	],
											  	'prorate' => false,
											]);
										}catch(Exception $e) { 
								            $api_error = $e->getMessage(); 
								    } 
							    }
								}else{

									$plans = 	\Stripe\Plan::all(['limit' => 1]);
									foreach ($plans->data as $key => $value) {
										$product_id = $value->product;
									}
									$productname = \Stripe\Product::retrieve($product_id);

									try { 
									        $plan = \Stripe\Plan::create(array( 
									            "product" => $product_id,
									            "nickname" => $userid.'_custom', 
									            "amount" => $amount*100, 
									            "currency" => 'usd', 
									            "interval" => $interval, 
									            "interval_count" => 1 
									        )); 
									      
								    }catch(Exception $e) { 
								    	
								        $api_error = $e->getMessage(); 
								    }

								    if(empty($api_error) && $plan){ 
								        // Creates a new subscription 
								        try { 

								            $updatesubcription = \Stripe\Subscription::create(array( 
								                "customer" => $customer_id, 
							                	"items" => array( 
								                    array( 
								                        "plan" => $plan->id, 
								                    ), 
							                	), 
								            )); 
								        }catch(Exception $e) { 
								            $api_error = $e->getMessage(); 
								        } 
								    }
								}
							}

				        if(empty($api_error) && $updatesubcription){ 
		            		// Retrieve subscription data 
	            		$downsubsData = $updatesubcription->jsonSerialize();

		            	if($downsubsData['status'] == 'active'){ 
			                	// Subscription info 
		                $subscrID = $downsubsData['id']; 
		                $custID = $downsubsData['customer']; 
		                $planID = $downsubsData['plan']['id']; 
		                $planAmount = ($downsubsData['plan']['amount']/100); 
		                $planCurrency = $downsubsData['plan']['currency']; 
		                $planinterval = $downsubsData['plan']['interval']; 
		                $planIntervalCount = $downsubsData['plan']['interval_count']; 
		                $created = date("Y-m-d H:i:s", $downsubsData['created']); 
		                $current_period_start = date("Y-m-d", $downsubsData['current_period_start']); 
		                $current_period_end = date("Y-m-d", $downsubsData['current_period_end']); 
		                $status = $downsubsData['status'];
		                $statusMsg = 'Your Subscription Payment has been Successful!'; 
				                // $where = [ 'mem_id'=>$getMembership->mem_id]; 
										$updatedownstatus = ['is_updated'=>1];
										$downwhere = ['mem_id'=>$oldplan->mem_id];
										$this->wpdb->update('wpoc_user_memberships',$updatedownstatus,$downwhere); 
										$downdata = ['status'=>'Pending','start_date'=>$current_period_start,'end_date'=>$current_period_end,'stripe_subscription_id'=>$subscrID,'plan_amount_currency'=>$planCurrency,'amount'=>$planAmount,'last_four'=>$lastfour,'stripe_plan_id'=>$planID,'duration'=>$newplan->frequency,'type'=>$newplan->mem_ship_id,'user_id'=>$userid,'basic_order'=>$basicOrder,'premium_order'=>$premiumOrder,'standard_order'=>$standardOrder,'video_mockups'=>$videoMockups,'is_updated'=>0,'created'  => date("Y-m-d H:i:s")];
										$this->wpdb->insert('wpoc_user_memberships',$downdata);
										// $wpdb->update($wpdb->user_memberships,$data,$where);
										$updatemocksOrder = $this->updatemockOrder($userid,$mockups,$orders);
								
								
										session_start();
							        $statusMsg = "Subcription Created Successful"; 
							        $_SESSION["mtabsuccess"]  = $statusMsg;
									       
									 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
			        				exit();   
									}else{ 
				            	
			            		session_start();
				            	
	                		$statusMsg = "Subscription activation failed!";
                	 		$_SESSION["membershiptaberror"]  = $statusMsg;
                	 		wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
		        					exit();
			                	 
          				}
	            	}else{
			        		session_start();
			        		$statusMsg = "Subscription creation failed! ".$api_error; 
			        		$_SESSION["membershiptaberror"]  = $statusMsg;
			        		wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=membership');
			        		exit();
	        			}
						}
						
					} 
				
			}else{

			}

		}else{
			echo "Invalid parameter supply";
			die();
		}
	}

	public function checkUpgradeDowngrade($oldplan,$newplan)
	{		

		
		if(strtolower($newplan->name) == 'bronze' && (strtolower($oldplan->name) == 'silver' || strtolower($oldplan->name) == 'gold' || strtolower($oldplan->name) == 'custom'))
		{
			return 'downgrade';
		}elseif(strtolower($newplan->name) == 'silver' && (strtolower($oldplan->name) == 'gold' || strtolower($oldplan->name) == 'custom'))
		{
			return 'downgrade';
		}elseif(strtolower($newplan->name) == 'gold' &&  strtolower($oldplan->name) == 'custom'){
			return 'downgrade';
		}
		elseif (strtolower($newplan->name) == 'free' && (strtolower($oldplan->name) == 'bronze' || strtolower($oldplan->name) == 'silver' || strtolower($oldplan->name) == 'gold' || strtolower($oldplan->name) == 'custom')) {
			return 'downgrade';
		}elseif(strtolower($newplan->name) == 'custom' && (strtolower($oldplan->name) == 'bronze' || strtolower($oldplan->name) == 'silver' || strtolower($oldplan->name) == 'gold' || strtolower($oldplan->name) == 'free'))
		{
			return 'upgrade';
		}elseif(strtolower($newplan->name) == 'gold' && (strtolower($oldplan->name) == 'bronze' || strtolower($oldplan->name) == 'silver'  || strtolower($oldplan->name) == 'free')){
			return 'upgrade';
		}elseif(strtolower($newplan->name) == 'silver' && (strtolower($oldplan->name) == 'bronze'   || strtolower($oldplan->name) == 'free')){
			return 'upgrade';
		}elseif(strtolower($newplan->name) == 'bronze'  && strtolower($oldplan->name) == 'free'){
			return 'upgrade';
		}elseif(strtolower($newplan->name) == 'bronze'  && strtolower($oldplan->name) == 'bronze'){
			if($new->plan->frequency =='M' && $oldplan->frequency == 'Y' )
			{
				return 'upgrade';
			}else{
				if($new->plan->frequency =='Y' && $oldplan->frequency == 'M' )
				{
					return 'downgrade';
				}
			}
		}elseif(strtolower($newplan->name) == 'silver'  && strtolower($oldplan->name) == 'silver'){
			if($new->plan->frequency =='M' && $oldplan->frequency == 'Y' )
			{
				return 'upgrade';
			}else{
				if($new->plan->frequency =='Y' && $oldplan->frequency == 'M' )
				{
					return 'downgrade';
				}
			}
		}elseif(strtolower($newplan->name) == 'gold'  && strtolower($oldplan->name) == 'gold'){
			if($new->plan->frequency =='M' && $oldplan->frequency == 'Y' )
			{
				return 'upgrade';
			}else{
				if($new->plan->frequency =='Y' && $oldplan->frequency == 'M' )
				{
					return 'downgrade';
				}
			}
		}elseif(strtolower($newplan->name) == 'custom'  && strtolower($oldplan->name) == 'custom'){
			if($new->plan->frequency =='M' && $oldplan->frequency == 'Y' )
			{
				return 'upgrade';
			}else{
				if($new->plan->frequency =='Y' && $oldplan->frequency == 'M' )
				{
					return 'downgrade';
				}
			}
		}
	}

	public function getUserStripeCustomerId($userid)
	{
		$userInfo = get_user_meta($userid);
		if(array_key_exists('customer_stripe_id', $userInfo))
		{
			if(get_user_meta($userid, 'customer_stripe_id', true)==''){
				$customer_id=stripe_create_customer();
				update_user_meta($userid, 'customer_stripe_id', $customer_id);

			}else{	
				$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
			}
		}else{
			$customer_id=stripe_create_customer();
			$data= ['user_id'=>$userid,'meta_key'=>'customer_stripe_id','meta_value'=>$customer_id ];
			$this->wpdb->insert('wpoc_usermeta',$data);
		}
		
		return $customer_id;
	}
	public function savenewcardCustomerbyId($customerid,$token)
	{
		$customer_id=$customerid;
		$stripe_settings = get_option('stripe_settings'); 
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		
		
		try {
			$card = \Stripe\Customer::createSource(
			  $customer_id,
			  [
				'source' => $token,
			  ]
			);
			return $card;
		}catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo '<h1>Too many requests made to the API too quickly!</h1>';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo '<h1>Authentication Failed, please try again!</h1>';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo '<h1>Server not Responding, please try again later!</h1>';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo "<h1>".$e->getMessage()."</h1>";
		}
	}

	public function refundamountToCustomer($oldplan){
		$objInvoiceCollection = \Stripe\Invoice::all([
		    'subscription' => $oldplan->stripe_subscription_id
		]);

		if ($objInvoiceCollection) {

			foreach ($objInvoiceCollection->data as $key => $value) {
				$chargeId = $value->charge;
			
			}
			
			try {
			$objRefund = \Stripe\Refund::create([
				'amount' => $oldplanamount*100,
				'charge' => $chargeId]);

			return  $objRefund;
			}catch(\Stripe\Error\Card $e) {
		  	// Since it's a decline, \Stripe\Error\Card will be caught
			  $body = $e->getJsonBody();
			  $err  = $body['error'];

			  print('Status is:' . $e->getHttpStatus() . "\n");
			  print('Type is:' . $err['type'] . "\n");
			  print('Code is:' . $err['code'] . "\n");
			  // param is '' in this case
			  print('Param is:' . $err['param'] . "\n");
			  print('Message is:' . $err['message'] . "\n");
			} catch (\Stripe\Error\RateLimit $e) {
			  // Too many requests made to the API too quickly
			   echo '<h1>Too many requests made to the API too quickly!</h1>';
			} catch (\Stripe\Error\InvalidRequest $e) {
			  // Invalid parameters were supplied to Stripe's API
			   echo '<h1>Invalid parameter supply. Please try again.</h1>';
			} catch (\Stripe\Error\Authentication $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
			   echo '<h1>Authentication Failed, please try again!</h1>';
			} catch (\Stripe\Error\ApiConnection $e) {
			  // Network communication with Stripe failed
			   echo '<h1>Server not Responding, please try again later!</h1>';
			} catch (\Stripe\Error\Base $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
			  echo "<h1>".$e->getMessage()."</h1>";
			}

		}

	}
	public function updatemockOrder($userid,$mockups,$orders)
	{
		$userInfo = get_user_meta($userid);
		if(array_key_exists('mockups', $userInfo))
		{
			$oldmock= get_user_meta($userid, 'mockups', true);
			$unused = $oldmock+$mockups;
			update_user_meta($userid, 'mockups', $unused);
		}else{
			$datamock= ['user_id'=>$userid,'meta_key'=>'mockups','meta_value'=>$mockups ];
	 		$this->wpdb->insert('wpoc_usermeta',$datamock);

	 	}
 		if(array_key_exists('unused_mockups', $userInfo))
 		{
 			$oldunusedmock= get_user_meta($userid, 'unused_mockups', true);
 			$unused = $oldunusedmock+$mockups;
 			update_user_meta($userid, 'unused_mockups', $unused);
 		}else{
 			$unused= ['user_id'=>$userid,'meta_key'=>'unused_mockups','meta_value'=>$mockups ];
 			$this->wpdb->insert('wpoc_usermeta',$unused);
 		}
 		if(!array_key_exists('used_mocks', $userinfo))
 		{
 			$used = ['user_id'=>$userid,'meta_key'=>'used_mocks','meta_value'=>0 ];
 			$this->wpdb->insert('wpoc_usermeta',$used);
 		}
 		if(array_key_exists('orders', $userInfo))
		{
			$oldorder= get_user_meta($userid, 'orders', true);
			$unused = $oldorder+$orders;
			update_user_meta($userid, 'orders', $unused);
		}else{
			$dataorder= ['user_id'=>$userid,'meta_key'=>'orders','meta_value'=>$orders ];
	 		$this->wpdb->insert('wpoc_usermeta',$dataorder);

	 	}
 		if(array_key_exists('unused_orders', $userInfo))
 		{
 			$oldunusedorder= get_user_meta($userid, 'unused_orders', true);
 			$unused = $oldunusedorder+$orders;
 			update_user_meta($userid, 'unused_orders', $unused);
 		}else{
 			$unused= ['user_id'=>$userid,'meta_key'=>'unused_orders','meta_value'=>$orders ];
 			$this->wpdb->insert('wpoc_usermeta',$unused);
 		}
 		if(!array_key_exists('used_orders', $userinfo))
 		{
 			$used = ['user_id'=>$userid,'meta_key'=>'used_orders','meta_value'=>0 ];
 			$this->wpdb->insert('wpoc_usermeta',$used);
 		}
		return true;		

	}

	


}
$function = new CustomFunctions;

?>