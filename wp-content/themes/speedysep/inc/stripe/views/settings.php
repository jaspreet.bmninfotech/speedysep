<div class="wrap">
	<div class=""><?php if(isset($_GET['record']) and $_GET['record']=='added'){ echo "<p>Record added successfully.</p>"; }elseif(isset($_GET['record']) and $_GET['record']=='updated'){ echo "<p>Records updated successfully</p>"; } ?></div>
	<div class="tab">
	  <button class="tablinks" onclick="openWpMbTab(event, 'stripe_settings')"><?php _e('Stripe settings', 'linda') ?></button>
	</div>

<?php $stripe_settings = get_option('stripe_settings'); ?>
<div id="stripe_settings" class="tabcontent current">
	<h1><?php _e('Stripe Settings', 'linda') ?></h1>
	<form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
	<input type="hidden" name="action" value="update_stripe_settings">
	<input type="hidden" name="page_url" value="speedysep-settings">
	 <?php wp_nonce_field( 'update_stripe_settings', 'update_stripe_settings_nonce' ); ?>
	<table class="form-table order" id="stripe_settings">
		<tbody>
			<tr>
				<th scope="row"><label for="public_key"><?php _e('Public key', 'linda'); ?></label></th>
				<td><input name="settings[stripe_settings][public_key]" type="text" id="public_key" value="<?php echo $stripe_settings['stripe_settings']['public_key'] ?>"  class="regular-text">
					<p class="description">Paste your stripe publishible key here.</p>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="secret_key"><?php _e('Secret Key', 'linda'); ?></label></th>
				<td><input name="settings[stripe_settings][secret_key]" type="text" id="secret_key" value="<?php echo $stripe_settings['stripe_settings']['secret_key'] ?>"  class="regular-text">
					<p class="description">Paste your sripe secret key here</p>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
	</form>
</div>


</div><!-- End wrapper -->

<script>
function openWpMbTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border-bottom: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    border: 1px solid #ccc;
    border-bottom: 0px;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    margin: 0px 5px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #0085ba;
	color: #fff;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 0px;
    border-top: none;
}

.tabcontent.current{
display:block;	
}
</style>