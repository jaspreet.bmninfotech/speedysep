<div class="spacer35"></div>
<div class="open-orders">
	
	

<?php
$user_id=get_current_user_id();
if(!isset($_GET['order_id'])){

// The Query
$args = array(
	'post_type' => 'service-orders',
	'posts_per_page'=>-1,
	"post_status"=>'any',
	"author"=>$user_id,
	'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'order_status',
							'value'    => 'process',
							'compare' => '=',
						),
						),
	);
$buyers_requests_query = new WP_Query( $args );

if ( $buyers_requests_query->have_posts() ) {
?>

<h3><?php _e('Open Orders', 'speedy') ?></h3>
<?php
	$count=1;
	// The Loop
	while ( $buyers_requests_query->have_posts() ) {
		$buyers_requests_query->the_post(); ?>
		<?php if($count==1 || $count-1==6){ ?> 
		<div class="row">
		<?php }?>
			<div class="col-sm-2">
				<a href="?order_id=<?php echo get_the_ID() ?>">
				<div class="order-inner">
					<p><?php _e('Order', 'speedy') ?> #<?php echo get_the_ID() ?></p>
					<?php $proj_files=get_post_meta(get_the_ID(), 'project_files', true);
						$image_available=false;
						if(!empty($proj_files)){
						foreach($proj_files as $proj_file){ ?>
						<?php 
						$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
						if ( $image_attributes ) : ?>
							<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
						<?php  $image_available=true; break; ?>
						<?php endif; ?>
						<?php } 
						} //end if
						?>
						<?php if($image_available==false){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
						<?php } ?>
				</div>
				</a>
			</div>
		<?php if($count==6){ ?> 
		</div>
		<?php }?>
	<?php 
	$count++;
	}

?>
<?php 
	wp_reset_postdata();
}else{ ?>
	<h3><?php _e('Open Orders', 'speedy') ?></h3>
	<p class="open-order-no-order"><?php _e('You have no open order at the moment.', 'speedy') ?></p>
<?php }
}elseif(isset($_GET['order_id'])){ 
	$order=get_post($_GET['order_id']);
	update_post_meta($_GET['order_id'], 'order_mess_status', 'read');
?>
	<h3><?php _e('Open Orders', 'speedy') ?></h3>
	<?php if($order->post_author==$user_id){
		if(get_post_meta($_GET['order_id'], 'order_status', true)=='delivered' || get_post_meta($_GET['order_id'], 'order_status', true)=='completed'){ ?>
			<div class="order-delivered text-center">
				<div class="order-no">
				<span>
				<?php _e('Order', 'speedy'); ?>#<?php echo $_GET['order_id'] ?>
				</span>
				</div>
				<div class="spacer35"></div>
				<div class="spacer10"></div>
				<div class="order-delivered-text">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-complete.png">
					<h3><?php _e('YOUR ORDER IS READY!!!', 'Speedy') ?></h3>
					<p>Please download and review all files.<br>
						If everything looks right, please mark<br>
						this order as completed.<br>
						"Order will be marked as completed if there<br>
						was no revision to it in 48 after after the delivery"</p>
						<div class="spacer10"></div>
						<a href="<?php echo get_post_meta($_GET['order_id'], 'delivered_files', true) ?>" target="_blank">
							<button class="black_button"><?php _e('DOWNLOAD FILES', 'speedy') ?></button>
						</a>
						<div class="spacer20"></div>
						<?php if(get_post_meta($_GET['order_id'], 'order_status', true)!='completed'){ ?>
						<div class="completed-deliverd-buttons">
								<?php $package=get_post_meta($_GET['order_id'], 'package', true); 
							if(get_post_meta($_GET['order_id'], 'order_type', true)=='seperation'){
								$page_id='414';
							}else{
								$page_id='294';
							}
							$modifications_allowed=get_post_meta($page_id, $package.'_modifications', true); 
							$add_modifications=get_post_meta($_GET['order_id'], 'add_modifications', true); 
							$modifications_allowed=$modifications_allowed+$add_modifications;
							$revisions=get_post_meta($_GET['order_id'], 'revisions', true);	
							if($revisions==''){ $revisions=0; }
							if($revisions<$modifications_allowed){
							?>
										
							<a href="<?php echo site_url() ?>/request-a-revision/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Request a Revision', 'speedy') ?></button> 
							</a>  
							<?php _e('OR', 'speedy') ?>
							<?php }else{ ?>
							<p style="color:red">Maximum Modifications reached. You can buy 5 modification at $5 here. 
							<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="getModifications">
							   <?php wp_nonce_field( 'get_modifications', 'get_modifications_nonce' ); ?>
							  
								<input type="hidden" name="action" value="get_modifications">
								<input type="hidden" name="order_id" value="<?php echo $_GET['order_id'] ?>">
							  <button type="submit" class="black_button" name="get_modifications">Buy Now</button>
							</form>
							<p>
							<div class="spacer10"></div>
							<?php } ?>
							<a href="<?php echo site_url() ?>/completed/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Mark as Completed', 'speedy') ?></button>
							</a>
						</div>
						<?php } ?>
				</div>
		<?php }else{
		?>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th class="order-column"><?php _e('Order', 'speedy') ?>#<?php echo $order->ID ?></th>
					<th><?php _e('Order Type', 'speedy') ?></th>
					<th><?php _e('Order Date', 'speedy') ?></th>
					<th><?php _e('Delivery Date', 'speedy') ?></th>
					<th><?php _e('Order Status', 'speedy') ?></th>
					<th><?php _e('Chat', 'speedy') ?></th>
					<th class="last_column"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><?php echo get_post_meta($order->ID, 'order_type', true) ?></td>
					<td><?php echo  date("d M Y", strtotime(get_post_meta($order->ID, 'start_date', true))); ?></td>
					<td><?php echo date("d M Y", strtotime(get_post_meta($order->ID, 'delivery_date', true)));  ?></td>
					<td><?php echo get_post_meta($order->ID, 'order_status', true) ?></td>
					<td><?php  if ( gearside_is_user_online(1) ){  echo "online"; }else{  echo "offline"; } ?></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
		<div class="row">
			<div class="col-sm-2 detail-order">
				<?php $proj_files=get_post_meta($order->ID, 'project_files', true);
						$image_available=false;
						if(!empty($proj_files)){
						foreach($proj_files as $proj_file){ ?>
						<?php 
						$image_attributes = wp_get_attachment_image_src( $proj_file, 'thumbnail' );
						if ( $image_attributes ) : ?>
							<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>" />
						<?php  $image_available=true; break; ?>
						<?php endif; ?>
						<?php } 
						} //end if
						?>
						<?php if($image_available==false){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
						<?php } ?>
			</div>
			<div class="col-sm-5 order-details">
				<div> <span><?php _e('Colors', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'no_of_colors_wanted', true) ?></span></div>
				<div> <span><?php _e('Dimension', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'dimension_width', true) ?>x<?php echo get_post_meta($order->ID, 'dimension_height', true) ?></span></div>
				<div> <span><?php _e('Modifications', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'modifications_details', true) ?></span></div>
				<div> <span><?php _e('Mockups', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'mockups', true) ?></span></div>
				<?php if(get_post_meta($order->ID, 'order_type', true)=='seperation'){  ?> 
				<div> <span><?php _e('Separation Type', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'sep_type', true) ?></span></div>
				<?php } ?>
				<div> <span><?php _e('Additional Formats', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'additional_formats', true) ?></span></div>
				<div> <span><?php _e('Order Total', 'speedy') ?></span><span class="right">$<?php echo get_post_meta($order->ID, 'order_total', true) ?></span></div>
				
			</div>
			<div class="col-sm-5 order-details">
				<?php if(get_post_meta($order->ID, 'speedy_standard_output_temp', true)=='on'){ ?>
				<div> <span><?php _e('Standard template ', 'speedy') ?></span><span class="right">Yes</span></div>
				<?php } ?>
				<div> <span><?php _e('Package', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'package', true) ?></span></div>
				<div> <span><?php _e('Halftones', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'halftones', true) ?></span></div>
				<div> <span><?php _e('Extrafast', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'extrafast', true) ?></span></div>
				<div> <span><?php _e('Concept Recreation', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'conceptual', true) ?></span></div>
				<div> <span><?php _e('Weekend Delivery', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'weekend', true) ?></span></div>
			</div>
			<div class="col-sm-10 order-details col-sm-offset-2">
				
				<div class="notes"> 
					<h5><?php _e('Notes', 'speedy') ?></h5>
					<p><?php echo get_post_meta($order->ID, 'notes', true) ?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-2">
				<div class="conversation">
					<h3><?php _e('Conversation', 'speedy') ?>:</h3>
					<?php
					
					// The Query
					$args = array(
						'post_type' => 'customers-messages',
						'posts_per_page'=>-1,
						"post_status"=>'any',
						'orderby' => 'ID',
						'order'   => 'ASC',
						'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_id',
									'value'    => $_GET['order_id'],
									'compare' => '=',
								),
								array(
								'relation' => 'OR',
								array(
									'key'     => 'sender',
									'value'   => $user_id,
									'compare' => '=',
								),
								array(
									'key'     => 'reciver',
									'value'   => $user_id,
									'compare' => '=',
								),
								),
								
								
							),
						);
					$messages_query = new WP_Query( $args );
					
					if ( $messages_query->have_posts() ) {
					?>
					<?php
						
						// The Loop 
						while ( $messages_query->have_posts() ) {
							$messages_query->the_post(); ?>
						<div class="conversation_message">
						 
						<?php if(get_post_meta(get_the_ID(), 'message_type', true)=='revision'){ ?>
							<div class="text-center">
							<span class="black_button"><?php _e('Revision Request', 'speedy') ?></span>
							</div>
							<div class="text-center revision">
							<b><?php echo strip_tags(get_the_content()) ?></b>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png"> 
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
							</div>
							<?php } ?>
						<?php }else{ ?> 
							<?php if(get_the_author_ID()==$user_id){ 
							echo get_user_meta($user_id, "first_name", true);
							}else{
								echo "SpeedySep";
							} ?>:<?php echo strip_tags(get_the_content()) ?>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">

							<?php $proj_files=get_post_meta(get_the_ID(), 'message_attach', true);
							$image_available=false;
							$extension = end(explode(".", $proj_files));
							$allowed_files=array('png', 'jpg', 'gif', 'jpeg', 'svg');
							if(in_array($extension, $allowed_files)){?>
								<img src="<?php echo $proj_files; ?>" />
							<?php  $image_available=true; ?>
							<?php  
							} //end if
							?>
							<?php if($image_available==false){ ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
							<?php } ?>
							
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
						<?php } ?>
						<?php }?>
						
						
						</div>
							
					<?php }
					} 	?>
							
				</div>
				<div class="conv-message-box">
				<form action="" method="post" enctype="multipart/form-data" class="send-message-form">
					<textarea name="requestMessage" id="requestMessage" required></textarea>
						<img src="<?php echo get_stylesheet_directory_uri().'/images/loader.gif' ?>" id="loader" style="display:none; width:15px; width: 15px; position: absolute; right: -5px; z-index: 555;" />
					<div class="buttons">
						<input type="file" name="attachment" id="attachment" class="inputfile" data-multiple-caption="{count} files selected">
						<label id="file-text" style="padding: 0; display: inline; border: 0;"><span></span></label>
						<label id="#attach" for="attachment"><i class="fas fa-paperclip"></i>
						</label>
						<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
						<input type="hidden" name="user_name" id="user_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
						<button type="submit" name="send_message" id="send_message_button"><?php _e('Send', 'speedy') ?></button>
						
				
					</div>
				</form>
				</div>
			</div>
		</div>
		<?php } //order not delivered ?>
	<?php }else{ ?> 
		<p><?php _e('No record found', 'speedy') ?></p>
	<?php }?>
<?php  } ?>
</div>
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
</script>