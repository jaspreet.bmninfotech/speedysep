<div class="spacer35"></div>
<?php if($_GET['type']=='seperation'){ 
	$page_id=294;
}else{ $page_id=414; } ?>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
					<div class="user_cards">
				<h2><i class="fas fa-calendar-alt"></i> <?php _e('Checkout', 'speedy') ?></h2>
				<div class="cards-inner">
					<div class="row">
						<?php $package=get_post_meta($_GET['order'], 'package', true); ?>
						<div class="col-md-12">
							<div class="order-summary vect-pricing">
								<table>
					<thead>
						<th><?php _e('Summary', 'speedy') ?></th>
						<th></th>
					</thead>
					<tbody>
						
						<tr class="odd">
							<td><?php echo get_field('illustrator_ai_text', $page_id)?></td>
							<td><?php if(get_field($package.'_illustrator_ai', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="even">
							<td><?php echo get_field('transparent_png_text', $page_id)?></td>
							<td><?php if(get_field($package.'_transparent_png', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('eps_text', $page_id)?></td>
							<td><?php if(get_field($package.'_eps', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="even">
							<td><?php echo get_field('other_formats_text', $page_id)?></td>
							<td><?php if(get_field($package.'_other_formats', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('concept_recreation_text', $page_id)?></td>
							<td><?php if(get_field($package.'_concept_recreation', $page_id)[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even">
							<td><?php echo get_field('modifications_text', $page_id)?></td>
							<td><?php echo get_field($package.'_modifications', $page_id)?></td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('mockup_text', $page_id)?></td>
							<td><?php if(get_field($package.'_mockup', $page_id)!=''){   echo get_field($package.'_mockup', $page_id); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
							
						</tr>
						<tr class="even details">
							<td><?php echo get_field('details_text', $page_id)?></td>
							<td>
								<?php echo get_field($package.'_details', $page_id)?>
							</td>
						</tr>
						<tr class="odd">
							<td><?php echo get_field('delivery_text', $page_id)?></td>
							<td><?php echo get_field($package.'_delivery_time', $page_id)?></td>
						</tr>
						
					</tbody>
				</table>
				
				<div class="spacer10"></div>
				<div class = "table-responsive">
				<table class="additional-cost-table">
					<thead>
						<th><?php _e('Service', 'speedy'); ?></th>
						<th><?php _e('Date', 'speedy'); ?></th>
						<th><?php _e('Delivery Date', 'speedy'); ?></th>
						<th><?php _e('Package', 'speedy'); ?></th>
						<th><?php _e('Quantity', 'speedy'); ?></th>
						<th><?php _e('Total', 'speedy'); ?></th>
					</thead>
					<tbody>
						<tr class="odd">
							<td><?php echo $_GET['type'] ?></td>
							<td><?php echo date('d M Y'); ?></td>
							<td><?php $now = current_time( 'mysql' ); 
									if(get_post_meta($_GET['order'], 'extra_fast', true)){
										$delivery_time=16;
									}else{
									$delivery_time=intval(get_field($package.'_delivery_time', $page_id));
									}
									$later = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); //7200 seconds = 2 hours
									echo date( 'd M Y', strtotime($later)); ?></td>
							<td><?php echo $package; ?></td>
							<td>1</td>
							<td><?php echo SITE_CURRENCY_SYMBOL.get_post_meta($_GET['order'], 'order_total', true) ?></td>
						</tr>
					</tbody>
				</table>
				</div>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="card-footer">
					<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="placeOrderFormCheckout">
					   <?php wp_nonce_field( 'place_order', 'place_order_nonce' ); ?>
					  <?php	global $post;
							$post_slug=$post->post_name; ?>
							
						<input type="hidden" name="page" value="<?php echo $post_slug; ?>">
						<input type="hidden" name="action" value="place_order">
						<input type="hidden" name="order_type" value="<?php echo $_GET['type'] ?>">
						<input type="hidden" name="delivery_time" value="<?php echo $delivery_time ?>">
						<input type="hidden" name="order_id" value="<?php echo $_GET['order'] ?>">
					  <button type="submit" class="btn btn-default" name="place_order">Submit Order</button>
					</form>
				</div>
			</div>
	</div>
</div>