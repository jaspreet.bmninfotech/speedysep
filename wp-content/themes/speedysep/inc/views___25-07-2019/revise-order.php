<div class="spacer35"></div>
<div class="open-orders">
	
	

<?php
$user_id=get_current_user_id();
if(isset($_GET['order_id'])){ 
	$order=get_post($_GET['order_id']);
?>
	<h3><?php _e('Open Orders', 'speedy') ?></h3>
	<?php if($order->post_author==$user_id){
		 ?>
			<div class="order-delivered text-center">
				<div class="order-no">
				<span>
				<?php _e('Order', 'speedy'); ?>#<?php echo $_GET['order_id'] ?>
				</span>
				</div>
				
				<div class="revision-req-text">
				<span><?php _e('Request a Revision', 'speedy'); ?></span>
				</div>
				
				<div class="spacer10"></div>
				<div class="order-delivered-text">
					<h5><?php _e('What do you want us to revise? Pleaes write us a brief note about revision.', 'Speedy') ?></h5>
					
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="conv-message-box">
						<form action="" method="post" enctype="multipart/form-data" class="revision-form">
							<textarea name="requestMessage" id="requestMessage" placeholder="Revision Notes" required></textarea>
							<div class="buttons">
								<label id="#attach"><i class="fas fa-paperclip"></i>
								<input type="file" name="attachment" id="attachment">
								</label>
								<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
								<input type="hidden" name="user_name" id="user_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
								<button type="submit" name="send_message" id="send_message_button"><i class="fas fa-paper-plane"></i></button>
							</div>
						</form>
						<div id="responseMessage" class="text-center"></div>
						</div>
					</div>
				</div>

	<?php }else{ ?> 
		<p><?php _e('No record found', 'speedy') ?></p>
	<?php }?>
<?php  } ?>
</div>