<div class="spacer30"></div>
<div class="container-fluid open-orders">

<?php
if(isset($_GET['order_id'])){ 
	$order=get_post($_GET['order_id']);
?>
	<h3><?php _e('Open Orders', 'speedy') ?></h3>
	<?php 
		if(get_post_meta($_GET['order_id'], 'order_status', true)=='delivered' || get_post_meta($_GET['order_id'], 'order_status', true)=='completed'){ ?>
			<div class="order-delivered text-center">
				<div class="order-no">
				<span>
				<?php _e('Order', 'speedy'); ?>#<?php echo $_GET['order_id'] ?>
				</span>
				</div>
				<div class="spacer35"></div>
				<div class="spacer10"></div>
				<div class="order-delivered-text">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-complete.png">
					<h3><?php _e('YOUR ORDER IS READY!!!', 'Speedy') ?></h3>
					<p>Please download and review all files.<br>
						If everything looks right, please mark<br>
						this order as completed.<br>
						"Order will be marked as completed if there<br>
						was no revision to it in 48 after after the delivery"</p>
						<div class="spacer10"></div>
						<a href="<?php echo get_post_meta($_GET['order_id'], 'delivered_files', true) ?>" target="_blank">
							<button class="black_button"><?php _e('DOWNLOAD FILES', 'speedy') ?></button>
						</a>
						<div class="spacer20"></div>
						<?php if(get_post_meta($_GET['order_id'], 'order_status', true)!='completed'){ ?>
						<div class="completed-deliverd-buttons">
							<a href="<?php echo site_url() ?>/request-a-revision/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Request a Revision', 'speedy') ?></button> 
							</a>  <?php _e('OR', 'speedy') ?>
							<a href="<?php echo site_url() ?>/completed/?order_id=<?php echo $_GET['order_id'] ?>">
							<button class="black_button"><?php _e('Mark as Completed', 'speedy') ?></button>
							</a>
						</div>
						<?php } ?>
				</div>
		<?php }else{
		?>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th class="order-column"><?php _e('Order', 'speedy') ?>#<?php echo $order->ID ?></th>
					<th><?php _e('Order Type', 'speedy') ?></th>
					<th><?php _e('Order Date', 'speedy') ?></th>
					<th><?php _e('Delivery Date', 'speedy') ?></th>
					<th><?php _e('Order Status', 'speedy') ?></th>
					<th><?php _e('Chat', 'speedy') ?></th>
					<th class="last_column"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><?php echo get_post_meta($order->ID, 'order_type', true) ?></td>
					<td><?php echo  date("d M Y", strtotime(get_post_meta($order->ID, 'start_date', true))); ?></td>
					<td><?php echo date("d M Y", strtotime(get_post_meta($order->ID, 'delivery_date', true)));  ?></td>
					<td><?php echo get_post_meta($order->ID, 'order_status', true) ?></td>
					<td><?php echo "online/offline"; ?></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
		<div class="row">
			<div class="col-sm-2 detail-order">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png">
			</div>
			<div class="col-sm-10 order-details">
				<div> <span><?php _e('Colors', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'no_of_colors_wanted', true) ?></span></div>
				<div> <span><?php _e('Dimension', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'dimensions_of_artwork', true) ?></span></div>
				<div> <span><?php _e('Modifications', 'speedy') ?></span><span class="right"><?php echo get_post_meta($order->ID, 'modifications_details', true) ?></span></div>
				
				<div class="notes"> 
					<h5><?php _e('Notes', 'speedy') ?></h5>
					<p><?php echo get_post_meta($order->ID, 'notes', true) ?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-2">
				<div class="conversation">
					<h3><?php _e('Conversation', 'speedy') ?>:</h3>
					<?php
					
					// The Query
					$args = array(
						'post_type' => 'customers-messages',
						'posts_per_page'=>-1,
						"post_status"=>'any',
						'orderby' => 'ID',
						'order'   => 'ASC',
						'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'order_id',
									'value'    => $_GET['order_id'],
									'compare' => '=',
								),
								array(
								'relation' => 'OR',
								array(
									'key'     => 'sender',
									'value'   => $user_id,
									'compare' => '=',
								),
								array(
									'key'     => 'reciver',
									'value'   => $user_id,
									'compare' => '=',
								),
								),
								
								
							),
						);
					$messages_query = new WP_Query( $args );
					
					if ( $messages_query->have_posts() ) {
					?>
					<?php
						
						// The Loop 
						while ( $messages_query->have_posts() ) {
							$messages_query->the_post(); ?>
						<div class="conversation_message">
						 
						<?php if(get_post_meta(get_the_ID(), 'message_type', true)=='revision'){ ?>
							<div class="text-center">
							<span class="black_button"><?php _e('Revision Request', 'speedy') ?></span>
							</div>
							<div class="text-center revision">
							<b><?php echo strip_tags(get_the_content()) ?></b>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png"> 
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
							</div>
							<?php } ?>
						<?php }else{ ?> 
							<?php if(get_the_author_ID()==$user_id){ 
							echo get_user_meta($user_id, "first_name", true);
							}else{
								echo "Designer";
							} ?>:<?php echo strip_tags(get_the_content()) ?>
							<?php 
							if(get_post_meta(get_the_ID(), 'message_attach', true)){ ?>
							<div class="message-attachment">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-img.png"> 
							<span><a href="<?php echo get_post_meta(get_the_ID(), 'message_attach', true) ?>" target="_blank"><i class="fa fa-download"></i></a></span>
							</div>
						<?php } ?>
						<?php }?>
						
						
						</div>
							
					<?php }
					} 	?>
							
				</div>
				<div class="conv-message-box">
				<form action="" method="post" enctype="multipart/form-data" class="send-message-form">
					<textarea name="requestMessage" id="requestMessage" required></textarea>
					<div class="buttons">
						<label id="#attach"><i class="fas fa-paperclip"></i>
						<input type="file" name="attachment" id="attachment">
						</label>
						<input type="hidden" name="order_id" id="order_id" value="<?php echo $_GET['order_id'] ?>">
						<input type="hidden" name="user_name" id="user_name" value="<?php echo get_user_meta($user_id, 'first_name', true) ?>">
						<button type="submit" name="send_message" id="send_message_button"><i class="fas fa-paper-plane"></i></button>
					</div>
				</form>
				</div>
			</div>
		</div>
		<?php } //order not delivered ?>
	
<?php  } ?>
</div>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!--admin css-->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/speedy_admin.css' ?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<script src="<?php echo get_stylesheet_directory_uri().'/js/script.js' ?>"></script>