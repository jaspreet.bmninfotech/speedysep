<div class="spacer20"></div>
<div class="container-fluid ">
	
		<div class="expense-form">
			<div class="expense-form-header"></div>
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="form-horizontal">
			<div class="row">
				<div class="col-sm-2 text-right"><?php  _e('Date', 'speedy')  ?><span class="required">*</span></div>
				<div class="col-sm-10"><input type="date" class="form-control" name="from_date"></div>
			</div>
			<div class="row">
				<div class="col-sm-2 text-right"><?php  _e('Description', 'speedy')  ?><span class="required">*</span></div>
				<div class="col-sm-10"><input type="text" class="form-control" name="description"></div>
			</div>
			<div class="row">
				<div class="col-sm-2 text-right"><?php  _e('Paid by', 'speedy')  ?><span class="required">*</span></div>
				<div class="col-sm-10"><input type="text" class="form-control" name="paid_by"></div>
			</div>
			<!--items -->
			<div class="cost_div">
			<div class="cost_inner" id='div_1'>
				<div class="row">
					<div class="col-sm-2 text-right"><?php  _e('Items', 'speedy')  ?><span class="required">*</span></div>
					<div class="col-sm-2"><input type="date" class="form-control" name="item[date][]"></div>
					<div class="col-sm-2"><input type="text" class="form-control" name="item[item][]" placeholder="item"></div>
					<div class="col-sm-2"><input type="text" class="form-control" name="item[desc][]" placeholder="Description/Department"></div>
					<div class="col-sm-2"><input type="number" class="form-control each_cost" name="item[cost][]" placeholder="Unit Cost"></div>
					<div class="col-sm-2"><i class="fa fa-plus add-new-cost"></i></div>
				</div>
			</div>
			</div>
			
			<div class="row">
				<div class="col-sm-2 text-right"><?php  _e('Total', 'speedy')  ?><span class="required">*</span></div>
				<div class="col-sm-8"><input type="number" class="form-control expense_total" name="total"></div>
				<div class="col-sm-2"> <a href="#" class="calculate_cost">Calculate Total Cost</a></div>
			</div>
			
			
			<div class="row">
				<div class="col-sm-2 text-right"></div>
				<input type="hidden" name="action" value="add_expense">
				<div class="col-sm-10"><button type="submit" name="submit" class="black_button" >Submit</button></div>
			</div>
			</form>
		</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
		$('.add-new-cost').on('click', function(e){
			
			 // Finding total number of cost_inners added
			  var total_cost_inner = $(".cost_inner").length;
			 
			  // last <div> with cost_inner class id
			  var lastid = $(".cost_inner:last").attr("id");
			  var split_id = lastid.split("_");
			  var nextindex = Number(split_id[1]) + 1;

			   // Adding new div container after last occurance of cost_inner class
			   $(".cost_inner:last").after("<div class='cost_inner' id='div_"+ nextindex +"'></div>");
			 
			   // Adding cost_inner to <div>
			   //$("#div_" + nextindex).append("<input type='text' placeholder='Enter your skill' id='txt_"+ nextindex +"'>&nbsp;<span id='remove_" + nextindex + "' class='remove'>X</span>");
			 
			  
			  
			 $("#div_" + nextindex).append('<div class="cost_inner" id="div_' + nextindex + '"> <div class="row"> <div class="col-sm-2 text-right"></div> <div class="col-sm-2"><input type="date" class="form-control" name="item[date][]"></div> <div class="col-sm-2"><input type="text" class="form-control" name="item[item][]" placeholder="item"></div> <div class="col-sm-2"><input type="text" class="form-control" name="item[desc][]" placeholder="Description/Department"></div> <div class="col-sm-2"><input type="number" class="form-control each_cost" name="item[cost][]" placeholder="Unit Cost"></div><div class="col-sm-2"><i class="fa fa-remove remove-cost remove" id="remove_' + nextindex + '"></i></div></div></div>');
		});
		
		
		// Remove element
		 $('.form-horizontal').on('click','.remove',function(){
		 
		  var id = this.id;
		  var split_id = id.split("_");
		  var deleteindex = split_id[1];

		  // Remove <div> with id
		  $("#div_" + deleteindex).remove();

		 }); 
	
	$('.calculate_cost').on('click', function(e){
		e.preventDefault();
	var inputs = $(".each_cost");	 
	var totalCost=0;
	for(var i = 0; i < inputs.length; i++){
		totalCost=totalCost + + ($(inputs[i]).val());
	}
	$('.expense_total').val(parseInt(totalCost));
	});
	
    });
</script>