<!DOCTYPE html>
<?php 
global $wpdb;
$memberships = $wpdb->get_results("SELECT * FROM $wpdb->memberships");

$month= [];$year=[];
foreach ($memberships as $key => $value) {

    if($value->frequency == 'M')
    {
      if(strtolower($value->name) == 'bronze')
      {
          $month['bronze'] = $value;
      }
      if(strtolower($value->name) == 'silver')
      {
          $month['silver'] = $value;
      }if(strtolower($value->name) == 'gold')
      {
          $month['gold'] = $value;
      }
      if(strtolower($value->name) == 'custom')
      {
          $month['custom'] = $value;
      }
       
    }
    if($value->frequency == 'Y')
    {
        if(strtolower($value->name) == 'bronze')
        {
            $year['bronze'] = $value;
        }
        if(strtolower($value->name) == 'silver')
        {
            $year['silver'] = $value;
        }if(strtolower($value->name) == 'gold')
        {
            $year['gold'] = $value;
        }
        if(strtolower($value->name) == 'custom')
        {
            $year['custom'] = $value;
        }
    }
}
/**
 * Template Name: Pricing Landing page
 *
 */
 ?>
<?php
  $show_status = showselectedPlan();
?> 
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->

<?php wp_head(); ?>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet"> 


<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>	


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
<div class="headerarea">
	<div class="container">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/logo.png" alt="" /></a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="https://speedysep.com/pricing/">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                          <li><a class="" href="<?php
                          $show_status = showselectedPlan();
                         if($show_status->status == 'Active'){
                          echo home_url().'/user-dashboard'; }else{
                              echo home_url().'/membership';
                         } ?>">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                        <li class="tryit"><a class="" href="<?php
                          $show_status = showselectedPlan();
                         if($show_status->status == 'Active'){echo home_url().'/user-dashboard'; }else{
                              echo home_url().'/membership';
                         } ?>">Try It Free</a></li>
                        <?php } ?> 
                     
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon.png" alt="">
                </div>
            </div>
            <div class="floatingnav">
                <div class="mobilemenuheading">
                    <div class="mobilelogo">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/mobile-logo.png" alt="">
                    </div>
                    <div class="closemenu">
                        <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon-close.png" alt=""></a>
                    </div>
                </div>
                <div class="menubox">
                    <ul>
                       	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                        <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                        <?php }else{ ?> 
                         <li><a class="" href="<?php
                          $show_status = showselectedPlan();
                         if($show_status->status == 'Active'){echo home_url().'/user-dashboard'; }else{
                              echo home_url().'/membership';
                         } ?>">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                        <li class="tryit"><a class="" href="<?php
                          $show_status = showselectedPlan();
                         if($show_status->status == 'Active'){echo home_url().'/user-dashboard'; }else{
                              echo home_url().'/membership';
                         } ?>">Try It Free</a></li>
                        <?php } ?> 
                     
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="toparea" style="background: #48C8FB;">
    
<div class="container">
        <div class="topinfo">
            <div style="padding-top:20px;padding-bottom:20px;">
            <h1 style="padding-bottom:0px;">SIMPLE AFFORDABLE PRICING</h1>
            <h2 style="font-size:30px;font-weight:bold;">TRY IT FREE, CANCEL ANYTIME</h2>
            

            <div class="tab1">
                <div class="mrg">
                    <button class="tablinks active" onclick="openCity(event, 'month')">Monthly</button>
                    <button class="tablinks year" onclick="openCity(event, 'year')">Yearly</button>
                </div>
            </div>
        </div>

           
        </div>
    </div>

</div>


<div id="month" class="tabcontent" style="display: block">
   

<div class="snip1214 month_wise">
    <!-- <h2 class="membr">Membership</h2> -->
  <div class="plan">
    <?php
        if(array_key_exists('bronze', $month) && $month['bronze'] != null) 
        {
            $bmem_id = $month['bronze']->mem_ship_id;
            $name = $month['bronze']->name;
            $amt = $month['bronze']->amount;
            $getamt = explode('.', $amt);
            $withoutdecbrz = $getamt[0];
            $withdecbrz = $getamt[1];
            $durat = $month['bronze']->frequency;
        }else{
            $bmem_id = 1;
            $name = 'BRONZE';
            $amt = '';
            $withoutdecbrz = '';
            $withdecbrz = '';
            $durat = $month['bronze']->frequency;
        }
    ?>
    <div class="plan-cost "><p class="mbronze_name">BRONZE</p>
        <sup class="dollr_99">$</sup><span class="plan-price m_bronze"><?php echo $withoutdecbrz ; ?></span><sup class="sup_99 m_brz_dec"><?php echo $withdecbrz ;?></sup>

        <div class="pln_type">PER USER/MONTH</div>
        <?php if(!is_user_logged_in()){ ?>
         <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $bmem_id ; ?>,<?php echo $amt; ?>,'<?php echo $durat ?>')">Try it free</a></h5>  
         <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                 <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>

              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $bmem_id ; ?>,<?php echo $amt; ?>,'<?php echo $durat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?>  
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>2 Mockups</li>
      <li ><i class="ion-checkmark"> </i>$9/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design</span></span> </li>
      <li><i class="ion-checkmark"> </i>Standard Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </span></span></li>
      <li><i class="ion-checkmark"> </i>Artwork Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trail</li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>
  <div class="plan">
    
    <?php
        if(array_key_exists('silver', $month) && $month['silver'] != null) 
        {
            $msmem_id = $month['silver']->mem_ship_id;
            $msname = $month['silver']->name;
            $msamt = $month['silver']->amount;
            $getsamt = explode('.', $msamt);
            $sil_withoutdecbrz = $getsamt[0];
            $sil_withdecbrz = $getsamt[1];
            $sdurat = $month['silver']->frequency;
        }else{
            $msmem_id = 2;
            $msname = 'SILVER';
            $msamt = '';
            $sil_withoutdecbrz = '';
            $sil_withdecbrz = '';
            $sdurat = $month['silver']->frequency;
        }
    ?>
    <div class="plan-cost"><p class="msilver_name">SILVER</p><sup class="dollr_99">$</sup><span class="plan-price m_silver"><?php echo $sil_withoutdecbrz ;?></span><sup class="sup_99 m_sl_dec"><?php echo $sil_withdecbrz ?></sup>
         <div class="pln_type">PER USER/MONTH</div>
         <?php if(!is_user_logged_in()){ ?>
          <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $msmem_id ; ?>,<?php echo $msamt; ?>,'<?php echo $sdurat ; ?>')">Try it free</a></h5>
           <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                 <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color popup-with-form"   onclick="savemembershipToUser(this,<?php echo $msmem_id ; ?>,<?php echo $msamt; ?>,'<?php echo $sdurat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?>
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>6 Mockups</li>
      <li><i class="ion-checkmark"> </i>$8/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Halftones</li>
      <li><i class="ion-checkmark"> </i>Standard Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Art Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trial</li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>

  <!-- featured -->
  <div class="plan featured">
    <?php
        if(array_key_exists('gold', $month) && $month['gold'] != null) 
        {
            $mgmem_id = $month['gold']->mem_ship_id;
            $mgname = $month['gold']->name;
            $gamt = $month['gold']->amount;
            $getgamt = explode('.', $gamt);
            $gld_withoutdecbrz = $getgamt[0];
            $gld_withdecbrz = $getgamt[1];
            $gdurat = $month['gold']->frequency;
        }else{
            $mgmem_id = 3;
            $mgname = 'GOLD';
            $gamt = '';
            $gld_withoutdecbrz = '';
            $gld_withdecbrz = '';
            $gdurat = $month['gold']->frequency;
        }
    ?>
    <p class="most_pop_plan">MOST POPULAR</p>
    <div class="plan-cost"><p class="feat mgold_name">GOLD</p><sup class="dollr_99">$</sup><span class="plan-price m_gold "><?php echo $gld_withoutdecbrz ;?></span><sup class="sup_99 m_gld_dec"><?php echo  $gld_withdecbrz ;?></sup>
         <div class="pln_type">PER USER/MONTH</div>
          <?php if(!is_user_logged_in()){ ?>
          <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $mgmem_id ; ?>,<?php echo $gamt; ?>,'<?php echo $gdurat ; ?>')">Try it free</a></h5>
          <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                  <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $mgmem_id ; ?>,<?php echo $gamt; ?>,'<?php echo $gdurat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?>
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>14 Mockups</li>
      <li><i class="ion-checkmark"> </i>$7/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design </br>+ </br> Premium Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Halftones</li>
      <li><i class="ion-checkmark"> </i>Conceptual Design <span class="badge badge-dark tooltip">? <span class="tooltiptext">Your sketch or idea into a unique design.                                                           </span></span></li>
      <li><i class="ion-checkmark"> </i>Priority Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design </br>+ </br> Premium Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Artwork Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design </br>+ </br> Premium Design </span></span></li>
      <li><i class="ion-checkmark"> </i>5 Video Mockups <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design </br>+ </br> Premium Design </span></span></li>
      <li><i class="ion-checkmark"> </i>Order Rollover <span class="badge badge-dark tooltip">? <span class="tooltiptext">1 un-used order rollover to the next month</span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trail</li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>
  <div class="plan under_tab">
    <?php
        if(array_key_exists('custom', $month) && $month['custom'] != null) 
        {
            $mcmem_id = $month['custom']->mem_ship_id;
            $mcname = $month['custom']->name;
            $camt = $month['custom']->amount;
            $getcamt = explode('.', $camt);
            $cus_withoutdecbrz = $getcamt[0];
            $cus_withdecbrz = $getcamt[1];
            $cdurat = $month['custom']->frequency;
        }else{
            $mcmem_id = 8;
            $mcname = 'CUSTOM';
            $camt = '';
            $cus_withoutdecbrz = '';
            $cus_withdecbrz = '';
            $cdurat = $month['custom']->frequency;
        }
    ?>
    <div class="plan-cost"><p class="custom_plan">CUSTOM</p>

        <?php if(!is_user_logged_in()){ ?>
          <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $mcmem_id ; ?>,<?php echo $camt; ?>,'<?php echo $cdurat ; ?>')">Try it free</a></h5>
          <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                  <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $mcmem_id ; ?>,<?php echo $camt; ?>,'<?php echo $cdurat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?>
    </div>
    <div class="cus_txt">
            <p>Need a more </br> personalized solution?</br> Contact us</p>
    </div>

  </div>
</div>


</div>


<div id="year" class="tabcontent">
  

<div class="snip1214 year_wise">

    <!-- <h2 class="membr">Membership</h2> -->
  <div class="plan">
    <?php
        if(array_key_exists('bronze', $year) && $year['bronze'] != null) 
        {
            $bymem_id = $year['bronze']->mem_ship_id;
            $ybname = $year['bronze']->name;
            $year_bamt = $year['bronze']->amount;
            $getYearBAmt = explode('.', $year_bamt);
            $year_bwithoutdecbrz = $getYearBAmt[0];
            $year_bwithdecbrz = $getYearBAmt[1];
            $bydurat = $year['bronze']->frequency;
        }else{
            $bymem_id = 4;
            $ybname = 'BRONZE';
            $year_bamt = '';
            $year_bwithoutdecbrz = '';
            $year_bwithdecbrz = '';
            $bydurat = $year['bronze']->frequency;
        }
    ?>
    <div class="plan-cost"><p class="ybronze_name">BRONZE</p><sup class="dollr_99">$</sup><span class="plan-price y_bronze"><?php echo $year_bwithoutdecbrz; ?></span><sup class="sup_99 y_brz_dec"><?php echo $year_bwithdecbrz ; ?></sup>

        <div class="pln_type">PER USER/MONTH</div>
        <?php if(!is_user_logged_in()){ ?>
         <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $bymem_id ; ?>,<?php echo $year_bamt; ?>,'<?php echo $bydurat ; ?>')">Try it free</a></h5>  
          <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                  <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $bymem_id ; ?>,<?php echo $year_bamt; ?>,'<?php echo $bydurat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?>  
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>2 Mockups</li>
      <li ><i class="ion-checkmark"> </i>$9/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design</span></span> </li>
      <li><i class="ion-checkmark"> </i>Standard Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </span></span></li>
      <li><i class="ion-checkmark"> </i>Artwork Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trail</li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>
  <div class="plan">
    <?php
        if(array_key_exists('silver', $year) && $year['silver'] != null) 
        {
            $ysmem_id = $year['silver']->mem_ship_id;
            $ysname = $year['silver']->name;
            $year_samt = $year['silver']->amount;
            $getYearSAmt = explode('.', $year_samt);
            $year_swithoutdecbrz = $getYearSAmt[0];
            $year_swithdecbrz = $getYearSAmt[1];
            $ysdurat = $year['silver']->frequency;
        }else{
            $ysmem_id = 5;
            $ysname = 'SILVER';
            $year_samt = '';
            $year_swithoutdecbrz = '';
            $year_swithdecbrz = '';
            $ysdurat = $year['silver']->frequency;
        }
    ?>
    <div class="plan-cost"><p class="ysilver_name">SILVER</p><sup class="dollr_99">$</sup><span class="plan-price y_silver"><?php echo $year_swithoutdecbrz; ?></span><sup class="sup_99 y_sl_dec"><?php echo $year_swithdecbrz; ?></sup>
         <div class="pln_type">PER USER/MONTH</div>
         <?php if(!is_user_logged_in()){ ?>
          <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $ysmem_id ; ?>,<?php echo $year_samt; ?>,'<?php echo $ysdurat ; ?>')">Try it free</a></h5>
          <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                  <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $ysmem_id ; ?>,<?php echo $year_samt; ?>,'<?php echo $ysdurat ?>')">Try it free</a></h5> 
              <?php } ?>
         <?php } ?> 
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>6 Mockups</li>
      <li><i class="ion-checkmark"> </i>$8/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Halftones</li>
      <li><i class="ion-checkmark"> </i>Standard Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Art Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trial</li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
      <li><i class="ion-checkmark"> </i></li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>

  <!-- featured -->
  <div class="plan featured">
    <?php
        if(array_key_exists('gold', $year) && $year['gold'] != null) 
        {
            $ygmem_id = $year['gold']->mem_ship_id;
            $ygname = $year['gold']->name;
            $year_gamt = $year['gold']->amount;
            $getYearGAmt = explode('.', $year_gamt);
            $year_gwithoutdecbrz = $getYearGAmt[0];
            $year_gwithdecbrz = $getYearGAmt[1];
            $ygdurat = $year['gold']->frequency;
        }else{
            $ygmem_id = 6;
            $ygname = 'gold';
            $year_gamt = '';
            $year_gwithoutdecbrz = '';
            $year_gwithdecbrz = '';
            $ygdurat = $year['gold']->frequency;
        }
    ?>
    <p class="most_pop_plan">MOST POPULAR</p>
    <div class="plan-cost feat"><p class="feat ygold_name">GOLD</p><sup class="dollr_99">$</sup><span class="plan-price y_gold"><?php echo $year_gwithoutdecbrz; ?></span><sup class="sup_99 y_gld_dec"><?php echo $year_gwithdecbrz ; ?></sup>
         <div class="pln_type">PER USER/MONTH</div>
        <?php if(!is_user_logged_in()){ ?>
          <h5><a class="color popup-with-form"  href="#popup-membership" onclick="getmemberId(<?php echo $ysgmem_id ; ?>,<?php echo $year_gamt; ?>,'<?php echo $ygdurat ; ?>')">Try it free</a></h5>
          <?php }else{ ?>
              <?php if($show_status != null && $show_status->status == 'Active')
              { ?>
                  <h5><a class="color" href="<?php echo home_url().'/membership'; ?>" >Try it free</a></h5>
              <?php }else{ ?>
              <h5><a class="color"   onclick="savemembershipToUser(this,<?php echo $ysgmem_id ; ?>,<?php echo $year_gamt; ?>,'<?php echo $ygdurat ?>')">Try it free</a></h5> 
              <?php } ?>
        <?php } ?> 
    </div>
    <ul class="plan-features">
      <li><i class="ion-checkmark"> </i>14 Mockups</li>
      <li><i class="ion-checkmark"> </i>$7/Order <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design </br>+ </br> Premium Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Halftones</li>
      <li><i class="ion-checkmark"> </i>Conceptual Design <span class="badge badge-dark tooltip">? <span class="tooltiptext">Your sketch or idea into a unique design.                                                           </span></span></li>
      <li><i class="ion-checkmark"> </i>Priority Support <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Artwork Complexity <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>5 Video Mockups <span class="badge badge-dark tooltip">? <span class="tooltiptext">Basic Design </br>+ </br> Standard Design</span></span></li>
      <li><i class="ion-checkmark"> </i>Order Rollover <span class="badge badge-dark tooltip">? <span class="tooltiptext">1 un-used order rollover to the next month</span></span></li>
      <li><i class="ion-checkmark"> </i>Free Trail</li>
    </ul>
    <!-- <div class="plan-select"><a href="">Select Plan</a></div> -->
  </div>
  <div class="plan under_tab">
    
    <div class="plan-cost"><p class="custom_plan">CUSTOM</p>


         <h5><a class="color popup-with-form cust"  href="#popup-membership">Try it free</a></h5>
    </div>
    <div class="cus_txt">
            <p>Need a more </br> personalized solution?</br> Contact us</p>
    </div>

  </div>
</div>

</div>

<div class="mn_sec" style="clear: both;">
    
    <div class="mn_sec_cont">

            <div class="mn_first">
                
                <div class="mn_first_img">
                             <img src="<?php bloginfo('template_directory')?>/images/play-btn.png" class="small-ic">
                </div>

                <div class="mn_first_cont">
                    <ul class="">
                        <li>7 Day money back guarantee.</li>
                        <li>100% Satisfactied or your money back.</li>
                        <li>Cancel anytime</li>
                        <li>First order is free</li>
                    </ul>
                </div>
            </div>
            <div class="mn_second">

                    <input type="text" id="user_password" class="user_password" placeholder="Enter Your Email...">
                    <button id="submit_user" clas="submit_user">START FREE TRIAL</button>

            </div>
            <div class="mn_third">
                
                <img src="<?php bloginfo('template_directory')?>/images/premium-example-popup.jpg" class="img-round">
                <img src="<?php bloginfo('template_directory')?>/images/premium-example-popup.jpg" class="img-round">
                <img src="<?php bloginfo('template_directory')?>/images/premium-example-popup.jpg" class="img-round">
                <img src="<?php bloginfo('template_directory')?>/images/premium-example-popup.jpg" class="img-round">

            </div>
    </div>
</div>


<div class="every_plan">
    
    <div class="rw">
        <div class="pan_feat">
            <h2>Included with every plan.</h2>
            <p>A world of features. A wold of possibilities.</p>
        </div>
    </div>
    <div class="rw">
            <div class="features"><span>Free Trial</span><span>Quick, helpful supports</span></div>
            <div class="features"><span>Chat with your designer</span><span>99.9% uptime</span><span>Upgrade/downgrade anytime</span></div>
    </div>

</div>

<div class="pricingarea compare" style="clear:both;">
    <div class="container">
        <h1 class="compare_plans">Compare the Plans.</h1>
        <div class="">
            
            
            <div class="gib1">
                <div class="p-1">&nbsp;</div>
                <div class="p-2">&nbsp;</div>

                <div class="p-3"> &nbsp;</div>
                <div class="p-4">
                    <p class="most_popular">MOST POPULAR</p>
        
                </div>
            </div>


            <div class="gib">
                <div class="p-1">&nbsp;</div>
                <div class="p-2 back_white">
                    <h4>BASIC</h4>
                    
                </div>
                <div class="p-3 back_white">
                    <h4>STANDARD</h4>
                    
                </div>
                <div class="p-4 back_white">
                    
                    <h4>PREMIUM</h4>
                    
                </div>
            </div>

         

             <div class="bg-black">
              
            </div>

            <div class="chartlist evenlist">
                <div class="price-col-1 paddtop"><p>SOURCE FILE</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
                <div class="price-col-1 paddtop"><p>HALFTONES</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
                <div class="price-col-1"><p>OTHER FORMATS</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
                <div class="price-col-1 paddtop"><p>CONCEPTUAL ARTWORK</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
                <div class="price-col-1 paddtop"><p>VIDEO MOCKUPS</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>

            <div class="chartlist">
                <div class="price-col-1 paddtop"><p>ORDER ROLLOVER</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
                <div class="price-col-1 paddtop"><p>MODIFICATIONS</p></div>
                <div class="price-col-2"><strong>5 MODIFICATIONS</strong></div>
                <div class="price-col-3"><strong>15 MODIFICATIONS</strong></div>
                <div class="price-col-4"><strong>UNLIMITED MODIFICATIONS</strong></div>
            </div>
            <div class="chartlist ">
                <div class="price-col-1 paddtop"><p>MOCKUP</p></div>
                <div class="price-col-2">2 MOCKUPS</div>
                <div class="price-col-3 extraheight">6 MOCKUPS</div>
                <div class="price-col-4 extraheight">14 MOCKUPS</div>
            </div>
           
            <div class="chartlist evenlist">
                <div class="price-col-1 paddtop"><p>DELIVERY TIME</p></div>
                <div class="price-col-2"><strong>48 HOURS</strong></div>
                <div class="price-col-3"><strong>36 HOURS</strong></div>
                <div class="price-col-4"><strong>24 HOURS</strong></div>
            </div>
            <div class="chartlist">
                <div class="price-col-1 paddtop"><p>SUPPORT</p></div>
                <div class="price-col-2"><strong>STANDARD</strong></div>
                <div class="price-col-3"><strong>STANDARD</strong></div>
                <div class="price-col-4"><strong>PRIORITY</strong></div>
            </div>
        </div>

    <style type="text/css"> 
    
        .promoleft.strt h2 {
        font-size: 50px;
        line-height: 76px;
        }

    </style>

    <div class="promoarea" style="margin-top:40px;">
    <div class="container">
        <div class="promoinfo" style="background:#fff;">
            <div class="promoleft strt">
                <h2>Start your first design free today!</h2>
                
            </div>
            <div class="promoright">
                <a class="popup-with-form" href="#popupcontent">Try It Free</a>
            </div>
        </div>
    </div>
</div>

       
    </div>
</div>


<div class="toparea">
    <div class="container">
        <div class="topinfo">
            <a class="membership" href="#popup-membership" onclick="getmemberId(1,9.99,'M')">Start Your Journey</a>
        </div>
    </div>
</div>

<div class="toparea">
	<div class="container">
    	<div class="topinfo">
        	<h1>How much does speedysep cost</h1>
            <h2>Start your first design free today!</h2>
            <p>Use coupon code <span>freespeedy</span></p>
            <a class="popup-with-form" href="#popupcontent">Try It Free</a>
        </div>
    </div>
</div>
<div class="pricingarea">
	<div class="container">
    	<h1>Custom Vector Design</h1>
    	<div class="pricechart">
        	<div class="pricechartheader">
            	<div class="price-col-1">&nbsp;</div>
                <div class="price-col-2"><strong>BASIC</strong></div>
                <div class="price-col-3"><strong>STANDARD</strong></div>
                <div class="price-col-4"><strong>PREMIUM</strong></div> 
            </div>
            <div class="chartlist big">
            	<div class="price-col-1"><p>CHOOSE <br> YOUR PLAN</p></div>
                <div class="price-col-2">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>9</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
                <div class="price-col-3">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>19</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
                <div class="price-col-4">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>29</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>ILLUSTRATOR AI</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>PNG</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1"><p>HALFTONES</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>OTHER FORMATS</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>CONCEPTUAL ARTWORK</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>MODIFICATIONS</p></div>
                <div class="price-col-2"><strong>3</strong></div>
                <div class="price-col-3"><strong>6</strong></div>
                <div class="price-col-4"><strong>9</strong></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>MOCKUP</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3 extraheight">1</div>
                <div class="price-col-4 extraheight">3</div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>DETAILS</p></div>
                 <div class="price-col-2"><h6><a class="color popup-with-form" href="#popupcontentbasic">Example</a></h6></div>
                <div class="price-col-3"><h6><a class="color popup-with-form" href="#popupcontentstandard">Example</a></h6></div>
                <div class="price-col-4"><h6><a class="color popup-with-form" href="#popupcontentpremium">Example</a></h6></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>DELIVERY</p></div>
                <div class="price-col-2"><strong>48 HOURS</strong></div>
                <div class="price-col-3"><strong>48 HOURS</strong></div>
                <div class="price-col-4"><strong>48 HOURS</strong></div>
            </div>
        </div>
        <div class="note">
        	<p>Get 24-hour rush turnaround for only $10!</p>
        </div>
    </div>
</div>
<div class="pricingarea">
	<div class="container">
    	<h3>Color Separations</h3>
    	<div class="pricechart">
        	<div class="pricechartheader">
            	<div class="price-col-1">&nbsp;</div>
                <div class="price-col-2"><strong>BASIC</strong></div>
                <div class="price-col-3"><strong>STANDARD</strong></div>
                <div class="price-col-4"><strong>PREMIUM</strong></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1"><p>CHOOSE <br> YOUR PLAN</p></div>
                <div class="price-col-2">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>9</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
                <div class="price-col-3">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>19</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
                <div class="price-col-4">
                	<h4><span class="price_symbol" style="font-size: 25px;">$</span>29</h4>
                    <h5><a class="color popup-with-form"  href="#popupcontent">Try it free</a></h5>
                </div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1"><p>ILLUSTRATOR AI</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1"><p>PNG</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1"><p>HALFTONES</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1"><p>OTHER FORMATS</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1"><p>CONCEPTUAL ARTWORK</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/check-icon.png" alt="" /></span></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>MODIFICATIONS</p></div>
                <div class="price-col-2"><strong>3</strong></div>
                <div class="price-col-3"><strong>6</strong></div>
                <div class="price-col-4"><strong>9</strong></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>MOCKUP</p></div>
                <div class="price-col-2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/close-icon.png" alt="" /></span></div>
                <div class="price-col-3 extraheight"><strong>1</strong></div>
                <div class="price-col-4 extraheight"><strong>3</strong></div>
            </div>
            <div class="chartlist">
            	<div class="price-col-1 paddtop"><p>DETAILS</p></div>
                <div class="price-col-2"><h6><a class="color popup-with-form" href="#popupcontentbasic">Example</a></h6></div>
                <div class="price-col-3"><h6><a class="color popup-with-form" href="#popupcontentstandard">Example</a></h6></div>
                <div class="price-col-4"><h6><a class="color popup-with-form" href="#popupcontentpremium">Example</a></h6></div>
            </div>
            <div class="chartlist evenlist">
            	<div class="price-col-1 paddtop"><p>DELIVERY</p></div>
                <div class="price-col-2"><strong>48 HOURS</strong></div>
                <div class="price-col-3"><strong>48 HOURS</strong></div>
                <div class="price-col-4"><strong>48 HOURS</strong></div>
            </div>
        </div>
        <div class="note">
        	<p>Get 24-hour rush turnaround for only $10!</p>
        </div>
    </div>
</div>
<div class="featuredarea">
	<div class="container">
    	<div class="featuredinfo">
        	<h3>All Plans Include</h3>
            <ul>
            	<li>Quick, helpful supports</li>
                <li>SSL security encryption</li>
                <li>Chat with your designer</li>
                <li>Upgrade/downgrade anytime</li>
                <li>No Software to install</li>
                <li>99.9% uptime</li>
            </ul>
        </div>
    </div>
</div>
<div class="promoarea">
	<div class="container">
    	<div class="promoinfo">
            <div class="promoleft">
            	<h2>Start your first design free today!</h2>
                <p>Use coupon code <span>freespeedy</span></p>
            </div>
            <div class="promoright">
                <a class="popup-with-form" href="#popupcontent">Try It Free</a>
            </div>  
        </div>
    </div>
</div>
<div class="faqarea">
	<div class="container">
    	<div class="faqinfo">
        	<h4>FAQ</h4>
            <div class="faqlist">
            	<h2>What do I need to get started?</h2>
                <p> simply signup, upload your concept (or reference) and choose a plan. Your file will be ready to download in 24-48 hours. </p>
            </div>
            <div class="faqlist">
            	<h2>What if I'm not satisfied?</h2>
                <p>All our services are always 100% money-back guaranteed.</p>
                <p>In the unlikely event of you (or your clients) being unhappy with the results, you will not be charged, and we will simply refund your money. </p>
            </div>
            <div class="faqlist">
            	<h2>How soon before I get something back?</h2>
                <p>We can turn things around in as little as 24 hours.</p>
            </div>
            
            <div class="faqlist">
                <h2> What are the file formats you provide?</h2>
                <p>  AI, EPS, PNG, JPG, PDF, DXF, SVG, DWG.</p>
            </div>
            
             <div class="faqlist">
                <h2> What type of color separation services you provide?</h2>
                <p>  We separate spot colors, simulation, and 4 color process.</p>
            </div>
            
            
              <div class="faqlist">
                <h2> Can you develop new concepts?</h2>
                <p> Yes! Just give us some directions and we will create a brand-new custom design.  </p>
            </div>
            
            
            
             <div class="faqlist">
                <h2> How many revisions you offer? </h2>
                <p>  Depending on the plan you choose, we offer up to 4 revisions before an additional charge. </p>
            </div>


            
            
            <div class="faqlist">
            	<h2>Who will be working on my artwork?</h2>
                <p>Our team is comprised of in-house designers who’ve completed thousands of artworks for screen printers around the country.</p>
                <p>We are experienced, motivated, and excited to help you with your graphic needs. </p>
                 
            </div>
        
            
            <div class="faqlist">
            	<h2>Do I need to pay a monthly membership?</h2>
                <p>No. You pay by the order. (we might add premium services in the future for members only) </p>
            </div>
        
        
        </div>
    </div>
</div>
<div class="footerarea">
	<div class="container">
    	<div class="footertop">
        	<div class="footertopleft">
            	<h2>Start your business <br> journey with SpeedySep</h2>
            </div>
            <div class="footertopright">
            	<a class="popup-with-form" href="#popupcontent">Try It Free</a>
            </div>
        </div>
    	<div class="footerinfo">
        	<div class="footerleft">
            	<div class="footerlogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/footer-logo.png" alt="" />
                </div>
            	<p>SpeedySep is risk-free with a 100% money-back guarantee. Simplify your screen printing process with SpeedySeps.</p>
            </div>
            <div class="footerright">
            	<div class="col-1">
                	<ul>
                    	<li><a href="https://speedysep.com/pricing">Pricings</a></li>
                        <li><a href="https://speedysep.com/speedy-sep-guide-for-vector-order/">Vectorizing</a></li>
                        <li><a href="https://speedysep.com/speedy-sep-guide-for-color-separation-order/">Color Separation</a></li>
						<?php if(!is_user_logged_in()){ ?>
						<li><a class="popup-with-form" href="#popupcontent" title="SignUp">SignUp</a></li>
						<?php }else{ ?> 
						<li><a href="<?php echo site_url() ?>/user-dashboard"  title="SignUp">SignUp</a></li>
						<?php } ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                    </ul>
                </div>
                <div class="col-1">
                	<ul>
                	    <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                    	<li><a href="https://speedysep.com/contact-us/">Contact Us</a></li>
                        <li><a href="https://speedysep.com/about-us/">About Us</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>
                        <li><a href="https://speedysep.com/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="https://speedysep.com/terms-and-conditions/">Terms of use</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyrightarea">
	<div class="container">
    	<div class="copyrightleft">
        	<ul>
            	<li><a href="https://twitter.com/sep_speedy" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/twitter-icon.png" alt="twitter" /></a></li>
                <li><a href="https://www.facebook.com/speedysep" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/facebook-icon.png" alt="facebook" /></a></li>
                <li><a href="https://www.instagram.com/speedysep/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/instagram-icon.png" alt="instagram" /></a></li>
                <li><a href="https://www.pinterest.com/speedysep/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/printerset-icon.png" alt="printerset" /></a></li>
            </ul>
        </div>
        <div class="copyrightright">
        	<p><a href="#">Sitemap</a> | <a href="#">Sitemap XML</a></p>
            <p>Copyright &copy; 2019 by <a href="<?php  site_url(); ?>">speedysep.com</a></p>
        </div>
    </div>
</div>
<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your first free order of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formlist">
						<input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="create your account">
					</div>
					
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>

<div style="display:none">
    <div id="popup-membership" class="popupouter">
        <div class="popupbox">
            <div class="contactform">
                <h1>Start your Journey with Speedysep</h1>
                <form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
                    <div class="formlist">
                        <input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
                        <input type="hidden" name="mem_id" id="pre_mem_id">
                        <input type="hidden" name="amount" id="pre_amount">
                        <input type="hidden" name="duration" id="pre_duration">
                    </div>
                    <div class="formlist">
                        <input name="" type="password" placeholder="Password" id="password" name="password" required >
                    </div>
                    <div class="formlist">
                        <input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
                    </div>
                    <div class="formsend">
                        <input name="" type="submit" id="submit" value="Create your account">
                    </div>
                </form>
                <br>
                <div class="registration-message"><p></p></div>
            </div>
        </div>
    </div>
</div>

<div style="display:none">
	<div id="popupcontentlogin" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Login to my Speedysep account</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="login-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
                        
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="Login to my account">
					</div>
					
					<div class="login-reset-pass" style="margin-top: 10px; text-align: right;">
						<p><a href="<?php echo site_url() ?>/wp-login.php?action=lostpassword" title="<?php _e('Reset Password', 'speedy') ?>" style="color:#fff;"><?php _e('Reset Password', 'speedy') ?></a></p>
					</div>
					
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>
<div style="display:none">
	<div id="popupcontentbasic" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/basic-example-popup.jpg" alt="basic-example" style="max-width: 100%;">
	</div>
</div>
<div style="display:none">
	<div id="popupcontentstandard" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/standard-example-popup.jpg" alt="standard-example" style="max-width: 100%;">
	</div>
</div>
<div style="display:none">
	<div id="popupcontentpremium" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/premium-example-popup.jpg" alt="premium-example" style="max-width: 100%;">
	</div>
</div>

<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.magnific-popup.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(".mobilemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '0'
			}, 300 );
		$(this).addClass("opened");
	});
	$(".closemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '-330px'
			}, 300 );
		$(this).removeClass("opened");
	});  
}); 

</script>


<script type="text/javascript">
    function getmemberId(id,amount,duration)
    {
      
        console.log('dfd');
        jQuery('#pre_mem_id').val('');
        jQuery('#pre_amount').val('');
        jQuery('#duration').val('');
        jQuery('#pre_mem_id').val(id);
        jQuery('#pre_amount').val(amount);
        jQuery('#pre_duration').val(duration);
    }

</script>
<script>
	jQuery('.popup-with-form').magnificPopup({
		type: 'inline',
	});
	jQuery('.popup-with-form-login').magnificPopup({
		type: 'inline',
	});

    jQuery('.membership').magnificPopup({
        type: 'inline'
    });

var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function($){
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
    if(jQuery('#pre_mem_id').val() == '')
    {
        var data = {
        'action': 'register_user',
        'full_name': jQuery('#full_name').val(),        
        'email': jQuery('#email').val(),
        'password':jQuery('#password').val(),
        
        };
    }else{
    	var data = {
    		'action': 'register_user',
    		'full_name': jQuery('#full_name').val(),		
    		'email': jQuery('#email').val(),
    		'password':jQuery('#password').val(),
            'mem_id' :  jQuery('#pre_mem_id').val(),
             'amount' :  jQuery('#pre_amount').val(),
             'duration' : jQuery('#pre_duration').val()

    	};
    }
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Account created successfully.');
				window.location = "<?php echo site_url() ?>/user-dashboard";
            }else if(response == 'loggedin and membership created'){
                jQuery('.registration-message').text('Account created successfully.');
                window.location = "<?php echo site_url() ?>/membership";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

//login form submit
$('.login-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'login_user',	
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Logging in...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='loggedin'){
				jQuery('.registration-message').text('Logging in...');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
					//setTimeout(function() {
					  window.location = "<?php echo site_url() ?>/user-dashboard";
					//}, 5000);
			}else if(response == 'logginMembership'){
         window.location = "<?php echo site_url() ?>/membership";
			}else {
				jQuery('.registration-message').html('<p class="error">Invalid Email/password.</p>');
			}
	});
});


$('#page_email_input').on('change', function(){
	$('#email').val($(this).val());
});

});
</script>
<script type="text/javascript">
    function openCity(evt, duration) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(duration).style.display = "block";
      evt.currentTarget.className += " active";
        var data = {
            'action': 'get_membership_price', 
            'duration': duration,
        };
        jQuery.getJSON(ajax_url, data, function(response) {

           // var res =  JSON.parse(response);
            // console.log(res.BRONZE);
            var len = Object.keys(response).length;
        
            if(len > 0 ){
                var bstr = response.BRONZE.amount;
                var brz = bstr.split(".");
                if(brz.length >1)
                {
                    var brzval = brz[0];
                    var brz_afterDec = brz[1];
                }else{
                    var brzval = brz[0];
                    var brz_afterDec = '';
                }
                var str = response.SILVER.amount;
                var res = str.split(".");
                if(res.length >1)
                {
                    var val = res[0];
                    var afterDec = res[1];
                }else{
                    var val = res[0];
                    var afterDec = '';
                }
                var gstr = response.GOLD.amount;
                var g_res = gstr.split(".");
                if(g_res.length >1)
                {
                    var g_val = g_res[0];
                    var g_afterDec = g_res[1];
                }else{
                    var g_val = g_res[0];
                    var g_afterDec = '';
                }
                if(duration == 'month')
                {
                    jQuery('#month').find('.m_bronze').html(brzval);  
                    jQuery('#month').find('.m_bronze').find('m_brz_dec').html(brz_afterDec);  
                    jQuery('#month').find('.mbronze_name').html(response.BRONZE.name); 
                  jQuery('#month').find('.m_silver').html(val);  
                  jQuery('#month').find('.m_silver').find('.m_sl_dec').html(afterDec);  

                  jQuery('#month').find('.msilver_name').html(response.SILVER.name);  
                  jQuery('#month').find('.mgold_name').html(response.GOLD.name);  
                  jQuery('#month').find('.m_gold').html(response.GOLD.amount); 
                  jQuery('#month').find('.m_gold').find('.m_gld_dec').html(g_afterDec); 
                }else if(duration == 'year')
                {

                  jQuery('#year').find('.y_bronze').html(brzval);  
                  jQuery('#year').find('.y_bronze').find('y_brz_dec').html(brz_afterDec);  
                  jQuery('#year').find('.ybronze_name').html(response.BRONZE.name);

                  jQuery('#year').find('.y_silver').html(val);  
                  jQuery('#year').find('.y_silver').find('.y_sl_dec').html(afterDec); 

                  jQuery('#year').find('.ysilver_name').html(response.SILVER.name);  
                  jQuery('#year').find('.ygold_name').html(response.GOLD.name);  
                  jQuery('#year').find('.y_gold').html(g_val);  
                  jQuery('#year').find('.y_gold').find('.y_gld_dec').html(g_afterDec);  
                }
            }
        });

}
</script>
<script type="text/javascript">
  function savemembershipToUser(e,id,amount,duration)
  {

    var data = {
        'action': 'save_new_membership',
        'mem_id' :  id,
        'amount' :  amount,
        'duration' : duration
      };
      // jQuery(e).prop('disabled', true);
      jQuery.post(ajax_url, data, function(response) {

        // jQuery(e).removeAttr("disabled");
        if(response=='membershipCreated'){
              window.location = "<?php echo site_url() ?>/membership";
          
        }else{
          
        }

      });
  }

</script>
<?php wp_footer(); ?>
</body>
</html>
