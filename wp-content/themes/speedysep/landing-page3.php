<!DOCTYPE html>
<?php 
/**
 * Template Name: Landing Page 3
 *
 */
 ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 

<head>
<?php do_action( 'wp_head' ); ?>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Speedy Sep</title>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->

<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/responsive.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="headerarea">
	<div class="header">
		<div class="headerleft">
			<div class="logo">
				<a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/logo.png" class="img-fluid" alt="Speedy Sep"></a>
			</div>
		</div>
		<div class="headerright">
			<a class="popup-with-form" href="#popupcontent">Start free trial</a>
		</div>
	</div>
</div>
<div class="printingarea">
	<h1>SIMPLIFY YOUR <br> SCREEN PRINTING PROCESS</h1>
	<div class="printinginfo">
		<div class="printingblock">
			<h3>VECTOR DRAWING</h3>
			<p>Pricing as low as $10/Order</p>
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/thumb01.png" class="img-fluid" alt="">
		</div>
		<div class="printingblock">
			<h3>COLOR SEPARATIONS</h3>
			<p>Pricing as low as $10/Order</p>
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/thumb02.png" class="img-fluid" alt="">
		</div>
	</div>
</div>
<div class="vectordrawingarea">
	<div class="drawingtitle">
		<h1>VECTOR DRAWING</h1>
	</div>
	<h4>WE TAKE YOUR FUZZY ART IMAGES AND TURN THEM TO A CLEAN AND PRINT READY VECTOR DESIGNS.</h4>
	<p>Login, choose a plan, and upload your design. <br> your artwork will be ready to download <br> in 24* hours with a 100% money-back guarantee.</p>
	<div class="fuzzyimagearea">
		<div class="fuzzyimage">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/fuzzy-image-pic.png" class="img-fluid" alt="">
			<span>FUZZY IMAGE</span>
		</div>
		<div class="drawarrow">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/arrow01.png" class="img-fluid" alt="">
		</div>
		<div class="vectorart">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/vector-img-pic.png" class="img-fluid" alt="">
			<span>VECTOR ART</span>
		</div>
		<div class="drawingtrialbutton">
			<a class="popup-with-form" href="#popupcontent">Start free trial</a>
		</div>
	</div>
</div>
<div class="colorseparationarea">
	<div class="drawingtitle">
		<h1>COLOR SEPARATION</h1>
	</div>
	<h4>WE TAKE YOUR ARTWORKS AND CREATE COLOR SEPARATIONS FOR SCREEN PRINTING</h4>
	<h5>CMYK, SPOT COLOR AND PROCESS SEPARATIONS</h5>
	<p>Login, choose a plan, and upload your design. <br> your artwork will be ready to download <br> in 24* hours with a 100% money-back guarantee.</p>
	<div class="fuzzyimagearea">
		<div class="originalatwork">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/original-atwork-pic.png" class="img-fluid" alt="">
			<span>ORIGINAL ARTWORK</span>
		</div>
		<div class="drawarrow">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/arrow01.png" class="img-fluid" alt="">
		</div>
		<div class="colorseperated">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/color-separeted-pic.png" class="img-fluid" alt="">
			<span>COLOR SEPARATED</span>
		</div>
		<div class="drawingtrialbutton colorseperationbutton">
			<a class="popup-with-form" href="#popupcontent">Start free trial</a>
		</div>
	</div>
</div>
<div class="clientarea">
	<div class="clientinfo">
		<p>Over 500 screen printers, both big and small, are growing their businesses with SpeedySep</p>
	</div>
	<div class="clientboxpc">
		<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logos.png" class="img-fluid" alt="">
	</div>
	<div class="clientboxmobile">
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo01.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo02.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo03.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo04.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo05.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo06.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo07.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo08.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/images/client-logo09.png" alt="">
		</div>
	</div>
</div>
<div class="contactarea">
	<h1>The Speedy Sep Guarantee</h1>
	<p>Color separation with SpeedySep is risk-free, <br> with no hidden fees or charges. <br> Your artwork will be processed by a team of professionals <br> with a 100% money-back guarantee. <br> Simplify your screen printing process with SpeedySep.</p>
	<h1>Contact Us</h1>
	<ul>
		<li>Phone: +1 954 470 2125</li>
		<li>Email: Speedysep@gmail.com</li>
	</ul>
</div>
<div class="footerarea">
	<div class="footerinfo">
		<p>USE COUPON CODE</p>
		<div class="cuponbox">
			<span>FREESPEEDY</span>
		</div>
		<h2>Start a free trial</h2>
		<div class="freetrial">
			<a class="popup-with-form" href="#popupcontent">Start free trial</a>
		</div>
	</div>
</div>
<div class="footerlink">
	<p><a href="<?php site_url(); ?>/terms-and-conditions/">Tems of Service</a> | <a href="<?php site_url(); ?>/privacy-policy/">Privacy Policy</a></p>
	<span>Copyright &copy; 2019 by speedysep.com</span>
</div>
<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formlist">
						<input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="create your account">
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.magnific-popup.js"></script>
<script>
	$('.popup-with-form').magnificPopup({
		type: 'inline',
	});

var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function($){
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name').val(),		
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Account create successfully.');
				window.location = "<?php echo site_url() ?>/user-dashboard";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

$('#page_email_input').on('change', function(){
	$('#email').val($(this).val());
});

});
</script>
<style>
@media only screen and (max-width: 767px) {
	.contactarea p br{
		display:none !important;		
	}
	.colorseparationarea p br{
		display:none;
	}
	.vectordrawingarea p br{
		display:none;
	}
}

</style>
</body>
</html>
