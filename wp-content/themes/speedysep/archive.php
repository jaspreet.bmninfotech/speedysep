<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<header class="front-page-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="logo-left">
				<?php $page_id=269; ?>
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#registrationModal" title="<?php echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text',true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text', true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="text-center site-logo hidden-xs">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="logo-right">
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#loginModal" title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
</header>
<div  class="container-fluid sep-home-buttons">
	<div class="btn-left"><a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/front-home.png"></a></div>
</div>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

				// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination(
				array(
					'prev_text'          => __( 'Previous page', 'twentysixteen' ),
					'next_text'          => __( 'Next page', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
				)
			);

			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
</div>
<footer class="front-page-footer">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="text-right">
				<ul class="nav navbar-nav">
				<?php
					$menu_name = 'speedysep-user-menu';
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menu_items = wp_get_nav_menu_items(9);
					foreach ( (array) $menu_items as $key => $menu_item ) { 
						$class='';
						$current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
						foreach($menu_item->classes as $item_class){
							$class.=$item_class." ";
						}
						$title = $menu_item->title;
						$url = $menu_item->url;
						if($menu_item->menu_item_parent>0){
							$menu_list .= '<li class="menu-sub-item ' . $current . '"><a href="'.$url.'">'.$title.' | </a></li>';
						}else{
						$menu_list .= '<li class="' . $current . '"><a href="'.$url.'">'.$title.' |</a></li>';
						}
					}
				echo $menu_list;
				?>
				<li class="copyright-text">SpeedySep &copy; <?php echo date('Y') ?></li>
				</ul>
			</div>
		</div>
		</div>
	</div>
</footer>
<?php get_footer(); ?>
