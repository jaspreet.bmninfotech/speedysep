<?php
/**
 * Template Name: Full width
 *
 */

get_header(); ?>
<!-- 
<header class="front-page-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="logo-left">
				<?php $page_id=269; ?>
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#registrationModal" title="<?php echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text',true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text', true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="text-center site-logo">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="logo-right">
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#loginModal" title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
</header>
-->

<div class="headerarea">
	<div class="container-header">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo home_url() ?>">
                	    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo.png" alt="logo">
                	</a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                       <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                     
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon.png" alt="">
                </div>
            </div>
        </div>
        <div class="floatingnav">
        	<div class="mobilemenuheading">
            	<div class="mobilelogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/mobile-logo.png" alt="mobile-logo">
                </div>
            	<div class="closemenu">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon-close.png" alt="icon-close"></a>
                </div>
            </div>
            <div class="menubox">
                <ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                       <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard">Try It Free</a></li>
                        <?php } ?> 
                     
                </ul>
            </div>
        </div>
    </div>
</div> <!-- header end here -->

<div  class="container-fluid sep-home-buttons">
	<div class="btn-left"><a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/front-home.png"></a></div>
	</div>
<div  class="container">
		<?php
		// Start the loop.
		while ( have_posts() ) :
			the_post();
		?>
		<div class="row front-contents">
			<div class="col-sm-12">
				<?php the_content() ?>
			</div>
		</div><!-- //row -->
		<?php
			// End of the loop.
		endwhile;
		?>
</div><!-- .content-area -->

<!--
<footer class="front-page-footer">
	<div class="container">
		<div class="row">
		    <div class="col-md-12">
			<div class="text-right">
				<ul class="nav navbar-nav">
				<?php
					$menu_name = 'speedysep-user-menu';
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menu_items = wp_get_nav_menu_items(9);
					foreach ( (array) $menu_items as $key => $menu_item ) { 
						$class='';
						$current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
						foreach($menu_item->classes as $item_class){
							$class.=$item_class." ";
						}
						$title = $menu_item->title;
						$url = $menu_item->url;
						if($menu_item->menu_item_parent>0){
							$menu_list .= '<li class="menu-sub-item ' . $current . '"><a href="'.$url.'">'.$title.' | </a></li>';
						}else{
						$menu_list .= '<li class="' . $current . '"><a href="'.$url.'">'.$title.' |</a></li>';
						}
					}
				echo $menu_list;
				?>
				<li class="copyright-text">SpeedySep &copy; <?php echo date('Y') ?></li>
				</ul>
			</div>
			</div>
		</div>
	</div>
</footer>
-->

<script>
var screenHeight=jQuery(window).height();
var screenWidth=jQuery(window).width();
if(screenWidth > 767){
var padding=(screenHeight - 282) / 2;
jQuery('.contents-inner').css('padding', padding + 'px 0px');
}
</script>
<?php get_footer(); ?>
