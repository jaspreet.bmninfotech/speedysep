<!DOCTYPE html>
<?php 
/**
 * Template Name: Portfolio 
 *
 */
?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<?php do_action( 'wp_head' ); ?>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Speedy Sep</title>



<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/js/jquery.magnific-popup.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/js/gallary.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/js/color-gallery.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/js/jquery.flexslider.js"></script>

<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/gallery.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/flexslider.css" rel="stylesheet" type="text/css" />


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->
	
<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>	

</head>
<body>
    
 <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="headerarea">
	<div class="container">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/logo.png" alt="" /></a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                     
                       
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                     
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon.png" alt="">
                </div>
            </div>
            <div class="floatingnav">
                <div class="mobilemenuheading">
                    <div class="mobilelogo">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/mobile-logo.png" alt="">
                    </div>
                    <div class="closemenu">
                        <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon-close.png" alt=""></a>
                    </div>
                </div>
                <div class="menubox">
                    <ul>
                       	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                     
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolioarea">
	<div class="container">
        <div class="portfolioinfo">
            <h1>We design t-shirts, create mockups &amp; color separate artwork</h1>
            <p>Here is our work</p>
            <a class="trybutton popup-with-form" href="#popupcontent">Try it free</a>
        </div>
    </div>
</div>
<div class="vectorarea">
	<div class="container">
    	<div class="vectorgallery">
        	<div class="portfolioinfo">
                <h1>Vectorizing your fuzzy low quality images</h1>
                <p>Simply, we can vectorize your images, restoring full quality so they can be <br> printed without loosing quality</p>
            </div>
            <div class="gallerybox">
                 
                 <section class="slider">
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/1-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/2-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/3-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before-and-after-02.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before-and-after-06.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_09.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_08.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_10.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_13.jpg" /></li>
                            
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider">
                        <ul class="slides">
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/1-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/2-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/3-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before-and-after-02.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before-and-after-06.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_09.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_08.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_10.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/vectorizing_Before_and_after_13.jpg" /></li>
                        </ul>
                    </div>
                 </section>
                 
                 
            </div>
            <div class="portfolioinfo">
                <a class="trybutton popup-with-form" href="#popupcontent">Try it free</a>
            </div>
        </div>
    </div>
</div>
<div class="recreationarea">
	<div class="container">
    	<div class="portfolioinfo">
            <h1>Conceptual Recreation</h1>
            <p>Upload your ideas, and receive unique t-shirt designs in 24 hours</p>
        </div>
        <div class="recreationbox">
        	<div class="recreationitem">
            	<div class="recreationpic">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/thumb01.jpg" alt="" />
                </div>
            </div>
            <div class="recreationitem seconditem">
            	<div class="recreationpic">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/thumb02.jpg" alt="" />
                </div>
            </div>
        </div>
        <div class="portfolioinfo">
            <a class="trybutton popup-with-form" href="#popupcontent">Try it free</a>
        </div>
    </div>
</div>
<div class="colorarea">
	<div class="container">
    	<div class="colorgallery">
        	<div class="portfolioinfo">
                <h1>Color Separation</h1>
                <p>We separate spot colors, simulation, and 4 color process</p>
            </div>
            <div class="gallerybox">
                <section class="slidercolor">
                    <div id="colorslider" class="flexslider">
                        <ul class="slides">
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-1-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-2-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-3-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-4-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-03.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-04.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-05.jpg" /></li>
                            
                        </ul>
                    </div>
                    <div id="colorcarousel" class="flexslider">
                        <ul class="slides">
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-1-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-2-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-3-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/c-4-big.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-03.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-04.jpg" /></li>
                            <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/portfolio/images/color-separation-05.jpg" /></li>
                            
                        </ul>
                    </div>
                </section>
                
            </div>
            <div class="portfolioinfo">
                <a class="trybutton popup-with-form" href="#popupcontent">Try it free</a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<!-- 
<div class="footerarea">
	<div class="container">
    	<div class="footertop">
        	<div class="footertopleft">
            	<h2>Start your business <br> journey with SpeedySep</h2>
            </div>
            <div class="footertopright">
            	<a class="popup-with-form" href="#popupcontent">Try it free</a>
            </div>
        </div>
    	<div class="footerinfo">
        	<div class="footerleft">
            	<div class="footerlogo">
                	<img src="images/footer-logo.png" alt="" />
                </div>
            	<p>SpeedySep is risk-free with a 100% money-back guarantee. Simplify your screen printing process with SpeedySeps.</p>
            </div>
            <div class="footerright">
            	<div class="col-1">
                	<ul>
                    	<li><a href="#">Pricing</a></li>
                        <li><a href="#">Vectorizing</a></li>
                        <li><a href="#">Color Separation</a></li>
                        <li><a href="#">SignUp</a></li>
                        <li><a href="#">Login</a></li>
                    </ul>
                </div>
                <div class="col-1">
                	<ul>
                    	<li><a href="#">Contact Us</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms of use</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyrightarea">
	<div class="container">
    	<div class="copyrightleft">
        	<ul>
            	<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/twitter-icon.png" alt="twitter" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/facebook-icon.png" alt="facebook" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/instagram-icon.png" alt="instagram" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/printerset-icon.png" alt="printerset" /></a></li>
            </ul>
        </div>
        <div class="copyrightright">
        	<p><a href="#">Sitemap</a> | <a href="#">Sitemap XML</a></p>
            <p>Copyright &copy; 2019 by <a href="#">speedysep.com</a></p>
        </div>
    </div>
</div>

-->

<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<div class="formlist">
					<input name="" type="text" placeholder="Email address">
				</div>
				<div class="formlist">
					<input name="" type="password" placeholder="Password">
				</div>
				<div class="formlist">
					<input name="" type="text" placeholder="Your store name">
				</div>
				<div class="formsend">
					<input name="" type="submit" value="create your account">
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
$(document).ready(function() {
	$(".mobilemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '0'
			}, 300 );
		$(this).addClass("opened");
	});
	$(".closemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '-370px'
			}, 300 );
		$(this).removeClass("opened");
	});  
}); 
</script>

<script type="text/javascript">
	$('.popup-with-form').magnificPopup({
		type: 'inline',
	});
</script>



<script type="text/javascript">
$("#color_thumbnail a").click(function() {
    $('html, body').animate({
        scrollTop: $("#coor_photo_container").offset().top
    }, 100);
});
$("#thumbnail a").click(function() {
    $('html, body').animate({
        scrollTop: $("#photo_container").offset().top
    }, 100);
});
</script>

<script type="text/javascript">

    $(window).load(function(){
        
      $('#carousel').flexslider({
    	animation: "slide",
    	controlNav: false,
    	animationLoop: false,
    	slideshow: false,
    	itemWidth: 300,
    	itemMargin:0,
    	asNavFor: '#slider'
      });
      
     $('#slider').flexslider({
    	animation: "slide",
    	controlNav: false,
    	animationLoop: false,
    	slideshow: false,
    	sync: "#carousel",
    	start: function(slider){
    	  $('body').removeClass('loading');
    	}
      });
    });
    
  $(window).load(function(){
      $('#colorcarousel').flexslider({
    	animation: "slide",
    	controlNav: false,
    	animationLoop: false,
    	slideshow: false,
    	itemWidth: 300,
    	itemMargin: 0,
    	asNavFor: '#colorslider'
      });
      
   $('#colorslider').flexslider({
    	animation: "slide",
    	controlNav: false,
    	animationLoop: false,
    	slideshow: false,
    	sync: "#colorcarousel",
    	start: function(slider){
    	  $('body').removeClass('loading');
    	}
      });
    });
    
</script>

</body>
</html>
