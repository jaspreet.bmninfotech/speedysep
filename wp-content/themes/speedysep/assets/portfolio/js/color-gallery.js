$(function(){
    var options = {
        color_thumbUl : $('#color_thumbnail'),  
        color_mainPhoto : $('#color_main_photo'), 
        color_parentDiv : $('#coor_photo_container'), 
        slideSpeed: 3000, 
        fadeSpeed: 500, 
        colorstartPlay: false, 
        colormaxWidth : 1920, 
        thumbcolormaxWidth : 295,  
        colorthumbMinWidth : 295 
    };
    
    var color_thumbs = options.color_thumbUl.find('a'),
        color_mainPhoto = options.color_mainPhoto,
        color_thumbFiles = [],
        color_mainFiles = [],
        colorcurrentNum = 0,
        colornextBtn = $('#colornext'),
        colorprevBtn = $('#colorprev'),
        colornowPlay = false, 
        timer,
        colorplayBtn = $('#colorplay_btn'),
        colorstopBtn = $('#colorstop_btn');       
	
    window.onload = function(){
        color_mainPhoto.height(color_mainPhoto.children('img').outerHeight());
    }
    
    options.color_parentDiv.css('colormaxWidth', options.colormaxWidth);
    var liWidth = Math.floor((options.thumbcolormaxWidth / options.colormaxWidth) * 100);
    options.color_thumbUl.children('li').css({
        width : liWidth + '%',
        colormaxWidth : options.thumbcolormaxWidth,
        minWidth : options.colorthumbMinWidth
    });

    for(var i = 0; i < color_thumbs.length; i++){
        color_mainFiles[i] = $('<img />');
        color_mainFiles[i].attr({
            src: $(color_thumbs[i]).attr('href'),
            alt: $(color_thumbs[i]).children('img').attr('alt')
        });
        color_mainFiles[i] = color_mainFiles[i][0];
        color_thumbFiles[i] = $(color_thumbs[i]).children('img');
        color_thumbFiles[i] = color_thumbFiles[i][0];
    }

    color_mainPhoto.prepend(color_mainFiles[0]);
    $(color_thumbFiles[0]).parent().parent().addClass('current');
    
    if(options.colorstartPlay) {
        colorcurrentNum--;
        colorautoPlay();
        colorplayBtnHide();
    } else {
        colorplayBtnShow();
    }
    
    color_thumbs.on('click', function(){
        colorcurrentNum = $.inArray($("img",this)[0], color_thumbFiles); 
        colormainView();
        colorstopPlay();
        colorplayBtnShow();
        return false;
    });
    
    colorprevBtn.on('click', function(){
        colorcurrentNum--;
        if(colorcurrentNum < 0){
			colorcurrentNum = color_mainFiles.length - 1;
		}
        colormainView();
        colorstopPlay();
        colorplayBtnShow();
    });
    
    colornextBtn.on('click', function(){
        colorcurrentNum++;
        if(colorcurrentNum > color_mainFiles.length - 1){
			colorcurrentNum = 0;
		}
        colormainView();
        colorstopPlay();
        colorplayBtnShow();
    });
    
    colorplayBtn.on('click', function(){
		if(colornowPlay) return;
		colorautoPlay();	
		colorplayBtnHide();
	});
    
	colorstopBtn.on('click', function(){
        colorstopPlay();
        colorplayBtnShow();
        
	});
    
    $(window).on('resize', function(){
        color_mainPhoto.height(color_mainPhoto.find('img').outerHeight());
    });
    
    function colormainView(){
        color_mainPhoto.prepend(color_mainFiles[colorcurrentNum]).find('img').show();
        color_mainPhoto.find('img:not(:first)').stop(true, true).fadeOut(options.fadeSpeed, function(){
		    $(this).remove();
		});
        
        color_thumbs.eq(colorcurrentNum).parent().addClass('current').siblings().removeClass('current');
    }
	
    function colorautoPlay(){
        colornowPlay = true;
		colorcurrentNum++;
        if(colorcurrentNum > color_mainFiles.length - 1){
			colorcurrentNum = 0;
		}
        colormainView();
		timer = setTimeout(function(){
	       colorautoPlay();
        }, options.slideSpeed);
	}
   
    function colorstopPlay(){
        clearTimeout(timer);
        colornowPlay = false;
    }
	
	function colorplayBtnShow(){
		if(colornowPlay === false){
			colorstopBtn.hide();
			colorplayBtn.show();
		}
	}
	function colorplayBtnHide(){
		if(colornowPlay === true){
			colorplayBtn.hide();
			colorstopBtn.show();	
		}
	}
    
});