jQuery(document).ready(function($){
//ajar request create order
$('.placeOrderForm').on('submit', function(e) {
		//e.preventDefault();
		//fire ajax call 
		var data = {
			'action': 'create_order',
			'formData': ''				
		};
		jQuery('.create_order').val('Please Wait');
		 jQuery('.create_order').prop('disabled', true);

		jQuery.post(ajaxurl, data, function(response) {
			console.log(response);	
			
		});
	});
	
$('.buy-now').on('click', function(e){
	e.preventDefault();
	var packageType=$(this).attr('id');
	$('#package').val(packageType);
	$('.buy-now').css('background', '#6bc43c');
	$('.buy-now.seperation').css('background', '#8fc4ef');
	$('#'+packageType).css('background', '#000000');
	 $('#setPricingPlan').removeAttr("disabled");
});
	
$('#extraFast').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#extra_fast').val('1');
    }else if($(this).prop("checked") == false){
         $('#extra_fast').val('');
    }
});
$('#additionalFormats').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#additional_formats').val('1');
    }else if($(this).prop("checked") == false){
         $('#additional_formats').val('');
    }
});
$('#conceptRecreation').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#concept_recreation').val('1');
    }else if($(this).prop("checked") == false){
         $('#concept_recreation').val('');
    }
});

jQuery("body").on("click", "label.any-label", function(e) {
  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".dimension-type");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  console.log(getInputRadio.attr("id"));
	jQuery("#any").prop("checked", true); 
	jQuery('.any-label').addClass("active");		
});

//send message 
$('.send-message-form').on('submit', function(e) {
	e.preventDefault();
	$('#loader').show();
	//fire ajax call 
	var file_data = $('#attachment').prop('files')[0];
	var form_data = new FormData(this);
	//form_data.append('file', file_data);
	form_data.append('request_message', jQuery('#requestMessage').val());
	form_data.append('user_name', jQuery('#user_name').val());
	form_data.append('order_id', jQuery('#order_id').val());
	form_data.append('buyer_id', jQuery('#buyer_id').val());
	form_data.append('action', 'send_message');
	
	jQuery('#send_message_button').prop('disabled', true);

	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		success: function (response) {
			console.log(response); 
			var message='<div class="conversation_message">' + jQuery('#user_name').val() + ': ' + jQuery('#requestMessage').val();
			
			 
			//console.log(obj.length);
			if(response!='no'){
				var obj = JSON.parse(response);
				var imgCounts=obj.length;
			}else{
				var imgCounts=0;
			}
			var i;
			
			message=message + '<div class="message-attachment">';
					
			for (i = 0; i < imgCounts; i++) { 
				if(typeof obj[i] !='undefined'){
					if(checkURL(obj[i])){
					message=message + '<img src="' + obj[i] + '">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>';
					}else if(checkPDF(obj[i])){
						message=message + '<img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/pdf.png">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>';
					}else{
					message=message + '<img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/unknown.png">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>'
					}
				}
			}
			
			message=message + '</div>'
			message=message + '</div>';
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			$('#loader').hide();
			$('#file-text').find( 'span' ).html('');
			$("#send_message_button").removeAttr("disabled");
		}
	});
	
});

//Deliver order
$('.deliver-order').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var file_data = $('#attachment_deliver').prop('files')[0];
	//var form_data = new FormData();
	 var form_data = new FormData(this);
	//form_data.append('file', file_data);
	form_data.append('order_id', jQuery('#deliver_order_id').val());
	form_data.append('action', 'deliver_order');
	
	jQuery('#deliver_button').prop('disabled', true);

	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		// this part is progress bar
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);
					$('.deliver-order-prog').show();
					//percentComplete=percentComplete - 2;
					$('.deliver-order-prog .progress-bar').text(percentComplete + '%');
					$('.deliver-order-prog .progress-bar').css('width', percentComplete + '%');
					console.log(percentComplete);
				}
			}, false);
			return xhr;
		},
		success: function (response) {
			console.log(response); 
			jQuery('#deliver_button').prop('disabled', false);
			var message='<div class="conversation_message">' + jQuery('#user_name').val() + ': ' + jQuery('#requestMessage').val();
			$('.deliver-order-prog .progress-bar').css('width', '100%');
					$('.deliver-order-prog .progress-bar').text('100%');
			$('.deliver-order-prog .progress-bar').css('background-color', '#5cb85c');
			var obj = JSON.parse(response);
			if(typeof obj.url !='undefined'){
			message=message + '<div class="message-attachment"><img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/order-img.png">  <span><a href="' + obj.url + '" target="_blank"><i class="fa fa-download"></i></a></span></div>'
			}
			message=message + '</div>';
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			jQuery('#attachment_deliver').val('');
			jQuery('.files-names span').text('');
			$("#send_message_button").removeAttr("disabled");
			//refresh page
			 window.location.reload(true);
		}
	});
	
});
//submit review
$('.rating-from').on('submit', function(e) {
	e.preventDefault();
	var data = {
			'action': 'create_rating',
			'response': jQuery('#response').val(),
			'service_as_expected': jQuery('#ServiceAsExpected').val(),
			'user_our_service_agian': jQuery('#userOurServiceAgian').val(),
			'message':jQuery('#requestMessage').val(),
			'order_id':jQuery('#order_id').val(),
		};
	
	$('#messaageResponse').text('Please wait...');
		
		 jQuery('#send_message_button').prop('disabled', true);

		jQuery.post(ajaxurl, data, function(response) {
			console.log(response);	
			$('#messaageResponse').text('Thank you for your feedback, It has been submitted successfully.');
			jQuery('#requestMessage').val('');
			$('ul.stars li').removeClass('selected');
			$("#send_message_button").removeAttr("disabled");
		});
	
});

//close order
$('.close-order').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	
	 var form_data = new FormData(this);
	form_data.append('order_id', jQuery('#close_order_id').val());
	form_data.append('action', 'close_order');
	
	jQuery('#close_button').prop('disabled', true);

	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		success: function (response) {
			console.log(response); 
			jQuery('#close_button').text('Closed');
			var message='<div class="conversation_message">' + jQuery('#user_name').val() + ': ' + jQuery('#requestMessage').val();
			
			var obj = JSON.parse(response);
			if(typeof obj.url !='undefined'){
			message=message + '<div class="message-attachment"><img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/order-img.png">  <span><a href="' + obj.url + '" target="_blank"><i class="fa fa-download"></i></a></span></div>'
			}
			message=message + '</div>';
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			$("#send_message_button").removeAttr("disabled");
		}
	});
	
});

//submit revision
$('.revision-form').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var file_data = $('#attachment').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('request_message', jQuery('#requestMessage').val());
	form_data.append('user_name', jQuery('#user_name').val());
	form_data.append('order_id', jQuery('#order_id').val());
	form_data.append('action', 'create_revision');
	
	jQuery('#send_message_button').prop('disabled', true);
	jQuery('#responseMessage').text('Please Wait...');
	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		success: function (response) {
			console.log(response); 
			window.location =ajax_object_speedy.site_url + '/open-orders/?order_id=' + jQuery('#order_id').val();
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			$("#send_message_button").removeAttr("disabled");
		}
	});
	
});

//check if image
function checkURL(url) {
    return(url.match(/\.(jpeg|jpg|gif|png|svg)$/) != null);
}

function checkPDF(url) {
    return(url.match(/\.(pdf)$/) != null);
}

function checkEPSAI(url) {
    return(url.match(/\.(eps|ai)$/) != null);
}


check_unread_messages();  
//check unread messages after every 10 sec
var interval = null;
interval = setInterval(function(){check_unread_messages();}, 10000);
function check_unread_messages(){
	//fire ajax call 
	var data = {
		'action': 'get_unread_notifications_of_user',		
	};
	
	jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			//console.log(response);
			var count=0;
			var result=JSON.parse(response);
			
			var messages=result.order_mess_status;
			var order_delivered=result.order_delivered_mess;
			var order_completed_mess=result.order_completed_mess;
			var order_placed_mess=result.order_placed_mess;
			
			if(typeof messages!='undefined'){
			messages.forEach(function(element) {
			  //console.log(element.order_id);
			  if($("#unread_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/wp-admin/?page=single-order&order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
			}
			//delivered orders
		if(typeof order_delivered!='undefined'){	
			order_delivered.forEach(function(element) {
			 // console.log(element.order_id);
			  if($("#unread_delivered_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_delivered_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/wp-admin/?page=single-order&order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					 count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
				
		}
		
		//completed orders
		if(typeof order_completed_mess!='undefined'){	
			order_completed_mess.forEach(function(element) {
			  //console.log(element.order_id);
			  if($("#unread_delivered_mess_" + element.order_id).length == 0 || $("#unread_delivered_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_delivered_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/wp-admin/?page=single-order&order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					 count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
				
		}
		
		//placed orders
		if(typeof order_placed_mess!='undefined'){	
			order_placed_mess.forEach(function(element) {
			  //console.log(element.order_id);
			  if($("#unread_delivered_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_delivered_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/wp-admin/?page=single-order&order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					 count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
				
		} 
	});
}

//delete users

	$("#deleteUsers").click(function(event){
		event.preventDefault();
		var userToDelete = [];
		$.each($("input[name='users']:checked"), function(){            
			userToDelete.push($(this).val());
		});
		var data = {
			'action': 'delete_customers',
			'userids': userToDelete				
		};
		jQuery('#delete_user_message').addClass('notice');
		jQuery('.delete_user_message').html('<p>Please Wait. We are working on you request.</p>');

		jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			window.location.href = ajax_object_speedy.site_url + '/wp-admin/admin.php?page=speedy-customer-list&users=deleted';
			jQuery('.delete_user_message').val('Selected Users deleted successfully.');
			
		});
	});


});