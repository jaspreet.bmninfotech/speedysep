jQuery(document).ready(function($){
//ajar request create order
$('.placeOrderForms').on('submit', function(e) {
		//e.preventDefault();
		//fire ajax call 
		var data = {
			'action': 'create_order',
			'formData': ''				
		};
		jQuery('.create_order').val('Please Wait');
		 jQuery('.create_order').prop('disabled', true); 

		jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			console.log(response);	
			
		});
	});
	
$('.buy-now').on('click', function(e){
	e.preventDefault();
	var packageType=$(this).attr('data-id');
	$('#package').val(packageType);
	//$('.buy-now').css('background', '#00c7fc');
	//$('.buy-now.seperation').css('background', '#fb3569');
	$('#'+packageType).css('background', '#000000');
	 $('#setPricingPlan').removeAttr("disabled");
	  $('.buy-now').removeClass('active-package');
	  $('.buy-now').removeClass('active-package-top');
	  $('.buy-now').removeClass('active-package-bottom');
	  calculate_order_cost();
	  var order_total=jQuery('#total_amount').val();
	 if(packageType=='basic'){
		 order_total=parseFloat(order_total)+10;
		 $('.buy-now-first').addClass('active-package');
		$('.buy-now-first.buy-now-top').addClass('active-package-top');
		$('.buy-now-first.buy-now-bottom').addClass('active-package-bottom');
		$('#additionalFormats').prop("checked", false);
		$('#conceptRecreation').prop("checked", false);
		$('#additional_formats').val('no');
		$('#concept_recreation').val('no');
	}else if(packageType=='standard'){
		 order_total=parseFloat(order_total)+20;
		 $('.buy-now-second').addClass('active-package');
		$('.buy-now-second.buy-now-top').addClass('active-package-top');
		$('.buy-now-second.buy-now-bottom').addClass('active-package-bottom');
		$('#additionalFormats').prop("checked", true);
		$('#conceptRecreation').prop("checked", false);
		$('#additional_formats').val('yes');
		$('#concept_recreation').val('no');
	}else if(packageType=='premium'){
		 order_total=parseFloat(order_total)+30;
		$('.buy-now-third').addClass('active-package');
		$('.buy-now-third.buy-now-top').addClass('active-package-top');
		$('.buy-now-third.buy-now-bottom').addClass('active-package-bottom');
		$('#additionalFormats').prop("checked", true);
		$('#conceptRecreation').prop("checked", true);
		$('#additional_formats').val('yes');
		$('#concept_recreation').val('yes');
	}
	//jQuery('#total_amount').val(order_total);
	//jQuery('#orderTotal').text(order_total);
	calculate_order_cost();
	
	calculate_order_cost();
});

$( ".buy-now" ).hover(
  function() {
	var packageType=$(this).attr('data-id'); //console.log(packageType);
	if(packageType=='basic'){
		 $('.buy-now-first').css('border-left', '2px solid #231f20');
		$('.buy-now-first').css('border-right', '2px solid #231f20');
		$('.buy-now-first.buy-now-top').css('border-top', '2px solid #231f20');
		$('.buy-now-first.buy-now-bottom').css('border-bottom', '2px solid #231f20');
	}else if(packageType=='standard'){
		 $('.buy-now-second').css('border-left', '2px solid #231f20');
		$('.buy-now-second').css('border-right', '2px solid #231f20');
		$('.buy-now-second.buy-now-top').css('border-top', '2px solid #231f20');
		$('.buy-now-second.buy-now-bottom').css('border-bottom', '2px solid #231f20');
	}else if(packageType=='premium'){
		$('.buy-now-third').css('border-left', '2px solid #231f20');
		$('.buy-now-third').css('border-right', '2px solid #231f20');
		$('.buy-now-third.buy-now-top').css('border-top', '2px solid #231f20');
		$('.buy-now-third.buy-now-bottom').css('border-bottom', '2px solid #231f20');
	}
   
  }, function() {
    $('.buy-now').css('border', '1px solid #e7e7e7');
  }
);
 
$( "li.fade" ).hover(function() {
  $( this ).fadeOut( 100 );
  $( this ).fadeIn( 500 );
});
	
$('#extraFast').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#extra_fast').val('yes');
    }else if($(this).prop("checked") == false){
         $('#extra_fast').val('no');
    }
	calculate_order_cost();
});
$('#additionalFormats').on('click', function(e){
	
	  $('.buy-now').removeClass('active-package');
	  $('.buy-now').removeClass('active-package-top');
	  $('.buy-now').removeClass('active-package-bottom');
	  
	if($(this).prop("checked") == true){
        $('#additional_formats').val('yes');
		if($('#conceptRecreation').prop("checked") == true){
			$('.buy-now-third').addClass('active-package');
			$('.buy-now-third.buy-now-top').addClass('active-package-top');
			$('.buy-now-third.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('premium');
		}else{
			$('.buy-now-second').addClass('active-package');
			$('.buy-now-second.buy-now-top').addClass('active-package-top');
			$('.buy-now-second.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('standard');
		}
    }else if($(this).prop("checked") == false){
         $('#additional_formats').val('no');
		$('.buy-now-first').addClass('active-package');
		$('.buy-now-first.buy-now-top').addClass('active-package-top');
		$('.buy-now-first.buy-now-bottom').addClass('active-package-bottom');
		$('#package').val('basic');
		
    }
	calculate_order_cost();
});
$('#conceptRecreation').on('click', function(e){
	
	  $('.buy-now').removeClass('active-package');
	  $('.buy-now').removeClass('active-package-top');
	  $('.buy-now').removeClass('active-package-bottom');
	  
	if($(this).prop("checked") == true){
        $('#concept_recreation').val('yes');
		if($('#additionalFormats').prop("checked") == true){
			$('.buy-now-third').addClass('active-package');
			$('.buy-now-third.buy-now-top').addClass('active-package-top');
			$('.buy-now-third.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('premium');
		}else{
			$('.buy-now-second').addClass('active-package');
			$('.buy-now-second.buy-now-top').addClass('active-package-top');
			$('.buy-now-second.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('standard');
		}
    }else if($(this).prop("checked") == false){
         $('#concept_recreation').val('no');
		 if($('#additionalFormats').prop("checked") == true){
			$('.buy-now-second').addClass('active-package');
			$('.buy-now-second.buy-now-top').addClass('active-package-top');
			$('.buy-now-second.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('standard');
		}else{
			$('.buy-now-first').addClass('active-package');
			$('.buy-now-first.buy-now-top').addClass('active-package-top');
			$('.buy-now-first.buy-now-bottom').addClass('active-package-bottom');
			$('#package').val('basic');
		}
    }
	calculate_order_cost();
});

$('#weekendDelivery').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#weekend_delivery').val('yes');
    }else if($(this).prop("checked") == false){
         $('#weekend_delivery').val('no');
    }
	calculate_order_cost();
});

$('#halftones').on('click', function(e){
	if($(this).prop("checked") == true){
        $('#half_tones').val('yes');
    }else if($(this).prop("checked") == false){
         $('#half_tones').val('no');
    }
	calculate_order_cost();
});

$('#modifications').on('change', function(e){
	var mod=$(this).val();
	$('#modifications1').val(mod);
	calculate_order_cost();
});

$('#mockup').on('change', function(e){
	var mod=$(this).val();
	$('#mockup1').val(mod);
	calculate_order_cost();
});

function calculate_order_cost(){
	var extra_fast=$('#extra_fast').val();
	var order_total=0;
	var packageType=$('#package').val();
	
	if(extra_fast=='yes'){
		order_total=order_total + +10;
	}
	if($('#weekend_delivery').val()=='yes'){
		order_total=order_total + +10;
	}
	console.log($('#additional_formats').val());
	if(packageType=='basic'){
	if($('#additional_formats').val()=='yes'){
		order_total=order_total + +5;
	}
	}
	//console.log(packageType);
	if(packageType!='premium'){
		if($('#concept_recreation').val()=='yes'){
			order_total=order_total + +10;
		}
	}
	
	if($('#half_tones').val()=='yes'){
		order_total=order_total + +5;
	}
	if($('#modifications1').val()>0){
		var modification_val=$('#modifications1').val();
		order_total=order_total + +(modification_val*2.5);
	}
	if($('#mockup1').val()>0){
		var mockup_val=$('#mockup1').val();
		order_total=order_total + +(mockup_val*1);
	}
	
	if(packageType=='basic'){
		order_total=order_total+9;
	}else if(packageType=='standard'){
		order_total=order_total+19;
	}else if(packageType=='premium'){
		order_total=order_total+29;
	}
	$('#total_amount').val(order_total);
	$('#orderTotal').text(order_total);
	
	//console.log(order_total);
	
}


jQuery("body").on("click", "label.any-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".extra-format");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]"); 
  
  //console.log(getValue);
  
 
  if($('#' + getValue).is(':checked')){
	jQuery("#" + getValue).prop("checked", false); 
	jQuery(this).removeClass("active");
	$('#' + getValue).attr('checked', false);
  }else{
	  jQuery("#" + getValue).prop("checked", true); 
	jQuery(this).addClass("active");
	$('#' + getValue).attr('checked', true);
  }	
  
	var formats = [];
	$.each($(".extra-format input:checked"), function(){            
		formats.push($(this).val());
	});
	
	if(jQuery.inArray( "psd", formats )>0 || jQuery.inArray( "eps", formats )>0 || jQuery.inArray( "pdf", formats )>0 || jQuery.inArray( "dxf", formats )>0 || jQuery.inArray( "svg", formats )>0 || jQuery.inArray( "dwg", formats )>0){
		 
		jQuery('.packages-label').removeClass('active');
		
		if($('#conceptualYes').is(":checked")){
			jQuery('.package_premium').addClass('active');
		console.log('zxc');
		}else{
			jQuery('.package_standard').addClass('active');
		console.log('asdf');
		}
	}
	
});

jQuery("body").on("click", "label.halftones-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".halftones");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
  //remove active classes
	jQuery("#halftonesYes").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	
	jQuery("#halftonesNo").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	
  if(getValue=='halftonesYes'){
	  //remove from no
	  jQuery("#halftonesNo").removeAttr('checked');	  
	jQuery("#halftonesNo").parent().removeClass("active");

	jQuery("#halftonesYes").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else{
	  //remove from yes
	  jQuery("#halftonesYes").removeAttr('checked');	  
	jQuery("#halftonesYes").parent().removeClass("active");

	jQuery("#halftonesNo").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }	
});

jQuery("body").on("click", "label.extrafast-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".extrafast");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
  //remove active classes
	jQuery("#extrafastYes").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	
	jQuery("#extrafastNo").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	//console.log(getValue);
  if(getValue=='extrafastYes'){
	  //remove from no	  
	  jQuery("#extrafastNo").removeAttr('checked');	  
	jQuery("#extrafastNo").parent().removeClass("active");

	jQuery("#extrafastYes").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else{
	  //remove from yes
	  jQuery("#extrafastYes").removeAttr('checked');	  
	jQuery("#extrafastYes").parent().removeClass("active");

	jQuery("#extrafastNo").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }	
});

jQuery("body").on("click", "label.underbase-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".underbase");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
  //remove active classes
	jQuery("#underbaseYes").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	
	jQuery("#underbaseNo").prop("checked", false); 
	jQuery(goToParent).removeClass("active");
	//console.log(getValue);
  if(getValue=='underbaseYes'){
	  //remove from no	  
	  jQuery("#underbaseNo").removeAttr('checked');	  
	jQuery("#underbaseNo").parent().removeClass("active");

	jQuery("#underbaseYes").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else{
	  //remove from yes
	  jQuery("#underbaseYes").removeAttr('checked');	  
	jQuery("#underbaseYes").parent().removeClass("active");

	jQuery("#underbaseNo").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }	
});

jQuery("body").on("click", "label.conceptual-recreation-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".weekend-delivery");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
 // console.log(getValue);
  if(getValue=='conceptualYes'){
	  //remove from no	  
	  jQuery("#conceptualNo").removeAttr('checked');	  
	jQuery("#conceptualNo").parent().removeClass("active");

	jQuery("#conceptualYes").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else{
	  //remove from yes
	  jQuery("#conceptualYes").removeAttr('checked');	  
	jQuery("#conceptualYes").parent().removeClass("active");

	jQuery("#conceptualNo").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }	
  
  var formats = [];
	$.each($(".extra-format input:checked"), function(){            
		formats.push($(this).val());
	});
	
	if(jQuery.inArray( "psd", formats )>0 || jQuery.inArray( "eps", formats )>0 || jQuery.inArray( "pdf", formats )>0 || jQuery.inArray( "dxf", formats )>0 || jQuery.inArray( "svg", formats )>0 || jQuery.inArray( "dwg", formats )>0){
		 
		jQuery('.packages-label').removeClass('active');
		
		if($('#conceptualYes').is(":checked")){
			jQuery('.package_premium').addClass('active');
		console.log('zxc');
		}else{
			jQuery('.package_standard').addClass('active');
		console.log('asdf');
		}
	}
  
  
});

jQuery("body").on("click", "label.weekend-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".conceptual-recreation");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
 // console.log(getValue);
  if(getValue=='weekendYes'){
	  //remove from no	  
	  jQuery("#weekendNo").removeAttr('checked');	  
	jQuery("#weekendNo").parent().removeClass("active");

	jQuery("#weekendYes").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else{
	  //remove from yes
	  jQuery("#weekendYes").removeAttr('checked');	  
	jQuery("#weekendYes").parent().removeClass("active");

	jQuery("#weekendNo").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }	
});

jQuery("body").on("click", "label.packages-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".packages");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
  
	jQuery('.extra-format .any-label').removeClass('active');
	jQuery('.extra-format input').prop('checked', false);
	
	//check the first three
	jQuery('.extra-format .illustrator-label').addClass('active');
	jQuery('.extra-format input.illustrator').prop('checked', true);
	
	jQuery('.extra-format .png-label').addClass('active');
	jQuery('.extra-format input.png').prop('checked', true);
	
	jQuery('.extra-format .jpg-label').addClass('active');
	jQuery('.extra-format input.jpg').prop('checked', true);
	
	jQuery('.extra-format .jpg-label').addClass('active');
	jQuery('.extra-format input.jpg').prop('checked', true);
	
	jQuery('.conceptual-label').removeClass('active');
	jQuery('.conceptual-label-no').addClass('active');
	jQuery('#conceptualYes').prop('checked', false);
	jQuery("#conceptualNo").attr('checked', 'checked');
	
	
  
 // console.log(getValue);
  if(getValue=='packageBasic'){
	  //remove from no	  
		jQuery("#packageStandard").removeAttr('checked');	  
		jQuery("#packageStandard").parent().removeClass("active");
		jQuery("#packagePremium").removeAttr('checked');	  
		jQuery("#packagePremium").parent().removeClass("active");

	jQuery("#packageBasic").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else if(getValue=='packageStandard'){
	//remove from no	  
		jQuery("#packageBasic").removeAttr('checked');	  
		jQuery("#packageBasic").parent().removeClass("active");
		jQuery("#packagePremium").removeAttr('checked');	  
		jQuery("#packagePremium").parent().removeClass("active");

		jQuery("#packageStandard").attr('checked', 'checked');
		jQuery(this).addClass("active"); 
		jQuery('.extra-format .any-label').addClass('active');
		jQuery('.extra-format input').prop('checked', true);
  }else{
	  //remove from no	  
		jQuery("#packageBasic").removeAttr('checked');	  
		jQuery("#packageBasic").parent().removeClass("active");
		jQuery("#packageStandard").removeAttr('checked');	  
		jQuery("#packageStandard").parent().removeClass("active");

		jQuery("#packagePremium").attr('checked', 'checked');
		jQuery(this).addClass("active");  
		jQuery('.extra-format .any-label').addClass('active');
		jQuery('.extra-format input').prop('checked', true);
		
		jQuery('.conceptual-label').addClass('active');
		jQuery('.conceptual-label-no').removeClass('active');
		
		jQuery('#conceptualNo').prop('checked', false);
		jQuery("#conceptualYes").attr('checked', 'checked');
  }	
});

jQuery("body").on("click", "label.color_type-label", function(e) {

  var getValue = jQuery(this).attr("for");
  var goToParent = jQuery(this).parents(".color_type");
  var getInputRadio = goToParent.find("input[id = " + getValue + "]");
  
  console.log(getValue);
  if(getValue=='spotColor'){
	  //remove from no	  
		jQuery(".color_type-label input").removeAttr('checked');	  
		jQuery(".color_type-label input").parent().removeClass("active");

	jQuery("#spotColor").attr('checked', 'checked');
	jQuery(this).addClass("active");
  }else if(getValue=='cmyk'){
	//remove from no	  
		jQuery(".color_type-label input").removeAttr('checked');	  
		jQuery(".color_type-label input").parent().removeClass("active");

		jQuery("#cmyk").attr('checked', 'checked');
		jQuery(this).addClass("active");
  }else if(getValue=='process_color'){
	  //remove from no	  
		jQuery(".color_type-label input").removeAttr('checked');	  
		jQuery(".color_type-label input").parent().removeClass("active");

		jQuery("#process_color").attr('checked', 'checked');
		jQuery(this).addClass("active");
  }else{
	   //remove from no	  
		jQuery(".color_type-label input").removeAttr('checked');	  
		jQuery(".color_type-label input").parent().removeClass("active");

		jQuery("#custom_color").attr('checked', 'checked');
		jQuery(this).addClass("active");
  }	
});

$('.placeOrderForm .inputfile').on('change', function(){
	jQuery("#files_link").removeAttr('required');
});

$('#files_link').on('change', function(){
	jQuery(".placeOrderForm .inputfile").removeAttr('required');
});

$('#outputTempFile').on('change', function(){
	jQuery(".speedy-standard-temp-input").removeAttr('required');
});


$('#speedyOutputTemp').on('change', function(){
	if($(this).prop("checked") == true){
		jQuery("#outputTempFile").removeAttr('required');
	}else{
		jQuery("#outputTempFile").attr('required', 'required');
	}
});



//send message
$('.send-message-form').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	$('#loader').show();
	var file_data = $('#attachment').prop('files')[0];
	var form_data = new FormData(this);
	//form_data.append('file', file_data);
	form_data.append('request_message', jQuery('#requestMessage').val());
	form_data.append('user_name', jQuery('#user_name').val());
	form_data.append('order_id', jQuery('#order_id').val());
	form_data.append('action', 'send_message');
	
	jQuery('#send_message_button').prop('disabled', true);

	jQuery.ajax({
		url: ajax_object_speedy.ajax_url,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		success: function (response) {
			console.log(response); 
			var message='<div class="conversation_message">' + jQuery('#user_name').val() + ': ' + jQuery('#requestMessage').val();
			
			//console.log(obj.length);
			if(response!='no'){
				var obj = JSON.parse(response);
				var imgCounts=obj.length;
			}else{
				var imgCounts=0;
			}
			var i;
			
			message=message + '<div class="message-attachment">';
					
			for (i = 0; i < imgCounts; i++) { 
				if(typeof obj[i] !='undefined'){
					if(checkURL(obj[i])){
					message=message + '<img src="' + obj[i] + '">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>';
					}else if(checkPDF(obj[i])){
						message=message + '<img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/pdf.png">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>';
					}else{
					message=message + '<img src="' + ajax_object_speedy.stylesheet_dir_uri + '/images/unknown.png">  <span><a href="' + obj[i] + '" target="_blank"><i class="fa fa-download"></i></a></span>'
					}
				}
			}
			
			message=message + '</div>'
			message=message + '</div>';
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			$('#loader').hide();
			$('#file-text').find( 'span' ).html('');
			$("#send_message_button").removeAttr("disabled");
		}
	});
	
});

//submit review
$('.rating-from').on('submit', function(e) {
	e.preventDefault();
	var data = {
			'action': 'create_rating',
			'response': jQuery('#response').val(),
			'service_as_expected': jQuery('#ServiceAsExpected').val(),
			'user_our_service_agian': jQuery('#userOurServiceAgian').val(),
			'message':jQuery('#requestMessage').val(),
			'order_id':jQuery('#order_id').val(),
		};
	
	$('#messaageResponse').text('Please wait...');
		
		 jQuery('#send_message_button').prop('disabled', true);

		jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			console.log(response);	
			$('#messaageResponse').text('Thank you for your feedback, It has been submitted successfully.');
			jQuery('#requestMessage').val('');
			$('ul.stars li').removeClass('selected');
			$("#send_message_button").removeAttr("disabled");
		});
	
});

//submit revision
$('.revision-form').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var file_data = $('#attachment').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('request_message', jQuery('#requestMessage').val());
	form_data.append('user_name', jQuery('#user_name').val());
	form_data.append('order_id', jQuery('#order_id').val());
	form_data.append('action', 'create_revision');
	
	jQuery('#send_message_button').prop('disabled', true);
	jQuery('#responseMessage').text('Please Wait...');
	jQuery.ajax({
		url: ajax_object_speedy.ajax_url,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		success: function (response) {
			console.log(response); 
			window.location =ajax_object_speedy.site_url + '/open-orders/?order_id=' + jQuery('#order_id').val();
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			$("#send_message_button").removeAttr("disabled");
		}
	});
	
});

//check if image
function checkURL(url) {
    return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
}
function checkPDF(url) {
    return(url.match(/\.(pdf)$/) != null);
}

function checkEPSAI(url) {
    return(url.match(/\.(eps|ai)$/) != null);
}


check_unread_messages();  
//check unread messages after every 10 sec
var interval = null;
interval = setInterval(function(){check_unread_messages();}, 10000);
function check_unread_messages(){
	//fire ajax call 
	var data = {
		'action': 'get_unread_notifications_of_user',		
	};
	
	jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			//console.log(response);
			var count=0;
			var result=JSON.parse(response);
			
			var messages=result.order_mess_status;
			var order_delivered=result.order_delivered_mess;
			
			if(typeof messages!='undefined'){
			messages.forEach(function(element) {
			  //console.log(element.order_id);
			  if($("#unread_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/open-orders/?order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
			}
			//delivered orders
		if(typeof order_delivered!='undefined'){	
			order_delivered.forEach(function(element) {
			  //console.log(element.order_id);
			  if($("#unread_delivered_mess_" + element.order_id).length == 0) {
				   jQuery('ul.notification-dropdown').append('<li id="unread_delivered_mess_' +element.order_id+ '"><a href="' +ajax_object_speedy.site_url+ '/delivered-orders/?order_id=' +element.order_id+ '"><div class="img-div"><img src="' +element.imagee+ '"></div><div class="text-div"><span class="notification-message">' + element.message + '</span><span class="notification-time">' +element.messsage_time+ ' - ' +element.order_type+ '</span></div></a></li>');
					 jQuery('ul.notification-dropdown').append('<li class="divider"></li>');
					 count++;
					//show count
					jQuery('.notif-count').show();
					jQuery('.notif-count').text(count);
				}else{
					//console.log('exists.');
				}
			 
			});
		}
	});
}

//create new order
$('.placeOrderForm').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	 var form_data = new FormData(this);
	//form_data.append('action', 'create_order');
	
	
	var selection = document.getElementById('file');
	for (var i=0; i<selection.files.length; i++) {
		var ext = selection.files[i].name.substr(-3);
		var ext2 = selection.files[i].name.substr(-2);
		if(ext!== "peg" && ext!== "jpg" && ext!== "png" && ext!== "gif" && ext!== "PEG" && ext!== "JPG" && ext!== "PNG" && ext!== "GIF" && ext2!=='ai')  {
			jQuery('#file-error').text('Please upload a valid image.');
			jQuery('#file').focus();
			return false;
		}
		
		sizee = $("#file")[i].files[i].size; //file size in bytes
		sizee = sizee / 1024; //file size in Kb
		sizee = sizee / 1024; //file size in Mb
		 
		//file size more than 10Mb
		if (sizee > 20) {
		jQuery('#file-error').text('Image size should be less than 20MB.');
		jQuery('#file').focus();
		return false;
		} 
	} 
	
	
	
	jQuery('.type_button').prop('disabled', true);
	$('.prog-main').css('display', 'block');
	jQuery.ajax({
		url: ajax_object_speedy.ajax_url,
		type: 'POST',
		contentType: false,
		processData: false,
		data: form_data,
		// this part is progress bar
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);
					$('.deliver-order-prog').show();
					//percentComplete=percentComplete - 2;
					$('.deliver-order-prog .progress-bar').text(percentComplete + '%');
					$('.deliver-order-prog .progress-bar').css('width', percentComplete + '%');
					console.log(percentComplete);
				}
			}, false);
			return xhr;
		},
		success: function (response) {
			console.log(response); 
			jQuery('#deliver_button').prop('disabled', false);
			var message='<div class="conversation_message">' + jQuery('#user_name').val() + ': ' + jQuery('#requestMessage').val();
			$('.deliver-order-prog .progress-bar').css('width', '100%');
					$('.deliver-order-prog .progress-bar').text('100%');
			$('.deliver-order-prog .progress-bar').css('background-color', '#5cb85c');
			var obj = JSON.parse(response);
			
			message=message + '</div>';
			jQuery('.conversation').append(message);
			jQuery('#requestMessage').val('');
			jQuery('#attachment').val('');
			jQuery('#file').val('');
			$(".type_button").removeAttr("disabled");
			window.location=ajax_object_speedy.site_url + '/place-an-order/?type=' + obj.type + '&order=' + obj.order_id + '&step=3';
		}
	});
	
});

jQuery('.placeOrderForm .type_button').click(function(){
	var error='<p>';
	//check for validation
	if(jQuery('#file').val()=='' && jQuery('#files_link').val()==''){
		error +="Please upload a file or enter file link.</br>";
	}
	if(jQuery('#dimension_width').val()==''){
		error +="Please enter Dimension width</br>";
	}
	if(jQuery('#dimension_height').val()==''){
		error +="Please enter Dimension height</br>";
	} 
	if(jQuery("input[name=order_type]").val()=='seperation'){
		if(jQuery("#outputTempFile").val()=='' && jQuery('#speedyOutputTemp').prop("checked") == false){
			error +="Please select default template or upload your own</br>";
		}
	}
	//console.log(jQuery('#file').val()); console.log(jQuery('#files_link').val())
	
	
	error +='</p>';
	jQuery('#formErrorMessaages').html(error);
});

//get coupon code
$('.couponForm').on('submit', function(e) {
	
	e.preventDefault();
	var data = {
			'action': 'get_coupon',
			'coupon': jQuery('#coupon').val(),
		};
	
	
	jQuery('.couponForm .btn').prop('disabled', true);
		
	$('.coupon-prog').css('display', 'inline-block');

		jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			console.log(response);	
			var orderTotal=$('#order_amount').val();
			var discount=0;
			if(response=='Free'){
				discount=orderTotal;
			}else if(response>0){
				discount=(response/100) * orderTotal;
			}else if(response=='coupon used'){
				alert("Coupon already used.");
			}else{
				alert("Invalid coupon code.");
			}
			//console.log(discount);
			$('#discountAmountText').text(discount);
			$('#discountValue').val(discount);
			var orderRemAmount=orderTotal - discount;
			$('#orderAmountText').text(orderRemAmount);
			var couponCode=$('#coupon').val();
			$('#couponCode').val(couponCode);
			jQuery('.couponForm .btn').prop('disabled', false);
			$('.coupon-prog').css('display', 'none');
		});
	
	
});

//get coupon code first form
$('.payFormCouponBtn').on('click', function(e) {
	
	e.preventDefault();
	var data = {
			'action': 'get_coupon',
			'coupon': jQuery('#coupon').val(),
		};
	
	
	jQuery('.couponForm .btn').prop('disabled', true);
		
	$('.coupon-prog').css('display', 'inline-block');

		jQuery.post(ajax_object_speedy.ajax_url, data, function(response) {
			console.log(response);	
			var orderTotal=$('#order_amount').val();
			var discount=0;
			if(response=='Free'){
				discount=orderTotal;
			}else if(response>0){
				discount=(response/100) * orderTotal;
			}else if(response=='coupon used'){
				alert("Coupon already used.");
			}else{
				alert("Invalid coupon code.");
			}
			//console.log(discount);
			$('#discountAmountText').text(discount);
			$('#discountValue').val(discount);
			var orderRemAmount=orderTotal - discount;
			$('#orderAmountText').text(orderRemAmount);
			var couponCode=$('#coupon').val();
			$('#couponCode').val(couponCode);
			jQuery('.couponForm .btn').prop('disabled', false);
			$('.coupon-prog').css('display', 'none');
		});
	
	
});

});