<!DOCTYPE html>
<?php 
/**
 * Template Name: Landing Page 2
 *
 */
 ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<?php do_action( 'wp_head' ); ?>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Speedy Sep</title>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->

<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/responsive.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="toparea">
	<div class="header">
		<div class="logo">
			<a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/logo.png" class="img-fluid" alt="Speedy Sep"></a>
		</div>
	</div>
	<div class="topinfo">
		<h1>Simplify your screen printing process with SpeedySep</h1>
		<h3>Trusted by over 500 screen printers worldwide</h3>
	</div>
	<div class="signuparea">
		<div class="signupbox">
			<div class="inputfild"><input name="" type="text" id="page_email_input" placeholder="Enter your email address"></div>
			<div class="sendbtn"><a class="popup-with-form" href="#popupcontent">Start free trial</a></div>
		</div>
		<p>Try Speedysep free for your 1st order. By entering your email, you <br> agree to receive marketing emails from Speedysep.</p>
	</div>
	<div class="laptoppic">
		<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/laptop-pic.png" class="img-fluid" alt="laptop">
	</div>
</div>
<div class="servicesarea">
	<div class="singleservice">
		<div class="servicebox">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/icon01.png" alt="">
			<h3>Vector Drawing</h3>
			<p>Save time and reduce your design costs by 80% with hand-drawn, vectored logo in 24* hours.</p>
		</div>
	</div>
	<div class="singleservice">
		<div class="servicebox">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/icon02.png" alt="">
			<h3>Pricing as low as $10/order</h3>
			<p>Whether you print from a warehouse, a garage, or the trunk of your car, SpeedySep has you covered. Receive ready to print files for just $10/order</p>
		</div>
	</div>
	<div class="singleservice">
		<div class="servicebox">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/icon03.png" alt="">
			<h3>Trusted by over 500 screen printers worldwide</h3>
			<p>Speedysep handles everything from vectoring to color separations all on one secured platform</p>
		</div>
	</div>
</div>
<div class="clientarea">
	<div class="clientinfo">
		<p>Over 500 screen printers, both big and small, are growing their businesses with SpeedySep</p>
	</div>
	<div class="clientboxpc">
		<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logos.png" class="img-fluid" alt="">
	</div>
	<div class="clientboxmobile">
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo01.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo02.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo03.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo04.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo05.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo06.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo07.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo08.png" alt="">
		</div>
		<div class="clientlogo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage02/images/client-logo09.png" alt="">
		</div>
	</div>
</div>
<div class="footerarea">
	<div class="footerinfo">
		<p>USE COUPON CODE</p>
		<div class="cuponbox">
			<span>FREESPEEDY</span>
			<div class="freetrial">
				<a class="popup-with-form" href="#popupcontent">Start free trial</a>
			</div>
		</div>
	</div>
</div>
<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required>
					</div>
					<div class="formlist">
						<input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="create your account">
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.magnific-popup.js"></script>
<script>
	$('.popup-with-form').magnificPopup({
		type: 'inline',
	});

var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function($){
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name').val(),		
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Account create successfully.');
				 window.location = "<?php echo site_url() ?>/user-dashboard";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

$('#page_email_input').on('change', function(){
	$('#email').val($(this).val());
});

});
</script>
</body>
</html>
