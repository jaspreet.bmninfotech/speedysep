<?php
/**
 * Template Name: Front page
 *
 */

get_header(); ?>
<header class="front-page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="logo-left">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
				<div class="logo-right">
					<a href="#" data-toggle="modal" data-target="#registrationModal" title="Login/Signup">Login/Signup</a>
				</div>
			</div>
		</div>
	</div>
</header>
<div  class="container">
		<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location.', 'twentysixteen' ); ?></p>

				</div><!-- .page-content -->
		</section><!-- .error-404 -->

</div><!-- .content-area -->



<?php get_footer(); ?>
