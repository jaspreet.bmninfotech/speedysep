<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<header class="front-page-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="logo-left">
				<?php $page_id=269; ?>
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#registrationModal" title="<?php echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text',true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text', true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="text-center site-logo hidden-xs">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="logo-right">
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#loginModal" title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
</header>
<div  class="container-fluid sep-home-buttons">
	<div class="btn-left"><a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/front-home.png"></a></div>
</div>
<div class="container">

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			// Start the loop.
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

				// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination(
				array(
					'prev_text'          => __( 'Previous page', 'twentysixteen' ),
					'next_text'          => __( 'Next page', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
				)
			);

			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_sidebar(); ?>
</div>


<footer class="front-page-footer">
	<div class="container">
		<div class="row">
			<div class="text-right">
				<ul class="nav navbar-nav">
				<?php
					$menu_name = 'speedysep-user-menu';
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menu_items = wp_get_nav_menu_items(9);
					foreach ( (array) $menu_items as $key => $menu_item ) { 
						$class='';
						$current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
						foreach($menu_item->classes as $item_class){
							$class.=$item_class." ";
						}
						$title = $menu_item->title;
						$url = $menu_item->url;
						if($menu_item->menu_item_parent>0){
							$menu_list .= '<li class="menu-sub-item ' . $current . '"><a href="'.$url.'">'.$title.' | </a></li>';
						}else{
						$menu_list .= '<li class="' . $current . '"><a href="'.$url.'">'.$title.' |</a></li>';
						}
					}
				echo $menu_list;
				?>
				<li class="copyright-text">SpeedySep &copy; <?php echo date('Y') ?></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<?php get_footer(); ?>
