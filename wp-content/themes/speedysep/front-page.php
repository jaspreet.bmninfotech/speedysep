<?php
/**
 * Template Name: Front page
 *
 */
?>

<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->


<?php wp_head(); ?>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage03/magnific-popup.css" rel="stylesheet" type="text/css" />

<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>	


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
    
<?php
		// Start the loop.
		while ( have_posts() ) :
			the_post();
		?>
<div class="headerarea">
	<div class="container">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/logo.png" alt="" /></a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                     
                        
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon.png" alt="">
                </div>
            </div>
        </div>
        <div class="floatingnav">
        	<div class="mobilemenuheading">
            	<div class="mobilelogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/mobile-logo.png" alt="">
                </div>
            	<div class="closemenu">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/menu-icon-close.png" alt=""></a>
                </div>
            </div>
            <div class="menubox">
                <ul>
                    <li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                    <li><a href="<?php echo home_url("/pricing"); ?>">Pricing</a></li>
                    <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                    <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                    <li><a href="https://speedysep.com/blog/">Blog</a></li>
                   
                 
                    <?php if(!is_user_logged_in()){ ?>
                    <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                     <?php }else{ ?> 
                     <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                    <?php } ?>
                    
                    
                    <?php if(!is_user_logged_in()){ ?>
                    <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                    <?php }else{ ?> 
                     <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                    <?php } ?> 
                     
                    
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="hometoparea">
	<div class="hometopinfo">
    	<div class="hometopleft">
        	<h1>Simplify Your Screen Printing Process</h1>
            <p> SpeedySep helps with custom t-shirt design, <br> color separations, and converting your artwork <br> to vector format. </p>
            <div class="newsletter">
			<div class="inputfild"><input name="" type="text" placeholder="Enter your email address" id="page_email_input"></div>
			<div class="send"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
		</div>
        </div>
        <div class="hometopright">
        	<div class="videoholder"> <!-- https://www.youtube.com/embed/ZgS4nB2-O6k -->
            	<iframe width="100%" height="433" src="https://www.youtube.com/embed/Ib0-NJwSHyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="gallerybox">
        <div id="owl-demo" class="owl-carousel">
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/thumb01.png" alt="thumb" />
                </div>
            </div>
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/thumb02.png" alt="thumb" />
                </div>
            </div>
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/thumb03.png" alt="thumb03" />
                </div>
            </div>
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/thumb04.png" alt="Dragon" />
                </div>
            </div>
            
            
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/Dragon.jpg" alt="Dragon" />
                </div>
            </div>
            
           
             <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/eat-more-3000x3000-01.jpg" alt="eat" />
                </div>
            </div>
            
           
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/Firefighter-V4.jpg" alt="Firefighter" />
                </div>
            </div>
            
             
             <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/Gable-Sardinia.jpg" alt="Sardinia" />
                </div>
            </div>
            
           
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/gorilla.jpg" alt="gorilla" />
                </div>
            </div>
            
            
            <div class="item">
                <div class="carsolitem">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/Phi-Alpha-Delta.jpg" alt="Alpha" />
                </div>
            </div>
            
           
            
        </div>
    </div>
</div>
<div class="homecontent">
	<div class="container" id="features">
    	<h3>With you for all of your design needs</h3>
        <h4>one platform for custom vector designs, and <br> color separated ready to print art.</h4>
        <div class="homeblock">
            <div class="blockright">
            	<h2>Recreate - Your fuzzy jpeg</h2>
                <p>Login, choose a plan, and upload your design. <br> Your artwork will be ready to download as a <br> ready to print file in 24* hours with <br> 100% money back guarantee.</p>
                <div class="hidemobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
            <div class="blockleft">
            	<div class="thumbnail block1">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/pic01.png" alt="" />
                </div>
                <div class="showmobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
        </div>
        <div class="homeblock">
            <div class="blockright left secondspace">
            	<h2>Color Separate your design</h2>
                <p>Our designers separate your image into <br> four colors process, spot colors, <br> or simulation in 24 hours.</p>
                <div class="hidemobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
            <div class="blockleft right">
            	<div class="thumbnail block2">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/pic02.png" alt="" />
                </div>
                <div class="showmobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
        </div>
        <div class="homeblock">
            <div class="blockright thirdspace">
            	<h2>Develop a concept</h2>
                <p>Easily convert any sketch to a <br> brand-new custom vector design and <br> get your artwork ready to print <br> in 24 hours.</p>
                <div class="hidemobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
            <div class="blockleft">
            	<div class="thumbnail block3">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/pic03.png" alt="" />
                </div>
                <div class="showmobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
        </div>
        <div class="homeblock">
            <div class="blockright left">
            	<h2>Manage everything</h2>
                <p>Use a single dashboard to manage <br> orders, upload and download <br> designs and make payments <br> anywhere you go.</p>
                <div class="hidemobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
            <div class="blockleft right">
            	<div class="thumbnail block4">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/pic04.png" alt="" />
                </div>
                <div class="showmobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
        </div>
        <div class="homeblock">
            <div class="blockright">
            	<h2>Control Your Design</h2>
                <p>Take control of the design process. <br> Speak to our designers, and download <br> your ready artwork from your desktop, <br> mobile or tablet.</p>
                <div class="hidemobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
            <div class="blockleft">
            	<div class="thumbnail block5">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/pic05.png" alt="" />
                </div>
                <div class="showmobile"><a class="popup-with-form" href="#popupcontent">Try It Free</a></div>
            </div>
        </div>
    </div>
</div>
<div class="customerarea">
	<div class="container">
		<div class="big-h4">
    	<h4>We love our customers</h4>
		</div>
        <h4>Shops of all types choose Speedysep</h4>
    	<div class="customerinfo">
        	<div class="customeritem">
            	<h3>500+</h3>
                <h4>Customers</h4>
                <div class="customerbox">
                	<div class="ratting"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/ratting-star.png" alt="" /></div>
                	<p>I dropped my cc subscription since <br> working with you has made my <br> prepress much simpler. <br> thank you!</p>
                    <div class="companyname">
                    	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/ben-harrison.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="customeritem">
            	<h3>10,000+</h3>
                <h4>Designs created</h4>
                <div class="customerbox">
                	<div class="ratting"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/ratting-star.png" alt="" /></div>
                	<p>This design worked well. <br> Thank you for including <br> the white underbase, <br> that was great.</p>
                    <div class="companyname">
                    	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/morgan-ufo.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="customeritem">
            	<h3>15,000+</h3>
                <h4>Color separations</h4>
                <div class="customerbox">
                	<div class="ratting"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/ratting-star.png" alt="" /></div>
                	<p>Very quick and responsive. Understood what need to be conceptualised from horrible drawings and then able to update and edit. Thank you</p>
                    <div class="companyname">
                    	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/alex-avomance.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="examplearea">
	<div class="container">
    	<div class="exampleinfo">
        	<div class="exampleinfoleft">
            	<h3>You are in <span>good company</span></h3>
                <p>Over 500 screen printers, <br> both big and small, are growing <br> their businesses with SpeedySep.</p>
            </div>
            <div class="exampleinforight">
            	<div class="partnerlogo">
                	<ul>
                    	<li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo01.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo02.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo03.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo04.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo05.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo06.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo07.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo08.png" alt="" /></li>
                        <li><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/partner-logo09.png" alt="" /></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footerarea">
	<div class="container">
    	<div class="footertop">
        	<div class="footertopleft">
            	<h5>Start your business <br> journey with SpeedySep</h5>
            </div>
            <div class="footertopright">
            	<a class="popup-with-form" href="#popupcontent">Try It Free</a>
            </div>
        </div>
    	<div class="footerinfo">
        	<div class="footerleft">
            	<div class="footerlogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/footer-logo.png" alt="" />
                </div>
            	<p>SpeedySep is risk-free with a 100% money-back guarantee. Simplify your screen printing process with SpeedySeps.</p>
            </div>
            <div class="footerright">
            	<div class="col-1">
                	<ul>
                    	<li><a href="https://speedysep.com/pricing">Pricing</a></li>
                        <li><a href="https://speedysep.com/speedy-sep-guide-for-vector-order/">Vectorizing</a></li>
                        <li><a href="https://speedysep.com/speedy-sep-guide-for-color-separation-order/">Color Separation</a></li>
                        <?php if(!is_user_logged_in()){ ?>
						<li><a class="popup-with-form" href="#popupcontent" title="SignUp">SignUp</a></li>
						<?php }else{ ?> 
						<li><a href="<?php echo home_url() ?>/user-dashboard"  title="SignUp">SignUp</a></li>
						<?php } ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                    </ul>
                </div>
                <div class="col-1">
                	<ul>
                	    <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                  
                    	<li><a href="https://speedysep.com/contact-us/">Contact Us</a></li>
                        <li><a href="https://speedysep.com/about-us/">About Us</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>
                        <li><a href="https://speedysep.com/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="https://speedysep.com/terms-and-conditions/">Terms of use</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyrightarea">
	<div class="container">
    	<div class="copyrightleft">
        	<ul>
            	<li><a href="https://twitter.com/sep_speedy" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/twitter-icon.png" alt="twitter" /></a></li>
                <li><a href="https://www.facebook.com/speedysep" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/facebook-icon.png" alt="facebook" /></a></li>
                <li><a href="https://www.instagram.com/speedysep/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/instagram-icon.png" alt="instagram" /></a></li>
                <li><a href="https://www.pinterest.com/speedysep/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/images/printerset-icon.png" alt="printerset" /></a></li>
            </ul>
        </div>
        <div class="copyrightright">
        	<p><a href="#">Sitemap</a> | <a href="#">Sitemap XML</a></p>
            <p>Copyright &copy; 2019 by <a href="<?php  home_url(); ?>">speedysep.com</a></p>
        </div>
    </div>
</div>
<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formlist">
						<input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="create your account">
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>

<div style="display:none">
    <div id="popup-membership" class="popupouter">
        <div class="popupbox">
            <div class="contactform">
                <h1>Start your Journey with Speedysep</h1>
                <form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
                    <div class="formlist">
                        <input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
                    </div>
                    <div class="formlist">
                        <input name="" type="password" placeholder="Password" id="password" name="password" required >
                    </div>
                    <div class="formlist">
                        <input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
                    </div>
                    <div class="formsend">
                        <input name="" type="submit" id="submit" value="create your account">
                    </div>
                </form>
                <br>
                <div class="registration-message"><p></p></div>
            </div>
        </div>
    </div>
</div>

<div style="display:none">
	<div id="popupcontentlogin" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Login to Speedysep account</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="login-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="Login to my account">
					</div>
					<div class="login-reset-pass" style="margin-top: 10px; text-align: right;">
						<p><a href="<?php echo site_url() ?>/wp-login.php?action=lostpassword" title="<?php _e('Reset Password', 'speedy') ?>" style="color:#fff;"><?php _e('Reset Password', 'speedy') ?></a></p>
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>
<div style="display:none">
	<div id="popupcontentbasic" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/basic-example-popup.jpg" style="max-width: 100%;">
	</div>
</div>
<div style="display:none">
	<div id="popupcontentstandard" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/standard-example-popup.jpg" style="max-width: 100%;">
	</div>
</div>
<div style="display:none">
	<div id="popupcontentpremium" class="popupouter" style="max-width:400px;margin:auto">
		<img src="https://speedysep.com/wp-content/themes/speedysep/images/premium-example-popup.jpg" style="max-width: 100%;">
	</div>
</div>
<?php
			// End of the loop.
		endwhile;
		?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/pricing/js/owl.carousel.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/landingpage01/js/jquery.magnific-popup.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(".mobilemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '0'
			}, 300 );
		$(this).addClass("opened");
	});
	$(".closemenu").click(function (event) {
		event.preventDefault();
		$( ".floatingnav" ).animate({
				'right': '-330px'
			}, 300 );
		$(this).removeClass("opened");
	});  
}); 

</script>
<script>
	jQuery('.popup-with-form').magnificPopup({
		type: 'inline',
	});

    jQuery('.membership').magnificPopup({
        type: 'inline'
    });

	jQuery('.popup-with-form-login').magnificPopup({
		type: 'inline',
	});

var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';

jQuery(document).ready(function($){
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name').val(),		
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Account created successfully.');
				window.location = "<?php echo home_url() ?>/user-dashboard";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

//login form submit
$('.login-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'login_user',	
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Logging in...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='loggedin'){
				jQuery('.registration-message').text('Logging in...');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
					//setTimeout(function() {
					  window.location = "<?php echo home_url() ?>/user-dashboard";
					//}, 5000);
			}else if(response == 'logginMembership'){
                window.location = "<?php echo site_url() ?>/membership";	
			}else{
				jQuery('.registration-message').html('<p class="error">Invalid Email/password.</p>');
			}
	});
});


$('#page_email_input').on('change', function(){
	$('#email').val($(this).val());
});

});
</script>
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
	    pagination : false,
	    rewindNav : true,
        navigation : true
      });
    });
</script> 
</body>
</html>
