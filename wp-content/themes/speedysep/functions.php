<?php
require_once get_template_directory() . '/inc/customfunctions.php';
/**
 * Twenty Sixteen functions and definitions
 *
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own twentysixteen_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function twentysixteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
		 * If you're building a theme based on Twenty Sixteen, use a find and replace
		 * to change 'twentysixteen' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'twentysixteen' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Twenty Sixteen 1.2
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'twentysixteen' ),
				'social'  => __( 'Social Links Menu', 'twentysixteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom color scheme.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Dark Gray', 'twentysixteen' ),
					'slug'  => 'dark-gray',
					'color' => '#1a1a1a',
				),
				array(
					'name'  => __( 'Medium Gray', 'twentysixteen' ),
					'slug'  => 'medium-gray',
					'color' => '#686868',
				),
				array(
					'name'  => __( 'Light Gray', 'twentysixteen' ),
					'slug'  => 'light-gray',
					'color' => '#e5e5e5',
				),
				array(
					'name'  => __( 'White', 'twentysixteen' ),
					'slug'  => 'white',
					'color' => '#fff',
				),
				array(
					'name'  => __( 'Blue Gray', 'twentysixteen' ),
					'slug'  => 'blue-gray',
					'color' => '#4d545c',
				),
				array(
					'name'  => __( 'Bright Blue', 'twentysixteen' ),
					'slug'  => 'bright-blue',
					'color' => '#007acc',
				),
				array(
					'name'  => __( 'Light Blue', 'twentysixteen' ),
					'slug'  => 'light-blue',
					'color' => '#9adffd',
				),
				array(
					'name'  => __( 'Dark Brown', 'twentysixteen' ),
					'slug'  => 'dark-brown',
					'color' => '#402b30',
				),
				array(
					'name'  => __( 'Medium Brown', 'twentysixteen' ),
					'slug'  => 'medium-brown',
					'color' => '#774e24',
				),
				array(
					'name'  => __( 'Dark Red', 'twentysixteen' ),
					'slug'  => 'dark-red',
					'color' => '#640c1f',
				),
				array(
					'name'  => __( 'Bright Red', 'twentysixteen' ),
					'slug'  => 'bright-red',
					'color' => '#ff675f',
				),
				array(
					'name'  => __( 'Yellow', 'twentysixteen' ),
					'slug'  => 'yellow',
					'color' => '#ffef8e',
				),
			)
		);

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Sixteen 1.6
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentysixteen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentysixteen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentysixteen_resource_hints', 10, 2 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'twentysixteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
	/**
	 * Register Google fonts for Twenty Sixteen.
	 *
	 * Create your own twentysixteen_fonts_url() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function twentysixteen_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family' => urlencode( implode( '|', $fonts ) ),
					'subset' => urlencode( $subsets ),
				),
				'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Theme block stylesheet.
	wp_enqueue_style( 'twentysixteen-block-style', get_template_directory_uri() . '/css/blocks.css', array( 'twentysixteen-style' ), '20181230' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20181230', true );

	wp_localize_script(
		'twentysixteen-script',
		'screenReaderText',
		array(
			'expand'   => __( 'expand child menu', 'twentysixteen' ),
			'collapse' => __( 'collapse child menu', 'twentysixteen' ),
		)
	);
	//include bootstrap
	wp_enqueue_style( 'speedysep-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');

//	wp_enqueue_style( 'popup-style', get_template_directory_uri() . '/assets/pricing/style.css' );
	
	wp_enqueue_style( 'popup-style', get_template_directory_uri() . '/assets/landingpage03/magnific-popup.css' );
	
	wp_enqueue_style( 'speedysep-googlefont', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap');

	
//	wp_enqueue_style( 'res-style', get_template_directory_uri() . '/responsive.css' ); 
//		wp_enqueue_style( 'style-chk-style', get_template_directory_uri() . '/style-chk.css' );
//	https://speedysep.com/wp-content/themes/speedysep/assets/landingpage03/magnific-popup.css
	wp_enqueue_script( 'popup-script', get_template_directory_uri() . '/assets/about-landing/js/jquery.magnific-popup.js', array('jquery') );
	
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Enqueue styles for the block-based editor.
 *
 * @since Twenty Sixteen 1.6
 */
function twentysixteen_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'twentysixteen-block-editor-style', get_template_directory_uri() . '/css/editor-blocks.css', array(), '20181230' );
	// Add custom fonts.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );
}
add_action( 'enqueue_block_editor_assets', 'twentysixteen_block_editor_styles' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

function speedysep_footer_menu() {
  register_nav_menus(
    array(
      'speedysep-footer-menu' => __( 'Speedy Sep Footer Menu' )
    )
  );
}
add_action( 'init', 'speedysep_footer_menu' );

function speedysep_user_menu() {
  register_nav_menus(
    array(
      'speedysep-user-menu' => __( 'Speedy Sep User Menu' )
    )
  );
}
add_action( 'init', 'speedysep_user_menu' );

/**
 * Proper way to enqueue scripts and styles.
 */
function speedysep_scripts() {
    wp_enqueue_style( 'bootstrap-style-cdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap-style-theme-cdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' );
    wp_enqueue_style( 'fontawsome-style', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css' );
    wp_enqueue_script( 'bootstrap-script', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'speedysep-script', get_stylesheet_directory_uri().'/js/script.js', array(), '1.0.0', true );
	// in JavaScript, object properties are accessed as ajax_object_ojsm.ajax_url
	wp_localize_script( 'speedysep-script', 'ajax_object_speedy',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'stylesheet_dir_uri'=>get_stylesheet_directory_uri(), 'site_url'=>site_url() ) );
}
add_action( 'wp_enqueue_scripts', 'speedysep_scripts' );

function speedysep_admin_scripts() {
     wp_enqueue_script( 'speedysep-script', get_stylesheet_directory_uri().'/js/admin-script.js', array(), '1.0.0', true );
	// in JavaScript, object properties are accessed as ajax_object_ojsm.ajax_url
	wp_localize_script( 'speedysep-script', 'ajax_object_speedy',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'stylesheet_dir_uri'=>get_stylesheet_directory_uri(), 'site_url'=>site_url() ) );
}
add_action( 'admin_enqueue_scripts', 'speedysep_admin_scripts' );



add_shortcode('speedysep_registration', 'speedysep_registration_callback');
function speedysep_registration_callback(){
	ob_start();
	include('inc/views/registration.php');
	return ob_get_clean();
}

add_shortcode('speedysep_login', 'speedysep_login_callback');
function speedysep_login_callback(){
	ob_start();
	include('inc/views/login.php');
	return ob_get_clean();
}

//thankyou shortcode
add_shortcode('speedysep_thankyou', 'speedysep_thankyou_callback');
function speedysep_thankyou_callback(){
	ob_start();
	include('inc/views/thankyou.php');
	return ob_get_clean();
}

//open orders shortcode
add_shortcode('speedysep_open_orders', 'speedysep_open_orders_callback');
function speedysep_open_orders_callback(){
	ob_start();
	include('inc/views/open-orders.php');
	return ob_get_clean();
}

//open orders shortcode
add_shortcode('speedysep_delivered_orders', 'speedysep_delivered_orders_callback');
function speedysep_delivered_orders_callback(){
	ob_start();
	include('inc/views/delivered-orders.php');
	return ob_get_clean();
}

//order-complete shortcode
add_shortcode('speedysep_order_complete', 'speedysep_order_complete_callback');
function speedysep_order_complete_callback(){
	if(isset($_GET['order_id'])){
		//update order status
		  update_post_meta($_GET['order_id'], 'order_status', 'completed');
	}
	ob_start();
	include('inc/views/order-complete.php');
	return ob_get_clean();
}

//order-complete shortcode
add_shortcode('speedysep_revise_order', 'speedysep_revise_order_callback');
function speedysep_revise_order_callback(){
	if(isset($_GET['order_id'])){
		//update order status
		  update_post_meta($_GET['order_id'], 'order_status', 'revised');
	}
	ob_start();
	include('inc/views/revise-order.php');
	return ob_get_clean();
}

//add payment method
add_shortcode('speedysep_add_payment_method', 'speedysep_add_payment_method_callback');
function speedysep_add_payment_method_callback(){
	ob_start();
	include('inc/views/payment-method.php');
	return ob_get_clean();
}

//add profile edit shortcode
add_shortcode('speedysep_edit_profile', 'speedysep_edit_profile_callback');
function speedysep_edit_profile_callback(){
	ob_start();
	include('inc/views/edit-profile.php');
	return ob_get_clean();
}


//register user
add_action( 'wp_ajax_register_user', 'register_user_callback' );
add_action( 'wp_ajax_nopriv_register_user', 'register_user_callback' );
function register_user_callback() { // print_r($_POST); die(0);
	//process form
	global $wpdb;
	if(array_key_exists('mem_id', $_POST) && $_POST['mem_id'] != '' && array_key_exists('amount', $_POST) && $_POST['amount'] != '')
	{
		$amount = $_POST['amount'];
		$mem_id = $_POST['mem_id'];
		$duration = $_POST['duration'];

	}else
	{
		$amount = 0;$mem_id = '';$duration = '';
	}
	
	$user_name=sanitize_text_field($_POST['email']);
	$random_password=sanitize_text_field($_POST['password']);
	$user_email=sanitize_text_field($_POST['email']);
	if($user_email!=''){
	if (email_exists($user_email) == false ) {
		//$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_id = wp_create_user( $user_name, $random_password, $user_email );
		update_user_meta($user_id, 'first_name', sanitize_text_field($_POST['full_name']));
		wp_new_user_notification($user_id, $deprecated = null, $notify = 'both');
		$rememberme=true;
		$creds = array(
			'user_login'    => sanitize_text_field($_POST['email']),
			'user_password' => sanitize_text_field($_POST['password']),
			'remember'      => $rememberme
		);
	 
		$user = wp_signon( $creds, true );
	 
		if ( is_wp_error( $user ) ) {
			$linda_error_message=$user->get_error_message();
			die( $linda_error_message);
		}else{
			if($mem_id != '')
			{
				echo "loggedin and membership created";
				$data = [
					'type' =>  $mem_id,
					'status' => 'Pending',
					'amount' => $amount,
					'user_id' => $user->ID,
					'duration' => $duration,
					'created'  => date("Y-m-d H:i:s")
				];
				// $table_name = $wpdb->prefix . "user_memberships";
				$membership = $wpdb->insert($wpdb->user_memberships,$data);

			}else{
				echo "loggedin";
			}
			// $sql = "INSERT INTO wpoc_memberships(type, status,amount,user_id,duration) VALUES (?,?,?,?,?)";
			// $stmt= $pdo->prepare($sql);
			// $stmt->execute($data);

		}
		//echo "yes";
	} else {
		echo "no";
		$user = $wpdb->get_row("SELECT * FROM $wpdb->users 
									WHERE  user_email = ".$user_email." Order By ID DESC LIMIT 1"
								);
		// $stmt = $pdo->prepare("SELECT * FROM wpoc_users WHERE user_email=?");
		// $stmt->execute([$user_email]); 
		// $user = $stmt->fetch();
		$data = [
				'type' =>  $mem_id,
				'status' => 'Pending',
				'amount' => $amount,

				'duration' => $duration,
				'updated'  => date("Y-m-d H:i:s")

			];
			$where = [ 'user_id' => $user->ID ]; 
		$wpdb->update($wpdb->user_memberships,$data,$where);
		// $sql = "UPDATE wpoc_memberships SET type=?, status=?, amount=? WHERE user
		// _id=?";
		// $stmt= $pdo->prepare($sql);
		// $stmt->execute([$mem_id, 'Pending',$amount, $user->ID]);

	}
	}else{
		echo "empty";
	}
	die(0);
}

//Login user
add_action( 'wp_ajax_login_user', 'login_user_callback' );
add_action( 'wp_ajax_nopriv_login_user', 'login_user_callback' );
function login_user_callback() { //print_r($_POST); 
	//process form.
	global $wpdb;
	$username=sanitize_text_field($_POST['email']);
	$password=sanitize_text_field($_POST['password']);
	$remember=$_POST['rememberme'];
	$rememberme=false;
	if($remember){
		$rememberme=true;
	}
	
	$creds = array(
        'user_login'    => sanitize_text_field($_POST['email']),
        'user_password' => sanitize_text_field($_POST['password']),
        'remember'      => $rememberme
    );
 
    $user = wp_signon( $creds, true );

    if ( is_wp_error( $user ) ) {
        $linda_error_message=$user->get_error_message();
		die( $linda_error_message);
    }else{
    	$existmembership = $wpdb->get_row("SELECT * FROM $wpdb->user_memberships 
									WHERE  user_id = ".$user->ID." AND status = 'Pending' Order By user_id DESC LIMIT 1"
								);
    	if($existmembership == null)
    	{
			echo "loggedin";
    	}else{
    		echo 'logginMembership';
    	}
	}
	
	die(0);
}

// user update when custom submit
add_action( 'wp_ajax_user_update', 'user_update_callback' );
add_action( 'wp_ajax_nopriv_user_update', 'user_update_callback' );
function user_update_callback() { //print_r($_POST); 
	//process form.
	global $wpdb;
	$email=sanitize_text_field($_POST['email']);
	$firstname=sanitize_text_field($_POST['first_name']);
	$lastname=sanitize_text_field($_POST['last_name']);
	$phone_no =sanitize_text_field($_POST['phone']);
	$mock =sanitize_text_field($_POST['mock']);
	$order =sanitize_text_field($_POST['order']);
	$user_id=get_current_user_id();
 	if($user_id != null)
 	{

		$where = [ 'ID' => $user_id]; 
		$userInfo = get_user_meta($user_id);
		if(array_key_exists('first_name', $userInfo))
		{
			update_user_meta($user_id, 'first_name', $firstname);
		}else{
			$data= ['user_id'=>$user_id,'meta_key'=>'first_name','meta_value'=>$firstname ];
			 $wpdb->insert($wpdb->usermeta,$data);
		}
		if(array_key_exists('last_name', $userInfo))
		{
			update_user_meta($user_id, 'last_name', $lastname);
		}else{
			$data= ['user_id'=>$user_id,'meta_key'=>'last_name','meta_value'=>$lastname ];
			$wpdb->insert($wpdb->usermeta,$data);
		}
		if(array_key_exists('phone_no', $userInfo))
		{
			update_user_meta($user_id, 'phone_no', $phone_no);
		}else{
			$data= ['user_id'=>$user_id,'meta_key'=>'phone_no','meta_value'=>$phone_no ];
			$wpdb->insert($wpdb->usermeta,$data);
		}
		
		if(array_key_exists('mockups', $userInfo))
		{
			update_user_meta($user_id, 'mockups', $mock);
		}else{
			$data= ['user_id'=>$user_id,'meta_key'=>'mockups','meta_value'=>$mock ];
			$wpdb->insert($wpdb->usermeta,$data);
		}
		if(array_key_exists('orders', $userInfo))
		{
			update_user_meta($user_id, 'orders', $order);
		}else{
			$data= ['user_id'=>$user_id,'meta_key'=>'orders','meta_value'=>$order ];
			$wpdb->insert($wpdb->usermeta,$data);
		}
		//send to admin
			$author_id=1;
			$admin_info = get_userdata($author_id);
			//Send delivered Order email
			$to = $admin_info->user_email;
			$subject = 'custom membership request on Speedysep';
			$body = "Hello,<br> 
					$firstname placed a new custom membership request on your site. <br>
					Request:<br>
					<b>Mockups#:</b> $mock <br>
					<b>Orders:</b> $order <br>
					
				
					<br>Thank you,<br>
					Speedysep Inc

					";
			$headers = array('Content-Type: text/html; charset=UTF-8');
			 
			wp_mail( $to, $subject, $body, $headers );

			//send to customer
			
			//Send delivered Order email
			$userdetail = get_userdata($user_id);
			$to = $userdetail->user_email;
			$subject = 'Request Sent';
			$body = 'Hello '.$firstname.',<br> 
					Your Custom membership request has been sent successfully. <br>
					We will contact you in 24hours .<br>
					
					<br>Thank you,<br>
					Speedysep Inc

					';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			
			wp_mail( $to, $subject, $body, $headers );

 	}
    if ( is_wp_error( $user_id ) ) {
        $linda_error_message=$user->get_error_message();
		die( $linda_error_message);
    }else{
    	echo 'updateuser';
	}
	
	die(0);
}



//Login user
add_action( 'wp_ajax_get_membership_price', 'get_membership_price' );
add_action( 'wp_ajax_nopriv_get_membership_price', 'get_membership_price' );
function get_membership_price() { //print_r($_POST); 
	//process form
	$duration	=	sanitize_text_field($_GET['duration']);
	
	$membership = getmembershipslistRelatedType($duration);

    if ( is_wp_error( $membership ) ) {
        $linda_error_message=$membership->get_error_message();
		die( $linda_error_message);
    }else{
		
 		echo json_encode($membership);
 		die(0);
	}
	
	die(0);
}

//savenewmembership when user login and not subcribe any plan
add_action( 'wp_ajax_save_new_membership', 'save_new_membership' );
add_action( 'wp_ajax_nopriv_save_new_membership', 'save_new_membership' );
function save_new_membership() { //print_r($_POST); 
	//process form
	global $wpdb;

	$userID = get_current_user_id();
		
	if($userID != null)
	{

		if(array_key_exists('mem_id', $_POST) && $_POST['mem_id'] != '' && array_key_exists('amount', $_POST) && $_POST['amount'] != '' )
		{

			$mem_id	=	sanitize_text_field($_POST['mem_id']);
			$amount	=	sanitize_text_field($_POST['amount']);
			$duration	=	sanitize_text_field($_POST['duration']);
			$data = [
				'type' =>  $mem_id,
				'status' => 'Pending',
				'amount' => $amount,
				'user_id' => $userID,
				'duration' => $duration,
				'created'  => date("Y-m-d H:i:s")
			];
			// $table_name = $wpdb->prefix . "user_memberships";
			$existmembership = $wpdb->get_row("SELECT * FROM $wpdb->user_memberships 
									WHERE  user_id = ".$userID." AND status = 'Pending' Order By user_id DESC LIMIT 1"
								);
			if($existmembership != null)
			{
				// print_r($data);
				// die();
				$where = ['mem_id' => $existmembership->mem_id,'user_id' => $userID ,'status' => 'Pending']; 
				$wpdb->update($wpdb->user_memberships,$data,$where);
	
			}else{
				$membership = $wpdb->insert($wpdb->user_memberships,$data);
			}
		 	if ( is_wp_error( $membership ) ) {
		        $linda_error_message=$membership->get_error_message();
				die( $linda_error_message);
		    }else{	
			 	echo "membershipCreated";
			 	die(0);
			}
		}else
		{
			echo 'empty';
			die();
		}
		echo "not authorize to create membership";
		die(0);
	}
	
	die(0);
}
function getmembershipslistRelatedType($duration)
{
	global $wpdb;
	if($duration == 'month')
	{
		$type = 'M';
	}elseif($duration == 'year'){
		$type = 'y';
	}else{
		$type = 'M';
	}
	$memberships = $wpdb->get_results("SELECT * FROM $wpdb->memberships 
									WHERE  frequency = '".$type."' ");

	
	// return $memberships;
	$mem = [];
	if(!empty($memberships))
	{
		$mem['BRONZE'] = '';$mem['SILVER'] = '';
		$mem['GOLD'] = '';
		foreach ($memberships as $key => $value) {
			if($value->name == 'BRONZE')
			{
				
				$mem['BRONZE'] = $value;
			}elseif($value->name == 'SILVER')
			{
				$mem['SILVER'] = $value;
			}
			elseif($value->name == 'GOLD')
			{
				$mem['GOLD'] = $value;
			}
			
		}
	}
	
	return $mem;
}


//remove adminbar for non admin
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

// Function to change email address
 
function ss_sender_email( $original_email_address ) {
    return 'info@speedysep.com';
}
 
// Function to change sender name
function ss_sender_name( $original_email_from ) {
    return 'SpeedySep';
}
 
// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'ss_sender_email' );
add_filter( 'wp_mail_from_name', 'ss_sender_name' );

/******************************************************USER DASHBOARD AREA  START***************************************/

add_shortcode('place_an_order', 'place_an_order_callback');
function place_an_order_callback(){
	ob_start();
	include('inc/views/place_an_order.php');
	return ob_get_clean();
}

add_shortcode('membership', 'membership_plan');
function membership_plan(){
	 ob_start();
	include('inc/views/membership.php');
	 return ob_get_clean();
}


add_shortcode('speedy_user_checkout', 'speedy_user_checkout_callback');
function speedy_user_checkout_callback(){
	ob_start();
	include('inc/views/checkout.php');
	return ob_get_clean();
}

/*
* Creating a function to create our CPT
*/
 
function speedysep_custom_post_type() {
	
	//register order
	$order_labels = array(
        'name'                => _x( 'Orders', 'Post Type General Name', 'ojsm' ),
        'singular_name'       => _x( 'Order', 'Post Type Singular Name', 'ojsm' ),
        'menu_name'           => __( 'Orders', 'ojsm' ),
        'parent_item_colon'   => __( 'Parent Order', 'ojsm' ),
        'all_items'           => __( 'Orders', 'ojsm' ),
        'view_item'           => __( 'View Order', 'ojsm' ),
        'add_new_item'        => __( 'Add New Order', 'ojsm' ),
        'add_new'             => __( 'Add New', 'ojsm' ),
        'edit_item'           => __( 'Edit Order', 'ojsm' ),
        'update_item'         => __( 'Update Order', 'ojsm' ),
        'search_items'        => __( 'Search Order', 'ojsm' ),
        'not_found'           => __( 'Not Found', 'ojsm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'ojsm' ),
    );
	
	$args_order = array(
        'label'               => __( 'Orders', 'ojsm' ),
        'description'         => __( 'Orders of online job and servies.', 'ojsm' ),
        'labels'              => $order_labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
		'show_in_menu'  =>	true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 9,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type' => 'post',
        'capabilities' => array(
        'create_posts' => false,
        ),
        'map_meta_cap' => true,
    );
     
    register_post_type( 'service-orders', $args_order );
	
	//messages
	  $messages_labels = array(
        'name'                => _x( 'Messages', 'Post Type General Name', 'ojsm' ),
        'singular_name'       => _x( 'Message', 'Post Type Singular Name', 'ojsm' ),
        'menu_name'           => __( 'Messages', 'ojsm' ),
        'parent_item_colon'   => __( 'Parent Message', 'ojsm' ),
        'all_items'           => __( 'Messages', 'ojsm' ),
        'view_item'           => __( 'View Message', 'ojsm' ),
        'add_new_item'        => __( 'Add New Message', 'ojsm' ),
        'add_new'             => __( 'Add New', 'ojsm' ),
        'edit_item'           => __( 'Edit Message', 'ojsm' ),
        'update_item'         => __( 'Update Message', 'ojsm' ),
        'search_items'        => __( 'Search Message', 'ojsm' ),
        'not_found'           => __( 'Not Found', 'ojsm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'ojsm' ),
    );
	
	$args_messages = array(
        'label'               => __( 'Messages', 'ojsm' ),
        'description'         => __( 'Messages of online job and servies.', 'ojsm' ),
        'labels'              => $messages_labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', ),
        
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 9,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type' => 'post',
        'capabilities' => array(
        'create_posts' => false,
        ),
        'map_meta_cap' => true,
    );
     
    register_post_type( 'customers-messages', $args_messages );
	
 
} 
//add_action( 'init', 'speedysep_custom_post_type', 0 );


function speedysep_coupons(){
		//coupons
	  $messages_labels = array(
        'name'                => _x( 'Coupons', 'Post Type General Name', 'ojsm' ),
        'singular_name'       => _x( 'Coupon', 'Post Type Singular Name', 'ojsm' ),
        'menu_name'           => __( 'Coupons', 'ojsm' ),
        'parent_item_colon'   => __( 'Parent Coupon', 'ojsm' ),
        'all_items'           => __( 'Coupons', 'ojsm' ),
        'view_item'           => __( 'View Coupon', 'ojsm' ),
        'add_new_item'        => __( 'Add New Coupon', 'ojsm' ),
        'add_new'             => __( 'Add New', 'ojsm' ),
        'edit_item'           => __( 'Edit Coupon', 'ojsm' ),
        'update_item'         => __( 'Update Coupon', 'ojsm' ),
        'search_items'        => __( 'Search Coupon', 'ojsm' ),
        'not_found'           => __( 'Not Found', 'ojsm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'ojsm' ),
    );
	
	$args_messages = array(
        'label'               => __( 'Coupons', 'ojsm' ),
        'description'         => __( 'Coupons of speedysep.', 'ojsm' ),
        'labels'              => $messages_labels,
        'supports'            => array( 'title',  'custom-fields', ),
        
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 9,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'capabilities' => array(
        'create_posts' => true,
        ),
        'map_meta_cap' => true,
    );
     
    register_post_type( 'ss-coupons', $args_messages );
}
add_action( 'init', 'speedysep_coupons', 0 );

//add_action( 'admin_post_nopriv_create_order', 'create_order_callback');
//add_action( 'admin_post_create_order', 'create_order_callback');

add_action( 'wp_ajax_create_order', 'create_order_callback' );
add_action( 'wp_ajax_nopriv_create_order', 'create_order_callback' );
function create_order_callback() { //echo "<pre>"; print_r( $_POST);  exit('asdf');
	//process form
	$req_post = array(
		  'post_title'    => sanitize_text_field($_POST['modifications']),
		  'post_content'  => sanitize_text_field($_POST['notes']),
		  'post_status'   => 'private',
		  'post_type'	  => 'service-orders',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		$_SESSION['order_id']=$post_id;
		
		update_post_meta($post_id, 'files_link', sanitize_text_field($_POST['files_link']));
		update_post_meta($post_id, 'package', sanitize_text_field($_POST['packages']));
		update_post_meta($post_id, 'no_of_colors_wanted', sanitize_text_field($_POST['color_no']));
		if($_POST['dimension_any']!=''){
		update_post_meta($post_id, 'dimensions_of_artwork', sanitize_text_field($_POST['dimension_any']));
		}else{
		update_post_meta($post_id, 'dimensions_of_artwork', sanitize_text_field($_POST['dimension']));	
		}
		
		
		update_post_meta($post_id, 'dimension_width', sanitize_text_field($_POST['dimension_width']));
		update_post_meta($post_id, 'dimension_height', sanitize_text_field($_POST['dimension_height']));
		
		
		update_post_meta($post_id, 'halftones', sanitize_text_field($_POST['halftones']));		
		update_post_meta($post_id, 'extrafast', sanitize_text_field($_POST['extrafast']));	
		update_post_meta($post_id, 'underbase', sanitize_text_field($_POST['underbase']));
		update_post_meta($post_id, 'conceptual', sanitize_text_field($_POST['conceptual']));
		update_post_meta($post_id, 'weekend', sanitize_text_field($_POST['weekend']));
		
		
		update_post_meta($post_id, 'extra-format', $_POST['extra-format']);
		
		update_post_meta($post_id, 'modifications_details', sanitize_text_field($_POST['modifications']));
		update_post_meta($post_id, 'mockups', sanitize_text_field($_POST['mockups']));
		update_post_meta($post_id, 'notes', sanitize_text_field($_POST['notes']));
		update_post_meta($post_id, 'order_type', sanitize_text_field($_POST['order_type']));
		
		//for separation orders only
		if(sanitize_text_field($_POST['order_type'])=='seperation'){
			update_post_meta($post_id, 'sep_type', sanitize_text_field($_POST['sep_type']));
			update_post_meta($post_id, 'speedy_standard_output_temp', sanitize_text_field($_POST['speedy_output_temp']));
			
		}
		
		
		update_post_meta($post_id, 'customer_name', get_user_meta(get_current_user_id(), 'first_name', true));
		//upload files
		if ( $_FILES ) { 
		$files = $_FILES["project_files"];  
		$files_temp = $_FILES["project_files_temp"];  
		
		foreach ($files['name'] as $key => $value) {            
				if ($files['name'][$key]) { 
					$file = array( 
						'name' => $files['name'][$key],
						'type' => $files['type'][$key], 
						'tmp_name' => $files['tmp_name'][$key], 
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					); 
					$_FILES = array ("project_files" => $file); 
					foreach ($_FILES as $file => $array) {              
						$newupload = my_handle_attachment($file,$post_id); 
					}
				} 
			} 
		//template files
		foreach ($files_temp['name'] as $key => $value) {            
				if ($files_temp['name'][$key]) { 
					$files_temp = array( 
						'name' => $files_temp['name'][$key],
						'type' => $files_temp['type'][$key], 
						'tmp_name' => $files_temp['tmp_name'][$key], 
						'error' => $files_temp['error'][$key],
						'size' => $files_temp['size'][$key]
					); 
					$_FILES = array ("project_files_temp" => $files_temp); 
					foreach ($_FILES as $files_temp => $array) {              
						$newupload = my_handle_attachment_temp($files_temp,$post_id); 
					}
				} 
			}
		}
		$data['type']=sanitize_text_field($_POST['order_type']);
		$data['order_id']=$post_id;
	//echo "<pre>"; print_r($files_temp); exit('asdf');
		//wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$post_id.'&step=3');
		echo json_encode($data);
	die(0);
}



add_action( 'admin_post_nopriv_create_order_pricing', 'create_order_pricing_callback');
add_action( 'admin_post_create_order_pricing', 'create_order_pricing_callback');

function create_order_pricing_callback() { //print_r($_POST); echo $_SESSION['order_id']; exit('asdf');
	//process form
	$order_id=$_POST['order_id'];
	update_post_meta($order_id, 'package', sanitize_text_field($_POST['package']));
	update_post_meta($order_id, 'extrafast', sanitize_text_field($_POST['extra_fast']));
	update_post_meta($order_id, 'additional_formats', sanitize_text_field($_POST['additional_formats']));
	update_post_meta($order_id, 'conceptual', sanitize_text_field($_POST['concept_recreation']));
	update_post_meta($order_id, 'weekend', sanitize_text_field($_POST['weekend_delivery']));
	update_post_meta($order_id, 'halftones', sanitize_text_field($_POST['half_tones']));
	
	
	update_post_meta($order_id, 'mockups', sanitize_text_field($_POST['mockup']));
	update_post_meta($order_id, 'modifications_details', sanitize_text_field($_POST['modifications']));
	//order total amount
	$page_id=sanitize_text_field($_POST['page_id']);
	$package_price=get_post_meta($page_id, sanitize_text_field($_POST['package']).'_plan_price', true);
	if(sanitize_text_field($_POST['extra_fast'])){
		$package_price+=get_post_meta($page_id, 'extra_fast_delivery_price', true);
	}
	if(sanitize_text_field($_POST['additional_formats'])){
		$package_price+=get_post_meta($page_id, 'additional_formats_price', true);
	}
	if(sanitize_text_field($_POST['concept_recreation'])){
		$package_price+=get_post_meta($page_id, 'concept_recreation_price', true);
	}
	$package_price=sanitize_text_field($_POST['total_amount']);
	update_post_meta($order_id, 'order_total', $package_price);
	
		
	wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=4');
		
	die(0);
}

add_action( 'admin_post_nopriv_admin_add_card_form', 'admin_add_card_form_callback');
add_action( 'admin_post_admin_add_card_form', 'admin_add_card_form_callback');
function admin_add_card_form_callback() { 

	global $wpdb;
	$userid=$_POST['userid'];
	$userInfo = get_user_meta($userid);
	if(array_key_exists('customer_stripe_id', $userInfo))
	{
		if(get_user_meta($userid, 'customer_stripe_id', true)==''){
			$customer_id=stripe_create_customer();
			update_user_meta($userid, 'customer_stripe_id', $customer_id);
		}
		$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
	}else{
		$customer_id=stripe_create_customer();
		$data= ['user_id'=>$userid,'meta_key'=>'customer_stripe_id','meta_value'=>$customer_id ];
		 $wpdb->insert($wpdb->usermeta,$data);
	}


	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	$token=$_POST['stripeToken'];
	try {
		$card = \Stripe\Customer::createSource(
		  $customer_id,
		  [
			'source' => $token,
		  ]
		);
		session_start();
            	
    	$statusMsg = 'Card Added Successfully';
    	 $_SESSION["success"]  = $statusMsg;
    	
		wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=payment');
        exit();
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   // echo '<h1>Invalid parameter supply. Please try again.</h1>';
	   session_start();
            	
    	$statusMsg = 'Invalid parameter supply. Please try again';
    	 $_SESSION["error"]  = $statusMsg;
    	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=payment');
		exit();
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   // echo '<h1>Authentication Failed, please try again!</h1>';
	   session_start();
            	
    	$statusMsg = 'Authentication Failed, please try again!';
    	 $_SESSION["error"]  = $statusMsg;
    	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=payment');
		exit();
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   // echo '<h1>Server not Responding, please try again later!</h1>';
	   $statusMsg = 'Server not Responding, please try again later!';
    	 $_SESSION["error"]  = $statusMsg;
    	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=payment');
		exit();
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}


}


add_action( 'admin_post_nopriv_payment_form', 'payment_form_callback');
add_action( 'admin_post_payment_form', 'payment_form_callback');

function payment_form_callback() { //print_r($_POST); exit('asdf');
	//process form
	
	$user_id=get_current_user_id();
	/* update_user_meta($user_id, 'business_name', sanitize_text_field($_POST['business_name']));
	//update_user_meta($user_id, 'billing_email', sanitize_text_field($_POST['billing_email']));
	//update_user_meta($user_id, 'phone_no', sanitize_text_field($_POST['phone_no']));
	update_user_meta($user_id, 'billing_address', sanitize_text_field($_POST['billing_address']));
	update_user_meta($user_id, 'city', sanitize_text_field($_POST['city']));
	update_user_meta($user_id, 'state', sanitize_text_field($_POST['state']));
	update_user_meta($user_id, 'billing_zipcode', sanitize_text_field($_POST['billing_zipcode']));
	//update_user_meta($user_id, 'resale_tax_id', sanitize_text_field($_POST['resale_tax_id']));
	 */
	if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)==''){
		$customer_id=stripe_create_customer();
		update_user_meta(get_current_user_id(), 'customer_stripe_id', $customer_id);
	}else{
		//save a new card
		save_new_card_stripe();
	}
		//place order
	 $order_id=$_POST['order_id']; //echo $order_id;
	   $amount=get_post_meta($order_id, 'order_total', true);

	   $discount=$_POST['discount'];
	   $user_used_coupons=get_user_meta(get_current_user_id(), 'user_coupons', true);
	   if(! is_array($user_used_coupons)){
		   $user_used_coupons=array();
	   }
	   $user_used_coupons[]=$_POST['coupon_code'];
	   update_user_meta(get_current_user_id(), 'user_coupons', $user_used_coupons);
		update_post_meta($order_id, 'coupon_code',$_POST['coupon_code']);
	   $amount=$amount - $discount;
	   $amount=round($amount, 2);
	   
	   if($amount<=0){
			update_post_meta($order_id, 'order_status', "process");
			update_post_meta($order_id, 'order_total', '0');
			update_post_meta($order_id, 'discount', $discount);
			update_post_meta($order_id, 'payment', "paid");
			$now = current_time( 'mysql' ); 
			update_post_meta($order_id, 'start_date', $now);
			$delivery_time=48; //intval(sanitize_text_field($_POST['delivery_time']));
			if($_POST['extraFast']=='yes'){
				$delivery_time=24;
			}
			$delivery_date = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); 
			update_post_meta($order_id, 'delivery_date', $delivery_date);
			update_post_meta($order_id, 'total_hours', $delivery_time);
			update_post_meta($order_id, 'order_placed_mess', 'unread');
			update_post_meta($order_id, 'order_placed_mess_time', current_time( 'mysql' ));
			
									
			 wp_redirect(site_url().'/thankyou?order='.$order_id);	
	   }else{
		$response=stripe_charge_customer($amount);
		
	   if($response->id){
			update_post_meta($order_id, 'order_status', "process");
			update_post_meta($order_id, 'order_total', $amount);
			update_post_meta($order_id, 'discount', $discount);
			update_post_meta($order_id, 'payment', "paid");
			update_post_meta($order_id, 'tx_id', $response->id);
			$now = current_time( 'mysql' ); 
			update_post_meta($order_id, 'start_date', $now);
			$delivery_time=48; //intval(sanitize_text_field($_POST['delivery_time']));
			if($_POST['extraFast']=='yes'){
				$delivery_time=24;
			}
			$delivery_date = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); 
			update_post_meta($order_id, 'delivery_date', $delivery_date);
			update_post_meta($order_id, 'total_hours', $delivery_time);
			update_post_meta($order_id, 'order_placed_mess', 'unread');
			update_post_meta($order_id, 'order_placed_mess_time', current_time( 'mysql' ));
		
								
			wp_redirect(site_url().'/thankyou?order='.$order_id);
			die(0);			
	   }else{
		    $order_id=$_POST['order_id'];
		  wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=5&payment=failed');	
			die(0);
	   }
	   }
	   
	$order_id=$_POST['order_id'];
	wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=4');	
	die(0);
}

add_action( 'admin_post_nopriv_membership_payment_form', 'membership_payment_form_callback');
add_action( 'admin_membership_payment_form', 'membership_payment_form_callback');

function membership_payment_form_callback() { //print_r($_POST); exit('asdf');
	//process form
	global $wpdb;
	$user_id = get_current_user_id();

	/* update_user_meta($user_id, 'business_name', sanitize_text_field($_POST['business_name']));
	//update_user_meta($user_id, 'billing_email', sanitize_text_field($_POST['billing_email']));
	//update_user_meta($user_id, 'phone_no', sanitize_text_field($_POST['phone_no']));
	update_user_meta($user_id, 'billing_address', sanitize_text_field($_POST['billing_address']));
	update_user_meta($user_id, 'city', sanitize_text_field($_POST['city']));
	update_user_meta($user_id, 'state', sanitize_text_field($_POST['state']));
	update_user_meta($user_id, 'billing_zipcode', sanitize_text_field($_POST['billing_zipcode']));
	//update_user_meta($user_id, 'resale_tax_id', sanitize_text_field($_POST['resale_tax_id']));
	 */
	if(get_user_meta(get_current_user_id(), 'customer_stripe_id', true)==''){
		$customer_id=stripe_create_customer();
		update_user_meta(get_current_user_id(), 'customer_stripe_id', $customer_id);
	
	}else{
		//save a new card
		save_new_card_stripe();
		$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);

	}
	$amount = $_POST['amount'];
	$card = $_POST['lastFour'];
	
	$membership = $wpdb->get_row("SELECT $wpdb->memberships.name,$wpdb->memberships.mockups,$wpdb->memberships.mem_order,$wpdb->user_memberships.amount as membershipamount,$wpdb->user_memberships.status,$wpdb->memberships.frequency,$wpdb->user_memberships.mem_id,$wpdb->user_memberships.type FROM $wpdb->user_memberships LEFT JOIN $wpdb->memberships 
		ON $wpdb->user_memberships.type = $wpdb->memberships.mem_ship_id  WHERE  $wpdb->user_memberships.user_id = '".$user_id."' AND $wpdb->user_memberships.status = 'Pending' ORDER BY $wpdb->user_memberships.user_id DESC LIMIT 1");
	
	$planId 	= $membership->type;
	$planName 	= $membership->name; 
    $planPrice 	= $membership->membershipamount; 
    $planInterval = $planInfo->frequency; 
    
	if($membership->membershipamount == $amount )
	{

		// $response=stripe_charge_customer($amount);

		// if($response->id){
		// 	$where = [ 'user_id' => $user->ID ]; 
		// 	$data = ['status'=>'Active'];
		// $wpdb->update($wpdb->user_memberships,$data,$where);
		// echo 'payment done successfully';
		// }else{
		// 	echo "payment failed";

		// }
			
		if($planId)
		{
			try { 
	            $subscription = \Stripe\Subscription::create(array( 
	                "customer" => $customer_id, 
	                "items" => array( 
	                    array( 
	                        "plan" => $planId, 
	                    ), 
	                ), 
	            )); 
	        }catch(Exception $e) { 
	            $api_error = $e->getMessage(); 
	        } 
	        if(empty($api_error) && $subscription){ 
            	// Retrieve subscription data 
            	$subsData = $subscription->jsonSerialize();

            	if($subsData['status'] == 'active'){ 
                	// Subscription info 
	                $subscrID = $subsData['id']; 
	                $custID = $subsData['customer']; 
	                $planID = $subsData['plan']['id']; 
	                $planAmount = ($subsData['plan']['amount']/100); 
	                $planCurrency = $subsData['plan']['currency']; 
	                $planinterval = $subsData['plan']['interval']; 
	                $planIntervalCount = $subsData['plan']['interval_count']; 
	                $created = date("Y-m-d H:i:s", $subsData['created']); 
	                $current_period_start = date("Y-m-d", $subsData['current_period_start']); 
	                $current_period_end = date("Y-m-d", $subsData['current_period_end']); 
	                $status = $subsData['status'];
	                $statusMsg = 'Your Subscription Payment has been Successful!'; 
	                $where = [ 'mem_id'=>$membership->mem_id]; 
					$data = ['status'=>'Active','start_date'=>$current_period_start,'end_date'=>$current_period_end,'stripe_subscription_id'=>$subscrID,'plan_amount_currency'=>$planCurrency,'amount'=>$planAmount,'last_four'=>$card];
					$wpdb->update($wpdb->user_memberships,$data,$where);
					if(strtolower($planName) != 'custom' && strtolower($planName) != 'free')
					{
						$mockups = $membership->mockups;
						$order = $membership->mem_order;
						$userInfo = get_user_meta($user_id);
						$cusfun = new CustomFunctions;
						$cusfun->updatemockOrder($user_id,$mockups,$order);
						// if(array_key_exists('mockups', $userInfo))
						// {
						// 	update_user_meta($user_id, 'mockups', $mockups);
						// }else{
						// 	$data= ['user_id'=>$user_id,'meta_key'=>'mockups','meta_value'=>$mockups ];
					 // 		$wpdb->insert($wpdb->usermeta,$data);
						// }
						// if(array_key_exists('orders', $userInfo))
						// {
						// 	update_user_meta($user_id, 'orders', $order);
						// }else{
						// 	$data= ['user_id'=>$user_id,'meta_key'=>'orders','meta_value'=>$order ];
						// 	$wpdb->insert($wpdb->usermeta,$data);
						// }
					}
					$dt = ['user_tb_id'=> $user_id, 'user_memship_id'=> $membership->mem_id,'type'=>'Charge','amount'=>$planPrice,'plan_name'=> $planName,'created_at'=>date("Y-m-d")];
					
					$transaction = $wpdb->insert($wpdb->transaction,$dt);

					wp_redirect(site_url().'/user-dashboard');
					exit();
	            }else{ 
	            	
	            	session_start();
	            	
                	$statusMsg = "Subscription activation failed!";
                	 $_SESSION["error"]  = $statusMsg;
                	wp_redirect(site_url().'/membership');
                	exit();
            	} 
        	}else{
        		
        		session_start();
        		$statusMsg = "Subscription creation failed! ".$api_error; 
        		$_SESSION["error"]  = $statusMsg;
        		wp_redirect(site_url().'/membership');
        		exit();
        	}
        	
		}

	}
	die(0);
	   
}

add_action( 'admin_post_nopriv_membership_custom_mem_update', 'custom_mem_update_callback');
add_action( 'admin_membership_custom_mem_update', 'custom_mem_update_callback');
function custom_mem_update_callback() { //print_r($_POST); exit('asdf');
	//process form

	global $wpdb;
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 
	$userid = $_POST['userid'];
	$last_four = $_POST['lastFour'];
	if(array_key_exists('stripeToken', $_POST))
	{
		$token=$_POST['stripeToken'];
	}

	
	
	$user = $wpdb->get_row("SELECT * FROM $wpdb->users 
									WHERE  ID = ".$userid."  LIMIT 1"
								);
	if($user != null)
	{

		$userInfo = get_user_meta($userid);
		if(array_key_exists('customer_stripe_id', $userInfo))
		{
			if(get_user_meta($userid, 'customer_stripe_id', true)==''){
				$customer_id=stripe_create_customer();
				update_user_meta($userid, 'customer_stripe_id', $customer_id);

			}
			$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
		}else{
			$customer_id=stripe_create_customer();
			$data= ['user_id'=>$userid,'meta_key'=>'customer_stripe_id','meta_value'=>$customer_id ];
			$wpdb->insert($wpdb->usermeta,$data);
		}
		$customer=retrive_customer(get_user_meta($userid, 'customer_stripe_id', true)); 
		if(isset($_POST['paywithcard']) || array_key_exists('paywithcard', $_POST))
		{
		$basicOrder = sanitize_text_field($_POST['basicOrder2']);
		$standardOrder = sanitize_text_field($_POST['standardOrder2']);
		$premiumOrder = sanitize_text_field($_POST['premiumOrder2']);
		$mockups = $_POST['editmemMock2'];
		$videoMockups = $_POST['editvideoMock2'];
		$fee = sanitize_text_field($_POST['editmonfee2']);
		if($customer->default_source == null){

			try {
				$card = \Stripe\Customer::createSource(
				  $customer_id,
				  [
					'source' => $token,
				  ]
				);

				//print_r($customer_default_card);
			} catch (Exception $e) {
				
  				// Something else happened, completely unrelated to Stripe
				session_start();
        	
	        	$statusMsg = $e->getMessage();
	        	$_SESSION["error"]  = $statusMsg;
	        	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
				exit();
			}
		}

		}else{
		$basicOrder = sanitize_text_field($_POST['basicOrder1']);
		$standardOrder = sanitize_text_field($_POST['standardOrder1']);
		$premiumOrder = sanitize_text_field($_POST['premiumOrder1']);
		$mockups = $_POST['editmemMock1'];
		$videoMockups = $_POST['editvideoMock1'];
		$fee = sanitize_text_field($_POST['editmonfee1']);
		if($customer->default_source != null){
			$card = $customer->default_source;
		}else{
			$card = null;
		}
		}

		

		$getMembership 	= $wpdb->get_row("SELECT $wpdb->memberships.name,$wpdb->user_memberships.amount as membershipamount,$wpdb->user_memberships.status,$wpdb->memberships.frequency,$wpdb->user_memberships.mem_id,$wpdb->user_memberships.type,$wpdb->user_memberships.stripe_subscription_id,$wpdb->user_memberships.stripe_plan_id FROM $wpdb->user_memberships LEFT JOIN $wpdb->memberships 
		ON $wpdb->user_memberships.type = $wpdb->memberships.mem_ship_id  WHERE  $wpdb->user_memberships.user_id = '".$userid."' AND $wpdb->user_memberships.status = 'Pending' ORDER BY $wpdb->user_memberships.user_id DESC LIMIT 1");

		if($getMembership != null)
		{

			$where = ['mem_id'=>$getMembership->mem_id]; 
			$plans = 	\Stripe\Plan::all(['limit' => 1]);

			foreach ($plans->data as $key => $value) {
				$product_id = $value->product;
			}
			$productname = \Stripe\Product::retrieve($product_id);
				 // Create a plan 
			// if($getMembership->stripe_plan_id != null)
			// {
			// 	session_start();
		            	
   //          	$statusMsg = "Subscription update not done!";
   //          	$_SESSION["error"]  = $statusMsg;
   //          	wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
   //          	die(0); 

			// }else{
			
			    try { 
			        $plan = \Stripe\Plan::create(array( 
			            "product" => $product_id,
			            "nickname" => $userid.'_custom', 
			            "amount" => $fee*100, 
			            "currency" => 'usd', 
			            "interval" => 'month', 
			            "interval_count" => 1 
			        )); 
			      
			    }catch(Exception $e) { 
			    	
			        $api_error = $e->getMessage(); 
			    } 

		    	if(empty($api_error) && $plan){ 
		        // Creates a new subscription 
		        try { 

		            $subscription = \Stripe\Subscription::create(array( 
		                "customer" => $customer_id, 
	                	"items" => array( 
		                    array( 
		                        "plan" => $plan->id, 
		                    ), 
	                	), 
		            )); 
		        }catch(Exception $e) { 
		            $api_error = $e->getMessage(); 
		        } 

		        if(empty($api_error) && $subscription){ 
            	// Retrieve subscription data 
            	$subsData = $subscription->jsonSerialize();

	            	if($subsData['status'] == 'active'){ 
	                	// Subscription info 
		                $subscrID = $subsData['id']; 
		                $custID = $subsData['customer']; 
		                $planID = $subsData['plan']['id']; 
		                $planAmount = ($subsData['plan']['amount']/100); 
		                $planCurrency = $subsData['plan']['currency']; 
		                $planinterval = $subsData['plan']['interval']; 
		                $planIntervalCount = $subsData['plan']['interval_count']; 
		                $created = date("Y-m-d H:i:s", $subsData['created']); 
		                $current_period_start = date("Y-m-d", $subsData['current_period_start']); 
		                $current_period_end = date("Y-m-d", $subsData['current_period_end']); 
		                $status = $subsData['status'];
		                $statusMsg = 'Your Subscription Payment has been Successful!'; 
		                $where = [ 'mem_id'=>$getMembership->mem_id]; 
						$data = ['status'=>'Active','start_date'=>$current_period_start,'end_date'=>$current_period_end,'stripe_subscription_id'=>$subscrID,'plan_amount_currency'=>$planCurrency,'amount'=>$planAmount,'last_four'=>$last_four,'basic_order'=> $basicOrder,'standard_order'=>$standardOrder,'premium_order' => $premiumOrder,'video_mockups' => $videoMockups,'stripe_plan_id'=>$planID,'is_updated'=>1,'updated'  => date("Y-m-d H:i:s")];
						$wpdb->update($wpdb->user_memberships,$data,$where);
						if(array_key_exists('mockups', $userInfo))
						{
							update_user_meta($userid, 'mockups', $mockups);
						}else{
							$data= ['user_id'=>$userid,'meta_key'=>'mockups','meta_value'=>$mockups ];
					 		$wpdb->insert($wpdb->usermeta,$data);
						}
						
						session_start();
					        $statusMsg = "Subcription Created Successful"; 
					        $_SESSION["success"]  = $statusMsg;

				        wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
        				exit();
				   
					}else{ 
		            	
		            	session_start();
		            	
	                	$statusMsg = "Subscription activation failed!";
	                	 $_SESSION["error"]  = $statusMsg;
	                	 wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
        				exit();
	                	 
	            	}
            	}else{
        		
        		session_start();
        		$statusMsg = "Subscription creation failed! ".$api_error; 
        		$_SESSION["error"]  = $statusMsg;
        		 wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
        				exit();
        		
        		} 
	    		}else{ 
	    			session_start();
			        $statusMsg = "Plan creation failed! ".$api_error; 
			        $_SESSION["error"]  = $statusMsg;
			        wp_redirect(admin_url() .'/admin.php?page=speedy-customer-list&customer='.$userid);
        				exit();
			    } 
			// }
		}
	}
	die(0);

}


add_action( 'admin_post_nopriv_cancel-membership', 'cancel_membership_callback');
add_action( 'admin_membership_cancel-membership', 'cancel_membership_callback');
function cancel_membership_callback() { //print_r($_POST); exit('asdf');
	//process form
	global $wpdb;
	$userid = $_POST['userid'];

	$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
		
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
		 $customer_details= \Stripe\Customer::retrieve($customer_id);
		 $membership = getUsermembershipDetail($userid);

		try {
			if($membership != null)
			{
				$subcription_id = $membership->stripe_subscription_id;
				$stripe_subcription =	\Stripe\Subscription::retrieve($subcription_id);
			
				if($stripe_subcription->status != 'canceled')
				{
					$stripe_subcription->cancel();
					
					$where = [ 'mem_id'=>$membership->mem_id]; 
						$data = ['status'=>'Cancel','is_updated'=>1,'updated'  => date("Y-m-d H:i:s")];
						$wpdb->update($wpdb->user_memberships,$data,$where);
						echo "cancelled";
				}else{
			       echo  "Already Cancelled Subcription"; 
				}
			}
		 
		}catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo 'Too many requests made to the API too quickly!';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo 'Invalid parameter supply. Please try again.';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo 'Authentication Failed, please try again!';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo 'Server not Responding, please try again later!';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo $e->getMessage();
		}
	die();
}





add_action( 'admin_post_nopriv_admin_refund', 'admin_refund_amount_callback');
add_action( 'admin_membership_admin_refund', 'admin_refund_amount_callback');

function admin_refund_amount_callback() { //print_r($_POST); exit('asdf');
	//process form

	global $wpdb;
	$userid = $_POST['userid'];

	$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
		
		require_once('inc/stripe/stripe-php/stripe_config.php'); 
	$amountRefund = $_POST['amountRefund'];
	$getMembership 	= 	getUsermembershipDetail($userid);

	if($getMembership != null)
	{
		$subcription_id = $getMembership->stripe_subscription_id;
		$objInvoiceCollection = \Stripe\Invoice::all([
		    'subscription' => $subcription_id
		]);
		
		if ($objInvoiceCollection) {

			foreach ($objInvoiceCollection->data as $key => $value) {
				$chargeId = $value->charge;
				 
			}
			
			try {
			$objRefund = \Stripe\Refund::create([
				'amount' => $amountRefund*100,
				'charge' => $chargeId]);
			 
			 session_start();
		            	
    		$statusMsg = '$'.$amountRefund. " Amount Refund Successfully";
    	 	$_SESSION["refundtabsuccess"]  = $statusMsg;
			wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
        		exit();

			}catch(\Stripe\Error\Card $e) {
		  	// Since it's a decline, \Stripe\Error\Card will be caught
			  $body = $e->getJsonBody();
			  $err  = $body['error'];

			 
			   session_start();
		            	
    		$statusMsg = $err['message'];
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
			  wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			} catch (\Stripe\Error\RateLimit $e) {
			  // Too many requests made to the API too quickly
			 
			   session_start();
		            	
    		$statusMsg = "Too many requests made to the API too quickly!";
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			} catch (\Stripe\Error\InvalidRequest $e) {
			  // Invalid parameters were supplied to Stripe's API
			  
			    session_start();
		            	
    		$statusMsg = "Invalid parameter supply. Please try again.";
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			} catch (\Stripe\Error\Authentication $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
			  
			    session_start();
		            	
    		$statusMsg = "Authentication Failed, please try again!";
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			} catch (\Stripe\Error\ApiConnection $e) {
			  // Network communication with Stripe failed
			  
			    session_start();
		            	
    		$statusMsg = "Server not Responding, please try again later!";
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			} catch (\Stripe\Error\Base $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
			 
			  session_start();
		            	
    		$statusMsg = $e->getMessage();
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 	wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
			}

		}

	}else{
			session_start();
		            	
    		$statusMsg = "Membership not found!";
    	 	$_SESSION["refundtaberror"]  = $statusMsg;
		 wp_redirect(admin_url() .'admin.php?page=speedy-customer-list&customer='.$userid.'&addcard=refund');
			  exit();
	}
	
	 die(0);
}

add_action( 'admin_menu', 'speedysep_user_admin_dashboard_menu');
function speedysep_user_admin_dashboard_menu() {
		add_menu_page('Settings', 'Settings', 'manage_options', 'speedysep-settings', 'speedysep_main_menu_callback', "");
}

//menu callback function
function speedysep_main_menu_callback(){
	//include settings view
	include_once(get_stylesheet_directory().'/inc/stripe/views/settings.php');
}

//save stripe settings
add_action( 'admin_post_nopriv_update_stripe_settings', 'save_update_stripe_settings' );
add_action( 'admin_post_update_stripe_settings', 'save_update_stripe_settings' );

function save_update_stripe_settings() { 
	//Secure XSS Attackes
	if (! isset( $_POST['update_stripe_settings_nonce'] ) || ! wp_verify_nonce( $_POST['update_stripe_settings_nonce'], 'update_stripe_settings' )){
		print 'Sorry, you cannot do it';
		exit;
	} else {
	   //save settings
	   update_option('stripe_settings', $_POST['settings']);
		wp_redirect(admin_url().'/admin.php?page='.$_POST['page_url']);
	}
	
}

function stripe_create_customer() {
		$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
		try {
		  // Use Stripe's library to make requests...
		  $token =$_POST['stripeToken'];
		   $email=$_POST['billing_email'];
		  $customer = \Stripe\Customer::create(array(
			  'email' => $email,
			  'source'  => $token
		  ));
		  
		  
		  return $customer->id;
		  
		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo '<h1>Too many requests made to the API too quickly!</h1>';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo '<h1>Authentication Failed, please try again!</h1>';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo '<h1>Server not Responding, please try again later!</h1>';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo "<h1>".$e->getMessage()."</h1>";
		}
}


function stripe_charge_customer($amount) {
		$stripe_settings = get_option('stripe_settings'); 
		session_start();
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
		
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
		try {

		  $charge = \Stripe\Charge::create(array(
			  'customer' => (string)$customer_id,
			  'amount'   => (int)$amount*100,
			  'currency' => 'usd',
			  'description' => 'charges'
		  ));
		  return $charge;
		}catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   echo '<h1>Too many requests made to the API too quickly!</h1>';
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   echo '<h1>Authentication Failed, please try again!</h1>';
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   echo '<h1>Server not Responding, please try again later!</h1>';
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  echo "<h1>".$e->getMessage()."</h1>";
		}
}

function retrive_customer($customer_stripe_id){
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		  
		 $customer_details= \Stripe\Customer::retrieve($customer_stripe_id);
		return $customer_details;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}
//echo "<pre>"; $customer=retrive_customer('cus_EvFG8rAlaQlybQ'); echo $customer->sources; echo "</pre>"; exit;

function save_new_card_stripe(){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	$token=$_POST['stripeToken'];
	try {
		$card = \Stripe\Customer::createSource(
		  $customer_id,
		  [
			'source' => $token,
		  ]
		);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}
 function delete_user_card($card_id,$userid)
 {

 	$customer_id =get_user_meta($userid, 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$card = \Stripe\Customer::deleteSource(
				  $customer_id,
				  $card_id
				);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
 }
//delete card
function delete_customer_card_stripe($card_id){

	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$card = \Stripe\Customer::deleteSource(
				  $customer_id,
				  $card_id
				);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//retrive a card
function retrive_customer_card_stripe($card_id, $customer_stripe_id=false){
	
	if($customer_stripe_id){
		$customer_id=$customer_stripe_id;
	}else{
		$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	}
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$customer = \Stripe\Customer::retrieve($customer_id);
		$card = $customer->sources->retrieve($card_id);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}
add_action( 'wp_ajax_change_user_default_card', 'change_user_default_card_callback' );
add_action( 'wp_ajax_change_user_default_card', 'change_user_default_card_callback' );
function change_user_default_card_callback()
{
	if(isset($_POST)  && array_key_exists('card',$_POST) && array_key_exists('userid',$_POST))
	{
		$card_id = $_POST['card'];
		$userid = $_POST['userid'];
		$customer_id=get_user_meta($userid, 'customer_stripe_id', true);
		$stripe_settings = get_option('stripe_settings'); 
		$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
		$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
		require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
		try {
			$card = \Stripe\Customer::update(
			  $customer_id,
			  [
				'default_source' => $card_id,
			  ]
			);
			echo json_encode(['code'=>200, 'msg'=>'Card change Successfully']);
			exit();
		}catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  // print('Status is:' . $e->getHttpStatus() . "\n");
		  // print('Type is:' . $err['type'] . "\n");
		  // print('Code is:' . $err['code'] . "\n");
		  // // param is '' in this case
		  // print('Param is:' . $err['param'] . "\n");
		  // print('Message is:' . $err['message'] . "\n");
		  echo json_encode(['code'=>400, 'msg'=>$err['message']]);
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		   // echo '<h1></h1>';
		   echo json_encode(['code'=>400, 'msg'=>'Too many requests made to the API too quickly!']);
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		   // echo '<h1></h1>';
		    echo json_encode(['code'=>400, 'msg'=>'Invalid parameter supply. Please try again.']);
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		   // echo '<h1>Authentication Failed, please try again!</h1>';
		   echo json_encode(['code'=>400, 'msg'=>'Authentication Failed, please try again!']);
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		   // echo '<h1>Server not Responding, please try again later!</h1>';
		    echo json_encode(['code'=>400, 'msg'=>'Server not Responding, please try again later!']);
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  // echo "<h1>".$e->getMessage()."</h1>";
			echo json_encode(['code'=>400, 'msg'=>$e->getMessage()]);
		}
		exit();
	}

}


//change default
function change_customer_default_card_stripe($card_id){
	$customer_id=get_user_meta(get_current_user_id(), 'customer_stripe_id', true);
	$stripe_settings = get_option('stripe_settings'); 
	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	require_once('inc/stripe/stripe-php/stripe_config.php'); 	
	
	try {
		$card = \Stripe\Customer::update(
		  $customer_id,
		  [
			'default_source' => $card_id,
		  ]
		);
		return $card;
	}catch(\Stripe\Error\Card $e) {
	  // Since it's a decline, \Stripe\Error\Card will be caught
	  $body = $e->getJsonBody();
	  $err  = $body['error'];

	  print('Status is:' . $e->getHttpStatus() . "\n");
	  print('Type is:' . $err['type'] . "\n");
	  print('Code is:' . $err['code'] . "\n");
	  // param is '' in this case
	  print('Param is:' . $err['param'] . "\n");
	  print('Message is:' . $err['message'] . "\n");
	} catch (\Stripe\Error\RateLimit $e) {
	  // Too many requests made to the API too quickly
	   echo '<h1>Too many requests made to the API too quickly!</h1>';
	} catch (\Stripe\Error\InvalidRequest $e) {
	  // Invalid parameters were supplied to Stripe's API
	   echo '<h1>Invalid parameter supplided. Please try again.</h1>';
	} catch (\Stripe\Error\Authentication $e) {
	  // Authentication with Stripe's API failed
	  // (maybe you changed API keys recently)
	   echo '<h1>Authentication Failed, please try again!</h1>';
	} catch (\Stripe\Error\ApiConnection $e) {
	  // Network communication with Stripe failed
	   echo '<h1>Server not Responding, please try again later!</h1>';
	} catch (\Stripe\Error\Base $e) {
	  // Display a very generic error to the user, and maybe send
	  // yourself an email
	} catch (Exception $e) {
	  // Something else happened, completely unrelated to Stripe
	  echo "<h1>".$e->getMessage()."</h1>";
	}
}

//place order
add_action( 'admin_post_nopriv_place_order', 'place_order_callback' );
add_action( 'admin_post_place_order', 'place_order_callback' );

function place_order_callback() { 
	//Secure XSS Attackes
	$show_status = showselectedPlan();
	if(isset($show_status) && $show_status->status == 'Pending'){
		wp_redirect(site_url().'/membership');
	}else{
		
		if (! isset( $_POST['place_order_nonce'] ) || ! wp_verify_nonce( $_POST['place_order_nonce'], 'place_order' )){
			print 'Sorry, you cannot do it';
			exit;
		} else {
	   $order_id=$_POST['order_id']; //echo $order_id;
	   $amount=get_post_meta($order_id, 'order_total', true);
	   $discount=$_POST['discount'];
	   $user_used_coupons=get_user_meta(get_current_user_id(), 'user_coupons', true);
	   if(! is_array($user_used_coupons)){
		   $user_used_coupons=array();
	   }
	   $user_used_coupons[]=$_POST['coupon_code'];
	   update_user_meta(get_current_user_id(), 'user_coupons', $user_used_coupons);
		update_post_meta($order_id, 'coupon_code',$_POST['coupon_code']);
	   $amount=$amount - $discount;
	   $amount=round($amount, 2);
	   if($amount<=0){
			update_post_meta($order_id, 'order_status', "process");
			update_post_meta($order_id, 'order_total', '0');
			update_post_meta($order_id, 'discount', $discount);
			update_post_meta($order_id, 'payment', "paid");
			$now = current_time( 'mysql' ); 
			update_post_meta($order_id, 'start_date', $now);
			$delivery_time=48; //intval(sanitize_text_field($_POST['delivery_time']));
			if($_POST['extraFast']=='yes'){
				$delivery_time=24;
			}
			$delivery_date = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); 
			update_post_meta($order_id, 'delivery_date', $delivery_date);
			update_post_meta($order_id, 'total_hours', $delivery_time);
			update_post_meta($order_id, 'order_placed_mess', 'unread');
			update_post_meta($order_id, 'order_placed_mess_time', current_time( 'mysql' ));
			$time=get_post_meta($order_id, 'order_placed_mess_time', true);
			$order_type=get_post_meta($order_id, 'order_type', true);
			$customer_name=get_post_meta($order_id, 'customer_name', true);
			
			$amount_withsymb="$".$amount;
			if($discount==''){ $discount=0; }
			$discount="$".$discount;
			$time=date("Y-m-d h:i:s a", strtotime($time));
			$delivery_date=date("Y-m-d h:i:s a", strtotime($delivery_date));
			
			//send to admin
			$author_id=1;
			$user_info = get_userdata($author_id);
			//Send delivered Order email
			$to = $user_info->user_email;
			$subject = 'New Order on Speedysep';
			$body = "Hello,<br> 
					Customer, $customer_name placed a new order on your site. <br>
					Order details are:<br>
					<b>Order#:</b> $order_id <br>
					<b>Ordered Time:</b> $time <br>
					<b>Order Total:</b> $amount_withsymb <br>
					<b>Total Discount:</b> $discount <br>
					<b>Delivery Date:</b> $delivery_date <br>
					<b>Type:</b> $order_type <br>
					
					
					
					<br>Thank you,<br>
					Speedysep Inc

					";
			$headers = array('Content-Type: text/html; charset=UTF-8');
			 
			wp_mail( $to, $subject, $body, $headers );
			
			//send to customer
			$author_id=get_current_user_id();
			$user_info = get_userdata($author_id);
			//Send delivered Order email
			$to = $user_info->user_email;
			$subject = 'Order Placed';
			$body = 'Hello '.$user_info->first_name.',<br> 
					Your order has been placed successfully. <br>
					We are working on your order.<br>
					
					<br>Thank you,<br>
					Speedysep Inc

					';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			
			wp_mail( $to, $subject, $body, $headers );
			
			 wp_redirect(site_url().'/thankyou?order='.$order_id);	
	   }else{
		$response=stripe_charge_customer($amount);
	   if($response->id){
			update_post_meta($order_id, 'order_status', "process");
			update_post_meta($order_id, 'order_total', $amount);
			update_post_meta($order_id, 'discount', $discount);
			update_post_meta($order_id, 'payment', "paid");
			update_post_meta($order_id, 'tx_id', $response->id);
			$now = current_time( 'mysql' ); 
			update_post_meta($order_id, 'start_date', $now);
			$delivery_time=48; //intval(sanitize_text_field($_POST['delivery_time']));
			if($_POST['extraFast']=='yes'){
				$delivery_time=24;
			}
			$delivery_date = date( 'Y-m-d H:i:s', strtotime( $now ) + $delivery_time*60*60 ); 
			update_post_meta($order_id, 'delivery_date', $delivery_date);
			update_post_meta($order_id, 'total_hours', $delivery_time);
			update_post_meta($order_id, 'order_placed_mess', 'unread');
			update_post_meta($order_id, 'order_placed_mess_time', current_time( 'mysql' ));
			$time=get_post_meta($order_id, 'order_placed_mess_time', true);
			$order_type=get_post_meta($order_id, 'order_type', true);
			$customer_name=get_post_meta($order_id, 'customer_name', true);
			
			$amount_withsymb="$".$amount;
			if($discount==''){ $discount=0; }
			$discount="$".$discount;
			$time=date("Y-m-d h:i:s a", strtotime($time));
			$delivery_date=date("Y-m-d h:i:s a", strtotime($delivery_date));
			
			//send to admin
			$author_id=1;
			$user_info = get_userdata($author_id);
			//Send delivered Order email
			$to = $user_info->user_email;
			$subject = 'New Order on Speedysep';
			$body = "Hello,<br> 
					Customer, $customer_name placed a new order on your site. <br>
					Order details are:<br>
					<b>Order#:</b> $order_id <br>
					<b>Ordered Time:</b> $time <br>
					<b>Order Total:</b> $amount_withsymb <br>
					<b>Total Discount:</b> $discount <br>
					<b>Delivery Date:</b> $delivery_date <br>
					<b>Type:</b> $order_type <br>
					
					
					
					<br>Thank you,<br>
					Speedysep Inc

					";
			$headers = array('Content-Type: text/html; charset=UTF-8');
			 
			wp_mail( $to, $subject, $body, $headers );
			
			//send to customer
			$author_id=get_current_user_id();
			$user_info = get_userdata($author_id);
			//Send delivered Order email
			$to = $user_info->user_email;
			$subject = 'Order Placed';
			$body = 'Hello '.$user_info->first_name.',<br> 
					Yor order has been placed successfully. <br>
					We are working on your order.<br>
					
					<br>Thank you,<br>
					Speedysep Inc

					';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			 
			wp_mail( $to, $subject, $body, $headers );
			
								
			wp_redirect(site_url().'/thankyou?order='.$order_id);	
	   }else{
		    $order_id=$_POST['order_id'];
		  wp_redirect(site_url().'/'.$_POST['page']."?type=".$_POST['order_type']."&order=".$order_id.'&step=5&payment=failed');	
	   }
	   }
	}
	}
	
}


//Get Coupon
//add_action( 'admin_post_nopriv_get_coupon', 'get_coupon_callback' );
//add_action( 'admin_post_get_coupon', 'get_coupon_callback' );

add_action( 'wp_ajax_get_coupon', 'get_coupon_callback' );
add_action( 'wp_ajax_nopriv_get_coupon', 'get_coupon_callback' );

function get_coupon_callback() { 
	//print_r($_POST);
	$args = array(
		'post_type'=>'ss-coupons',
		'post_status'=>'publish',
		'meta_query' => array(
			array(
				'key' => 'coupon_code',
				'value' => $_POST['coupon'],
				'compare' => '=',
			),
		),
	);
	$query = new WP_Query( $args );
	$posts=$query->posts; //print_r($query->post_count);	
	$post=$posts[0];
	if($query->post_count>0){
		//token is available, now check if user already uses the coupon
		if(in_array($_POST['coupon'], get_user_meta(get_current_user_id(), 'user_coupons', true))){
			echo "coupon used";
		}else{
			//everything fine, use coupon for discount
			
			$coupon_type=get_post_meta($post->ID, 'coupon_type', true);
			if('Free'==$coupon_type){
				echo  "Free";
			}elseif('Percent Discount'==$coupon_type){
				echo get_post_meta($post->ID, 'coupon_amount', true);
			}
		}
	}
	die(0);
	
}


//Get Modification
add_action( 'admin_post_nopriv_get_modifications', 'get_modifications_callback' );
add_action( 'admin_post_get_modifications', 'get_modifications_callback' );

function get_modifications_callback() { 
	//Secure XSS Attackes
	if (! isset( $_POST['get_modifications_nonce'] ) || ! wp_verify_nonce( $_POST['get_modifications_nonce'], 'get_modifications' )){
		print 'Sorry, you cannot do it';
		exit;
	} else {
	   $order_id=$_POST['order_id']; //echo $order_id;
		$response=stripe_charge_customer(5);
	   if($response->id){
		update_post_meta($order_id, 'order_status', "delivered");
		update_post_meta($order_id, 'mod_payment', "paid");
		update_post_meta($order_id, 'mod_tx_id', $response->id);
		
		$modifications=get_post_meta($order_id, 'add_modifications', true); 
		 $modifications=$modifications+5;
		update_post_meta($order_id, 'add_modifications', $modifications);	
		
								
		 wp_redirect(site_url().'/delivered-orders/?order_id='.$order_id.'&mod=1');	
	   }else{
		    $order_id=$_POST['order_id'];
			wp_redirect(site_url().'/delivered-orders/?order_id='.$order_id.'&mod=0');		
	   }
	}
	
}


//send message on offer popup
add_action( 'wp_ajax_send_message', 'send_message_callback' );
add_action( 'wp_ajax_nopriv_send_message', 'send_message_callback' );
function send_message_callback() { //print_r($_POST); 
	//process form
	$upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
  // print_r($upload);

	$req_post = array(
		  'post_title'    => sanitize_text_field(mb_substr($_POST['request_message'], 0, 40)),
		  'post_content'  => sanitize_text_field($_POST['request_message']),
		  'post_status'   => 'private',
		  'post_type'	  => 'customers-messages',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		update_post_meta($post_id, 'order_id', sanitize_text_field($_POST['order_id']));
		//update_post_meta($post_id, 'message_attach', $upload['url']);
		//print_r($_FILES["attachment"]); die(0);
		//upload files
		if ( $_FILES ) { 
		$files = $_FILES["attachment"];  
		foreach ($files['name'] as $key => $value) {            
				if ($files['name'][$key]) { 
					$file = array( 
						'name' => $files['name'][$key],
						'type' => $files['type'][$key], 
						'tmp_name' => $files['tmp_name'][$key], 
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					); 
					$_FILES = array ("attachment" => $file); 
					foreach ($_FILES as $file => $array) {              
						$attach_id = upload_multipe_attachment($file,$post_id);
						if ( is_numeric( $attach_id ) ) {
						 
							$delivered_files_arr=array();
							if(get_post_meta( $post_id, 'message_attach',  true)==''){
								$delivered_files_arr[]=$attach_id;
								update_post_meta( $post_id, 'message_attach',  $delivered_files_arr); 
							}else{
								$delivered_files_arr=get_post_meta( $post_id, 'message_attach',  true);
								if(! is_array($delivered_files_arr)){
									$delivered_files_arr=array();
								}
								$delivered_files_arr[]=$attach_id;
								update_post_meta( $post_id, 'message_attach',  $delivered_files_arr); 
							}
						}
					}
				} 
			} 
		}
		
		//update order message status
		if(get_current_user_id()==1){
			update_post_meta( sanitize_text_field($_POST['order_id']), 'order_mess_status', 'unread');
			update_post_meta( sanitize_text_field($_POST['order_id']), 'unread_message_time', current_time( 'mysql' )); 
		}else{
			//message for admin by customer
			update_post_meta( sanitize_text_field($_POST['order_id']), 'order_admin_mess_status', 'unread');
			update_post_meta( sanitize_text_field($_POST['order_id']), 'unread_admin_message_time', current_time( 'mysql' )); 
		}
		$order_id=$_POST['order_id'];
		if(isset($_POST['buyer_id'])){
				$buyer_user_id=$_POST['buyer_id'];
				//message from admin
				$post = get_post($_POST['order_id']);
				$author_id=$post->post_author;
				$user_info = get_userdata($author_id);
				$to = $user_info->user_email;
				$subject = 'A message from SpeedySep regarding your order';
				$body = 'Hello '.$user_info->first_name.',<br> 
						<p>SpeedySep just left you the following message on the platform regarding your order. </p>
						<br>
						They write:<br>
						<p>'.sanitize_text_field($_POST['request_message']).'</p>
						<br>
						<a href="'.site_url().'/open-orders/?order_id='.$order_id.'"><b>Click Here to reply to them</b></a>
						<br>
						<br>Thank you,<br>
						SpeedySep Inc
						';
				$headers = array('Content-Type: text/html; charset=UTF-8');
				 
				wp_mail( $to, $subject, $body, $headers );
				
		}else{
			$buyer_user_id=get_current_user_id();
			//message from customer
				$author_id=1;
				$user_info = get_userdata($author_id);
				$to = $user_info->user_email;
				//customer data
				$post = get_post($_POST['order_id']);
				$customer_id=$post->post_author;
				$cust_info = get_userdata($customer_id);
				$customer_name=$cust_info->first_name;
				
				$subject = 'Message From Customer '.$customer_name.' in SpeedySep';
				$body = 'Hello Dan & Ubaid, <br> 
						<p>Customer '.$customer_name.' just left you the following message on the platform.</p>
						<br>
						Here it is:<br>
						<p>'.sanitize_text_field($_POST['request_message']).'</p>
						<br>Thank you,<br>
						SpeedySep Inc
						';
				$headers = array('Content-Type: text/html; charset=UTF-8');
				 
				wp_mail( $to, $subject, $body, $headers );
		}
		update_post_meta($post_id, 'sender', $buyer_user_id);
		update_post_meta($post_id, 'reciver', '1');
		
		
		if(get_post_meta($post_id, 'message_attach', true)){ 
		 $message_upload_files=array();
		$order_delivered_files=get_post_meta($post_id, 'message_attach', true);  
				
				
				if(is_array($order_delivered_files)){
				foreach($order_delivered_files as $order_deliver_file){ 
					$message_upload_files[]=wp_get_attachment_url($order_deliver_file); 
				 } }
		}				 
					
		if(!empty($message_upload_files)){
		echo json_encode($message_upload_files);
		}else{
			echo "no";
		}
		//echo $post_id;
	die(0);
}

add_action( 'wp_ajax_create_rating', 'create_rating_callback' );
add_action( 'wp_ajax_nopriv_create_rating', 'create_rating_callback' );
function create_rating_callback() { print_r($_POST); // exit('asdf');
	//process form
	$req_post = array(
		  'post_title'    => sanitize_text_field(mb_substr($_POST['message'], 0, 40)),
		  'post_content'  => sanitize_text_field($_POST['message']),
		  'post_status'   => 'private',
		  'post_type'	  => 'customers-rating',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		update_post_meta($post_id, 'response', sanitize_text_field($_POST['response']));	
		update_post_meta($post_id, 'service_as_expected', sanitize_text_field($_POST['service_as_expected']));
		update_post_meta($post_id, 'user_our_service_agian', sanitize_text_field($_POST['user_our_service_agian']));
		update_post_meta($post_id, 'order_id', sanitize_text_field($_POST['order_id']));
		update_post_meta(sanitize_text_field($_POST['order_id']), 'order_completed_mess', 'unread');
		update_post_meta(sanitize_text_field($_POST['order_id']), 'order_completed_mess_time', current_time( 'mysql' ));
	die(0);
}

//submit revision
add_action( 'wp_ajax_create_revision', 'create_revision_callback' );
add_action( 'wp_ajax_nopriv_create_revision', 'create_revision_callback' );
function create_revision_callback() { //print_r($_POST); 
	//process form
	$upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
  // print_r($upload);

	$req_post = array(
		  'post_title'    => sanitize_text_field(mb_substr($_POST['request_message'], 0, 40)),
		  'post_content'  => sanitize_text_field($_POST['request_message']),
		  'post_status'   => 'private',
		  'post_type'	  => 'customers-messages',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		update_post_meta($post_id, 'order_id', sanitize_text_field($_POST['order_id']));
		
		$revisions=get_post_meta(sanitize_text_field($_POST['order_id']), 'revisions', true);		
		update_post_meta(sanitize_text_field($_POST['order_id']), 'revisions', $revisions+1);
		
		update_post_meta($post_id, 'message_attach', $upload['url']);
		update_post_meta($post_id, 'message_type', 'revision');
		update_post_meta($post_id, 'order_revision_mess', 'unread');
		update_post_meta(sanitize_text_field($_POST['order_id']), 'order_revision_mess', 'unread');
		update_post_meta(sanitize_text_field($_POST['order_id']), 'order_revision_mess_time', current_time( 'mysql' ));
				
		$buyer_user_id=get_current_user_id();
		
		update_post_meta($post_id, 'sender', $buyer_user_id);
		update_post_meta($post_id, 'reciver', '1');
		
		//send to admin
		$customer_name=get_post_meta($_POST['order_id'], 'customer_name', true);
		$author_id=1;
		$user_info = get_userdata($author_id);
		//Send delivered Order email
		$to = $user_info->user_email;
		$subject = 'Revision on Order#'.$_POST['order_id'];
		$body = "Hello,<br> 
				Customer, $customer_name made a revision on order#".$_POST['order_id'].". <br>
				
				Message:<br>
				".$_POST['request_message']." <br>
				
				<br>Thank you,<br>
				Speedysep Inc

				";
		$headers = array('Content-Type: text/html; charset=UTF-8');
		 
		wp_mail( $to, $subject, $body, $headers );
		
		echo json_encode($upload);
		//echo $post_id;
	die(0);
}


//update profile
add_action( 'admin_post_nopriv_update_user_profile', 'update_user_profile_callback');
add_action( 'admin_post_update_user_profile', 'update_user_profile_callback');

function update_user_profile_callback() { //print_r($_POST);   exit('asdf');
	//process form
	$user_id=get_current_user_id();
	update_user_meta($user_id, 'first_name', sanitize_text_field($_POST['full_name']));
	update_user_meta($user_id, 'company_name', sanitize_text_field($_POST['company_name']));
	update_user_meta($user_id, 'phone_no', sanitize_text_field($_POST['phone_no']));
	update_user_meta($user_id, 'address', sanitize_text_field($_POST['street_address']));
	
	//$upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
	
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

	$uploadedfile = $_FILES['profile_img'];

	$upload_overrides = array( 'test_form' => false );

	$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

	if ( $movefile && ! isset( $movefile['error'] ) ) {
		//echo "File is valid, and was successfully uploaded.\n";
		//var_dump( $movefile['url'] );
		update_user_meta($user_id, 'profile_pic', $movefile['url']);
	} else {
		/**
		 * Error generated by _wp_handle_upload()
		 * @see _wp_handle_upload() in wp-admin/includes/file.php
		 */
		echo $movefile['error'];
	}
	
	$user_id = wp_update_user( 
							array( 'ID' => $user_id, 'user_email' => sanitize_text_field($_POST['user_email']) ) 
							);
							
	//current_pass new_pass confirm_pass
	$query_string='message=success';
	$user = get_userdata($user_id);
	if ( sanitize_text_field($_POST['new_pass'])!='' and sanitize_text_field($_POST['new_pass'])==sanitize_text_field($_POST['confirm_pass']) and wp_check_password( sanitize_text_field($_POST['current_pass']), $user->data->user_pass, $user->ID) ){
		$pass_changed = wp_update_user( 
							array( 'ID' => $user_id, 'user_pass' => sanitize_text_field($_POST['new_pass']) ) 
							);
			if($pass_changed){
				$query_string='message=success&pass=changed';
			}
	}else{
	  
	}
		wp_redirect(site_url().'/'.$_POST['page'].'?'.$query_string);
		
	die(0);
}

/******************************************************USER DASHBOARD AREA ends***************************************/


/******************************************************Admin Dashboard************************************************/

add_action('admin_menu', 'add_pages_to_admin_dashboard');
function add_pages_to_admin_dashboard() {
	add_menu_page('Dashboard', 'Dashboard', 'manage_options', 'speedy-dashboard-settings', 'dashboard_settings_main_menu_callback', '', 1);
	add_submenu_page( 'speedy-dashboard-settings', 'Orders', 'Orders', 'manage_options', 'speedy-dashboard-settings', 'dashboard_settings_main_menu_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'Delivered Orders', 'Delivered Orders', 'manage_options', 'speedy-delivered-orders', 'speedy_delivered_order_list');
	add_submenu_page( 'speedy-dashboard-settings', 'Customer List', 'Customer List', 'manage_options', 'speedy-customer-list', 'speedy_customer_list_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'Add Customer', 'Add Customer', 'manage_options', 'speedy-add-customer', 'speedy_add_customer_callback');
	//add_submenu_page( 'speedy-dashboard-settings', 'Expenses Reports', 'Expenses Reports', 'manage_options', 'speedy-expenses-reports', 'speedy_customer_list_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'add expense', 'add expense', 'manage_options', 'speedy-aadd-expens', 'speedy_add_expense_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'Expenses Reports', 'Expenses Reports', 'manage_options', 'speedy-expenses-reports', 'speedy_expenses_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'single order', 'single order', 'manage_options', 'single-order', 'single_order_menu_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'Sale Reports', 'Sale Reports', 'manage_options', 'sales-reports', 'sales_reports_menu_callback');
	add_submenu_page( 'speedy-dashboard-settings', 'Report Dashboard', 'Report Dashboard', 'manage_options', 'reports-dashboard', 'reports_dashboard_menu_callback');
}

	//  function getInvoice($userid)
	// {
	// 	$stripe_settings = get_option('stripe_settings'); 
	// 	$_SESSION['stripe_privatekey']=$stripe_settings['stripe_settings']['secret_key'];
	// 	$_SESSION['stripe_publickey']=$stripe_settings['stripe_settings']['public_key'];
	// 	require_once('inc/stripe/stripe-php/stripe_config.php'); 
	// 		$cusfn= new CustomFunctions;
	// 		$customerid = $cusfn->getUserStripeCustomerId($userid);		
	// 			$plan = 			getUsermembershipDetail($userid);
	// 		$subscription = \Stripe\Subscription::retrieve($plan->stripe_subscription_id);
	// 		echo '<pre>';
	// 		print($subscription->latest);
	// 		die();
	// }


function dashboard_settings_main_menu_callback(){
	include_once('inc/views/admin/order-list.php');
}

function speedy_customer_list_callback(){
	include('inc/views/admin/customers-list.php');
}

function speedy_delivered_order_list(){
	include('inc/views/admin/delivered-orders-list.php');
}

function single_order_menu_callback(){
	include('inc/views/admin/single-order.php');
}

function sales_reports_menu_callback(){
	include('inc/views/admin/sales-reports.php');
}

function reports_dashboard_menu_callback(){
	include('inc/views/admin/report-dashboard.php');
}

function speedy_add_expense_callback(){
	include('inc/views/admin/add-expense.php');
}

function speedy_expenses_callback(){
	include('inc/views/admin/expenses.php');
}

function speedy_add_customer_callback(){
	include('inc/views/admin/add-customer.php');
}


//deliver order
//add_action( 'admin_post_nopriv_deliver_order', 'deliver_order_callback');
//add_action( 'admin_post_deliver_order', 'deliver_order_callback');
add_action( 'wp_ajax_deliver_order', 'deliver_order_callback' );
add_action( 'wp_ajax_nopriv_deliver_order', 'deliver_order_callback' );
function deliver_order_callback() { //print_r($_POST); print_r($_FILES); exit;
	//process form
	$order_id=$_POST['deliver_order_id'];
	update_post_meta($order_id, 'order_status', 'delivered');
	update_post_meta($order_id, 'order_delivered_mess', 'unread');
	update_post_meta($order_id, 'delivered_date',current_time( 'mysql' ));
	update_post_meta( $order_id, 'delivered_files',  '');
	
	$post = get_post($order_id);
	$author_id=$post->post_author;
	$user_info = get_userdata($author_id);
	//Send delivered Order email
	$to = $user_info->user_email;
	$subject = 'Your Speedysep order is ready to review';
	$body = 'Hello,<br> 
			Your order is ready 😊 <br>
			Please click the link below and go to delivered orders to review it.<br>
			<a href="'.site_url().'/delivered-orders/?order_id='.$order_id.'"><button style="padding:10px 40px; background:#000; color:#fff; border-radius:5px; border:0px;">Review Order</button></a>
			<br>Thank you,<br>
			Speedysep Inc

			';
	$headers = array('Content-Type: text/html; charset=UTF-8');
	 
	wp_mail( $to, $subject, $body, $headers );
	
	
	//upload files
		if ( $_FILES ) { 
		$files = $_FILES["attachment_deliver"];  
		foreach ($files['name'] as $key => $value) {            
				if ($files['name'][$key]) { 
					$file = array( 
						'name' => $files['name'][$key],
						'type' => $files['type'][$key], 
						'tmp_name' => $files['tmp_name'][$key], 
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					); 
					$_FILES = array ("attachment_deliver" => $file); 
					foreach ($_FILES as $file => $array) {              
						$attach_id = upload_multipe_attachment($file,$order_id);
						if ( is_numeric( $attach_id ) ) {
						 // if(get_post_meta( $order_id, 'delivered_files', true)==''){
						//	  $project_files=array($attach_id);
						//	update_post_meta( $order_id, 'delivered_files', $project_files );
						//  }else{
							//  $project_files=get_post_meta( $order_id, 'delivered_files', true);
							//   $project_files[]=$attach_id;
							$delivered_files_arr=array();
							if(get_post_meta( $order_id, 'delivered_files',  true)==''){
								$delivered_files_arr[]=$attach_id;
								update_post_meta( $order_id, 'delivered_files',  $delivered_files_arr); 
							}else{
								$delivered_files_arr=get_post_meta( $order_id, 'delivered_files',  true);
								if(! is_array($delivered_files_arr)){
									$delivered_files_arr=array();
								}
								$delivered_files_arr[]=$attach_id;
								update_post_meta( $order_id, 'delivered_files',  $delivered_files_arr); 
							}
						//  }
						}
					}
				} 
			} 
		}
		
		//create message
		$req_post = array(
		  'post_title'    => 'Order Delivered',
		  'post_content'  => 'Order Delivered',
		  'post_status'   => 'private',
		  'post_type'	  => 'customers-messages',
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );
		update_post_meta($post_id, 'message_type', 'order-delivered');
		update_post_meta($post_id, 'order_id', $order_id);
		update_post_meta($post_id, 'project_files', $delivered_files_arr);
		
		update_post_meta($post_id, 'sender', '1');
		//update_post_meta($post_id, 'reciver', $buyer_id);
		
		echo json_encode('Completed');
		wp_die();
	//wp_redirect(site_url().'/wp-admin/?page=single-order&order_id='.$order_id);
}


add_action( 'wp_ajax_close_order', 'close_order_callback' );
add_action( 'wp_ajax_nopriv_close_order', 'close_order_callback' );
function close_order_callback() { //print_r($_POST); print_r($_FILES); exit;
	//process form
	$order_id=$_POST['close_order_id'];
	update_post_meta($order_id, 'order_status', 'closed');
	//upload files
		echo json_encode('Closed');
		wp_die();
}
//send messages to user
add_action( 'wp_ajax_get_unread_notifications_of_user', 'get_unread_notifications_of_user_callback' );
add_action( 'wp_ajax_nopriv_get_unread_messages_of_user', 'get_unread_notifications_of_user_callback' );
function get_unread_notifications_of_user_callback() {  //print_r($_POST);
	if(get_current_user_id()==1){
		$args = array(
	'post_type' => 'service-orders',
	'posts_per_page'=>-1,
	"post_status"=>'any',
	'orderby'   => 'ID',
     'order' => 'DESC',
	'meta_query' => array(
		'relation' => 'AND',
		/*array(
			'key'     => 'read',
			'compare' => 'NOT EXISTS',
		), */ 
		array(
		'relation' => 'OR',
		array(
			'key'     => 'order_admin_mess_status',
			'value'   => 'unread',
			'compare' => '=',
		),
		array(
			'key'     => 'order_completed_mess',
			'value'   => 'unread',
			'compare' => '=',
		),
		array(
			'key'     => 'order_placed_mess',
			'value'   => 'unread',
			'compare' => '=',
		),
		array(
			'key'     => 'order_revision_mess',
			'value'   => 'unread',
			'compare' => '=',
		),
		),
		
		
	),
	);
	
		$messages_query = new WP_Query( $args );
	$messages=array();
	foreach($messages_query->posts as $message){
		$image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png';
		$order_files=get_post_meta($message->ID, 'project_files', true); 
					if(!empty($order_files)){
					foreach($order_files as $order_file){ 
							$image_available=false;
							$image_attributes = wp_get_attachment_image_src( $order_file, 'thumbnail' );
						if ( $image_attributes ) : 
							 $image=$image_attributes[0];
						  $image_available=true; break;
						 endif; 
						
							 if($image_available==false){ 
							$image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png';
							 } 
							
					 }	} else{ $image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png'; }
					 
		$order_type=get_post_meta($message->ID, 'order_type', true);
					 
		if(get_post_meta($message->ID, 'order_completed_mess', true)=='unread'){
			$messages['order_completed_mess'][]=array('order_id'=>$message->ID, 'message'=>"Order#".$message->ID." Completed successfully", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'order_completed_mess_time', true))));
		}
		if(get_post_meta($message->ID, 'order_placed_mess', true)=='unread'){
			$messages['order_placed_mess'][]=array('order_id'=>$message->ID, 'message'=>"Order Placed successfully", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'order_placed_mess_time', true))));	
		}
		if(get_post_meta($message->ID, 'order_admin_mess_status', true)=='unread'){
			$messages['order_mess_status'][]=array('order_id'=>$message->ID, 'message'=>"New message on Order#".$message->ID, 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'unread_admin_message_time', true))));
		}
		//deliovered order
		if(get_post_meta($message->ID, 'order_revision_mess', true)=='unread'){
			$messages['order_delivered_mess'][]=array('order_id'=>$message->ID, 'message'=>"Revision Requested for Order#".$message->ID." ", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'order_revision_mess_time', true))));
		}
	}
		
	}else{
	$args = array(
	'post_type' => 'service-orders',
	'posts_per_page'=>-1,
	"post_status"=>'any',
	'author'=>get_current_user_id(),
	'orderby'   => 'ID',
     'order' => 'DESC',
	'meta_query' => array(
		'relation' => 'AND',
		/*array(
			'key'     => 'read',
			'compare' => 'NOT EXISTS',
		), */ 
		array(
		'relation' => 'OR',
		array(
			'key'     => 'order_mess_status',
			'value'   => 'unread',
			'compare' => '=',
		),
		array(
			'key'     => 'order_delivered_mess',
			'value'   => 'unread',
			'compare' => '=',
		),
		),
		
		
	),
	);
	
		$messages_query = new WP_Query( $args );
	$messages=array();
	foreach($messages_query->posts as $message){
		$image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png';
		$order_files=get_post_meta($message->ID, 'project_files', true); 
					if(!empty($order_files)){
					foreach($order_files as $order_file){ 
							$image_available=false;
							$image_attributes = wp_get_attachment_image_src( $order_file, 'thumbnail' );
						if ( $image_attributes ) : 
							 $image=$image_attributes[0];
						  $image_available=true; break;
						 endif; 
						
							 if($image_available==false){ 
							$image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png';
							 } 
							
					 }	} else{ $image=get_stylesheet_directory_uri().'/images/profile-pic-icon.png'; }
					 
		$order_type=get_post_meta($message->ID, 'order_type', true);
					 
		if(get_post_meta($message->ID, 'order_completed_mess', true)=='unread'){
			$messages['order_completed_mess'][]=array('order_id'=>$message->ID, 'message'=>"Order#".$message->ID." Completed successfully", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'unread_message_time', true))));
		}
		if(get_post_meta($message->ID, 'order_placed_mess', true)=='unread'){
			$messages['order_placed_mess'][]=array('order_id'=>$message->ID, 'message'=>"Order Placed successfully", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'unread_message_time', true))));	
		}
		if(get_post_meta($message->ID, 'order_mess_status', true)=='unread'){
			$messages['order_mess_status'][]=array('order_id'=>$message->ID, 'message'=>"New message on Order#".$message->ID, 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'unread_message_time', true))));
		}
		//deliovered order
		if(get_post_meta($message->ID, 'order_delivered_mess', true)=='unread'){
			$messages['order_delivered_mess'][]=array('order_id'=>$message->ID, 'message'=>"Order#".$message->ID." has been Delivered.", 'imagee'=>$image, 'order_type'=>ucfirst($order_type), 'messsage_time'=>get_time_ago(strtotime(get_post_meta($message->ID, 'delivered_date', true))));
		}
	}
	
	}
	
	echo json_encode($messages);
		//print_r($messages_query->posts);
	die(0);
}


//update user profile from admin
function update_user_info_callback() {
	$user_id = $_POST['user_id'];

	$user_id = wp_update_user( array( 'ID' => $user_id, 'user_email' => $_POST['email'] ) );
	update_user_meta($user_id, 'first_name', $_POST['name']);
	update_user_meta($user_id, 'phone_no', $_POST['phone']);
	update_user_meta($user_id, 'address', $_POST['address']);
	update_user_meta($user_id, 'state', $_POST['state']);
	update_user_meta($user_id, 'billing_zipcode', $_POST['zip']);

	if ( is_wp_error( $user_id ) ) {
		// There was an error, probably that user doesn't exist.
		wp_redirect(admin_url().'admin.php?page=speedy-customer-list&customer='.$user_id.'&update=0');
	} else {
		// Success!
		wp_redirect(admin_url().'admin.php?page=speedy-customer-list&customer='.$user_id.'&update=1');
	}		
}
add_action( 'admin_post_nopriv_update_user_info', 'update_user_info_callback' );
add_action( 'admin_post_update_user_info', 'update_user_info_callback' );




//add_expense
function add_expense_callback() { //echo "<pre>"; print_r($_POST['item']); exit;
	$req_post = array(
		  'post_title'    => 'Expenses',
		  'post_content'  => sanitize_text_field($_POST['description']),
		  'post_status'   => 'private',
		  'post_type'	  => 'speedy-expenses',
		  'post_date'	=>date('Y-m-d h:i:s', strtotime(sanitize_text_field($_POST['from_date']))),
		  'post_author'   => get_current_user_id()
		);
		 
		// Insert the post into the database
		$post_id=wp_insert_post( $req_post );	
		update_post_meta($post_id, 'from_date', sanitize_text_field($_POST['from_date']));
		update_post_meta($post_id, 'added_date', date('Y-m-d h:i:s'));
		update_post_meta($post_id, 'paid_by', sanitize_text_field($_POST['paid_by']));
		update_post_meta($post_id, 'total', sanitize_text_field($_POST['total']));
		update_post_meta($post_id, 'items', $_POST['item']);
		wp_redirect(admin_url().'/admin.php?page=speedy-aadd-expens');
}
add_action( 'admin_post_nopriv_add_expense', 'add_expense_callback' );
add_action( 'admin_post_add_expense', 'add_expense_callback' );

//add user from admin
function add_customer_callback() { 
	$user_name=sanitize_text_field($_POST['email']);
	$random_password = wp_generate_password( $length = 12, $include_standard_special_chars = false );
	$user_email=sanitize_text_field($_POST['email']);
	if($user_email!=''){
	if (email_exists($user_email) == false ) {
		//$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_id = wp_create_user( $user_name, $random_password, $user_email );
		update_user_meta($user_id, 'first_name', $_POST['name']);
		update_user_meta($user_id, 'phone_no', $_POST['phone']);
		update_user_meta($user_id, 'address', $_POST['address']);
		update_user_meta($user_id, 'state', $_POST['state']);
		update_user_meta($user_id, 'billing_zipcode', $_POST['zip']);
		update_user_meta($user_id, 'company_name', $_POST['company_name']);
		wp_new_user_notification($user_id, $deprecated = null, $notify = 'both');
		wp_redirect(admin_url().'/admin.php?page=speedy-customer-list&customer='.$user_id.'&user=added');
	} else {
		wp_redirect(admin_url().'/admin.php?page=speedy-customer-list&customer='.$user_id.'&user=no&email=exists');
	}
	}else{
		wp_redirect(admin_url().'/admin.php?page=speedy-customer-list&customer='.$user_id.'&user=no&email=empty');
	}
			
}
add_action( 'admin_post_nopriv_add_customer', 'add_customer_callback' );
add_action( 'admin_post_add_customer', 'add_customer_callback' );

/*****************************************************Admin Dashboard ends ************************************************/

function get_time_ago( $time ){
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'less than 1 second ago'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
        }
    }
}

// define constants
define('SITE_CURRENCY', 'usd');
define('SITE_CURRENCY_SYMBOL', '$');

function my_handle_attachment($file_handler,$post_id,$set_thu=false) {
  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );
  if ( is_numeric( $attach_id ) ) {
	  if(get_post_meta( $post_id, 'project_files', true)==''){
		  $project_files=array($attach_id);
		update_post_meta( $post_id, 'project_files', $project_files );
	  }else{
		  $project_files=get_post_meta( $post_id, 'project_files', true);
		   $project_files[]=$attach_id;
		 update_post_meta( $post_id, 'project_files',  $project_files); 
	  }
  }
  return $attach_id;
}

function my_handle_attachment_temp($file_handler,$post_id,$set_thu=false) {
  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );
  if ( is_numeric( $attach_id ) ) {
	  if(get_post_meta( $post_id, 'project_files_temp', true)==''){
		  $project_files=array($attach_id);
		update_post_meta( $post_id, 'project_files_temp', $project_files );
	  }else{
		  $project_files=get_post_meta( $post_id, 'project_files_temp', true);
		   $project_files[]=$attach_id;
		 update_post_meta( $post_id, 'project_files_temp',  $project_files); 
	  }
  }
  return $attach_id;
}



function upload_multipe_attachment($file_handler,$post_id,$set_thu=false) {
  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );
  
  return $attach_id;
}

function admin_default_page() {
  return site_url().'/wp-admin/admin.php?page=speedy-dashboard-settings';
}

add_filter('login_redirect', 'admin_default_page');

add_action('wp_footer', 'add_script_to_footer');

function add_script_to_footer(){
	if(is_page( 'contact' )){
		include('inc/views/footer-script.php');
	}
}


/*==========================
 This snippet contains utility functions to create/update and pull data from the active user transient.
 Copy these contents to functions.php
 ===========================*/

//Update user online status
add_action('init', 'gearside_users_status_init');
add_action('admin_init', 'gearside_users_status_init');
function gearside_users_status_init(){
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	$user = wp_get_current_user(); //Get the current user's data

	//Update the user if they are not on the list, or if they have not been online in the last 900 seconds (15 minutes)
	if ( !isset($logged_in_users[$user->ID]['last']) || $logged_in_users[$user->ID]['last'] <= time()-900 ){
		$logged_in_users[$user->ID] = array(
			'id' => $user->ID,
			'username' => $user->user_login,
			'last' => time(),
		);
		set_transient('users_status', $logged_in_users, 900); //Set this transient to expire 15 minutes after it is created.
	}
}

//Check if a user has been online in the last 15 minutes
function gearside_is_user_online($id){	
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	
	return isset($logged_in_users[$id]['last']) && $logged_in_users[$id]['last'] > time()-900; //Return boolean if the user has been online in the last 900 seconds (15 minutes).
}

//Check when a user was last online.
function gearside_user_last_online($id){
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	
	//Determine if the user has ever been logged in (and return their last active date if so).
	if ( isset($logged_in_users[$id]['last']) ){
		return $logged_in_users[$id]['last'];
	} else {
		return false;
	}
}

//Add columns to user listings
add_filter('manage_users_columns', 'gearside_user_columns_head');
function gearside_user_columns_head($defaults){
    $defaults['status'] = 'Status';
    return $defaults;
}
add_action('manage_users_custom_column', 'gearside_user_columns_content', 15, 3);
function gearside_user_columns_content($value='', $column_name, $id){
    if ( $column_name == 'status' ){
		if ( gearside_is_user_online($id) ){
			return '<strong style="color: green;">Online Now</strong>';
		} else {
			return ( gearside_user_last_online($id) )? '<small>Last Seen: <br /><em>' . date('M j, Y @ g:ia', gearside_user_last_online($id)) . '</em></small>' : ''; //Return the user's "Last Seen" date, or return empty if that user has never logged in.
		}
	}
}

//Active Users Metabox
add_action('wp_dashboard_setup', 'gearside_activeusers_metabox');
function gearside_activeusers_metabox(){
	global $wp_meta_boxes;
	wp_add_dashboard_widget('gearside_activeusers', 'Active Users', 'dashboard_gearside_activeusers');
}
function dashboard_gearside_activeusers(){
		$user_count = count_users();
		$users_plural = ( $user_count['total_users'] == 1 )? 'User' : 'Users'; //Determine singular/plural tense
		echo '<div><a href="users.php">' . $user_count['total_users'] . ' ' . $users_plural . '</a> <small>(' . gearside_online_users('count') . ' currently active)</small></div>';
}
//Get a count of online users, or an array of online user IDs.
//Pass 'count' (or nothing) as the parameter to simply return a count, otherwise it will return an array of online user data.
function gearside_online_users($return='count'){
	$logged_in_users = get_transient('users_status');
	
	//If no users are online
	if ( empty($logged_in_users) ){
		return ( $return == 'count' )? 0 : false; //If requesting a count return 0, if requesting user data return false.
	}
	
	$user_online_count = 0;
	$online_users = array();
	foreach ( $logged_in_users as $user ){
		if ( !empty($user['username']) && isset($user['last']) && $user['last'] > time()-900 ){ //If the user has been online in the last 900 seconds, add them to the array and increase the online count.
			$online_users[] = $user;
			$user_online_count++;
		}
	}
	return ( $return == 'count' )? $user_online_count : $online_users; //Return either an integer count, or an array of all online user data.
}

function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('about');
    $wp_admin_bar->remove_node('wporg');
    $wp_admin_bar->remove_node('documentation');
    $wp_admin_bar->remove_node('support-forums');
    $wp_admin_bar->remove_node('feedback');
    $wp_admin_bar->remove_node('view-site');
		$wp_admin_bar->remove_node('comments');
	$wp_admin_bar->remove_node('customize');
	$wp_admin_bar->remove_node('customize-background');
	$wp_admin_bar->remove_node('customize-header');
	echo '
	<style type="text/css">
	#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
	background-image: url(' . site_url() . '/wp-content/uploads/2019/04/cropped-Speedy-Seps-f.png) !important;
	background-position: 0 0;
	color:rgba(0, 0, 0, 0);
	}
	#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
	background-position: 0 0;
	}
	</style>
	';
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );
add_action( 'admin_bar_menu', 'wp_admin_bar_shortlink_menu',90 );

add_action('admin_footer', 'my_admin_footer_function');
function my_admin_footer_function() {
	include('inc/views/admin/inc/header.php');
}

function download_stylesheet(){
	$count=1;
	$data=array();
			$args = array(
				'role' => 'subscriber', 
				'meta_query' => array(
					array(
						'key' => 'first_name',
						'value' => $_GET['customer_name'],
						'compare' => 'LIKE'
					)
				)
			);
			$users= get_users($args);
			//$users = get_users('orderby=nicename&role=subscriber');
			foreach ($users as $user) {
			//get this user orders data
			$total_orders=0;
			$total_spend=0;
			$avg_price=0;
			global $wpdb;
			$another_where='';
			if(isset($_GET['from_date']) and $_GET['from_date']!='' and isset($_GET['to_date']) and $_GET['to_date']!=''){
				$another_where=" AND (post_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')";
			}
			
			
			$user_orders = $wpdb->get_results( 
												"
												SELECT ID, post_title 
												FROM $wpdb->posts
												WHERE  post_type = 'service-orders' AND post_author = ".$user->ID." $another_where
												"
											);

			foreach ( $user_orders as $user_order ){
				if(isset($_GET['type']) and $_GET['type']!=''){
					if(get_post_meta($user_order->ID, 'order_type', true)== $_GET['type']){
						$total_orders+=1;
						$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
					}
				}else{
				$total_orders+=1;
				$total_spend+=get_post_meta($user_order->ID, 'order_total', true);
				}
			}
			
			if($total_orders==0){				
				$avg_price=0;
			}else{
				$avg_price=round($total_spend/$total_orders);
			}
			
			if(isset($_GET['type']) and $_GET['type']!=''){
				if($_GET['type']=='seperation'){
					$type='separation';
				}else{
				$type=$_GET['type'];
				}
			}else{
				$type='--';
			}
			$data[]=["S.No" =>  $count, "Customer Name" => get_user_meta($user->ID, 'first_name', true), "type" => $type, "Orders"=>$total_orders, "Total Spent"=>SITE_CURRENCY_SYMBOL.$total_spend, "Avg. Order Price"=>SITE_CURRENCY_SYMBOL.$avg_price];
			 
			$count++; }
			
	
	

  // filename for download
  $filename = "sales_reports_" . date('Ymd') . ".xls";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($data as $row) {
    if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\r\n";
      $flag = true;
    }
    array_walk($row, __NAMESPACE__ . '\cleanData');
    echo implode("\t", array_values($row)) . "\r\n";
  }
  exit;
}

function cleanData(&$str){
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}
//download_stylesheet();

add_action('admin_init' , 'create_excel_Sheet_callback');
function create_excel_Sheet_callback(){
	if(isset($_GET['download_excel'])){
			download_stylesheet();
		}
	if(isset($_GET['download_excel_expenses'])){
		download_stylesheet_expenses_reports();
	}
	if(isset($_GET['download_users_expenses_list'])){
		download_customer_list();
	}
}

function download_stylesheet_expenses_reports(){
				
			$count=1;
			$data=array();
			global $wpdb;
			$another_where='';
			if(isset($_GET['from_date']) and $_GET['from_date']!='' and isset($_GET['to_date']) and $_GET['to_date']!=''){
				$another_where=" AND (post_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')";
			}
			
			
			$expenses = $wpdb->get_results( 
												"
												SELECT ID, post_content, post_date 
												FROM $wpdb->posts
												WHERE  post_type = 'speedy-expenses'  $another_where ORDER BY ID DESC
												"
											);

			foreach ( $expenses as $expense ){ 
			$data[]=["S.No" =>  $count, "Date" => $expense->post_date, "Description"=>$expense->post_content, "Total Cost"=>SITE_CURRENCY_SYMBOL.get_post_meta($expense->ID, 'total', true), "Paid By"=>get_post_meta($expense->ID, 'paid_by', true)];
			 
			$count++; }
			
	
	

  // filename for download
  $filename = "expenses_reports_" . date('Ymd') . ".xls";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($data as $row) {
    if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\r\n";
      $flag = true;
    }
    array_walk($row, __NAMESPACE__ . '\cleanData');
    echo implode("\t", array_values($row)) . "\r\n";
  }
  exit;
}

add_action('init' ,'check_for_delivered_hours');

function check_for_delivered_hours(){
	// The Query
$args = array(
	'post_type' => 'service-orders',
	'posts_per_page'=>-1,
	"post_status"=>'any',
	'meta_query' => array(
							'relation' => 'AND',
							array(
								'relation' => 'OR',
								array(
									'key' => 'order_status',
									'value'    => 'delivered',
									'compare' => '=',
								),
							)							
							
						),
	);
$buyers_requests_query = new WP_Query( $args );
//print_r($buyers_requests_query->posts);
if ( $buyers_requests_query->have_posts() ) {
	foreach($buyers_requests_query->posts as $order){
	$delivered_date=get_post_meta($order->ID, 'delivered_date', true); 
	
	$hours=differenceInHours($delivered_date, date('Y-m-d h:i:s'));
	if($hours>48){
		update_post_meta($order->ID, 'order_status', 'closed');
	}
	}
}
}

function differenceInHours($startdate,$enddate){
	$starttimestamp = strtotime($startdate);
	$endtimestamp = strtotime($enddate);
	$difference = abs($endtimestamp - $starttimestamp)/3600;
	return $difference;
}

function remove_element($array,$value) {
  return array_diff($array, (is_array($value) ? $value : array($value)));
}

function ss_login_logo() { ?>
    <style type="text/css">
        #login-designer-logo, body.login #login h1 a {
		background-size: 100% !important;
	}
	#login-designer-logo-h1, body.login #login h1 a {
		width: 265px !important;
		height: 75px !important;
	}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'ss_login_logo' );



function analytics_code(){ 

?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145673706-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145673706-1');
</script>



<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1803011733328206'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1803011733328206&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>



<?php 
} 
add_action('wp_head', 'analytics_code');

//delete selected customers
add_action( 'wp_ajax_delete_customers', 'delete_customers_callback' );
add_action( 'wp_ajax_nopriv_delete_customers', 'delete_customers_callback' );
function delete_customers_callback(){
	foreach($_POST['userids'] as $user_id){
		wp_delete_user($user_id);
	}
}

//download users
function download_customer_list(){
				
			$count=1;
			$data=array();
			global $wpdb;
			$another_where='';
			
						
			$count=1;
			$args = array(
				'role' => 'subscriber', 
			);
			$users= get_users($args);
			//$users = get_users('orderby=nicename&role=subscriber');
			foreach ($users as $user) {
			
			$data[]=["S.No" =>  $count, "Name" =>get_user_meta($user->ID, 'first_name', true), "Company"=>get_user_meta($user->ID, 'company_name', true), "Location"=>get_user_meta($user->ID, 'address', true), "Email"=>$user->user_email, 
			"Phone"=>get_user_meta($user->ID, 'phone_no', true)];
			 
			
			 $count++; }
			
	
	

  // filename for download
  $filename = "customers_list_" . date('Ymd') . ".xls";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($data as $row) {
    if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\r\n";
      $flag = true;
    }
    array_walk($row, __NAMESPACE__ . '\cleanData');
    echo implode("\t", array_values($row)) . "\r\n";
  }
  exit;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes[ 'eps' ] = 'application/postscript';
  $mimes[ 'ai' ] = 'application/postscript';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function dequeue_my_css() {
	if(is_page(array('landing-page-1', 'landing-page-2', 'landing-page-3',  'pricing', 'home-landing', 'front-page')) || is_front_page()){ 
	  wp_dequeue_style('twentysixteen-style');
	  wp_deregister_style('twentysixteen-style');
	  
	  wp_dequeue_style('speedysep-bootstrap');
	  wp_deregister_style('speedysep-bootstrap');
	  
	  wp_dequeue_style('bootstrap-style-cdn');
	  wp_deregister_style('bootstrap-style-cdn');
	}
}
add_action('wp_enqueue_scripts','dequeue_my_css');



add_action( 'send_headers', 'tgm_io_strict_transport_security' );
/**
 * Enables the HTTP Strict Transport Security (HSTS) header.
 *
 * @since 1.0.0
 */
function tgm_io_strict_transport_security() {
 
    header( 'Strict-Transport-Security: max-age=10886400; includeSubDomains; preload' );
 
}


// add_action( 'wp_footer', 'pum14_popup_reg_form_check', 500 );

function pum14_popup_reg_form_check() {
    
     echo "test ";
    
        if ( isset( $_POST['pum_form_popup_id'] ) && $_POST['pum_form_popup_id'] == '1828' ) {
             echo "test 3"; die(0);
                ?>
                
        <script type="text/javascript">
        (function ($, document, undefined) {

            jQuery('#popmake-1828').trigger('pumSetCookie');

            setTimeout(function () {
                jQuery('#popmake-1828').popmake('close');
            }, 5000); // 5 seconds
            
           dataLayer.push({ 'event':'popUp-Submission'  })
           
        }(jQuery, document))
        </script>
        
      
        
                <?php
        }
     
        
}




// $threshold = (int) apply_filters( 'big_image_size_threshold', 2560, $imagesize, $file, $attachment_id );

function increase_image_size_threshold( $imagesize, $file, $attachment_id ){
    
    return 9600;
    
}

function showselectedPlan(){

global $wpdb;

$user = wp_get_current_user();

$customer = $wpdb->get_row("SELECT p.status,p.last_four,p.start_date,p.end_date, u.mem_ship_id, u.name, u.amount,  u.frequency, u.type,u.plan_data
FROM $wpdb->user_memberships p
INNER JOIN $wpdb->memberships u ON p.type = u.mem_ship_id WHERE p.user_id = $user->ID Order By p.mem_id DESC LIMIT 1" );

return $customer;	
}
function showselectedPlanByUser($userid){

global $wpdb;

$customer = $wpdb->get_row("SELECT p.status,p.last_four,p.start_date,p.end_date, u.mem_ship_id, u.name, u.amount,  u.frequency, u.type,u.mockups,u.mem_order,p.basic_order,p.standard_order,p.premium_order,p.video_mockups,p.is_updated
FROM $wpdb->user_memberships p
INNER JOIN $wpdb->memberships u ON p.type = u.mem_ship_id WHERE p.user_id = '".$userid."'  Order By p.mem_id DESC LIMIT 1" );

return $customer;	
}

function showMembershipDataYearly(){

global $wpdb;
$plan_yearly = $wpdb->get_results("SELECT * FROM $wpdb->memberships WHERE frequency='Y' ");

return $plan_yearly;	
}

function showMembershipDataMonthly(){

global $wpdb;
$plan_monthly = $wpdb->get_results("SELECT * FROM $wpdb->memberships WHERE frequency='M' ");

return $plan_monthly;	
}


function getUsermembershipDetail($userid)
{
	global $wpdb;
 	$membership = $wpdb->get_row("SELECT $wpdb->memberships.name,$wpdb->user_memberships.amount as membershipamount,$wpdb->user_memberships.status,$wpdb->memberships.frequency,$wpdb->user_memberships.mem_id,$wpdb->user_memberships.stripe_subscription_id,$wpdb->user_memberships.type,$wpdb->user_memberships.start_date,$wpdb->user_memberships.end_date FROM $wpdb->user_memberships LEFT JOIN $wpdb->memberships 
		ON $wpdb->user_memberships.type = $wpdb->memberships.mem_ship_id  WHERE  $wpdb->user_memberships.user_id = '".$userid."' AND $wpdb->user_memberships.status = 'Active' ORDER BY $wpdb->user_memberships.user_id DESC LIMIT 1");
 	return $membership;
}

function getpendingmembership($userid)
{
	global $wpdb;
 	$membership = $wpdb->get_row("SELECT $wpdb->memberships.name,$wpdb->user_memberships.amount as membershipamount,$wpdb->user_memberships.status,$wpdb->memberships.frequency,$wpdb->user_memberships.mem_id,$wpdb->user_memberships.stripe_subscription_id,$wpdb->user_memberships.type,$wpdb->user_memberships.start_date,$wpdb->user_memberships.end_date FROM $wpdb->user_memberships LEFT JOIN $wpdb->memberships 
		ON $wpdb->user_memberships.type = $wpdb->memberships.mem_ship_id  WHERE  $wpdb->user_memberships.user_id = '".$userid."' AND $wpdb->user_memberships.status = 'Pending' ORDER BY $wpdb->user_memberships.user_id DESC LIMIT 1");
 	return $membership;
}

// add_filter( 'big_image_size_threshold', 'increase_image_size_threshold',10,3 );

// add_filter( 'big_image_size_threshold', '__return_false' );
function getUsermembershipHistory($userid)
{
	global $wpdb;
 	$membership = $wpdb->get_results("SELECT $wpdb->user_memberships.status, $wpdb->memberships.name,$wpdb->user_memberships.amount as membershipamount,$wpdb->user_memberships.start_date,$wpdb->memberships.frequency,$wpdb->user_memberships.end_date FROM $wpdb->user_memberships LEFT JOIN $wpdb->memberships 
		ON $wpdb->user_memberships.type = $wpdb->memberships.mem_ship_id  WHERE  $wpdb->user_memberships.user_id = '".$userid."' ORDER BY $wpdb->user_memberships.mem_id DESC");
 	return $membership;
}	

?>