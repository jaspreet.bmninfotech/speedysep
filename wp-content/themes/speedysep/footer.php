<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
	
	
		</div><!-- .site-inner -->
</div><!-- .site -->

		
			<div class="footerarea">
	<div class="container-footer">
    	<div class="footertop">
        	<div class="footertopleft">
            	<h2>Start your business <br> journey with SpeedySep</h2>
            </div>
            <div class="footertopright">
             
            	
            	    <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Start free trial </a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard""> Start free trial </a></li>
                        <?php } ?> 
            	
            	
            </div>
        </div>
    	<div class="footerinfo">
        	<div class="footerleft">
            	<div class="footerlogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/footer-logo.png" alt="" />
                </div>
            	<p>SpeedySep is risk-free with a 100% money-back guarantee. Simplify your screen printing process with SpeedySeps.</p>
            </div>
            <div class="footerright">
            	<div class="col-1">
                	<ul>
                    	<li><a href="<?php echo home_url() ?>/pricing/">Pricing</a></li>
                        <li><a href="<?php echo home_url() ?>/speedy-sep-guide-for-vector-order/">Vectorizing</a></li>
                        <li><a href="<?php echo home_url() ?>/speedy-sep-guide-for-color-separation-order/">Color Separation</a></li>
                         
                        
                        
                         <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">SignUp </a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard""> SignUp </a></li>
                        <?php } ?> 
                    
                        <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                    
                    
                   
                        
                    </ul>
                </div>
                <div class="col-1">
                	<ul> <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                    	<li><a href="<?php echo home_url() ?>/contact-us/">Contact Us</a></li>
                        <li><a href="<?php echo home_url() ?>/about-us/">About Us</a></li>
                        <li><a href="<?php echo home_url() ?>/blog">Blog</a></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyrightarea">
	<div class="container-footer">
    	<div class="copyrightleft">
        	<ul>
            	<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/twitter-icon.png" alt="twitter" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/facebook-icon.png" alt="facebook" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/instagram-icon.png" alt="instagram" /></a></li>
                <li><a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/printerset-icon.png" alt="printerset" /></a></li>
            </ul>
        </div>
        <div class="copyrightright">
        	
        	<p><a href="<?php echo home_url() ?>/privacy-policy/">Privacy Policy</a> | <a href="<?php echo home_url() ?>/terms-and-conditions/">Terms of use</a> </p>
              
            <p>Copyright &copy; 2019 by <a href="<?php echo home_url() ?>">speedysep.com</a></p>
            
            
            
        </div>
    </div>
</div>	
		
<!--
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_class'     => 'primary-menu',
							)
						);
					?>
				
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>',
							)
						);
					?>
				</nav>
			<?php endif; ?>

			<div class="site-info">
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
				<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}
				?>
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentysixteen' ) ); ?>" class="imprint">
					<?php printf( __( 'Proudly powered by %s', 'twentysixteen' ), 'WordPress' ); ?>
				</a>
			</div>
		</footer> -->

<?php 
//if(! is_page( array( 'color-seperations' ) )) {
?>
  <!-- Modal -->
  <div class="modal fade" id="registrationModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <?php echo do_shortcode('[speedysep_registration]'); ?>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal Login -->
  <div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <?php echo do_shortcode('[speedysep_login]'); ?>
        </div>
      </div>
      
    </div>
  </div>
  
 <div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formlist">
						<input name="" type="text" placeholder="Your shop name" id="full_name" name="full_name".>
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="create your account">
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>
<div style="display:none">
	<div id="popupcontentlogin" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Login to Speedysep account</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="login-from">
					<div class="formlist">
						<input name="" type="email" placeholder="Email address" id="email" name="eamil" required>
					</div>
					<div class="formlist">
						<input name="" type="password" placeholder="Password" id="password" name="password" required >
					</div>
					<div class="formsend">
						<input name="" type="submit" id="submit" value="Login to my account">
					</div>
					<div class="login-reset-pass" style="margin-top: 10px; text-align: right;">
						<p><a href="<?php echo site_url() ?>/wp-login.php?action=lostpassword" title="<?php _e('Reset Password', 'speedy') ?>" style="color:#fff;"><?php _e('Reset Password', 'speedy') ?></a></p>
					</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>


<?php //} ?>
<?php wp_footer(); ?>
<script>
var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function($){
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name').val(),		
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Logging in...');
				  window.location = "<?php echo site_url() ?>/user-dashboard";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

$('.registration-from-page').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name_page').val(),		
		'email': jQuery('#email_page').val(),
		'password':jQuery('#password_page').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name_page').val('');		
				jQuery('#email_page').val('');
				jQuery('#password_page').val('');
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
					jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

$('.login-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'login_user',	
		'email': jQuery('#login_email').val(),
		'password':jQuery('#login_password').val()
	};
	jQuery('.registration-message').text('Logging in...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='loggedin'){
				jQuery('.registration-message').text('Logging in...');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
					//setTimeout(function() {
					  window.location = "<?php echo site_url() ?>/user-dashboard";
					//}, 5000);
					
			}else{
				jQuery('.registration-message').html('<p class="error">Invalid Email/password.</p>');
			}
	});
});

$('#speedyLogin').on('click', function(){

	$('#registrationModal').modal('toggle');
	setTimeout(function() {
	   $('body').addClass('modal-open');
	}, 400);
	
	$('body').css('padding-right', '0px');
	
});

$('#speedySignup').on('click', function(){
	$('#loginModal').modal('toggle');
	setTimeout(function() {
	   $('body').addClass('modal-open');
	}, 400);
	
	$('body').css('padding-right', '0px');
});
	
});


jQuery(document).ready(function() {
	jQuery(".mobilemenu").click(function (event) {
		event.preventDefault();
		jQuery( ".floatingnav" ).animate({
				'right': '0'
			}, 300 );
		jQuery(this).addClass("opened");
	});
	jQuery(".closemenu").click(function (event) {
		event.preventDefault();
		jQuery( ".floatingnav" ).animate({
				'right': '-330px'
			}, 300 );
		jQuery(this).removeClass("opened");
	});  
}); 
</script>




<script>


var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
jQuery(document).ready(function($){
    
   	jQuery('.popup-with-form').magnificPopup({
		type: 'inline',
	});
	jQuery('.popup-with-form-login').magnificPopup({
		type: 'inline',
	});
	
	jQuery('.membership').magnificPopup({
        type: 'inline'
    });
	
$('.registration-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'register_user',
		'full_name': jQuery('#full_name').val(),		
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Please Wait...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='yes'){
				jQuery('.registration-message').text('Your registration is successful, please check your email.');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
			}else if(response=='loggedin'){
				jQuery('.registration-message').text('Account created successfully.');
				window.location = "<?php echo home_url() ?>/user-dashboard";
			}else if(response=='no'){
				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
			}else{
				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
			}
	});
});

// blog and post popup 
jQuery(document).ready(function() {
    
        $('.registration-from-blog').on('submit', function(e) {
        	e.preventDefault();
        	//fire ajax call 
        	var data = {
        		'action': 'register_user',
        		'full_name': jQuery('#full_name-popup').val(),		
        		'email': jQuery('#email-popup').val(),
        		'password':jQuery('#password-popup').val()
        	};
        	
        //	alert( jQuery('#full_name-popup').val() );
			//			alert( jQuery('#email-popup').val() );
			//			alert( jQuery('#password-popup').val() );
						
						
        	jQuery('.registration-message').text('Please Wait...');
        	 jQuery('#submit-popup').prop('disabled', true);
        
        	jQuery.post(ajax_url, data, function(response) {
        			console.log(" RESPONSE " + response);
        			jQuery('#submit-popup').removeAttr("disabled");
        		//	alert(response );
        			if(response=='yes'){
        				jQuery('.registration-message').text('Your registration is successful, please check your email.');
        				jQuery('#full_name-popup').val('');		
        				jQuery('#email-popup').val('');
        				jQuery('#password-popup').val('');
        				 dataLayer.push({ 'event':'popUp-Submission'  });
        				 
        			}else if(response=='loggedin'){
        				jQuery('.registration-message').text('Account created successfully.');
        				jQuery('.before-login').hide();
        			  	jQuery('.after-login').show();
        			  	 dataLayer.push({ 'event':'popUp-Submission'  });
        				// window.location = "<?php echo home_url() ?>/user-dashboard";
        			}else if(response=='no'){
        				jQuery('.registration-message').html('<p class="error">Email already exists.</p>');
        				//	jQuery( ".welcome-login" ).trigger( "click" );
        				
        				
        			}else{
        				jQuery('.registration-message').html('<p class="error">Please set all fields.</p>');
        			}
        	});
        });
        
        
        $("#submit-popup-login").click(function(){
             window.location = "<?php echo home_url() ?>/user-dashboard";
          });
        
        
         $("#submit-popup-reading").click(function(){
             // alert("The paragraph was clicked.");
              $( ".popmake-close" ).trigger( "click" );
              
          });
          
          
   });
   

//login form submit
$('.login-from').on('submit', function(e) {
	e.preventDefault();
	//fire ajax call 
	var data = {
		'action': 'login_user',	
		'email': jQuery('#email').val(),
		'password':jQuery('#password').val()
	};
	jQuery('.registration-message').text('Logging in...');
	 jQuery('#submit').prop('disabled', true);

	jQuery.post(ajax_url, data, function(response) {
			console.log(response);
			jQuery('#submit').removeAttr("disabled");
			if(response=='loggedin'){
				jQuery('.registration-message').text('Logging in...');
				jQuery('#full_name').val('');		
				jQuery('#email').val('');
				jQuery('#password').val('');
					//setTimeout(function() {
					  window.location = "<?php echo home_url() ?>/user-dashboard";
					//}, 5000);
					
			}else{
				jQuery('.registration-message').html('<p class="error">Invalid Email/password.</p>');
			}
	});
});


$('#page_email_input').on('change', function(){
	$('#email').val($(this).val());
});

});
</script>



</body>
</html>
