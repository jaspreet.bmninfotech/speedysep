<!DOCTYPE html>
<?php 
/**
 * Template Name: About Us
 *
 */
 ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<?php do_action( 'wp_head' ); ?>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5WSJKL');</script>
<!-- End Google Tag Manager -->


<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/responsive.css" rel="stylesheet" type="text/css" />

<!-- Hotjar Tracking Code for www.speedysep.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1465266,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>


</head>
<body>
    
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5WSJKL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="headerarea">
	<div class="container-header">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo home_url() ?>">
                	    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo.png" alt="logo">
                	</a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="https://speedysep.com/pricing/">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon.png" alt="">
                </div>
            </div>
        </div>
        <div class="floatingnav">
        	<div class="mobilemenuheading">
            	<div class="mobilelogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/mobile-logo.png" alt="mobile-logo">
                </div>
            	<div class="closemenu">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon-close.png" alt="icon-close"></a>
                </div>
            </div>
            <div class="menubox">
                <ul>
                    
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="https://speedysep.com/pricing/">Pricing</a></li>
                        <li><a href=" https://speedysep.com/portfolio/">Portfolio</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                    
                    
                </ul>
            </div>
        </div>
    </div>
</div> <!-- header end here -->


<div class="toparea">
	<div class="topleft">
    	<h1>ABOUT US</h1>
        <h1>SpeedySep powers <br> over 500 screen printers <br> worldwide</h1>
        <p>The all-in-one platform <br> for vector design and <br> color separations.</p>
    </div>
    <div class="topright">
    	<div class="toppic">
        	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/pic01.png" alt="about us" />
        </div>
    </div>
</div>
<div class="middlecontent">
	<div class="middlecontainer">
    	<div class="storywrap">
            <div class="storyinfo">
                <h3>SPEEDYSEP STORY</h3>
                <h3>The First <br> Speedysep Design</h3>
                <p>Over a decade ago, we started designing t-shirts for screen-printers. None of the platforms at the time gave us the control we needed to be successful—so we built our own. Today, screen-printers of all sizes use SpeedySep when they need a new vector design or color separations.</p>
            </div>
            <div class="storypic">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/story-pic.png" alt="" />
            </div>
        </div>
    </div>
</div>
<div class="teamarea">
	<div class="teamcontent">
    	<div class="teamleft-old">
        	<h2>OUR TEAM</h2>
            <h3>Creating a community</h3>
            <p>SpeedySep is a small team of designers and marketers. <br> We care deeply about the work we do, and we’re committed <br> to your shop success. We are on a mission to help <br> you increase your profit margins, get work done faster, <br> and more efficiently.</p>
        </div>
        <!-- 
        <div class="teamright">
        	<div class="teambox">
            	<div id="owl-demo" class="owl-carousel">
                	<div class="item">
                        <div class="singleteam">
                            <div class="userpic">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/ubaid.jpg" alt="Dan Nestelbaum" />
                            </div>
                            <h4>Ubaid Hussain</h4>
                            <span>Art Director</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="singleteam">
                            <div class="userpic">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/user-picture.png" alt="Dan Nestelbaum" />
                            </div>
                            <h4>Dan Nestelbaum</h4>
                            <span>Marketing Director</span> 
                        </div>
                    </div>
            -->
                  <!--                  
				  <div class="item">
                        <div class="singleteam">
                            <div class="userpic">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/user-picture.png" alt="Dan Nestelbaum" />
                            </div>
                            <h4>Dan Nestelbaum</h4>
                            <span>Marketing Director</span>
                        </div>
                    </div>
                  
				  <div class="item">
                        <div class="singleteam">
                            <div class="userpic">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/images/user-picture.png" alt="Dan Nestelbaum" />
                            </div>
                            <h4>Dan Nestelbaum</h4>
                            <span>Marketing Director</span>
                        </div>
						-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footerarea">
	<p>Try SpeedySep for free, and explore all the services you <br> need to grow your business.</p>
    <div class="trialbutton">
    	<a class="popup-with-form" href="#popupcontent">Start free trial</a>
    </div>
</div>



<?php get_footer(); ?>

<div style="display:none">
	<div id="popupcontent" class="popupouter">
		<div class="popupbox">
			<div class="contactform">
				<h1>Start your free trial of Speedysep</h1>
				<form action="<?php echo admin_url('admin-ajax.php') ?>" method="post" class="registration-from">
				<div class="formlist">
					<input name="" type="text" placeholder="Email address">
				</div>
				<div class="formlist">
					<input name="" type="password" placeholder="Password">
				</div>
				<div class="formlist">
					<input name="" type="text" placeholder="Your store name">
				</div>
				<div class="formsend">
					<input name="" type="submit" value="create your account">
				</div>
				</form>
				<br>
				<div class="registration-message"><p></p></div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/js/owl.carousel.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/about-landing/js/jquery.magnific-popup.js"></script>
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
	    pagination : false,
	    rewindNav : true,
        navigation : true
      });
    });
</script>
<script>
			$('.popup-with-form').magnificPopup({
				type: 'inline',
			});

			var ajax_url='<?php echo admin_url('admin-ajax.php') ?>';
		jQuery(document).ready(function($){
		

		$('#page_email_input').on('change', function(){
			$('#email').val($(this).val());
		});

		});	
		
		
   
   
   		
</script>
<?php  // do_action( 'wp_footer' ) ?>
</body>
</html>
