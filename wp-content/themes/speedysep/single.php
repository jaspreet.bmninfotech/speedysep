<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<!--
<header class="front-page-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="logo-left">
				<?php $page_id=269; ?>
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#registrationModal" title="<?php echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text',true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'join_us_text', true); ?>"><button class="btn signup-btn"><span><?php echo get_post_meta($page_id, 'join_us_text', true); ?></span> <i class="fa fa-user"></i> </button></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="text-center site-logo">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="logo-right">
					<?php if(!is_user_logged_in()){ ?>
					<a href="#" data-toggle="modal" data-target="#loginModal" title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php }else{ ?> 
					<a href="<?php echo site_url() ?>/user-dashboard"  title="<?php  echo get_post_meta($page_id, 'login_text', true); ?>"><button class="btn signup-btn login-btn"><span><?php  echo get_post_meta($page_id, 'login_text', true); ?></span> <i class="fas fa-sign-out-alt"></i> </button></a>
					<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
</header>
-->

<div class="headerarea">
	<div class="container-header">
    	<div class="header">
        	<div class="headerleft">
            	<div class="logo">
                	<a href="<?php echo home_url() ?>">
                	    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo.png" alt="logo">
                	</a>
                </div>
            </div>
            <div class="headerright">
            	<div class="navigation">
                	<ul>
                    	<li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="https://speedysep.com/pricing/">Pricing</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                    </ul>
                </div>
                <div class="mobilemenu">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon.png" alt="">
                </div>
            </div>
        </div>
        <div class="floatingnav">
        	<div class="mobilemenuheading">
            	<div class="mobilelogo">
                	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/mobile-logo.png" alt="mobile-logo">
                </div>
            	<div class="closemenu">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/menu-icon-close.png" alt="icon-close"></a>
                </div>
            </div>
            <div class="menubox">
                <ul>
                        <li><a href="tel:(954) 707-7155">(954) 707-7155</a></li>
                        <li><a href="https://speedysep.com/pricing/">Pricing</a></li>
                        <li><a href="<?php echo home_url("/").'#features' ?>">Features</a></li>
                        <li><a href="https://speedysep.com/blog/">Blog</a></li>

                         <?php if(!is_user_logged_in()){ ?>
                        <li><a class="popup-with-form-login" href="#popupcontentlogin">Login</a></li>
                         <?php }else{ ?> 
                         <li><a class="" href="<?php echo home_url() ?>/user-dashboard">Login</a></li> 
                        <?php } ?>
                        
                        
                        <?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent">Try It Free</a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard"">Try It Free</a></li>
                        <?php } ?> 
                </ul>
            </div>
        </div>
    </div>
</div> <!-- header end here -->


<div  class="container-fluid sep-home-buttons">
	<div class="btn-left"><a href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/front-home.png"></a></div>
</div>
<div class="container">
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) :
			the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				//comments_template();
			}

			if ( is_singular( 'attachment' ) ) {
				// Parent post navigation.
				the_post_navigation(
					array(
						'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
					)
				);
			} elseif ( is_singular( 'post' ) ) {
				// Previous/next post navigation.
				the_post_navigation(
					array(
						'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next:', 'twentysixteen' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Next post:', 'twentysixteen' ) . '</span> ' .
							'<span class="post-title">%title</span>',
						'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous:', 'twentysixteen' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Previous post:', 'twentysixteen' ) . '</span> ' .
							'<span class="post-title">%title</span>',
					)
				);
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->
	
	<div class="admin-message-box">
		<h4>About SpeedySep</h4>
		<p>Speedysep helps screen printers simplify their screen-printing process. We take your art works and create color separations for screen printing. We can also take your fuzzy art images and turn them to a clean and print ready vector art. 
			Our services are risk-free with no hidden fees or charges. Your artwork will be processed by a team of professionals with 100% money-back guarantee.
		</p>
		<!-- <a href="#" data-toggle="modal" data-target="#registrationModal" title="Signup"><button class="btn signup-btn">Try SpeedySep</button></a> -->
		
		<?php if(!is_user_logged_in()){ ?>
                        <li class="tryit"><a class="popup-with-form" href="#popupcontent"><button class="btn signup-btn">Try SpeedySep</button></a></li>
                        <?php }else{ ?> 
                         <li class="tryit"><a class="" href="<?php echo home_url() ?>/user-dashboard""><button class="btn signup-btn">Try SpeedySep</button></a></li>
                        <?php } ?> 
		
	</div>
	<ul class="next-three-posts">
	<div class="row">
		<?php
		
		 
		  
           // Default arguments
                $args = array(
                	'posts_per_page' => 3, // How many items to display
                	'post__not_in'   => array( get_the_ID() ), // Exclude current post
                	'no_found_rows'  => true, // We don't ned pagination so this speeds up the query
                );
                
                // Check for current post category and add tax_query to the query arguments
                $cats = wp_get_post_terms( get_the_ID(), 'category' ); 
                $cats_ids = array();  
                foreach( $cats as $wpex_related_cat ) {
                	$cats_ids[] = $wpex_related_cat->term_id; 
                }
                if ( ! empty( $cats_ids ) ) {
                	$args['category__in'] = $cats_ids;
                }
                
                // Query posts
                $wpex_query = new wp_query( $args );
                
                
                
              //  print_r( $wpex_query );
                
                // Loop through posts
                foreach( $wpex_query->posts as $post ) : setup_postdata( $post ); 
                
                    	$cat=get_the_category();
                		echo "<div class='col-sm-4'>";
                	      echo "<a href='".get_the_permalink()."'>";
                		    echo '<li style="background-image:url('.get_the_post_thumbnail_url().')">';
                		     echo "<div class='next-posts-inner'>";
                		     
                		     	if(is_array($cat)){
                					echo '<h5>'.$cat[0]->name.'</h5>';
                					}
                                		     
                		       	echo '<h3>' . get_the_title() . '</h3>';
               
                
                            echo "</div>";
					       echo '</li>';
				      	 echo '</a>';
				    	echo "</div>";
                // End loop
                endforeach;
                
                // Reset post data
                wp_reset_postdata(); 
                
              
                		
		
		 /*
		  global $post;
			$current_post = $post;

			for($i = 1; $i <= 3; $i++):
				
					$post = get_next_post();
					setup_postdata($post);
					if($post->ID){
					$cat=get_the_category();
					echo "<div class='col-sm-4'>";
					echo "<a href='".get_the_permalink()."'>";
					echo '<li style="background-image:url('.get_the_post_thumbnail_url().')">';
					echo "<div class='next-posts-inner'>";
					if(is_array($cat)){
					echo '<h5>'.$cat[0]->name.'</h5>';
					}
					echo '<h3>' . get_the_title() . '</h3>';
					echo "</div>";
					echo '</li>';
					echo '</a>';
					echo "</div>";
				}
			endfor;
			$post = $current_post; 
          */
          
          
		?>
		</div>
	</ul>
</div><!-- .content-area -->

<?php get_sidebar(); ?>
</div>

<!--
<footer class="front-page-footer">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="text-right">
				<ul class="nav navbar-nav">
				<?php
					$menu_name = 'speedysep-user-menu';
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menu_items = wp_get_nav_menu_items(9);
					foreach ( (array) $menu_items as $key => $menu_item ) { 
						$class='';
						$current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
						foreach($menu_item->classes as $item_class){
							$class.=$item_class." ";
						}
						$title = $menu_item->title;
						$url = $menu_item->url;
						if($menu_item->menu_item_parent>0){
							$menu_list .= '<li class="menu-sub-item ' . $current . '"><a href="'.$url.'">'.$title.' | </a></li>';
						}else{
						$menu_list .= '<li class="' . $current . '"><a href="'.$url.'">'.$title.' |</a></li>';
						}
					}
				echo $menu_list;
				?>
				<li class="copyright-text">SpeedySep &copy; <?php echo date('Y') ?></li>
				</ul>
			</div>
			</div>
		</div>
	</div>
</footer>

-->
<?php get_footer(); ?>