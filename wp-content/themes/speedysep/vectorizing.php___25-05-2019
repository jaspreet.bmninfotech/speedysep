<?php
/**
 * Template Name: vectorizing
 *
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="vect-sec vect-top">
			<div class="mobile-header">
				<div class="logo-left">
					<?php twentysixteen_the_custom_logo(); ?>
				</div>
				<div class="logo-right">
					<span style="font-size:30px;cursor:pointer;padding-right:10px;" onclick="openNav()">&#9776;</span>
				</div>
				
			</div>
			<div class="container">
				<div class="row">
				<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
							?>
					<div class="col-sm-8">
						<div class="play-btn">
							<span class="helper"></span>
							<a href="#" data-toggle="modal" data-target="#videoModal">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-btn.png" alt="speedysep" title="play">
							</a>
						</div>
					</div>
					
					<span class="home-btn">
						<a href="<?php echo site_url() ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home-btn.png" alt="speedysep" title="Go to home">
						</a>
					</span>
					<div id="myNav" class="overlay">
					  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="<?php echo get_stylesheet_directory_uri().'/images/close.png' ?>" alt="close"></a>
					  <div class="overlay-content">
						<ul class="nav">
							<li><a href="#manual-vect" onclick="closeNav()">Manual Vectorization</a></li>
							<li><a href="#features" onclick="closeNav()">Features</a></li>
							<li><a href="#customization" onclick="closeNav()">Customizations</a></li>
							<li><a href="#formats" onclick="closeNav()">Formats</a></li>
							<li><a href="#pricing" onclick="closeNav()">Pricing</a></li>
							<li><a href="#faq" onclick="closeNav()">FAQ</a></li>
							<li><a href="#order-now" onclick="closeNav()">Order Now</a></li>
						</ul>
					  </div>
					</div>

					<div class="sidebar-menu-right">
						<ul class="nav">
							<li class="vectorizing"><?php the_title() ?></li>
							<li><a class="active" href="#manual-vect">Manual Vectorization</a></li>
							<li><a class="active" href="#features" class="">Features</a></li>
							<li><a class="active" href="#customization" class="">Customizations</a></li>
							<li><a class="active" href="#formats">Formats</a></li>
							<li><a class="active" href="#pricing">Pricing</a></li>
							<li><a class="active" href="#faq">FAQ</a></li>
							<li><a class="active" href="#order-now">Order Now</a></li>
						</ul>
					</div>
					<?php 
					// End of the loop.
					endwhile;
					?>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-manual-vect" id="manual-vect">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
					<img src="<?php the_field('vectorization_image')?>" alt="manual vectorization" title="manual vectorization" data-aos="flip-left" data-aos-duration="2000">
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('manual_vectorization_contents'); ?>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-features" id="features">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('features_title'); ?>
					</div>
					<img src="<?php the_field('features_image')?>" alt="features" title="Features" data-aos="flip-left" data-aos-duration="2000">
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('features_contents'); ?>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-customization" id="customization">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
					<img src="<?php the_field('customization_image')?>" alt="Customization" title="Customization" data-aos="flip-left" data-aos-duration="2000">
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('customizations_contents'); ?>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-formats" id="formats">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
					<p class="format_title">
					<strong data-aos="fade-right" data-aos-duration="1000"><?php echo the_field('available_formats_title'); ?></strong>
					</p>
					<img src="<?php the_field('formats_image')?>" alt="Formats" title="Formats" data-aos="flip-left" data-aos-duration="2000">
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('formats_contents'); ?>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-pricing" id="pricing">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						 <div class = "table-responsive">
							<table data-aos="fade-right" data-aos-duration="1000">
								<thead>
									<th></th>
									<th><?php the_field('basic_plan_text')?></th>
									<th><?php the_field('standard_plan_text')?></th>
									<th><?php the_field('premium_plan_text')?></th>
								</thead>
								<tbody>
									<tr class="even">
										<td><?php the_field('choose_plan_text')?></td>
										<td>
											<span class="plan-price"><?php  echo SITE_CURRENCY_SYMBOL."".get_field('basic_plan_price')?></span>
											<a href="<?php the_field('basic_buy_now_link')?>" title="<?php the_field('buy_now_text')?>" class="buy-now"><?php the_field('buy_now_text')?></a>
										</td>
										<td>
											<span class="plan-price"><?php echo SITE_CURRENCY_SYMBOL."".get_field('standard_plan_price')?></span>
											<a href="<?php the_field('standard_buy_now_link')?>" title="<?php the_field('buy_now_text')?>" class="buy-now"><?php the_field('buy_now_text')?></a>
										</td>
										<td>
											<span class="plan-price"><?php echo SITE_CURRENCY_SYMBOL."".get_field('premium_plan_price')?></span>
											<a href="<?php the_field('premium_buy_now_link')?>" title="<?php the_field('buy_now_text')?>" class="buy-now"><?php the_field('buy_now_text')?></a>
										</td>
									</tr>
									<tr class="odd">
										<td><?php the_field('illustrator_ai_text')?></td>
										<td><?php if(get_field('basic_illustrator_ai')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_illustrator_ai')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_illustrator_ai')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
									</tr>
									<tr class="even">
										<td><?php the_field('transparent_png_text')?></td>
										<td><?php if(get_field('basic_transparent_png')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_transparent_png')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_transparent_png')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										
									</tr>
									<tr class="odd">
										<td><?php the_field('eps_text')?></td>
										<td><?php if(get_field('basic_eps')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_eps')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_eps')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										
									</tr>
									<tr class="even">
										<td><?php the_field('other_formats_text')?></td>
										<td><?php if(get_field('basic_other_formats')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_other_formats')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_other_formats')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										
									</tr>
									<tr class="odd">
										<td><?php the_field('concept_recreation_text')?></td>
										<td><?php if(get_field('basic_concept_recreation')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_concept_recreation')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_concept_recreation')[0]=='Yes'){  ?><i class="fa fa-check" aria-hidden="true"></i> <?php }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										
									</tr>
									<tr class="even">
										<td><?php the_field('modifications_text')?></td>
										<td><?php the_field('basic_modifications')?></td>
										<td><?php the_field('standard_modifications')?></td>
										<td><?php the_field('premium_modifications')?></td>
									</tr>
									<tr class="odd">
										<td><?php the_field('mockup_text')?></td>
										<td><?php if(get_field('basic_mockup')!=''){   the_field('basic_mockup'); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('standard_mockup')!=''){   the_field('standard_mockup'); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										<td><?php if(get_field('premium_mockup')!=''){   the_field('premium_mockup'); }else{ ?><i class="fa fa-times" aria-hidden="true"></i><?php } ?></td>
										
									</tr>
									<tr class="even details">
										<td><?php the_field('details_text')?></td>
										<td>
											<?php the_field('basic_details')?>
										</td>
										<td>
											<?php the_field('standard_details')?>
										</td>
										<td>
											<?php the_field('premium_details')?>
										</td>
									</tr>
									<tr class="odd">
										<td><?php the_field('delivery_text')?></td>
										<td><?php the_field('basic_delivery_time')?></td>
										<td><?php the_field('standard_delivery_time')?></td>
										<td><?php the_field('premium_delivery_time')?></td>
									</tr>
									
								</tbody>
							</table>
						</div>
						<p class="table-desc" data-aos="fade-right" data-aos-duration="1000"><?php the_field('below_table_text')?></p>
					</div>
				</div>
			</div>
		</section>
		
		
		<section class="vect-sec vect-faq" id="faq">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
					<h3 data-aos="fade-right" data-aos-duration="1000"><?php echo the_field('faq_title'); ?></h3>
					<div data-aos="fade-right" data-aos-duration="1000">
					<?php echo the_field('faq_contents'); ?>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="vect-sec vect-order-now" id="order-now">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
							<p class="start-order-text" data-aos="fade-right" data-aos-duration="1000"><?php the_field('order_now_text')?></p>
							<a href="<?php the_field('order_now_button_link')?>" title="<?php the_field('order_now_button_text')?>" data-toggle="modal" data-target="#registrationModal" data-aos="fade-right" data-aos-duration="1000"><?php the_field('order_now_button_text')?></a>
					</div>
				</div>
			</div>
		</section>
		
		
	</main><!-- .site-main -->


</div><!-- .content-area -->
<footer>
	<div class="top-footer text-center" data-aos="fade-up" data-aos-duration="3000">
		<div class="container">
			<h2><?php the_field('top_foooter_title')?></h2>
			<p><?php the_field('top_footer_contents')?></p>
			<div class="contact-details">
				<h2><?php the_field('contact_title')?></h2>
				<p><?php the_field('contact_details')?></p>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="footer-left">
			<a href="" title="twitter"><i class="fab fa-twitter"></i></a>
			<a href="" title="facebook"><i class="fab fa-facebook-f"></i></a>
			<a href="" title="instagram"><i class="fab fa-instagram"></i></a>
			<a href="" title="pinterest"><i class="fab fa-pinterest"></i></a>
			</div>
			<div class="footer-right">
				
				<?php echo wp_nav_menu( array( 
									'theme_location' => 'speedysep-footer-menu', 
									'container_class' => 'speedy-footer-menu' )
									); 
				?>
				<p>Copyright © 2019 by speedyseps.com</p>
			</div>
		</div>
	</div>
</footer>
<script>
jQuery(document).ready(function($){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  //add and remove active class on clicking menu 
  $('.nav li a').on('click', function(){
	  $('.nav li a').removeClass("active");
    $(this).addClass("active");
  });

	var screenHeight=jQuery(window).height();
	jQuery('#videoModal iframe').css('height', screenHeight +'px');
	
	jQuery("#videoClose").click(function() {
      // changes the iframe src to prevent playback or stop the video playback in our case
      $('#videoModal iframe').each(function(index) {
        $(this).attr('src', $(this).attr('src'));
        return false;
      });
	});
	  
	//add active class on scroll 
		$('a[href*=#]').bind('click', function(e) {
				e.preventDefault(); // prevent hard jump, the default behavior

				var target = $(this).attr("href"); // Set the target as variable

				// perform animated scrolling by getting top-position of target-element and set it as scroll target
				$('html, body').stop().animate({
						scrollTop: $(target).offset().top
				}, 600, function() {
						location.hash = target; //attach the hash (#jumptarget) to the pageurl
				});

				return false;
		});

$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}
	
		// Assign active class to nav links while scolling
		$('.vect-sec').each(function(i) {
				if ($(this).position().top + +200 <= scrollDistance) {
						$('.sidebar-menu-right ul li a.active').removeClass('active');
						$('.sidebar-menu-right ul li a').eq(i).addClass('active');
				}
		});
}).scroll();
});

function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>

<link href="<?php echo get_stylesheet_directory_uri() ?>/css/animations.css" rel="stylesheet">
<script src="<?php echo get_stylesheet_directory_uri() ?>/js/animations.js"></script>
<script>
  AOS.init();
</script>
<!-- Modal Video -->
  <div class="modal fade" id="videoModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" id="videoClose"><img src="<?php echo get_stylesheet_directory_uri().'/images/close.png' ?>" alt="close"></button>
        <div class="modal-body">
          <iframe width="100%" height="100%" src="https://www.youtube.com/embed/u2o1qKDr13A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      
    </div>
  </div>
<?php get_footer(); ?>